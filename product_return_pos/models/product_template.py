from odoo import models, api, fields

class AgregaCampoValidez(models.Model):

    _inherit = 'product.template'
    _name = _inherit

    
    dias_garantia = fields.Integer(
        string='Dias de garantia',
        required=True,
        default=15
    )
    