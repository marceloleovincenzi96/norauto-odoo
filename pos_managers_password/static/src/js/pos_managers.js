odoo.define('pos.managers', function (require) {
    "use strict";

    console.log('fafafa');
    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var ScreenWidget = screens.ScreenWidget;
    var gui = require('point_of_sale.gui');
    var core = require('web.core');
    var QWeb = core.qweb;
    var PopupWidget = require('point_of_sale.popups');
    var BarcodeEvents = require('barcodes.BarcodeEvents').BarcodeEvents;

    models.load_fields('pos.order', ['aprobado_por', 'hora_aprobacion']);

    models.load_models({
        model: 'pos.managers',
        fields: ['user_id', 'pos_password', 'operating_unit_id'],
        loaded: function (self, managers) {

            var filtrados = managers.filter(function (manager) {

                return manager.operating_unit_id[0] == self.config.operating_unit_id[0];
            })

            self.pos_managers = filtrados;
        }
    })

    screens.ProductScreenWidget.include({

        show: function (reset) {
            this._super(reset);
            $('.input-button.numpad-minus').prop('disabled', true);
        }

    });

    var PasswordWidget = PopupWidget.extend({
        template: 'PasswordWidget',

        init: function (parent, options) {
            this._super(parent, options);
            this.options = {};
        },

        show: function (options) {
            var self = this;
            this._super(options);
            this.render_list(this.pos);
            console.log('LA ORDEN: ', this.pos.get_order())
            console.log('LA ORDERLINE: ', this.pos.get_order().get_orderlines())
            $('input.password-pos').prop('type', 'password');
            console.log('Osiris el osito mameluco paseando por la calle Pernambuco')
        },

        events: {
            'click .button.passcancel': 'click_password_cancel',
            'click .button.passconfirm': 'click_password_confirm',
        },

        click_password_confirm: function () {

            var self = this;

            var password = $('input[name=password_pos]').val();

            var passwords_managers = [];

            var pos_managers = this.pos.pos_managers;

            for (var i = 0; i < pos_managers.length; i++) {

                passwords_managers.push(pos_managers[i].pos_password);

            }

            if (!(passwords_managers.includes(password))) {

                this.gui.show_screen('products');

                this.gui.show_popup('error', {
                    title: "Password incorrecto",
                    body: "Ha introducido un password incorrecto",
                });

            } else if ($('select#rubrica_descuento') && $('select#rubrica_descuento').val() == ""){

                console.log('Bruh.')

            } else {

                
                var productos_con_descuento = this.pos.get_order()['productos_id_con_descuento']
                var rubricas_descuento = $('select#rubrica_descuento').map(function(i, rubrica_id){
                    return $(rubrica_id).val()
                })
                var orderlines = this.pos.get_order().get_orderlines()
                

                for (var i = 0; i < orderlines.length; i++){

                    for (var j = 0; j < productos_con_descuento.length; j++){ //productos_con_descuento y rubricas_descuento tienen la misma length

                        if (orderlines[i].product.id == productos_con_descuento[j] && rubricas_descuento[j] != undefined){

                            console.log('Que passa pibe, vos quere morir en este instante?')

                            orderlines[i].rubrica_descuento = parseInt(rubricas_descuento[j])

                            // console.log('orderline i: ', orderlines[i])
                            // console.log('producto_con_descuento_j: ', productos_con_descuento[j])
                            // console.log('rubrica_descuento_j: ', rubricas_descuento[j])
                        }
                    }
                }
                console.log('Un caso de estos de m... : ', this.pos.get_order().get_orderlines())
                this.gui.show_screen('payment');
            }

        },

        click_password_cancel: function () {
            var self = this;
            self.gui.close_popup();
            self.gui.show_screen('products');

        },

        render_list: function (options) {
            $("#table-body").empty();

            var self = this;

            var orderlines = options.get_order().get_orderlines()

            orderlines.forEach(function (orderline) {

                var rows = "";
                var producto = orderline.product.display_name
                var precio = orderline.price;
                var qty = orderline.quantity;
                var product_id = orderline.product.id;
                var discount = orderline.discount;

                var rubricas_descuentos = self.pos.rubricas_descuentos;
                if (discount > 0 || product_id == 2 || precio != orderline.product.lst_price) {
                    // console.log('descuento: ', discount)
                    // console.log('producto_id: ', product_id)
                    self.pos.get_order().productos_id_con_descuento.push(product_id)

                    var select = $("<select name='rubrica_descuento' id='rubrica_descuento' placeholder='Aplicar rubrica'>")

                    select.append("<option></option>")
                    rubricas_descuentos.forEach(function (rubrica) {

                        select.append($('<option>', {
                            value: rubrica.id,
                            text: rubrica.name
                        }))
                    })

                    // console.log('El select: ', select)

                    rows = ''

                    if (discount != 0){

                        rows += "<tr><td>" + product_id + "</td><td>" + producto + "</td><td>" + precio + " </td><td>" + qty + "</td><td>" + discount + "</td><td>" + select[0].outerHTML + "</td></tr>";
                    }

                    else{
                        
                        rows += "<tr><td>" + product_id + "</td><td>" + producto + "</td><td>" + precio + " </td><td>" + qty + "</td><td>" + '&nbsp;' + "</td><td>" + select[0].outerHTML + "</td></tr>";

                    }

                    
                    $(rows).appendTo("#list-managers tbody");
                    var rows = document.getElementById('list').rows;
                    for (var row = 0; row < rows.length; row++) {
                        var cols = rows[row].cells;
                        cols[0].style.display = 'none';
                        cols[1].style.display = 'none';
                        cols[5].style.display = 'none';

                    }


                    var table = document.getElementById('list');
                    var tr = table.getElementsByTagName("tr");
                    for (var i = 1; i < tr.length; i++) {
                        var td = document.createElement('td');
                        var input = document.createElement('input');
                        input.setAttribute("type", "text");
                        input.setAttribute("value", 0);
                        input.setAttribute("id", "text" + i);
                        td.appendChild(input);
                        tr[i].appendChild(td);

                    }

                }
            });

        }

    })
    gui.define_popup({ name: 'PasswordWidget', widget: PasswordWidget });

    var _super_order = models.Order.prototype;
    models.Order = models.Order.extend({
        initialize: function (attr, options) {
            this.aprobador = null;
            this.hora_aprobacion = null;
            this.productos_id_con_descuento = []
            _super_order.initialize.call(this, attr, options);
        },

        export_as_JSON: function () {
            var json = _super_order.export_as_JSON.apply(this, arguments);
            json.aprobador = this.aprobador;
            json.hora_aprobacion = this.hora_aprobacion;
            json.productos_id_con_descuento = this.productos_id_con_descuento
            
            return json;
        },
        init_from_JSON: function (json) {
            _super_order.init_from_JSON.apply(this, arguments);
            this.aprobador = json.aprobador;
            this.hora_aprobacion = json.hora_aprobacion;
            this.productos_id_con_descuento = json.productos_id_con_descuento
        }
    })

    var _super_payment_widget = screens.PaymentScreenWidget.prototype;
    screens.PaymentScreenWidget.include({

        init: function (parent, options) {
            console.log('La caracola ha hablado');
            this._super(parent, options);
            var self = this;
            this.keyboard_handler = function (event) {
                if ($(".modal-dialog.pos_password_manager").not('.oe_hidden').length) {
                        return;
                }
                
                else {
                    // On mobile Chrome BarcodeEvents relies on an invisible
                    // input being filled by a barcode device. Let events go
                    // through when this input is focused.
                    if (BarcodeEvents.$barcodeInput && BarcodeEvents.$barcodeInput.is(":focus")) {
                        return;
                    }

                    var key = '';

                    if (event.type === "keypress") {
                        if (event.keyCode === 13) { // Enter
                            self.validate_order();
                        } else if ( event.keyCode === 190 || // Dot
                                    event.keyCode === 110 ||  // Decimal point (numpad)
                                    event.keyCode === 188 ||  // Comma
                                    event.keyCode === 46 ) {  // Numpad dot
                            key = self.decimal_point;
                        } else if (event.keyCode >= 48 && event.keyCode <= 57) { // Numbers
                            key = '' + (event.keyCode - 48);
                        } else if (event.keyCode === 45) { // Minus
                            key = '-';
                        } else if (event.keyCode === 43) { // Plus
                            key = '+';
                        }
                    } else { // keyup/keydown
                        if (event.keyCode === 46) { // Delete
                            key = 'CLEAR';
                        } else if (event.keyCode === 8) { // Backspace
                            key = 'BACKSPACE';
                        }
                    }

                    self.payment_input(key);
                    event.preventDefault();
                };
                
            }
            
            this.keyboard_keydown_handler = function (event) {
                if ($(".modal-dialog.pos_password_manager").not('.oe_hidden').length) {
                    return;
                }
    
                else {
                    if (event.keyCode === 8 || event.keyCode === 46) { // Backspace and Delete
                        event.preventDefault();
        
                        // These do not generate keypress events in
                        // Chrom{e,ium}. Even if they did, we just called
                        // preventDefault which will cancel any keypress that
                        // would normally follow. So we call keyboard_handler
                        // explicitly with this keydown event.
                        self.keyboard_handler(event);
                    }
                }
            }
        },
        
        show: function () {

            var self = this;
            console.log('No man te fuiste al carajo man como vas a decir eso le pudo haber pasado a un amigo, no flaco disculpame voy a llamar a la cana.')
            this._super();

            var order_lines = this.pos.get_order().orderlines
            var count = 0;

            order_lines.forEach(function (orderline) {
                if ((orderline.discount > 0 || orderline.quantity < 0 || self.pos.config.discount_product_id[0] == orderline.product.id || orderline.price != orderline.product.lst_price) && !self.pos.get_order().promotion_id) {
                    count++;
                }
            });

            if (count > 0) {
                self.gui.show_popup('PasswordWidget');
            }
        },


        validate_order: function (force_validation) {

            var fecha = new Date();

            this.pos.get_order().aprobador = this.pos.user.id;

            this.pos.get_order().hora_aprobacion = fecha.getHours();

            var order = this.pos.get_order();

            // console.log('order id: ', order);

            this._super(force_validation);

        }

    })


})