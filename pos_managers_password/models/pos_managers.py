from odoo import fields, models, api

class PosManagers(models.Model):

    _name = 'pos.managers'
    _description = "POS Managers"
    _rec_name = 'user_id'


    user_id = fields.Many2one(
        string='Usuario',
        comodel_name='res.users',
        ondelete='restrict',
        required=True
    )
    
    pos_password = fields.Char(
        string='POS Password',
        required=True
    )

    operating_unit_id = fields.Many2one(
        string = 'Unidad operativa',
        comodel_name="operating.unit",
        required=True
        
    )
    
    '''@api.model
    def create(self, vals):

        res = super(PosManagers, self).create(vals)

        operating_unit = self.env['operating.unit'].browse(self.operating_unit_id.id)

        operating_unit.write({

            'pos_managers_ids': [(4, res.id)]

        })

        return res'''
