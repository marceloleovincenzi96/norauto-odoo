from odoo import models, fields, api


class PosOrders(models.Model):

    _inherit = 'pos.order'
    _name = _inherit

    aprobado_por = fields.Many2one(
        string='Aprobado por',
        comodel_name='res.users'
    )

    hora_aprobacion = fields.Integer(
        string='Hora de aprobacion',
    )
    

    @api.model
    def _order_fields(self, ui_order):
        order_fields = super(PosOrders, self)._order_fields(ui_order)
        order_fields['aprobado_por'] = ui_order.get('aprobador')
        order_fields['hora_aprobacion'] = ui_order.get('hora_aprobacion') 
        return order_fields



