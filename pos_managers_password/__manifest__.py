# -*- coding: utf-8 -*-
{
    'name': 'POS Managers Password',
    'version': '13.0.0.0.0',
    'summary': """Password for POS Users to validate POS Orders""",
    'category': 'Point of Sale',
    'license': 'LGPL-3',
    'author': "Nybble Group",
    'website': "https://www.nybblegroup.com/",
    'depends': ['point_of_sale', 'base', 'rubricas_descuentos'],
    'data': [
        'views/pos_assets.xml',
        'views/pos_orders.xml',
        'views/pos_managers.xml',
        'security/ir.model.access.csv'
    ],
    'images': [],
    'installable': True,
    'application': True,
    'qweb': ['static/src/xml/pos_managers.xml'],
} 
