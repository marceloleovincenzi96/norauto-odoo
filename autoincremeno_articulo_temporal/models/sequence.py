# -*- coding: utf-8 -*-

from odoo import api, models
import logging

_logger = logging.getLogger(__name__)


class IrSequence(models.Model):
    _inherit = 'ir.sequence'

    @api.model
    def next_by_code(self, sequence_code, sequence_date=None):
        if sequence_code.rsplit('.', 1)[0] == 'producto.temporal':
            # devolver el codigo de entre 1 y 2 millones, y despues de 3 millones
            self.check_access_rights('read')
            force_company = self._context.get('force_company')
            if not force_company:
                force_company = self.env.company.id

            seq_ids = self.search([('code', '=', sequence_code), ('company_id', 'in', [force_company, False])], order='company_id')
            if not seq_ids:
                _logger.debug("No ir.sequence has been found for code '%s'. Please make sure a sequence is set for current company." % sequence_code)
                return False
            seq_id = seq_ids[0]
            if seq_id.number_next_actual == 2000000:
                seq_id.number_next_actual = 3000000
            return seq_id._next(sequence_date=sequence_date)

        res = super(IrSequence, self).next_by_code(sequence_code, sequence_date)
        return res
