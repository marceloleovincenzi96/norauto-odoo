# -*- coding: utf-8 -*-

from odoo import api, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    ##################################################
    # ORM OVERRIDES
    ##################################################
    @api.model
    def create(self, vals):
        if vals.get('tipoart_dos') == '2':
            if self._context.get('sucursal_id'):
                operating_unit_code = self.env['operating.unit'].browse(self._context.get('sucursal_id')).code
            else:
                operating_unit_code = self.env.user.default_operating_unit_id.code
            code = 'producto.temporal.%s' % operating_unit_code
            vals['idart'] = self.env['ir.sequence'].next_by_code(code)
        elif vals.get('tipoart_dos') == '1':
            vals['idart'] = self.env['ir.sequence'].next_by_code('producto.no.temporal')
        vals['default_code'] = str(vals['idart'])
        print('-----------------------------------------------------')
        print('vals: ', vals)
        res = super(ProductTemplate, self).create(vals)
        return res
