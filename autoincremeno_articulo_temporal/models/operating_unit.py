# -*- coding:utf-8 -*-

from odoo import api, models


class OperatingUnit(models.Model):
    _inherit = 'operating.unit'

    ##################################################
    # ORM OVERRIDES
    ##################################################
    @api.model
    def create(self, vals):
        # if res.tipoart_dos == 'CCA':
        #     if not self.env.user.has_group('xml id referencia'):
        #         raise Exception("No tiene permisos")

        res = super(OperatingUnit, self).create(vals)
        sequence = self.env['ir.sequence'].create({
            'name': "Secuencia Producto Temporal %s" % res.name,
            'code': "producto.temporal.%s" % res.code,
            'active': True,
            'padding': 7,
            'number_next_actual': 1000000,
        })
        sequence.number_next_actual = 1000000
        return res

    def write(self, vals):
        if 'code' in vals:
            # vals['code'] != self.code
            sequence = self.env['ir.sequence'].search([('code', '=', 'producto.temporal.%s' % self.code)])
            sequence.code = "producto.temporal.%s" % vals.get('code')
        res = super(OperatingUnit, self).write(vals)
        return res

    def unlink(self):
        for rec in self:
            sequence = self.env['ir.sequence'].search([('code', '=', 'producto.temporal.%s' % rec.code)])
            sequence.unlink()
        res = super(OperatingUnit, self).unlink()
        return res
