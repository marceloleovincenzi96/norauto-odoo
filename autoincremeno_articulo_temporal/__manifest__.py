# -*- encoding: utf-8 -*-

{
    "name": "Autoincremento de articulos temporales",
    "version": "1.0",
    'author': 'Nybble Group',
    "summary": """Autoincremente del código para artículos temporales""",
    "license": "AGPL-3",
    "depends": ['fields_norauto', 'operating_unit'],
    "data": [
        "data/sequence.xml",
    ],
    'qweb': [],
    "application": False,
    "installable": True,
    "active": False,
}
