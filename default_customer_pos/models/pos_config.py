# -*- coding: utf-8 -*-
from odoo import fields, models, api


class PosConfigInherit(models.Model):
    _inherit = 'pos.config'

    # cargo solo los clientes con id menor que 100, como en el pos
    customer_default_id = fields.Many2one('res.partner', string='Default customer',
                                          domain="[('id', '<', 100)]", compute='_get_consumidor_final_anonimo')

    @api.depends('customer_default_id')
    def _get_consumidor_final_anonimo(self):
        # 7 es el id de consumidor final anonimo
        for record in self:
            record.customer_default_id = None
            if not record.customer_default_id:
                record.customer_default_id = self.env['res.partner'].browse(7).id
