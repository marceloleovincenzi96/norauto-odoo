# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def create_from_ui(self, partner):
        if partner.get('l10n_latam_identification_type_id'):
            partner['l10n_latam_identification_type_id'] = int(partner['l10n_latam_identification_type_id'])
        res = super(ResPartner, self).create_from_ui(partner)
        return res
