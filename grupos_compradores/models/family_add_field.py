from odoo import api, fields, models

class AddGrupoCompradorField(models.Model):
    _inherit = 'product.category'

    grupo_comprador = fields.Many2one(
        'stock.grupocomp',
        'Grupo Comprador'
    )
