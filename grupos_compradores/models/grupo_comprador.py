from odoo import api, fields, models
from odoo.exceptions import UserError
from datetime import date


class GrupoComprador(models.Model):
    _name = "stock.grupocomp"
    _description ="Grupo comprador"

    @api.model
    def create(self, vals):

        res = super(GrupoComprador, self).create(vals)

        grupos_compradores = self.env['stock.grupocomp'].search([])

        numeros_grupos_compradores = [grupo_comprador.number for grupo_comprador in grupos_compradores]

        if res.number in numeros_grupos_compradores:

            raise UserError('Ya existe un grupo comprador con el numero ' + str(res.number))
        
        return res
    

    def cambiar_limite(self):

        grupos = self.env['stock.grupocomp'].search([])
        
        for record in grupos:
            if record.fecha_nuevo_limite == date.today() and record.nuevo_limite_segunda_aprobacion:
                record.limite_segunda_aprobacion = record.nuevo_limite_segunda_aprobacion



    number = fields.Integer(
        size=3,
        string="Numero de grupo comprador"
    )

    familias = fields.One2many('product.category', 'grupo_comprador', 'Familias')

    limite_segunda_aprobacion = fields.Float(
        string="Limite para segunda aprobación actual",
        readonly=True 
        
    )

    nuevo_limite_segunda_aprobacion = fields.Float(
        string="Nuevo limite para segunda aprobación",
    )

    fecha_nuevo_limite = fields.Date(
        string="Fecha de vigencia del nuevo limite"
    )

    _rec_name = "number"
