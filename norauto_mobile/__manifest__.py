
# -*- encoding: utf-8 -*-

{
    'name': 'Norauto Mobile',
    'version': '13.0.1.0',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'Norauto Mobile',
    'depends': ['stock', 'norauto_gondola'],
    'author': 'NybbleGroup',
    'description': """
NorAuto Norauto Mobile
===============
Agrega vistas tipo wizard en menues independientes para funcionalidades especificas de mobile.
""",
    'data': [
        'security/groups.xml',
        'wizards/visor_precio.xml',
        'views/dossiers.xml',
        'views/dossier_parcial_mobile.xml',
        'views/inventario_parcial.xml',
        'views/transferencias.xml'
    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
