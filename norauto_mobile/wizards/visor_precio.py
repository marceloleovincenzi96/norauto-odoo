# -*- coding: utf-8 -*-

from odoo import api, fields, models


class VisorPrecio(models.TransientModel):
    _name = 'norauto.visor_precio'

    barcode = fields.Char('Código Barras')
    product_id = fields.Many2one('product.template', 'Producto')
    nombre_producto = fields.Char('Producto', related='product_id.name')
    precio = fields.Float('Precio con impuestos', related='product_id.precio_con_impuestos')

    @api.onchange('barcode')
    def _onchage_barcode(self):
        for rec in self:
            if rec.barcode:
                producto = self.env['product.template'].search([('barcode', '=', rec.barcode)])
                if producto:
                    rec['product_id'] = producto.id
