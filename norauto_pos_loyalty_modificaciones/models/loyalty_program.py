from odoo import models, fields, api


class LoyaltyProgram(models.Model):

    _inherit = 'loyalty.program'
    _name = _inherit


    def mostrar_wizard_recompensa_familia(self):

        return {

            'name': 'Crear recompensas por familia de articulos',
            'type': 'ir.actions.act_window',
            'res_model': 'pos.loyalty.recompensa_por_familia',
            'view_mode': 'form',
            'target': 'new',
            "context": {'programa_lealtad_id': self.id}

        } 