from odoo import models, fields, api
from datetime import datetime, timedelta

class PosOrder(models.Model):

    _inherit = 'pos.order'
    _name = _inherit

    def anular_puntos_orden(self):

        dias = int(self.env['ir.config_parameter'].sudo().get_param("dias_validez_puntos_orden"))

        if self.date_order + timedelta(days=dias) >= datetime.now():
            self.partner_id.loyalty_points -= self.loyalty_points 
            self.loyalty_points = 0