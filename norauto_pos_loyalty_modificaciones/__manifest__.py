
# -*- encoding: utf-8 -*-

{
    'name': 'Norauto Modificaciones Programa de Lealtad',
    'version': '13.0.0.1',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'Programa de Lealtad',
    'depends': [
        'point_of_sale',
        'pos_loyalty'
    ],
    'author': 'NybbleGroup',
    'data': [
        
        "data/cron_anular_puntos_orden.xml",
        "views/loyalty_program.xml",
        "wizard/wizard_recompensa_por_familia.xml",
        "wizard/wizard_dias_validez_puntos_orden.xml"

    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
