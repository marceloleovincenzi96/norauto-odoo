from odoo import models, fields, api

class WizardDiasValidezPuntosOrden(models.TransientModel):

    _name='pos.order.dias_validez_puntos_orden'
    _description='Actualiza dias validez'

    dias_validez = fields.Integer('Cantidad de dias de validez', default=lambda self: self._obtener_dias_validez())

    def actualizar_dias_validez(self):    
        self.env['ir.config_parameter'].sudo().set_param("dias_validez_puntos_orden", self.dias_validez)

    def _obtener_dias_validez(self):
        dias = int(self.env['ir.config_parameter'].sudo().get_param("dias_validez_puntos_orden"))

        if not dias:

            return 365
        
        return dias

