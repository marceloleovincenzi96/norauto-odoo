from odoo import models, fields, api


class WizardRecompensaPorFamilia(models.TransientModel):

    _name = 'pos.loyalty.recompensa_por_familia'

    nombre_de_recompensa = fields.Char(
        string='Nombre de recompensa',
        default='CLUB NORAUTO',
        required=True
    )

    reward_type = fields.Selection([('gift', 'Regalo'), ('discount', 'Descuento (en %)'), (
        'resale', 'Descuento (en valor)')], string='Tipo de recompensa', old_name='type', required=True)
    
     
    familia_id = fields.Many2one(
        string='Familia',
        comodel_name='product.category',
        ondelete='restrict',
        required=True
    )

    discount = fields.Float(string='Descuento')

    point_cost = fields.Float(string='Costo de recompensa')
    
    minimum_points = fields.Float(string='Puntos minimos')

    
    def crear_recompensas(self):

        productos = self.env['product.product'].search([('categ_id', '=', self.familia_id.id)])

        if self.reward_type == 'gift':

            for producto in productos:

                self.env['loyalty.reward'].create({

                    'name': self.nombre_de_recompensa,
                    'loyalty_program_id': self._context.get('programa_lealtad_id'),
                    'reward_type': self.reward_type,
                    'gift_product_id': producto.id,
                    'point_cost': self.point_cost,
                    'minimum_points': self.minimum_points

                })
        
        elif self.reward_type == 'discount':

            for producto in productos:

                self.env['loyalty.reward'].create({

                    'name': self.nombre_de_recompensa,
                    'loyalty_program_id': self._context.get('programa_lealtad_id'),
                    'reward_type': self.reward_type,
                    'discount': self.discount,
                    'discount_product_id': producto.id,
                    'point_cost': self.point_cost,
                    'minimum_points': self.minimum_points

                })

        elif self.reward_type == 'resale':

            for producto in productos:

                self.env['loyalty.reward'].create({

                    'name': self.nombre_de_recompensa,
                    'loyalty_program_id': self._context.get('programa_lealtad_id'),
                    'reward_type': self.reward_type,
                    'point_product_id': producto.id,
                    'minimum_points': self.minimum_points

                })

    

