from odoo import models, fields, exceptions

class MotivoRechazoWizard(models.TransientModel):
    _name = "update.motivo_rechazo"
    _description = "Asignar motivo de rechazo"

    motivo_rechazo = fields.Char('Motivo de rechazo', required=True)

    def guardar(self):
        mensaje = 'Se ha rechazado este vale por el siguiente motivo: ' + self.motivo_rechazo + '. Se ha devuelto a estado de Borrador.' 
        vale = self.env['norauto.vales_cajachica'].browse(self.env.context.get('id_vale'))
        vale.write({'estado': 'Borrador'})
        vale.message_post(body=mensaje)
