from odoo import models, fields
from odoo.exceptions import UserError

class ReintegroParcialWizard(models.TransientModel):
    _name = "update.monto_reintegro"
    _description = "Reintegro parcial"

    monto_a_reintegrar = fields.Float('Monto a reintegrar', required=True)

    def reintegrar(self):
        mensaje = 'Se han reintegrado $%.2f en el monto fijo' % self.monto_a_reintegrar
        vale = self.env['norauto.vales_cajachica'].browse(self.env.context.get('id_vale'))
        fondo_operating_unit = self.env['fondo_fijo.tienda'].search([('operating_unit_id', '=', vale.operating_unit_id.id)])

        reintegro = fondo_operating_unit.fondo_fijo_ahora + self.monto_a_reintegrar

        if reintegro > fondo_operating_unit.fondo_fijo:
            raise UserError('Accion denegada. Introdujo una cantidad con la que se superaria el monto original del fondo fijo. Fondo fijo original: $%.2f. Fondo fijo actual: $%.2f. Fondo fijo actual con reintegro: $%.2f.' % (fondo_operating_unit.fondo_fijo, fondo_operating_unit.fondo_fijo_ahora, reintegro))

        fondo_operating_unit.write({'fondo_fijo_ahora': reintegro})

        if reintegro == fondo_operating_unit.fondo_fijo:
            vale.write({'estado': 'Reintegrado'})
        
        vale.message_post(body=mensaje)