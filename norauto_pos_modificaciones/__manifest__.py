# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    All Rights Reserved.
#
############################################################################
#
############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name": "Modificaciones de POS",
    "version": "1.0",
    'author': 'Nybble Group',
    "summary": """POS para Norauto""",
    "license": "AGPL-3",
    "depends": ['base', 'account'],
    "data": [
        'data/rubricas_gastos.xml',
        'views/vales.xml',
        'views/secuencia_vales.xml',
        'views/vale_asociado.xml',
        'views/informes_gastos.xml',
        'views/gastos.xml',
        'views/secuencia_rendicion.xml',
        'views/rendicion_gastos.xml',
        'views/gastos_a_conciliar_con_vales.xml',
        'wizards/motivo_devolucion_wizard.xml',
        'wizards/reintegro_parcial_wizard.xml',
        'security/ir.model.access.csv'
    ],
    'qweb': [],
    "installable": True,
    "active": False,
}
