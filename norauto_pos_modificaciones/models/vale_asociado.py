from odoo import fields, models

class ValeAsociado(models.Model):

    _name = 'account.move'
    _inherit = _name

    
    es_gasto_caja_chica = fields.Boolean(
        string='Es gasto de caja chica'
    )

    
    vale_asociado_id = fields.Many2one(
        string='Vale asociado',
        comodel_name='norauto.vales_cajachica'
    )
    
    
