from odoo import fields, models, api
from odoo.exceptions import UserError

class Rendicion(models.Model):

    _name = 'pos.rendicion_gastos'
    _description = 'Rendicion de gastos'
    _rec_name = 'nombre_rendicion'

    
    estado = fields.Selection(
        string='Estado',
        selection=[('Borrador', 'Borrador'), ('Espera Aprobacion', 'Espera Aprobacion'), ('Aprobado', 'Aprobado')],
        default='Borrador'
    )
    

    nombre_rendicion =  fields.Char(
        string='Nombre de rendicion'
    )
    
    fecha_rendicion = fields.Date(
        string='Fecha de rendicion'
    )
    
    monto_rendido =  fields.Float(
        string='Monto rendido',
        compute='calcular_monto_rendido'
    )

    
    gastos_ids = fields.Many2many(
        string='Gastos',
        comodel_name='hr.expense',
    )
    

    informes_gastos_ids = fields.Many2many(
        string='Informes de gastos',
        comodel_name='hr.expense.sheet',
        domain=lambda self: [("operating_unit_id", "=", self.env.user.default_operating_unit_id.id)]
    )


    @api.onchange('informes_gastos_ids')
    def _agregar_gastos(self):

        gastos = []

        for record in self.informes_gastos_ids:

            for gasto in record.expense_line_ids:

                gastos.append(gasto.id)
        
        self.gastos_ids = [(6, 0, tuple(gastos))]
        

    def pasar_a_espera_aprobacion(self):

        if not (self.env.user.has_group('norauto_rights.group_supervisor_tienda') or self.env.user.has_group('norauto_rights.group_director_tienda')):
            raise UserError('Permiso denegado. Usted no es director o supervisor de tienda.')

        self.write({'estado': 'Espera Aprobacion'})


    def pasar_a_aprobado(self):
        
        if not self.env.user.has_group('norauto_rights.group_gerente_red'):
            raise UserError('Permiso denegado. Usted no es un gerente de red.')

        self.write({'estado': 'Aprobado'})
    

    @api.depends('informes_gastos_ids')
    def calcular_monto_rendido(self):

        for record in self:

            record['monto_rendido'] = sum(record.informes_gastos_ids.mapped('total_amount'))





    
    
    