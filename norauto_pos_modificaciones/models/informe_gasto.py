from odoo import fields, models

class InformeGasto(models.Model):

    _inherit = 'hr.expense.sheet'
    _name = _inherit

    operating_unit_id = fields.Many2one(
        string="Unidad operativa",
        comodel_name="operating.unit",
        related="employee_id.user_id.default_operating_unit_id"
    )