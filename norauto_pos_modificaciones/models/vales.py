from odoo import fields, models, api
from odoo.exceptions import UserError


class Vales(models.Model):

    _name = 'norauto.vales_cajachica'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    _rec_name = 'name'
    _description = 'Vale'

    name = fields.Char('Nombre del vale')

    facturas_pago = fields.Many2one(
        string='Facturas de pago',
        comodel_name='account.move',
        ondelete='restrict'

    )

    
    rubricas_gastos_id = fields.Many2one(
        string='Rubrica de gasto',
        comodel_name='norauto.rubricas_gastos',
        ondelete='restrict',
    )
    

    dinero_otorgado_a = fields.Many2one(
        string='Dinero otorgado a',
        comodel_name='hr.employee',
        ondelete='restrict',
        required=True

    )
    
    estado = fields.Selection(
        string='Estado',
        selection=[('Borrador', 'Borrador'), ('A aprobar', 'A aprobar'), ('Aprobado', 'Aprobado'), ('Rechazado', 'Rechazado'),
                   ('Totalmente pagado', 'Totalmente pagado'), ('Reintegrado parcialmente', 'Reintegrado parcialmente'), ('Reintegrado', 'Reintegrado')],
        default='Borrador'
    )

    operating_unit_id = fields.Many2one('operating.unit', 'Unidad operativa', default=lambda self: self.env.user.default_operating_unit_id)

    valor_en_pesos = fields.Float(
        string='Valor en pesos',
        required=True

    )

    
    motivo_salida = fields.Char(
        'Motivo de la salida',
        required=True
    )
    

    descripcion_gasto = fields.Char(
        'Descripcion del gasto',
        required=True

    )

    @api.model
    def create(self, vals):

        seq = self.env['ir.sequence'].next_by_code('norauto.vales_cajachica') or '/'

        vals['name'] = seq

        return super(Vales, self).create(vals)
        

    def pasar_a_aprobar(self):

        if self.env.user.has_group('norauto_rights.group_cajero_tienda'):

            self.write({'estado': 'A aprobar'})

        else:
            raise UserError(
                'Permiso denegado. Usted no pertenece al grupo de Cajeros.')

    def pasar_a_aprobado(self):

        if self.env.user.has_group('norauto_rights.group_supervisor_tienda'):

            self.write({'estado': 'Aprobado'})
            self.descontar_fondo_fijo()

        else:
            raise UserError(
                'Permiso denegado. Usted no es un supervisor de tienda.')

    def pasar_a_totalmente_pagado(self):

        if self.env.user.has_group('norauto_rights.group_supervisor_tienda'):

            self.write({'estado': 'Totalmente pagado'})

        else:
            raise UserError(
                'Permiso denegado. Usted no es un supervisor de tienda.')

    def pasar_a_rechazado(self):

        if self.env.user.has_group('norauto_rights.group_supervisor_tienda'):

            self.write({'estado': 'Rechazado'})

            return {
                'name': 'Escriba el motivo de rechazo',
                'type': 'ir.actions.act_window',
                'res_model': 'update.motivo_rechazo',
                'view_mode': 'form',
                'target': 'new',
                'context': {'id_vale': self.id}
            }

        else:
            raise UserError(
                'Permiso denegado. Usted no es un supervisor de tienda.')

    def pasar_a_reintegrado_parcialmente(self):

        if self.env.user.has_group('norauto_rights.group_supervisor_tienda'):

            if self.estado != 'Reintegrado parcialmente': #para que no lo haga de nuevo si hago una nueva reintegracion
                self.write({'estado': 'Reintegrado parcialmente'})

            return {
                'name': 'Asigne el monto a reintegrar en el fondo fijo',
                'type': 'ir.actions.act_window',
                'res_model': 'update.monto_reintegro',
                'view_mode': 'form',
                'target': 'new',
                'context': {'id_vale': self.id}
            }

        else:
            raise UserError(
                'Permiso denegado. Usted no es un supervisor de tienda.')

    def pasar_a_reintegrado(self):

        if self.env.user.has_group('norauto_rights.group_supervisor_tienda'):

            fondo_operating_unit = self.env['fondo_fijo.tienda'].search([('operating_unit_id', '=', self.operating_unit_id.id)])

            for record in fondo_operating_unit:
                record.write({'fondo_fijo_ahora': record.fondo_fijo})

            self.write({'estado': 'Reintegrado'})

        else:
            raise UserError(
                'Permiso denegado. Usted no es un supervisor de tienda.')
    

    def descontar_fondo_fijo(self):

        fondo_operating_unit = self.env['fondo_fijo.tienda'].search([('operating_unit_id', '=', self.operating_unit_id.id)])

        for record in fondo_operating_unit:

            fondo_fijo = record.fondo_fijo_ahora - self.valor_en_pesos

            record.write({'fondo_fijo_ahora': fondo_fijo})
