from odoo import fields, models, api

class PosOrderLine(models.Model):

    _inherit = 'pos.order.line'
    _name = _inherit

    
    total_iva = fields.Float(
        string='Total IVA',
        digits=(12,2),
        compute='_obtener_total_iva'
    )
    
    @api.depends('tax_ids_after_fiscal_position')
    def _obtener_total_iva(self):

        for linea in self.lines:

            for impuesto in linea.tax_ids_after_fiscal_position:

                self.total_iva += (impuesto.amount * linea.price_subtotal)/100