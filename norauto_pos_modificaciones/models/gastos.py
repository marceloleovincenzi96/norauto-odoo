from odoo import fields, models, api
from odoo.exceptions import UserError, ValidationError


class Gastos(models.Model):

    _inherit = 'hr.expense'
    _name = _inherit

    state = fields.Selection(selection_add=[('Conciliado', 'Conciliado')])

    rubrica_gasto_id = fields.Many2one(
        string='Rubrica de gasto',
        comodel_name='norauto.rubricas_gastos'
    )

    product_id = fields.Many2one(
        string='Producto',
        comodel_name='product.product',
        default=lambda self: self._get_producto_gasto()
    )

    tienda_iniciadora_id = fields.Many2one(
        string='Tienda iniciadora',
        comodel_name='operating.unit',
        default=lambda self: self._get_tienda_iniciadora()
    )

    tienda_beneficiada_id = fields.Many2one(
        string='Tienda beneficiada',
        comodel_name='operating.unit',
        ondelete='restrict'
    )

    factura_gasto_id = fields.Many2one(
        string='Factura del gasto',
        comodel_name='account.move',
        ondelete='restrict',
    )
    

    employee_id = fields.Many2one(
        string='Empleado',
        comodel_name='hr.employee',
        ondelete='restrict',
        default=lambda self: self._get_empleado()
    )

    vale_id = fields.Many2one(
        string='Nombre de vale',
        comodel_name='norauto.vales_cajachica',
        ondelete='restrict',
    )

    valor_vale = fields.Float(
        string='Valor del vale',
        related='vale_id.valor_en_pesos'
    )


    
    validacion_realizada = fields.Boolean(
        string='Validacion realizada',
        default=False
    )
    

    def realizar_validacion(self):

        fondo_fijo_tienda = self.env["fondo_fijo.tienda"].search([('operating_unit_id', '=', self.tienda_iniciadora_id.id)])

        if self.vale_id:

            diferencia = self.valor_vale - self.total_amount 

            if diferencia >= 0:

                fondo_fijo_actualizado = fondo_fijo_tienda.fondo_fijo_ahora + diferencia

                if fondo_fijo_actualizado > fondo_fijo_tienda.fondo_fijo:
                    raise ValidationError('Accion denegada. Introdujo una cantidad con la que se superaria el monto original del fondo fijo. Fondo fijo original: $%.2f. Fondo fijo actual: $%.2f. Fondo fijo actual con diferencia: $%.2f.' % (fondo_fijo_tienda.fondo_fijo, fondo_fijo_tienda.fondo_fijo_ahora, fondo_fijo_actualizado))

                fondo_fijo_tienda.write({
                    'fondo_fijo_ahora': fondo_fijo_actualizado
                })
            
            else:

                fondo_fijo_actualizado = fondo_fijo_tienda.fondo_fijo_ahora - diferencia

                if fondo_fijo_actualizado < 0:
                    raise ValidationError('Accion denegada. Introdujo una cantidad con la que el fondo fijo actual se haria negativo. Fondo fijo original: $%.2f. Fondo fijo actual: $%.2f. Fondo fijo actual con diferencia: $%.2f.' % (fondo_fijo_tienda.fondo_fijo, fondo_fijo_tienda.fondo_fijo_ahora, fondo_fijo_actualizado))
                
                fondo_fijo_tienda.write({
                    'fondo_fijo_ahora': fondo_fijo_actualizado
                })

        else:

            fondo_fijo_actualizado = fondo_fijo_tienda.fondo_fijo_ahora - self.total_amount

            if fondo_fijo_actualizado < 0:
                raise ValidationError('Accion denegada. Introdujo una cantidad con la que el fondo fijo actual se haria negativo. Fondo fijo original: $%.2f. Fondo fijo actual: $%.2f. Fondo fijo actual con diferencia: $%.2f.' % (fondo_fijo_tienda.fondo_fijo, fondo_fijo_tienda.fondo_fijo_ahora, fondo_fijo_actualizado))

            fondo_fijo_tienda.write({
                    'fondo_fijo_ahora': fondo_fijo_actualizado
                })
            
        self.validacion_realizada = True
        self.write({'state': 'Conciliado'})


    def _get_producto_gasto(self):

        return self.env['product.product'].search([('name', '=', 'GASTO')]).id

    def _get_tienda_iniciadora(self):

        return self.env['operating.unit'].search([('id', '=', self.env.user.default_operating_unit_id.id)]).id

    def _get_empleado(self):

        empleado = self.env['res.users'].search([('id', '=', self.env.uid)])

        if not empleado.employee_id:

            empleado = self.env['res.users'].search([('id', '=', self.env.uid)]).action_create_employee()

        return empleado.employee_id.id
