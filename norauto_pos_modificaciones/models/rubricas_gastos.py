from odoo import fields, models, api
from odoo.exceptions import UserError

class RubricasGastos(models.Model):

    _name = 'norauto.rubricas_gastos'

    
    number = fields.Integer(
        string='Numero',
    )
    

    name = fields.Char(
        string='Nombre',
    )

    def name_get(self):
        res = []

        for rec in self:
            res.append((rec.number, rec.name))
        
        return res
    