from odoo import fields, models, api
from odoo.exceptions import UserError, ValidationError


class GastosWizard(models.Model):

    _inherit = 'hr.expense.sheet.register.payment.wizard'
    _name = _inherit

    @api.onchange('document_type_id', 'document_number')
    def _inverse_document_number(self):

        res = super(GastosWizard, self)._inverse_document_number()

        



        