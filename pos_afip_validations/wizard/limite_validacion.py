from odoo import fields, models, api

class LimiteValidacion(models.TransientModel):
    _inherit = 'res.config.settings'
    _name = _inherit

    limite_validacion_tarjeta_afip = fields.Float(
        string='Limite para aprobar sin presentar numero o tipo de documento para tarjeta',
        default=17472
    )

    
    limite_validacion_efectivo_afip = fields.Float(
        string='Limite para aprobar sin presentar numero o tipo de documento para efectivo',
        default=8736
    )
    
    
    def set_values(self):
        super(LimiteValidacion, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param("limite_validacion_tarjeta_afip", self.limite_validacion_tarjeta_afip)
        self.env['ir.config_parameter'].sudo().set_param("limite_validacion_efectivo_afip", self.limite_validacion_efectivo_afip)
    
    @api.model
    def get_values(self):
        res = super(LimiteValidacion, self).get_values()
        
        params = self.env['ir.config_parameter'].sudo()
        
        limite_validacion = float(params.get_param('limite_validacion_tarjeta_afip', False))
        limite_validacion_efectivo = float(params.get_param('limite_validacion_efectivo_afip', False))
        
        res.update(
            limite_validacion_tarjeta_afip=limite_validacion,
            limite_validacion_efectivo_afip=limite_validacion_efectivo
        )

        return res