# -*- encoding: utf-8 -*-

{
    "name": "Pos AFIP Validations",
    "version": "13.0.1.0",
    'author': 'Nybble Group',
    "summary": """Pos AFIP validations""",
    "license": "AGPL-3",
    "depends": ['point_of_sale', 'l10n_ar_pos_fields_partner'],
    "data": [
        'views/resources.xml',
        'wizard/limite_validacion.xml'
    ],
    'qweb': [],
    "installable": True,
    "active": False,
}