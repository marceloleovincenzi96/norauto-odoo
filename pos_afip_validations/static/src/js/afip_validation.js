odoo.define('pos_afip.validations', function (require) {
    "use strict";

    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');

    models.load_models({
        model: 'res.config.settings',
        fields: ['limite_validacion_tarjeta_afip', 'limite_validacion_efectivo_afip'],
        loaded: function (self, limite_validacion_tarjeta_afip, limite_validacion_efectivo_afip) {
            console.log('Limite validacion efectivo: ', limite_validacion_efectivo_afip)
            self.limite_validacion_tarjeta_afip = limite_validacion_tarjeta_afip;
            self.limite_validacion_efectivo_afip = limite_validacion_efectivo_afip;
        }
    });

    screens.PaymentScreenWidget.include({

        init: function (parent, options) {
            var self = this;
            this._super(parent, options);
            this.limite_validacion_efectivo_afip = 0;
            this.limite_validacion_tarjeta_afip = 0;

            this._rpc({

                model: 'ir.config_parameter',
                method: 'get_param',
                args: [['key', '=', 'limite_validacion_efectivo_afip']],

            }).then(function(value){

                self.limite_validacion_efectivo_afip = value
                
            })

            this._rpc({

                model: 'ir.config_parameter',
                method: 'get_param',
                args: [['key', '=', 'limite_validacion_tarjeta_afip']],

            }).then(function(value){

                self.limite_validacion_tarjeta_afip = value
                
            })

        },



        validate_order: function (force_validation) {
            /* Verificar si el monto de la orden es mayor al limite establecido por AFIP para informar tipo y numero de documento
            por ahora el valor va a estar harcodeado, lo optimo seria hacer una configuracion en el pos.config, o en
            parametros de sistema */
            var self = this;
            var order = this.pos.get_order()
            var cliente = order.get_client()
            //var monto_maximo = 17472
            // console.log('cliente: ', cliente)
            // console.log('cliente.l10n_latam_identification_type_id[1]: ', cliente.l10n_latam_identification_type_id[1])
            // console.log('cliente.vat: ', cliente.vat)
            // console.log('resultado comparacion or or: ', (cliente.l10n_latam_identification_type_id[1] == 'Sigd' || (cliente.vat == '' || cliente.vat == false)))
            // console.log('order.get_total_with_tax(): ', order.get_total_with_tax())
            // console.log('monto_maximo: ', monto_maximo)
            // console.log('comparacion montos: ', order.get_total_with_tax() >= monto_maximo)
            var paymentlines = order.get_paymentlines();

            paymentlines.forEach(function (line) {

                switch (line.payment_method.id) {

                    case 15: //15 es el id de Tarjeta

                        if (order.get_total_with_tax() >= self.limite_validacion_tarjeta_afip && (cliente.l10n_latam_identification_type_id[1] == 'Sigd' || (cliente.vat == '' || cliente.vat == false))) {
                            self.gui.show_popup('error', {
                                'title': 'Error de Validación',
                                'body': `Para facturas B mayor o igual a $${self.limite_validacion_tarjeta_afip}, el cliente debe tener un tipo de documento diferente de Sigd y un número de documento mayor a 0.\n\nPor favor cambie el cliente o llene esos datos correctamente.`,
                            });
                        }
                        else {
                            self._super(force_validation);
                        }

                        break;

                    case 1: //1 es el id de efectivo
                        console.log('limite efectivo: ', self.limite_validacion_efectivo_afip)
                        if (order.get_total_with_tax() >= self.limite_validacion_efectivo_afip && (cliente.l10n_latam_identification_type_id[1] == 'Sigd' || (cliente.vat == '' || cliente.vat == false))) {
                            self.gui.show_popup('error', {
                                'title': 'Error de Validación',
                                'body': `Para facturas B mayor o igual a $${self.limite_validacion_efectivo_afip}, el cliente debe tener un tipo de documento diferente de Sigd y un número de documento mayor a 0.\n\nPor favor cambie el cliente o llene esos datos correctamente.`,
                            });
                        }
                        else {
                            self._super(force_validation);
                        }

                        break;
                    
                    default:

                        self._super(force_validation);
                        break;


                }


            });


        }
    })

});