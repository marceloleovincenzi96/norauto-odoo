# -*- coding: utf-8 -*-
{
    "name": "Stock Demand Trends and Forecast",
    "version": "13.0.1.0.1",
    "category": "Warehouse",
    "author": "Odoo Tools",
    "website": "https://odootools.com/apps/13.0/stock-demand-trends-and-forecast-450",
    "license": "Other proprietary",
    "application": True,
    "installable": True,
    "auto_install": False,
    "depends": [
        "stock"
    ],
    "data": [
        "security/ir.model.access.csv",
        "security/security.xml",
        "data/data.xml",
        "wizard/open_stock_series.xml",
        "views/res_config_settings.xml",
        "views/product_product.xml",
        "views/product_template.xml",
        "views/stock_location.xml",
        "reports/report_stock_demand.xml"
    ],
    "qweb": [
        
    ],
    "js": [
        
    ],
    "demo": [
        
    ],
    "external_dependencies": {
        "python": [
                "pandas",
                "numpy",
                "statsmodels",
                "scipy",
                "xlsxwriter"
        ]
},
    "summary": "The tool to calculate stock demand trends and make prediction for future demand statistically. Stock Forecast",
    "description": """
    If you knew stock demand trends per warehouses, you would have a clue to decrease keeping costs and to have a flawless supply chain. Regretfully, you can't know the future. However, you can predict it with a certain reliability. This is the tool for that goal. The app let you construct stock demand per periods and forecast further demand. 

    When this tool should be used
    Stock demand forecasting interface
    Scientific approach for forecasting
    Topical data under consideration
    # Statistical methods for forecast
    <div class="alert alert-info">
    <span style="font-size:18px">
            <i class="fa fa-info-circle"> </i>
Some statistical methods require deeper knowledge in statistics. To start with read <a href='https://machinelearningmastery.com/time-series-forecasting-methods-in-python-cheat-sheet/'>this</a>, <a href='https://www.digitalocean.com/community/tutorials/a-guide-to-time-series-forecasting-with-arima-in-python-3'>this</a>,  and <a href='https://people.duke.edu/~rnau/411arim.htm'>this</a> articles
    </span>
</div>

<h5 style='font-size:18px;'><strong>Autoregression (AR)</strong></h5>
<p style='font-size:18px;'>It is the simplest but still widely used statistical method for time series forecast. Using the method you consider stock demand trends being linear without seasonal effects, without a purely defined trend, and without smoothing abnormal observation.</p>
<h5 style='font-size:18px;'><strong>Moving Average (MA) and Autoregressive Moving Average (ARMA)
</strong></h5>
<p style='font-size:18px;'>The moving average method takes into account 'errors' in previous observations, and in comparison to the AR method smooths abnormal data.</p>
<p style='font-size:18px;'>The autoregressive moving average method is a combination of both AR and MA methods. To apply the ARMA method use the MA method with auto regression coefficient (P coefficient) as 2</p>

<h5 style='font-size:18px;'><strong>Autoregressive Integrated Moving Average (ARIMA)</strong></h5>
<p style='font-size:18px;'>The method which also combines the methods AR and MA, but also tries to make data stationary. It is appropriate to use for historical data with pure trend but without seasonal changes.</p>

<h5 style='font-size:18px;'><strong>Seasonal Autoregressive Integrated Moving-Average (SARIMA)</strong></h5>
<p style='font-size:18px;'>The SARIMA method enriches the ARIMA method with considering seasonal changes. It is one of the most complex and wide spread methods utilized for forecasting time series now</p>

<h5 style='font-size:18px;'><strong>Simple Exponential Smoothing (SES)</strong></h5>
<p style='font-size:18px;'>The SES model usage is similar to the AR method, but instead of relying upon linear function, it exploits exponential one</p>

<h5 style='font-size:18px;'><strong>Holt Winter’s Exponential Smoothing (HWES)</strong></h5>
<p style='font-size:18px;'>The HWES method enriches the SES method to work with time series trends and seasonal effects.</p>
    Target your analysis by locations
    Experiment with various statistical models
    Stock demand trends and forecast chart
    Stock demand trends and forecast as an xlsx table
    Odoo pivot view of stock trends and forecast
    Stock demand forecast in a few clicks
    Grant the right for the forecast report
    I faced the error: QWeb2: Template 'X' not found
    <div class="knowsystem_block_title_text">
            <div class="knowsystem_snippet_general" style="margin:0px auto 0px auto;width:100%;">
                <table align="center" cellspacing="0" cellpadding="0" border="0" class="knowsystem_table_styles" style="width:100%;background-color:transparent;border-collapse:separate;">
                    <tbody>
                        <tr>
                            <td width="100%" class="knowsystem_h_padding knowsystem_v_padding o_knowsystem_no_colorpicker" style="padding:20px;vertical-align:top;text-align:inherit;">
                                
                                <ol style="margin:0px 0 10px 0;list-style-type:decimal;"><li><p class="" style="margin:0px;">Restart your Odoo server and update the module</p></li><li><p class="" style="margin:0px;">Clean your browser cache (Ctrl + Shift + R) or open Odoo in a private window.</p></li></ol></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    What are update policies of your tools?
    
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


<p style="margin:0px 0px 0.25cm 0px;line-height:120%;">
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


</p><p style="margin:0px 0px 0.25cm 0px;line-height:120%;">
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


</p><p style="margin:0px 0px 0.25cm 0px;line-height:120%;">
	
	
	<style type="text/css">
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 115% }
	</style>


</p><p lang="en-US" style="margin:0px 0px 0.25cm 0px;line-height:120%;">According to the current Odoo Apps Store policies:</p><ul style="margin:0px 0 10px 0;list-style-type:disc;"><li><p lang="en-US" style="margin:0px;line-height:120%;"> every module bought for the version 12.0 and prior gives you an access to the all versions up to 12.0. </p></li><li><p lang="en-US" style="margin:0px;line-height:120%;">starting from the version 13.0, every version of the module should be purchased separately.</p></li><li><p lang="en-US" style="margin:0px;line-height:120%;">disregarding the version, purchasing a tool grants you a right for all updates and bug fixes within a major version.<br></p></li></ul><p lang="en-US" style="margin:0px 0px 0.25cm 0px;line-height:120%;">Take into account that Odoo Tools team does not control those policies. By all questions please contact the Odoo Apps Store representatives <a href="https://www.odoo.com/contactus" style="text-decoration:none;color:rgb(13, 103, 89);background-color:transparent;">directly</a>.</p>
    May I buy your app from your company directly?
    
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


<p style="margin:0px 0px 10px 0px;">Sorry, but no. We distribute the
tools only through the <a href="https://apps.odoo.com/apps" style="text-decoration:none;color:rgb(13, 103, 89);background-color:transparent;">official Odoo apps store</a></p>
    How should I install your app?
    
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


<p style="line-height:120%;margin:0px 0px 10px 0px;">
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


</p><ol style="margin:0px 0 10px 0;list-style-type:decimal;">
	<li><p style="margin:0px;line-height:120%;">Unzip source code of purchased tools in one of your Odoo
	add-ons directory</p>
	</li><li><p style="margin:0px;line-height:120%;">Re-start the Odoo server</p>
	</li><li><p style="margin:0px;line-height:120%;">Turn on the developer mode (technical settings)</p>
	</li><li><p style="margin:0px;line-height:120%;">Update the apps' list (the apps' menu)</p>
	</li><li><p style="margin:0px;line-height:120%;">Find the app and push the button 'Install'</p>
	</li><li><p style="margin:0px;line-height:120%;">Follow the guidelines on the app's page if those exist.</p>
</li></ol>
    Your tool has dependencies on other app(s). Should I purchase those?
    
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


<p style="margin:0px 0px 0.25cm 0px;line-height:120%;">Yes, all modules marked in dependencies are absolutely required for a correct work of our tool. Take into account that price marked on the app page already includes all necessary dependencies.&nbsp;&nbsp;</p>
    I noticed that your app has extra add-ons. May I purchase them afterwards?
    
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


<p style="margin:0px 0px 0.25cm 0px;line-height:120%;">
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


</p><p style="margin:0px 0px 0.25cm 0px;line-height:120%;">Yes, sure. Take into account that Odoo
automatically adds all dependencies to a cart. You should exclude
previously purchased tools.</p>
    I would like to get a discount
    
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


<p style="margin:0px 0px 0.25cm 0px;line-height:120%;">
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


</p><p style="margin:0px 0px 0.25cm 0px;line-height:120%;">
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


</p><p style="margin:0px 0px 0.25cm 0px;line-height:120%;">
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


</p><p style="margin:0px 0px 0.25cm 0px;line-height:120%;">Regretfully, we do not have a
technical possibility to provide individual prices.</p>
    I deployed your app on Odoo.sh and see warning / error in tests
    <p style="margin:0px 0px 10px 0px;">Red / orange warning itself does not influence features of the app. Regretfully, sometimes our modules do not pass standard automatic tests, since the latter assumes behavior which is in conflict with our apps goals. For example, we change price calculation, while standard Odoo module tests compare final price to standard algorithm.</p><p style="margin:0px 0px 10px 0px;">So, first of all, please check deployed database features. Does everything work correctly?</p><p style="margin:0px 0px 10px 0px;">If you still assume that warning influences real features, please contact us and forward full installation logs and the full lists of deployed modules (including core and third party ones).</p>
    How can I install your app on Odoo.sh?
    
	
	
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
	-->
	</style>


<p style="margin:0px 0px 10px 0px;">As soon as you purchased the
app, the button 'Deploy on Odoo.sh' will appear on the app's page in
the Odoo store. Push this button and follow the instructions.</p>
<p style="margin:0px 0px 10px 0px;">Take into account that for paid
tools you need to have a private GIT repository linked to your
Odoo.sh projects</p>
    May I install the app on my Odoo Online (SaaS) database?
    <p style="margin:0px 0px 10px 0px;">No, third party apps can not be used on Odoo Online.</p>
""",
    "images": [
        "static/description/main.png"
    ],
    "price": "198.0",
    "currency": "EUR",
}