
# -*- encoding: utf-8 -*-

{
    'name': 'Transeferencias internas',
    'version': '1.0',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'Transeferencias internas',
    'depends': [
        'norauto_rights',
        'stock',
        'stock_operating_unit',
    ],
    'author': 'NybbleGroup',
    'description': """
NorAuto Transferencias Internas
===============

""",
    'data': [
        "views/stock_picking_view.xml",
        "views/stock_transfer_view.xml",
        "views/stock_return_photo.xml",
        "views/templates.xml",
        "security/transferencias_internas_security.xml",
        "security/ir.model.access.csv",
    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
