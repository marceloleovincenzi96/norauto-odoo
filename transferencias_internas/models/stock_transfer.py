# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import ValidationError


class StockTransfer(models.Model):
    _name = 'stock.internal.transfer'
    _rec_name = 'picking_id'

    location_id = fields.Many2one(
        comodel_name='stock.location', string='Origen')
    location_dest_id = fields.Many2one(
        comodel_name='stock.location', string='Destino')
    stock_transfer_line_ids = fields.One2many(
        comodel_name='stock.internal.transfer.line',
        inverse_name='stock_transfer_id',
        string='Lineas')
    stock_transfer_line_filtradas_ids = fields.One2many(
        comodel_name='stock.internal.transfer.line',
        inverse_name='stock_transfer_filtro_id',
        string='Lineas filtradas')
    picking_id = fields.Many2one(
        comodel_name='stock.picking',
        string='Transferencia Interna')
    estado = fields.Selection(
        selection=[
            ('borrador', 'Borrador'),
            ('confirmada', 'Confirmada'),
            ('en_transito', 'En Tránsito'),
            ('recibida', 'Recibida')
        ], string="Estado")
    mostrar_guardar_validar = fields.Boolean(
        'Mostrar boton guardar y validar', compute='_compute_mostrar_guardar_validar')
    product_barcode = fields.Char(
        string='Codigo de barra de producto',
    )
    ocultar_todos_productos = fields.Boolean(
        string='Filtro activado',
        default=True
        
    )

        

    def write(self, vals):

        #import pdb; pdb.set_trace()

        return super(StockTransfer, self).write(vals)

    @api.onchange('product_barcode')
    def add_line(self):
        result = []
        for record in self:
            if record.product_barcode:
                product = self.env['product.product'].search(
                    [('barcode', '=',  record.product_barcode)])
                if product:
                    controller = []
                    for item in record.stock_transfer_line_ids:
                        if item.product_id.id == product.id:
                            result.append((1, item.id, {'cantidad_recibida': item.cantidad_recibida + 1}))
                        elif product.id not in record.stock_transfer_line_ids.mapped('product_id').ids and product.id not in controller:
                            transf = self.env['stock.internal.transfer.line'].create({'product_id': product.id, 'cantidad_pedida': 0, 'cantidad_enviada': 0, 'cantidad_recibida': 1, 'stock_transfer_id': self.id})
                            result.append((1, transf.id, {'cantidad_recibida': 1}))
                            controller.append(transf.id)
                else:
                    raise ValidationError ('No se ha encontrado ningun producto con el codigo de barras ingresado.')
            record.stock_transfer_line_ids = result
            if self.ocultar_todos_productos:
                record.stock_transfer_line_filtradas_ids = record.stock_transfer_line_ids.filtered(lambda linea: linea.product_id.barcode == record.product_barcode)
            record.product_barcode = ''

    def _compute_mostrar_guardar_validar(self):
        for rec in self:
            rec['mostrar_guardar_validar'] = self.env.context.get(
                'recepcion', False)

    def guardar_confirmar(self):
        line_vals = [(1, line.id, {'cantidad_recibida': line.cantidad_recibida})
                     for line in self.stock_transfer_line_ids]
        self.write({'stock_transfer_line_ids': line_vals, 'estado': 'recibida'})
        # NOTE: crear stock picking de ajuste de inventario si es necesario
        diferencias = []
        for line in self.stock_transfer_line_ids:
            if line.cantidad_diferencia != 0:
                diferencias.append(
                    {'product_id': line.product_id.id, 'cantidad': line.cantidad_diferencia})
        if diferencias:
            StockQuant = self.env['stock.quant']
            for dif in diferencias:
                quant = StockQuant.search(
                    [('location_id', '=', self.picking_id.location_dest_id.id), ('product_id', '=', dif['product_id'])])
                quant.with_context({'inventory_mode': True}).write({
                    'inventory_quantity': quant.quantity - dif['cantidad']
                })

        # TODO: pasar picking a estado preparado
        self.picking_id.assign_and_validate()


class StockTransferLine(models.Model):
    _name = 'stock.internal.transfer.line'

    stock_transfer_id = fields.Many2one(
        comodel_name='stock.internal.transfer', string='Transferencia')
    move_line_id = fields.Many2one(
        comodel_name='stock.move', string='Stock Move')
    product_id = fields.Many2one(
        comodel_name='product.product', string='Producto')
    cantidad_pedida = fields.Integer(string='Cantidad Pedida')
    cantidad_enviada = fields.Integer(string='Cantidad Enviada')
    cantidad_recibida = fields.Integer(string='Cantidad Recibida')
    cantidad_diferencia = fields.Integer(
        string='Diferencia', compute='_compute_cantidad_diferencia', store='True')
    
    stock_transfer_filtro_id = fields.Many2one(
        string='Transferencia filtro',
        comodel_name='stock.internal.transfer',
        ondelete='restrict', 
    )

    @api.depends('cantidad_enviada', 'cantidad_recibida')
    def _compute_cantidad_diferencia(self):
        for rec in self:
            rec['cantidad_diferencia'] = rec.cantidad_enviada - \
                rec.cantidad_recibida

