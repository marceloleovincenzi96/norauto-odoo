# -*- coding: utf-8 -*-

from lxml import etree
from odoo import api, exceptions, fields, models, _
from odoo.tools.float_utils import float_compare, float_is_zero, float_round


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    state = fields.Selection(selection_add=[('in_transit', 'En Tránsito')])
    internal_transaction = fields.Boolean('Transferencia Interna', compute='_compute_internal_transaction')
    transferencia_deposito_central = fields.Boolean('Transferencia desde dep central', compute="_compute_transferencia_deposito_central")
    internal_transfer_id = fields.Many2one('stock.internal.transfer', 'Transferencia Interna')
    photo_return_count = fields.Integer('Retornos con Foto', compute='_compute_photo_return_count')
    picking_type_id = fields.Many2one(
        'stock.picking.type', 'Operation Type',
        required=True, readonly=True,
        states={'draft': [('readonly', False)]},
        default=lambda self: self._default_picking_type_id())
    
    partner_id =  fields.Many2one(
        string='Contacto',
        comodel_name='res.partner',
        default=lambda self: self._default_partner_id()
    )

    voucher_ids = fields.One2many(
        string='Remitos',
        comodel_name='stock.picking.voucher',
        inverse_name='picking_id',
        required=True
    )
    
    

    def _default_picking_type_id(self):
        warehouse = self.env['stock.warehouse'].search([('operating_unit_id', '=', self.env.user.default_operating_unit_id.id)], limit=1)
        if warehouse:
            return warehouse.int_type_id.id

    def _default_partner_id(self):
        norauto = self.env['res.partner'].search([('id', '=', 1)])
        return norauto

    ##################################################
    # COMPUTED METHODS
    ##################################################
    @api.depends('internal_transaction')
    def _compute_transferencia_deposito_central(self):
        for rec in self:
            if rec.internal_transaction and rec.location_id == self.env.ref('stock.stock_location_stock'):
                rec['transferencia_deposito_central'] = True
            else:
                rec['transferencia_deposito_central'] = False

    def _compute_photo_return_count(self):
        for rec in self:
            rec['photo_return_count'] = self.env['stock.return.photo'].search_count([('stock_picking_id', '=', rec.id)])

    @api.depends('location_id', 'location_dest_id')
    def _compute_internal_transaction(self):
        for rec in self:
            rec['internal_transaction'] = rec.location_id.usage == 'internal' and rec.location_dest_id.usage == 'internal'

    ##################################################
    # ACTION METHODS
    ##################################################
    def action_confirm(self):
        # NOTE: al confirmar envio se pone para generar el enivio de idoc
        res = super(StockPicking, self).action_confirm()
        if self.internal_transaction:
            lines = []
            for line in self.move_ids_without_package:
                lines.append((0, 0, {
                    'move_line_id': line.id,
                    'product_id': line.product_id.id,
                    'cantidad_pedida': line.product_uom_qty,
                }))
            internal_transfer = self.env['stock.internal.transfer'].create({
                'location_id': self.location_id.id,
                'location_dest_id': self.location_dest_id.id,
                'estado': 'confirmada',
                'picking_id': self.id,
                'stock_transfer_line_ids': lines
            })
            self.write({'internal_transfer_id': internal_transfer.id})
        return res

    def confirmar_envio(self):
        if not self.internal_transaction:
            raise exceptions.ValidationError("No es una transferencia interna")
        if self.internal_transaction and self.location_id.operating_unit_id not in self.env.user.operating_unit_ids:
            raise exceptions.ValidationError("No tiene permiso para continuar el proceso")

        precision_digits = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        no_quantities_done = all(float_is_zero(move_line.qty_done, precision_digits=precision_digits) for move_line in self.move_line_ids.filtered(lambda m: m.state not in ('done', 'cancel')))

        if no_quantities_done:
            for move in self.move_lines.filtered(lambda m: m.state not in ['done', 'cancel']):
                for move_line in move.move_line_ids:
                    move_line.qty_done = move_line.product_uom_qty

        # TODO: sacar stock manualmente de los quants (de reservado y de cantidad)
        for move in self.move_line_ids_without_package:
            quant = self.env['stock.quant'].search([('location_id', '=', self.location_id.id), ('product_id', '=', move.product_id.id)])
            quant._update_reserved_quantity(move.product_id, self.location_id, -move.qty_done)
            # NOTE: can it be done with sql query?
            # NOTE: YES,  pero la validacion de inventario sigue con las mismas cosas y anda a saber que mas
            # query = "UPDATE stock_quant SET quantity=%s WHERE id=%s" % (quant.quantity - move.qty_done, quant.id)
            # self._cr.execute(query)
            quant.with_context({'inventory_mode': True}).write({
                'inventory_quantity': quant.quantity - move.qty_done
            })
        # TODO: asignar al transfer.internal la cantidad enviada
        for line in self.move_ids_without_package:
            transfer_line = self.env['stock.internal.transfer.line'].search([('move_line_id', '=', line.id)])
            transfer_line.write({'cantidad_enviada': line.product_uom_qty})
        self.internal_transfer_id.write({'estado': 'en_transito'})
        # TODO: pasar a estado en transito
        self.write({'state': 'in_transit'})

    def confirmar_recepcion(self):
        if not self.internal_transaction:
            raise exceptions.ValidationError("No es una transferencia interna")
        if self.internal_transaction and self.location_dest_id.operating_unit_id not in self.env.user.operating_unit_ids:
            raise exceptions.ValidationError("No tiene permiso para continuar el proceso")
        # TODO: devolver a stock quants manualmente la cantidad reserveda y cantidad que se sacaron
        for move in self.move_line_ids_without_package:
            quant = self.env['stock.quant'].search([('location_id', '=', self.location_id.id), ('product_id', '=', move.product_id.id)])
            quant.with_context({'inventory_mode': True}).write({
                'inventory_quantity': quant.quantity + move.qty_done
            })
            # query = "UPDATE stock_quant SET quantity=%s WHERE id=%s" % (quant.quantity + move.qty_done, quant.id)
            # self._cr.execute(query)
            quant._update_reserved_quantity(move.product_id, self.location_id, move.qty_done)

        # TODO: asignar al transfer.internal la cantidad recibida
        # NOTE: esto tiene que ser por el wizard
        return {
            'name': "Recepción de Productos",
            'view_mode': 'form',
            'res_model': 'stock.internal.transfer',
            'res_id': self.internal_transfer_id.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'domain': '[]',
            'flags': {'action_buttons': False},
            'context': {'recepcion': True, 'picking_id': self.id}
        }
        # TODO: pasar a estado preparado
        self.write({'state': 'assigned'})
        # TODO: call validar stock picking
        self.button_validate()

    def action_show_photo_returns(self):
        context = {'default_stock_picking_id': self.id}
        return {
            'name': "Devoluciones por foto",
            'view_mode': 'tree,form',
            'view_id': False,
            'res_model': 'stock.return.photo',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': "[('stock_picking_id', '=', %s)]" % self.id,
            'context': context
        }

    ##################################################
    # OVERRIDES
    ##################################################
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        """Agrega dominio custom a la vista search de transferencias internas para que busque solo las que tiene seteadas
        Agregia dominio al filtro de transferencias provenientes de otras sucursales
        """
        res = super(StockPicking, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if view_type == 'search':
            operating_unit_ids = self.env.user.operating_unit_ids.ids
            doc = etree.XML(res['arch'])
            for node in doc.xpath("//filter[@name='my_operating_unit']"):
                if node is not None:
                    domain = "[('location_id.operating_unit_id', 'in', %s)]" % operating_unit_ids
                    node.set('domain', domain)

            for node in doc.xpath("//filter[@name='other_operating_units']"):
                if node is not None:
                    domain = "[('location_id.operating_unit_id', 'not in', %s)]" % operating_unit_ids
                    node.set('domain', domain)
            res['arch'] = etree.tostring(doc)
        return res

    def action_assign(self):
        if self.internal_transaction and self.location_id.operating_unit_id not in self.env.user.operating_unit_ids:
            raise exceptions.ValidationError("No tiene permiso para continuar el proceso")
        res = super(StockPicking, self).action_assign()
        return res

    @api.depends('state', 'is_locked', 'internal_transaction')
    def _compute_show_validate(self):
        for picking in self:
            if picking.internal_transaction:
                picking.show_validate = False
            elif not (picking.immediate_transfer) and picking.state == 'draft':         # desde aca es la funcion original
                picking.show_validate = False
            elif picking.state not in ('draft', 'waiting', 'confirmed', 'assigned') or not picking.is_locked:
                picking.show_validate = False
            else:
                picking.show_validate = True

    def assign_and_validate(self):
        # TODO: call validar stock picking
        # self.button_validate()
        self.action_done()
        # self.write({'state': 'assigned'})
