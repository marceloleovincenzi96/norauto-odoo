# -*- coding: utf-8 -*-

from lxml import etree
from odoo import api, exceptions, fields, models


class StockReturnPhoto(models.Model):
    _name = 'stock.return.photo'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Envio fotos de articulos recibidos en mal estado a Depósito Central'
    _rec_name = 'stock_picking_id'

    stock_picking_id = fields.Many2one(
        comodel_name='stock.picking',
        string='Transferencia',
        required=True)
    operating_unit_id = fields.Many2one(
        comodel_name='operating.unit',
        string='Sucursal',
        compute='_compute_operating_unit_id',
        store=True
    )
    estado = fields.Selection(
        selection=[
            ('borrador', 'Borrador'),
            ('pendiente', 'Pendiente'),
            ('aprobada', 'Aprobada'),
            ('rechazada', 'Rechazada')
        ],
        string='Estado',
        default='borrador'
    )
    product_id = fields.Many2one(comodel_name='product.product', string='Producto')
    picking_products_ids = fields.Many2many(
        comodel_name='product.product',
        string="Productos de la transferencia",
        compute="_compute_picking_product_ids")

    fotos_ids = fields.One2many(
        comodel_name='stock.return.photo.photo',
        inverse_name='stock_return_photo_id',
        string='Fotos'
    )
    show_confirm = fields.Boolean('Mostrar boton confirmar', compute='_compute_show_confirm')
    show_accept_reject = fields.Boolean('Mostrar boton Aceptar/Rechazar', compute='_compute_show_accept_reject')
    user_deposito = fields.Boolean('Es usuario de depósito', compute='_compute_user_deposito')
    # NOTE: agrego el siguiente campo como una forma rebuscada de manejar las reglas de registro
    deposito_central_id = fields.Many2one(
        comodel_name='operating.unit',
        string='Deposito Central',
        default=lambda self: self.env.ref('operating_unit.main_operating_unit').id
    )
    quantity = fields.Integer("Cantidad a devolver")

    ##################################################
    # COMPUTE METHODS
    ##################################################
    def _compute_user_deposito(self):
        for rec in self:
            rec['user_deposito'] = rec.env.ref('operating_unit.main_operating_unit') in self.env.user.operating_unit_ids

    @api.depends('stock_picking_id')
    def _compute_operating_unit_id(self):
        for rec in self:
            rec['operating_unit_id'] = rec.stock_picking_id.location_dest_id.operating_unit_id.id

    def _compute_show_confirm(self):
        for rec in self:
            rec['show_confirm'] = rec.operating_unit_id in rec.env.user.operating_unit_ids

    def _compute_show_accept_reject(self):
        for rec in self:
            rec['show_accept_reject'] = rec.env.ref('operating_unit.main_operating_unit') in rec.env.user.operating_unit_ids

    @api.depends('stock_picking_id')
    def _compute_picking_product_ids(self):
        for rec in self:
            rec['picking_products_ids'] = [(6, 0, [line.product_id.id for line in rec.stock_picking_id.move_ids_without_package])]

    ##################################################
    # ACTION METHODS
    ##################################################
    def button_confirmar(self):
        # suscribir a todos del almacen central como seguidores
        # main_wh_users = self.env['res.users'].search([]).filtered(lambda r: self.env.ref('operating_unit.main_operating_unit') in r.operating_unit_ids)
        # wh_partners_ids = [u.partner_id.id for u in main_wh_users]
        # self.message_subscribe(partner_ids=wh_partners_ids, channel_ids=self.message_channel_ids)
        # # crear actividad para notificacion
        # summary = "Aprobar o rechazar la devolución por foto del producto %s de la transferencia %s." % (self.product_id.product_tmpl_id.name, self.stock_picking_id.name)
        # user = main_wh_users.filtered(lambda r: r.has_group('norauto_rights.group_manager') and r.id is not self.env.ref('base.user_admin').id)
        # self.activity_schedule(act_type_xmlid='mail.mail_activity_data_todo', summary=summary, user_id=user.id)
        # pasar estado a pendiente
        self.write({'estado': 'pendiente'})

    def button_aprobar(self):
        self.write({'estado': 'aprobada'})
        quant = self.env['stock.quant'].search([('location_id', '=', self.stock_picking_id.location_id.id), ('product_id', '=', self.product_id.id)])

        quant._update_reserved_quantity(self.product_id, self.stock_picking_id.location_id, self.quantity)
        quant._update_available_quantity(self.product_id, self.stock_picking_id.location_id, self.quantity)

    def button_rechazar(self):
        self.write({'estado': 'rechazada'})


class StockReturnPhotoPhoto(models.Model):
    _name = 'stock.return.photo.photo'

    foto = fields.Image(string='Foto')
    name = fields.Char(string='Descripcion foto')
    stock_return_photo_id = fields.Many2one(comodel_name='stock.return.photo')
