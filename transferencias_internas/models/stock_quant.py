from odoo import api, fields, models

class StockQuant(models.Model):

    _inherit = 'stock.quant'
    _name = _inherit

    inventory_quantity = fields.Float(
        'Inventoried Quantity', compute='_compute_inventory_quantity',
        inverse='_set_inventory_quantity', groups='stock.group_stock_manager')