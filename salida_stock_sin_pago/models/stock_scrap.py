# -*- coding: utf-8 -*-

from odoo import api, fields, models


class StockScrap(models.Model):
    _inherit = 'stock.scrap'

    ##################################################
    # DEFAULT METHODS
    ##################################################
    def _get_default_scrap_location_id(self):
        company_id = self.env.context.get('default_company_id') or self.env.company.id
        operating_unit = self.env.user.default_operating_unit_id
        stock_salida = self.env['stock.location'].search([
            ('scrap_location', '=', True),
            ('company_id', 'in', [company_id, False]),
            ('operating_unit_id', 'in', [operating_unit.id, False]),
        ], limit=1)
        if stock_salida:
            res = stock_salida.id
            return res
        return None

    def _get_default_location_id(self):
        company_id = self.env.context.get('default_company_id') or self.env.company.id
        operating_unit = self.env.user.default_operating_unit_id
        warehouse = self.env['stock.warehouse'].search([('company_id', '=', company_id), ('operating_unit_id', '=', operating_unit.id)], limit=1)
        if warehouse:
            res = warehouse.lot_stock_id.id
            return res
        return None

    def _get_default_operating_unit_id(self):
        return self.env.user.default_operating_unit_id.id

    # def _get_default_user_id(self):
    #     return self.env.user.id

    ##################################################
    # FIELDS
    ##################################################
    scrap_type_id = fields.Many2one(comodel_name='stock.scrap.type', string='Rúbrica', required=True)
    barcode = fields.Char('Código de Barras')
    idart = fields.Integer('Id de Artículo')
    user_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario',
        # default=_get_default_user_id
    )
    location_id = fields.Many2one(
        comodel_name='stock.location',
        string='Source Location',
        domain="[('usage', '=', 'internal'), ('company_id', 'in', [company_id, False]), ('operating_unit_id', 'in', [operating_unit_id, False]), ('scrap_location', '=', False)]",       # Cambiar el domain a operating_unit tambien
        required=True,
        states={'done': [('readonly', True)]},
        default=_get_default_location_id,
        check_company=True
    )
    scrap_location_id = fields.Many2one(
        comodel_name='stock.location',
        string='Scrap Location',
        default=_get_default_scrap_location_id,
        domain="[('scrap_location', '=', True), ('company_id', 'in', [company_id, False]), ('operating_unit_id', 'in', [operating_unit_id, False]), ('scrap_location', '=', True)]",
        required=True,
        states={'done': [('readonly', True)]},
        check_company=True
    )
    operating_unit_id = fields.Many2one(comodel_name='operating.unit', string='Sucursal', default=_get_default_operating_unit_id)

    ##################################################
    # ONCHANGE
    ##################################################
    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id:
            super(StockScrap, self)._onchange_product_id()
            self.barcode = self.product_id.product_tmpl_id.barcode
            self.idart = self.product_id.product_tmpl_id.idart

    @api.onchange('barcode')
    def _onchange_barcode(self):
        if self.barcode:
            product_template = self.env['product.template'].search([('barcode', '=', self.barcode)])
            product = self.env['product.product'].search([('product_tmpl_id', '=', product_template.id)])
            self.product_id = product
            self.idart = product_template.idart

    @api.onchange('idart')
    def _onchange_idart(self):
        if self.idart:
            product_template = self.env['product.template'].search([('idart', '=', self.idart)])
            product = self.env['product.product'].search([('product_tmpl_id', '=', product_template.id)])
            self.product_id = product
            self.barcode = product_template.barcode

    @api.onchange('operating_unit_id')
    def _onchange_operating_unit_id(self):
        if self.operating_unit_id:
            stock_salida = self.env['stock.location'].search([
                ('scrap_location', '=', True),
                ('company_id', 'in', [self.company_id.id, False]),
                ('operating_unit_id', 'in', [self.operating_unit_id.id, False]),
            ], limit=1)
            if stock_salida:
                self.scrap_location_id = stock_salida.id
            else:
                self.scrap_location_id = None
            warehouse = self.env['stock.warehouse'].search([('company_id', '=', self.company_id.id), ('operating_unit_id', '=', self.operating_unit_id.id)], limit=1)
            if warehouse:
                self.location_id = warehouse.lot_stock_id.id
            else:
                self.location_id = None

    @api.onchange('company_id')
    def _onchange_company_id(self):
        if self.company_id:
            warehouse = self.env['stock.warehouse'].search([('company_id', '=', self.company_id.id), ('operating_unit_id', '=', self.operating_unit_id.id)], limit=1)
            if warehouse:
                self.location_id = warehouse.lot_stock_id.id
            else:
                self.location_id = None

            stock_salida = self.env['stock.location'].search([
                ('scrap_location', '=', True),
                ('company_id', 'in', [self.company_id.id, False]),
                ('operating_unit_id', 'in', [self.operating_unit_id.id, False]),
            ], limit=1)
            if stock_salida:
                self.scrap_location_id = stock_salida.id
            else:
                self.scrap_location_id = None

    ##################################################
    # ACTIONS METHODS
    ##################################################
    def action_validate(self):
        res = super(StockScrap, self).action_validate()
        self.write({'user_id': self.env.uid})
        return res


class StockScrapType(models.Model):
    _name = 'stock.scrap.type'

    name = fields.Char('Nombre')
