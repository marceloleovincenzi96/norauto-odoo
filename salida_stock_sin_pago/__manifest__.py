
# -*- encoding: utf-8 -*-

{
    'name': 'Salida de Stock sin Pago',
    'version': '1.0',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'salida de stock sin pago',
    'depends': [
        'norauto_rights',
        'stock',
        'stock_operating_unit',
    ],
    'author': 'NybbleGroup',
    'description': """
NorAuto Orders
===============

""",
    'data': [
        "data/stock_scrap_data.xml",
        "views/stock_scrap_view.xml",
        "security/sssp_security.xml",
        "security/ir.model.access.csv",
    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
