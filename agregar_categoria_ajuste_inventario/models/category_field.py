from odoo import fields, models, api

class CategoryField(models.Model):
    _inherit = "stock.inventory"

    categoria = fields.Many2one('product.category', "Familia", domain=[('child_id', '=', False)])

    @api.onchange('categoria')
    def cargar_articulos (self):

        articulos_familia = self.env['product.product'].search([('categ_id', '=', self.categoria.id)])

        if not articulos_familia:
            self.update({'product_ids': [(5)]})

        for articulo in articulos_familia:
            self.update({'product_ids': (4, articulo.id)})

        
        


            