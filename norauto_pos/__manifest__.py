
# -*- encoding: utf-8 -*-

{
    'name': 'Norauto POS',
    'version': '13.0.0.1',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'Norauto POS',
    'depends': [
        'point_of_sale',
        'aspl_pos_promotion',
    ],
    'author': 'NybbleGroup',
    'description': """
NorAuto POS
===============
Graba la pos order con la relación de la promocióon aplicada
""",
    'data': [
        "views/pos_order_view.xml",
        "views/norauto_pos_view.xml",
    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
