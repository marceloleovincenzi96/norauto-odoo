# -*- coding: utf-8 -*-

from odoo import api, fields, models


class PosOrder(models.Model):
    _inherit = "pos.order"

    promotion_id = fields.Many2one('pos.promotion', 'Promoción')
    # x_studio_field_tdCxD

    @api.model
    def _order_fields(self, ui_order):
        res = super(PosOrder, self)._order_fields(ui_order)
        res.update(promotion_id=ui_order['promotion_id'] if "promotion_id" in ui_order else False)
        return res
