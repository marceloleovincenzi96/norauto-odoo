odoo.define('pos_partner_load_custom', function (require) {
    "use strict";

    var core = require('web.core');
    var models = require('point_of_sale.models');
    var PosModelSuper = models.PosModel;
    var pos_screens = require('point_of_sale.screens');
    var ClientListScreenWidget = pos_screens.ClientListScreenWidget
    var QWeb = core.qweb;
    var _t = core._t;

    // NOTE: limita los partners a los que tengan ID menor a 100
    var loaded_models = PosModelSuper.prototype.models
    for (let key in loaded_models){
        if (loaded_models[key].model === 'res.partner'){
            loaded_models[key].domain = [['id', '<', 100]]
        }
    }

    // NOTE: recien hace una busqueda despues de la 5ta letra introducida
    ClientListScreenWidget.include({
        perform_search: function(query, associate_result){
            var self = this;
            var _super=this._super.bind(this);
            if(query.length > 5){
                // TODO: llamar a funcion aqui para que se añadan nuevos partners a la db
                this._rpc({
                    model:'res.partner',
                    method:'search_read',
                    fields:['name','street','city','state_id','country_id','vat',
                            'phone','zip','mobile','email','barcode','write_date',
                            'property_account_position_id','property_product_pricelist',
                            'l10n_latam_identification_type_id', 'l10n_ar_afip_responsibility_type_id'],
                    domain: ['|', ['name', 'ilike', query], ['vat', 'ilike', query]],
                    args: []
    
                }).then(function(new_partners){
                    if (new_partners){
                        self.pos.db.add_partners(new_partners)
                    }
                    _super(query, associate_result)
 
                })
            }
        },
    })

    // TODO: Heredar la funcion del Enter de la busqueda de partners y hacer rpc query
    // y añadir a la lista de partners los resultados, despues devolver el resultado normal
});