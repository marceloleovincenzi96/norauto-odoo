# -*- encoding: utf-8 -*-

{
    "name": "Pos partner load",
    "version": "13.0.1.0",
    'author': 'Nybble Group',
    "summary": """Pos partner load""",
    "license": "AGPL-3",
    "depends": ['point_of_sale', 'default_customer_pos'],
    "data": [
        'views/pos_partner_load.xml'

    ],
    'qweb': [],
    "installable": True,
    "active": False,
}