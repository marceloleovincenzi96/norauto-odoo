odoo.define('pos.rubricas_descuentos', function (require) {
    "use strict";
    var models = require('point_of_sale.models');

    models.load_fields('pos.order.line', ['rubrica_descuento_id']);

    models.load_models({
        model: 'rubrica.descuento',
        fields: ['id', 'numero', 'name'],
        loaded: function (self, rubricas) {
            self.rubricas_descuentos = rubricas;
            console.log('Rubricas descuentos: ', self.rubricas_descuentos)
        }
    });

    var _super_orderline = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        initialize: function (attr, options) {
            this.rubrica_descuento = null;
            _super_orderline.initialize.call(this, attr, options);
        },

        export_as_JSON: function () {
            var json = _super_orderline.export_as_JSON.apply(this, arguments);
            json.rubrica_descuento_id = this.rubrica_descuento;
            return json;
        },
        init_from_JSON: function (json) {
            _super_orderline.init_from_JSON.apply(this, arguments);
            this.rubrica_descuento = json.rubrica_descuento_id;
        }
    })


})