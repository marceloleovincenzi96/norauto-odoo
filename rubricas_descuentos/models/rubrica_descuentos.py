from odoo import models, fields

class RubricaDescuento(models.Model):

    _name = 'rubrica.descuento'
    _descripcion = 'Rubrica de descuento'
    _rec_name = 'name'

    
    numero = fields.Integer(
        string = 'Numero'
    )
    
    
    name = fields.Char(
        string = 'Nombre'
    )
    