from odoo import models, fields

class PosOrderLine(models.Model):

    _inherit = 'pos.order.line'
    _name = _inherit

    
    rubrica_descuento_id = fields.Many2one(
        string='Rubrica de descuento',
        comodel_name='rubrica.descuento',
        ondelete='restrict',
    )
    