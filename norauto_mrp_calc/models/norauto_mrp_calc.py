# -*- coding: utf-8 -*-

from odoo import api, fields, models
from .a_generar_modelo_PKL import AGenerarModeloPKL as A
from .b_predecir import BPredecir as B
import xmlrpc.client


class NorautoMrpCalc(models.Model):
    _name = 'norauto.mrp.calc'

    def get_prediccion(self, data={}):
        print('---------------------------------------------------')
        print('Inicio del rest')
        res = {}
        # data = {'operating_units': ['652'], 'products_codes': ['62119','59256','71560','65984','575919','58493','155444','2068362','113444','134743','147475','500822','66585','569330','2027424','116141','342496','107133','133070','130616','303879']}
        print("Data corriendo:", data)
        if 'operating_units' in data and 'products_codes' in data:
            for op in data['operating_units']:
                res[op] = []
                for prod in data['products_codes']:
                    print("====================")
                    print("prod", prod)
                    print("op", op)
                    # a_generar_modelo_PKL.generarModelo(prod, op)
                    prediccion = B.predecir(self, prod, op)
                    print("Predicción origen:", prediccion)
                    # MAGIA DEL ARCHIVO PKL INDIVIDUAL
                    # En el data tienen que venir todos los datos necesarios para la magia
                    res[op].append({
                        'product_code': prod,
                        'prediccion': prediccion,
                    })
                    # asi se llaman las cosas con el request.env
                    # certificado = request.env['ofreser.certificado'].search([('numero', '=', numero_certificado)])
            return res
        return {'failed': 'No se mandaron operating_units o products_codes'}

    def generar_modelos_mrp(self, data={}):
        print('---------------------------------------------------')
        print('Inicio del rest')
        res = {}
        # data = {'operating_units': ['652'], 'products_codes': ['62119','59256','71560','65984','575919','58493','155444','2068362','113444','134743','147475','500822','66585','569330','2027424','116141','342496','107133','133070','130616','303879']}
        print("Data corriendo:", data)
        if 'operating_units' in data and 'products_codes' in data:
            for sucursal in data['operating_units']:
                res[sucursal] = []
                for articulo in data['products_codes']:
                    print("====================")
                    print("prod", articulo)
                    print("op", sucursal)
                    A.generarModelo(self, articulo, sucursal)

        return {'failed': 'No se mandaron operating_units o products_codes'}

    def get_mejores_parametros(self, data={}):
        print('data: %s' % data)
        # pasar unidad_operativa
        # En el data tienen que venir todos los datos necesarios para la magia
        # MAGIA DEL CALCULO DEL MODELO
        res = {
            'producto': 73860,
            'sucursal': 1,
            'parametros': 1
        }
        return res
