#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 16:20:18 2020

MODULOS VARIOS:
Este archivo contiene diferentes funciones que se usan en el programa:
1.- conexion a DB Odoo: ObtenerDatosDeDB. 
2.- Preparación de datos: a partir del dataset que contiene ventas diarias, 
    se obtiene un dataframe pandas con ventas semanales.
3.- calculo de mejores parámetros: Calcula parámetros necesarios para el 
    calculo del modelo: p, d, q, bias. Esos parametros se persisten (csv o db)

@author: jorgek
"""
# ------------------------------------------------------------------------------
#
# LIBRERIAS:
from odoo import api, fields, models
import pandas as pd
from pandas import read_csv
from sklearn.metrics import mean_squared_error
from math import sqrt
import statistics as stats
import warnings
from statsmodels.tsa.arima_model import ARIMA
import numpy
import xmlrpc.client
import datetime as dt
from datetime import timedelta
import odoorpc

#
# ------------------------------------------------------------------------------
#
# JARCODEO DE PARAMETROS:
PATH_SERVER = '/var/lib/odoo/.local/share/Odoo/addons/13.0/addons-norauto' \
              '/nybble_norauto/norauto_mrp_calc/modelos/ '
PATH_LOCAL = 'norauto_mrp_calc/modelos/'

archivo_local = 'norauto_mrp_calc/datasets/db_mejores_parametros.csv'
archivo = '/var/lib/odoo/.local/share/Odoo/addons/13.0/addons-norauto' \
          '/nybble_norauto/norauto_mrp_calc/datasets/db_mejores_parametros' \
          '.csv'
estacionalidad = 4
alcance = 4
train_prop = 0.8
meses12 = 52
# ------------------------------------------------------------------------------
#
# 1.- OBTENCION DE DATOS DE ODOO
"""
Este módulo requiere de Sucursal y Producto y devuelve el dataset de ventas 
diaras en formato DataFrame Pandas.
"""


def getConection(odoo=False):
    try:
        if odoo.config:
            return odoo
    except AttributeError:
        db_name = 'norauto'
        # db_name = 'norauto-3108'
        user = 'admin'
        psw = 'Devoo9060!!'
        # Prepare the connection to the server
        # odoo = odoorpc.ODOO('localhost', port=8069)
        odoo = odoorpc.ODOO('200.10.108.182', port=80)
        # setea el timeout a una hora
        odoo.config['timeout'] = 36000
        odoo.login(db_name, user, psw)
        return odoo


# ------------------------------------------------------------------------------
#
# 2.- PREPARACION DE DATOS
"""
    Partiendo del dataset que contiene el histórico diario de ventas, entrega a la 
    salida un dataframe pandas con las ventas semanales con el formato:
    "SEMANA","VENTAS"
    con: "SEMANA": AÑO-SEMANA (ej.: 2019-34)
         "VENTAS": INT.
"""


def consultarSiExisteFila(ds, unArt, unaSuc):
    res = False
    for i in range(len(ds)):
        if ds.iloc[i, 0] == int(unaSuc) and ds.iloc[i, 1] == int(unArt):
            res = i
            break
    return res


# agrupar ventas diarias (si hay más de un registro de venta en un día):
def agruparVentasDiarias(df):
    dia = list(set(df['FECHA']))
    ventas = []
    producto = []
    for i in range(len(dia)):
        ventaDiaria = 0
        for j in range(len(df)):
            if df.iloc[j, 0] == dia[i]:
                ventaDiaria = ventaDiaria + df.iloc[j, 2]
        ventas.append(ventaDiaria)
        producto.append(df.iloc[i, 1])
    df2 = pd.DataFrame()
    df2['FECHA'] = dia
    df2['PRODUCTO'] = producto
    df2['VENTAS'] = ventas
    return df2


# Cambiar formato Fecha:
def cambiarFormatoFecha(ds):
    FECHA = []
    for i in range(len(ds)):
        fecha = ds['FECHA'][i]
        FECHA.append(fecha)
    return FECHA


# Listar las ventas:
def listarVentas(df1):
    VENTAS = []
    for i in range(len(df1)):
        VENTAS.append(df1.iloc[i, 2])
    return VENTAS


# completar ventas en to do el calendario
def completarVentas(df):
    #    print(df)
    fecha = []
    ventas = []
    fechaFin = dt.datetime.strptime(str(df.iloc[len(df) - 1, 0]),
                                    '%Y-%m-%d')
    fecha.append(dt.datetime.strptime(str(df.iloc[0, 0]), '%Y-%m-%d'))
    ventas.append(df.iloc[0, 1])
    i = 0
    j = 1
    while fecha[i] != fechaFin:
        fecha.append(fecha[i] + timedelta(days=1))
        if (fecha[i] + timedelta(days=1) == dt.datetime.strptime(
                str(df.iloc[j, 0]), '%Y-%m-%d')):
            ventas.append(df.iloc[j, 1])
            j = j + 1
        else:
            ventas.append(0)
        i = i + 1
    dfx = pd.DataFrame()
    dfx["FECHA"] = fecha
    dfx["VENTAS"] = ventas
    return dfx


# agregar columna semana:
def agregarSemana(df):
    semana = []
    for i in range(len(df)):
        f = df.iloc[i, 0]
        sem = f.strftime('%Y-%W')
        semana.append(sem)
    dfx = pd.DataFrame()
    ventas = df['VENTAS']
    dfx["SEMANA"] = semana
    dfx["VENTAS"] = ventas
    return dfx


# Agrupar ventas de día a semana
def agruparVentasSemanales(df):
    semana = list(set(df['SEMANA']))
    ventas = []
    for i in range(len(semana)):
        ventaSemanal = 0
        for j in range(len(df)):
            if df.iloc[j, 0] == semana[i]:
                ventaSemanal = ventaSemanal + df.iloc[j, 1]
        ventas.append(ventaSemanal)
    df2 = pd.DataFrame()
    df2['SEMANA'] = semana
    df2['VENTAS'] = ventas
    df3 = pd.DataFrame()
    df3 = df2.sort_values('SEMANA')
    return df3


# Funición principal del módulo Preparar Datos.
def prepararDatos(ds):
    ds = agruparVentasDiarias(ds)
    obj_fechas = cambiarFormatoFecha(ds)
    ds['FECHA'] = obj_fechas
    df = pd.DataFrame()
    df['Obj_Fecha'] = obj_fechas
    ventas = listarVentas(ds)
    df['VENTAS'] = ventas
    df2 = pd.DataFrame()
    df2 = df.sort_values('Obj_Fecha')
    dfx = completarVentas(df2)
    dfx2 = agregarSemana(dfx)
    dfx3 = agruparVentasSemanales(dfx2)
    return dfx3


class ZModulos(models.Model):
    _name = 'mrp.modulos'
    _description = 'Z Modulos'

    def obtenerDatosDeDB(self, sucursal, articulo, odoo):
        # # print("self z:", self)
        # try:
        #     odoo = getConection(odoo)
        # except NameError:
        #     odoo = getConection()
        # # # ---- DATOS DE CONEXION ----
        # # url = 'http://200.10.108.182'
        # # db = 'norauto'
        # # # url = 'http://localhost:8069'
        # # # db = 'norauto-3108'
        # # username = 'admin'
        # # password = 'Devoo9060!'
        # # # ---- CONEXION ----
        # # common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
        # # uid = common.authenticate(db, username, password, {})
        # # models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
        # # ---- BUSCAR HISTORICO DE VENTAS ----
        # HistoricoVentas = odoo.env['mrp.historico.venta']
        # res = HistoricoVentas.get_historicos([], sucursal, articulo)
        domain = []
        if sucursal:
            unidad_operativa = self.env['operating.unit'].search(
                [('code', '=', sucursal)], limit=1)
            domain.append(('operating_unit_id', '=', unidad_operativa.id))
        if articulo:
            producto_id = self.env['product.template'].search(
                [('default_code', '=', articulo)])
            domain.append(('product_id', '=', producto_id.id))
            # domain.append(('product_code', '=', producto_id))
        historicos = self.env['mrp.historico.venta'].search(domain, order="fecha asc")
        dicto = {'FECHA': [], 'PRODUCTO': [], 'VENTAS': []}
        for h in historicos:
            dicto['FECHA'].append(h.fecha)
            dicto['PRODUCTO'].append(h.product_id.default_code)
            dicto['VENTAS'].append(h.cantidad)

        # res = models.execute_kw(db, uid, password,
        #     'mrp.historico.venta', 'get_historicos', [[], unidad_operativa_id, product_id],{})
        historico = pd.DataFrame(data=dicto)
        return historico

    # ------------------------------------------------------------------------------
    #
    # 3.- MEJORES PARAMETROS
    """
    Calcula parámetros necesarios para el calculo del modelo: p, d, q, bias. Esos 
    parametros se persisten (csv o db) para ser usados en otros módulos.
    """

    def obtenerDatasetMP(self):
        ds = read_csv(archivo)
        return ds

    def obtenerEstacionalidad(self, ds, art, suc):
        fila = consultarSiExisteFila(ds, art, suc)
        if not fila:
            semanas_estacionalidad = estacionalidad
        else:
            semanas_estacionalidad = ds.iloc[fila, 8]
        return semanas_estacionalidad

    def difference(self, dataset, interval=1):
        diff = list()
        for i in range(interval, len(dataset)):
            valor = dataset[i] - dataset[i - interval]
            diff.append(valor)
        return numpy.array(diff)

    def inverse_difference(self, history, yhat, interval=1):
        return yhat + history[-interval]

    # A partir del mejor orden del arima, se obtiene el bias
    def calcularBias(self, history, predictions):
        residuos = []
        for i in range(len(predictions)):
            dif = len(history) - len(predictions)
            residuo = predictions[i] - history[dif + i]
            res = 0
            if type(residuo) == numpy.ndarray:
                res = residuo[0]
            else:
                res = residuo
            residuos.append(res)
        bias = stats.mean(residuos)
        return bias

    def obtener_bias(self, X, arima_order, estacionalidad):
        X = X.astype('float32')
        train_size = int(len(X) * train_prop)
        train, test = X[0:train_size], X[train_size:]
        history = [x for x in train]
        predictions = list()
        for t in range(len(test)):
            diff = self.difference(history, estacionalidad)
            model = ARIMA(diff, order=arima_order)
            model_fit = model.fit(trend='nc', disp=0)
            yhat = model_fit.forecast()[0]
            yhat = self.inverse_difference(history, yhat, estacionalidad)
            predictions.append(yhat)
            history.append(test[t])
        bias = self.calcularBias(history, predictions)
        return bias

    # Evalua un modelo ARIMA de orden (p,d,q) y devuelve el RMSE
    def evaluate_arima_model(self, X, arima_order, estacionalidad):
        X = X.astype('float32')
        train_size = int(len(X) * train_prop)
        train, test = X[0:train_size], X[train_size:]
        history = [x for x in train]
        predictions = list()
        for t in range(len(test)):
            diff = self.difference(history, estacionalidad)
            model = ARIMA(diff, order=arima_order)
            model_fit = model.fit(trend='nc', disp=0)
            yhat = model_fit.forecast()[0]
            yhat = self.inverse_difference(history, yhat, estacionalidad)
            predictions.append(yhat)
            history.append(test[t])
        mse = mean_squared_error(test, predictions)
        rmse = sqrt(mse)
        return rmse

    # Evalúa combinaciones de p, d y q para modelos ARIMA
    def evaluate_models(self, dataset, p_values, d_values, q_values,
                        estacionalidad):
        dataset = dataset.astype('float32')
        best_score, best_cfg = float("inf"), None
        for p in p_values:
            for d in d_values:
                for q in q_values:
                    order = (p, d, q)
                    try:
                        mse = self.evaluate_arima_model(dataset, order,
                                                        estacionalidad)
                        if mse < best_score:
                            best_score, best_cfg = mse, order
                        print('ARIMA%s RMSE=%.3f' % (order, mse))
                    except:
                        continue
        print('Best ARIMA%s RMSE=%.3f' % (best_cfg, best_score))
        return best_cfg, best_score

    # Función principal del cálculo de los mejores parámetros
    def calcularPars(self, archivo, art, suc, estacionalidad):
        df_db = pd.read_csv(archivo, index_col=False)
        ds = self.obtenerDatosDeDB(suc, art)
        df = prepararDatos(ds)
        df.set_index('SEMANA', inplace=True)
        series = df.T.squeeze()
        split_point = int(len(series) * train_prop)
        datos, validacion = series[0:split_point], series[split_point:]
        inicio = dt.datetime.now()
        fila = consultarSiExisteFila(df_db, art, suc)
        if not fila:
            p = 0
            d = 0
            q = 1
            bias = 1
        else:
            p = df_db.iloc[fila, 3]
            d = df_db.iloc[fila, 4]
            q = df_db.iloc[fila, 5]
            bias = df_db.iloc[fila, 6]
        # evaluar parametros
        # p_values = range(0, 9)
        p_values = range(0, 3)
        d_values = range(0, 3)
        q_values = range(0, 3)
        warnings.filterwarnings("ignore")
        best_order = self.evaluate_models(series.values, p_values, d_values,
                                          q_values, estacionalidad)
        bias = self.obtener_bias(series.values, best_order[0], estacionalidad)
        fin = dt.datetime.now()
        delta = fin - inicio
        p = best_order[0][0]
        d = best_order[0][1]
        q = best_order[0][2]
        rmse = best_order[1]
        demora = delta
        df_db2 = self.actualizarFila(df_db, art, suc, p, d, q, bias, rmse,
                                     estacionalidad, demora)
        return df_db2

    # Persistencia de los mejores parámetros
    def persistir(self, params, archivo, sucursal, articulo, fecha, p, d, q,
                  bias,
                  rmse, estacionalidad, demora):
        tipo = 'csv'
        # tipo=bd
        if tipo == 'csv':
            params.to_csv(archivo, index=False)
        elif tipo == 'db':
            pass
            # existe=select * from Tabla where articulo='art' and sucursal='suc'
            # if(existe==False):
            #   Insertar en Tabla:
            # else:
            #   Update
        return

    def insertarFila(self, ds, unaSuc, unArt, unP, unD, unQ, unBias, unRMSE,
                     unaEstacionalidad, unaDemora, unaFecha):
        # Origen de datos: Listamos todos los archivos para verlos luego uno por uno.
        import pandas as pd
        p = list(ds.iloc[:, 3])
        d = list(ds.iloc[:, 4])
        q = list(ds.iloc[:, 5])
        bias = list(ds.iloc[:, 6])
        rmse = list(ds.iloc[:, 7])
        estacionalidad = list(ds.iloc[:, 8])
        demora = list(ds.iloc[:, 9])
        sucursal = list(ds.iloc[:, 0])
        articulo = list(ds.iloc[:, 1])
        fecha = list(ds.iloc[:, 2])
        sucursal.append(unaSuc)
        articulo.append(unArt)
        p.append(unP)
        d.append(unD)
        q.append(unQ)
        bias.append(unBias)
        rmse.append(unRMSE)
        estacionalidad.append(unaEstacionalidad)
        demora.append(unaDemora)
        fecha.append(unaFecha)
        params = pd.DataFrame()
        params['sucursal'] = sucursal
        params['articulo'] = articulo
        params['fecha'] = fecha
        params['p'] = p
        params['d'] = d
        params['q'] = q
        params['bias'] = bias
        params['rmse'] = rmse
        params['semanas_estacionalidad'] = estacionalidad
        params['demora'] = demora
        self.persistir(params, archivo, sucursal, articulo, fecha, p, d, q,
                       bias,
                       rmse, estacionalidad, demora)
        return params

    def modificarFila(self, ds, fila, unP, unD, unQ, unBias, unRMSE,
                      unaEstacionalidad, unaDemora, unaFecha):
        if ds.iloc[fila, 7] >= unRMSE:
            ds.iloc[fila, 2] = unaFecha
            ds.iloc[fila, 3] = unP
            ds.iloc[fila, 4] = unD
            ds.iloc[fila, 5] = unQ
            ds.iloc[fila, 6] = unBias
            ds.iloc[fila, 7] = unRMSE
            ds.iloc[fila, 8] = unaEstacionalidad
            ds.iloc[fila, 9] = unaDemora
            # ds.iloc[fila,0]=suc
            # ds.iloc[fila,0]=art
        else:
            pass
        suc = ds.iloc[fila, 0]
        art = ds.iloc[fila, 1]
        self.persistir(ds, archivo, suc, art, unaFecha, unP, unD, unQ, unBias,
                       unRMSE, unaEstacionalidad, unaDemora)
        return ds

    def actualizarFila(self, ds, unArt, unaSuc, unP, unD, unQ, unBias, unRMSE,
                       unaEstacionalidad, unaDemora):
        import datetime as dt
        hoy = dt.date.today()
        unaFecha = dt.datetime.strftime(hoy, '%d/%m/%Y')
        fila = consultarSiExisteFila(ds, unArt, unaSuc)
        print('Fila: ' + str(fila))
        if (fila == False):
            ds2 = self.insertarFila(ds, unaSuc, unArt, unP, unD, unQ, unBias,
                                    unRMSE, unaEstacionalidad, unaDemora,
                                    unaFecha)
        else:
            ds2 = self.modificarFila(ds, fila, unP, unD, unQ, unBias, unRMSE,
                                     unaEstacionalidad, unaDemora, unaFecha)
        return ds2


# ------------------------------------------------------------------------------
#
# PEDIR VALORES JARCODEADOS
def devolverEstacionalidad():
    return estacionalidad


def devolverAlcance():
    return alcance


def devolverMeses12():
    return meses12
# ------------------------------------------------------------------------------
