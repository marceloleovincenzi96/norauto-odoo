#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 15:41:31 2020

Este es el módulo de predicción.
Dado un artículo, y una sucursal, el módulo obtiene una predicción semanal de 
las unidades que se preveen vender en el período próximo (vector de 
longitud=alcance_predicción).
Tiene la posibilidad de presentar los gráficos que comparan las predicciones
con el valor estacional histórivo (descomentar linea 86: graficar).
Nota: Obtener una predicción tarda entre 2 y 5 segundos en promedio en mi PC.

@author: jorgek
"""

# Librerías
from odoo import api, fields, models
import numpy
from matplotlib import pyplot
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.arima_model import ARIMAResults
from .z_modulos import ZModulos as z
from . import z_modulos as z_out
from sklearn.linear_model import LinearRegression
import pickle

PATH_SERVER = '/var/lib/odoo/.local/share/Odoo/addons/13.0/addons-norauto' \
              '/nybble_norauto/norauto_mrp_calc/modelos/ '
PATH_LOCAL = 'norauto_mrp_calc/modelos/'


class BPredecir(models.AbstractModel):
    _name = 'mrp.predecir'
    _description = 'Predecir'

    # FUNCIONES:
    # Series diferenciadas
    def difference(self, dataset, interval=1):
        diff = list()
        for i in range(interval, len(dataset)):
            value = dataset[i] - dataset[i - interval]
            diff.append(value)
        return diff

    # Invertir valores diferenciados
    def inverse_difference(self, history, yhat, interval=1):
        return yhat + history[-interval]

    # Función principal de predicción
    def predecirArima(self, articulo, sucursal):
        nombre = str(sucursal) + "_" + str(articulo)
        df_mp = self.env['mrp.modulos'].obtenerDatasetMP()
        ds = self.env['mrp.modulos'].obtenerDatosDeDB(sucursal, articulo, odoo=False)
        df = z_out.prepararDatos(ds)
        df.set_index('SEMANA', inplace=True)
        series = df.T.squeeze()
        fila = z_out.consultarSiExisteFila(df_mp, articulo, sucursal)
        if not fila:
            p = 0
            d = 0
            q = 1
            bias = 1
        else:
            p = df_mp.iloc[fila, 3]
            d = df_mp.iloc[fila, 4]
            q = df_mp.iloc[fila, 5]
            bias = df_mp.iloc[fila, 6]
        estacionalidad = self.env['mrp.modulos'].obtenerEstacionalidad(df_mp, articulo, sucursal)
        parametros = (p, d, q)
        alcance = z_out.devolverAlcance()
        split_point = len(series) - estacionalidad
        datos, validacion = series[0:], series[
                                        split_point:split_point + alcance]
        X = datos.values.astype('float32')
        history = [x for x in X]
        y = validacion.values.astype('float32')
        # Cargar modelo y bias desde PKLs
        model_fit = ARIMAResults.load(PATH_SERVER + str(nombre) + '_model.pkl')
        bias = numpy.load(PATH_SERVER + str(nombre) + '_bias.npy')
        predicciones = list()
        yhat = float(model_fit.forecast()[0])
        yhat = int(
            bias + self.inverse_difference(history, yhat, estacionalidad))
        predicciones.append(yhat)
        history.append(y[0])
        # Predicciones
        for i in range(1, alcance):
            diff = self.difference(history, estacionalidad)
            model = ARIMA(diff, order=parametros)
            model_fit = model.fit(trend='nc', disp=0)
            yhat = model_fit.forecast()[0]
            yhat = int(
                bias + self.inverse_difference(history, yhat, estacionalidad))
            predicciones.append(yhat)
            obs = y[i]
            history.append(obs)

        def graficar(articulo, sucursal, nombre):
            pyplot.plot(y, label='Valor Estacional')
            pyplot.xlabel('semanas')
            pyplot.ylabel('Unidades previstas')
            pyplot.title(
                'Suc.: ' + str(sucursal) + ' - Art.: ' + str(articulo))
            pyplot.plot(predicciones, color='red', label='Valor Predicho')
            pyplot.legend(loc='upper right')
            pyplot.savefig('graficos/' + str(nombre) + '_graph.png')

        # graficar(articulo,sucursal,nombre)
        return predicciones

    def predecirRegresion(self, articulo, sucursal):
        nombre = str(sucursal) + "_" + str(articulo)
        alcance = z_out.devolverAlcance()
        meses12 = z_out.devolverMeses12()
        modelo = pickle.load(
            open(PATH_SERVER + str(nombre) + '_model.pkl', 'rb'))
        y_pred = numpy.array([])
        x = numpy.array([])
        for i in range(meses12, meses12 + alcance):
            x = numpy.append(x, i)
        for i in range(alcance):
            y_pred = numpy.append(y_pred,
                                  int(modelo.predict(x[i].reshape(-1, 1))))
        return y_pred.tolist()

    def predecir(self, articulo, sucursal):
        pred = self.env['mrp.predecir']
        nombre = str(sucursal) + "_" + str(articulo)
        prediccion = []
        try:
            bias = str(numpy.load(PATH_SERVER + str(nombre) + '_bias.npy'))
            if bias == 'x':
                prediccion = pred.predecirRegresion(articulo, sucursal)
            else:
                prediccion = pred.predecirArima(articulo, sucursal)
        except FileNotFoundError:
            print('Modelo Inexistente para este artículo en esta sucursal')
        # si hay algún valor nulo, lo trunco a cero
        for i in range(len(prediccion)):
            if prediccion[i] < 0:
                prediccion[i] = 0
        return prediccion
