#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 15:41:31 2020

@author: jorgek
"""

# Librerías necesarias
from odoo import api, fields, models
import numpy
from statsmodels.tsa.arima_model import ARIMA
# import z_modulos as z
from .z_modulos import ZModulos as z
from . import z_modulos as z_out
from sklearn.linear_model import LinearRegression
import pickle

PATH_SERVER = '/var/lib/odoo/.local/share/Odoo/addons/13.0/addons-norauto' \
              '/nybble_norauto/norauto_mrp_calc/modelos/ '
PATH_LOCAL = 'norauto_mrp_calc/modelos/'


class AGenerarModeloPKL(models.AbstractModel):
    _name = 'mrp.generar.modelo'
    _description = 'Generar Modelo PKL'

    # Funciones
    def generarModeloArima(self, sucursal, articulo):
        nombre = str(sucursal) + "_" + str(articulo)
        ds = self.env['mrp.modulos'].obtenerDatosDeDB(sucursal, articulo, odoo=False)
        df = z_out.prepararDatos(ds)
        df.set_index('SEMANA', inplace=True)
        series = df.T.squeeze()
        df_mp = self.env['mrp.modulos'].obtenerDatasetMP()
        fila= z_out.consultarSiExisteFila(df_mp, articulo, sucursal)
        if not fila:
            p = 0
            d = 0
            q = 1
            bias = 1
        else:
            p = df_mp.iloc[fila, 3]
            d = df_mp.iloc[fila, 4]
            q = df_mp.iloc[fila, 5]
            bias = df_mp.iloc[fila, 6]
        estacionalidad = self.env['mrp.modulos'].obtenerEstacionalidad(df_mp, articulo, sucursal)
        parametros = (p, d, q)
        split_point = len(series)
        datos = series[0:split_point]

        def __getnewargs__(self):
            return ((self.endog), (self.k_lags, self.k_diff, self.k_ma))

        ARIMA.__getnewargs__ = __getnewargs__

        # create a differenced series
        def difference(dataset, interval=1):
            diff = list()
            for i in range(interval, len(dataset)):
                value = dataset[i] - dataset[i - interval]
                diff.append(value)
            return diff

        X = datos.values
        X = X.astype('float32')
        # difference data
        diff = difference(X, estacionalidad)
        # fit model
        model = ARIMA(diff, order=parametros)
        model_fit = model.fit(trend='nc', disp=0)
        model_fit.save(PATH_SERVER + str(nombre) + '_model.pkl')
        numpy.save(PATH_SERVER + str(nombre) + '_bias', [bias])
        return ()

    def generarModeloLineal(self, sucursal, articulo):
        nombre = str(sucursal) + "_" + str(articulo)
        ds = self.env['mrp.modulos'].obtenerDatosDeDB(sucursal, articulo, odoo=False)
        df = z_out.prepararDatos(ds)
        meses12 = 52
        if len(df) < 52:
            meses12 = len(df)
        x = numpy.array([])
        y = numpy.array([])
        j = 0
        for i in range(len(df) - meses12, len(df)):
            y = numpy.append(y, df.iloc[i, 1])
            x = numpy.append(x, j)
            j = j + 1
        # Modelo d Regresion Lineal
        modelo_regresion_lineal = LinearRegression()  # instancia de LinearRegression
        modelo_regresion_lineal.fit(x.reshape(-1, 1), y.reshape(-1, 1))
        # Archivos modelo:
        nombreModelo = PATH_SERVER + str(nombre) + '_model.pkl'
        nombreBias = PATH_SERVER + str(nombre) + '_bias'
        pickle.dump(modelo_regresion_lineal, open(nombreModelo, 'wb'))
        bias = 'x'
        numpy.save(nombreBias, bias)
        # Graficos:
        #    Y_pred=numpy.array([])
        #    for i in range(len(x)):
        #        Y_pred=numpy.append(Y_pred,modelo_regresion_lineal.predict(x[i].reshape(-1,1)))
        #    plt.scatter(x,y,label='datos', color='blue')
        #    plt.xlabel('semanas')
        #    plt.ylabel('Unidades previstas')
        #    plt.title('Suc.: '+str(sucursal)+' - Art.: '+str(articulo))
        #    plt.legend(loc='upper right')
        #    plt.plot(x,Y_pred, color='red', label='Valor Predicho')
        #    plt.legend(loc='upper right')
        return ()

    def generarModeloVacio(self, sucursal, articulo):
        nombre = str(sucursal) + "_" + str(articulo)
        x = numpy.array([])
        y = numpy.array([])
        meses12 = 52
        j = 0
        for i in range(0, meses12):
            y = numpy.append(y, 0)
            x = numpy.append(x, j)
            j = j + 1
        # Modelo d Regresion Lineal
        modelo_regresion_lineal = LinearRegression()  # instancia de LinearRegression
        modelo_regresion_lineal.fit(x.reshape(-1, 1), y.reshape(-1, 1))
        # Archivos modelo:
        nombreModelo = PATH_SERVER + str(nombre) + '_model.pkl'
        nombreBias = PATH_SERVER + str(nombre) + '_bias'
        pickle.dump(modelo_regresion_lineal, open(nombreModelo, 'wb'))
        bias = 'x'
        numpy.save(nombreBias, bias)
        return ()

    def generarModelo(self, articulo, sucursal):
        modelo = self.env['mrp.generar.modelo']
        ds = self.env['mrp.modulos'].obtenerDatosDeDB(sucursal, articulo, odoo=False)
        if len(ds) <= 1:
            modelo.generarModeloVacio(sucursal, articulo)
        else:
            df = z_out.prepararDatos(ds)
            meses12 = z_out.devolverMeses12()
            umbral = int(52 / 3)
            ventas52 = []
            if len(df) < 52:
                meses12 = len(df)
            for i in range(len(df) - meses12, len(df)):
                if df.iloc[i, 1] == 0:
                    pass
                else:
                    ventas52.append(df.iloc[i, 1])
            if len(ventas52) < umbral:
                modelo.generarModeloLineal(sucursal, articulo)
            else:
                import warnings
                with warnings.catch_warnings(record=True) as w:
                    modelo.generarModeloArima(sucursal, articulo)
                if len(w) == 0:
                    modelo.generarModeloArima(sucursal, articulo)
                else:
                    modelo.generarModeloLineal(sucursal, articulo)
        return
