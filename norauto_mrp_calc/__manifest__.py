
# -*- encoding: utf-8 -*-

{
    'name': 'Norauto MRP Calculation',
    'version': '13.0.1.0',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'Norauto MRP Calculation',
    'depends': ['base'],
    'author': 'NybbleGroup',
    'description': """
NorAuto MRP Calculation
===============
Aggrega funcion para llamar por xmlrpc u odoorpc para obtener predicciones de venta
""",
    'data': [],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
