
from odoo import api, fields, models
from dateutil.relativedelta import relativedelta


class ResPartner(models.Model):
    _inherit = "res.partner"

    @api.model
    def create(self, vals):
        res = super(ResPartner, self).create(vals)
        # res.actualizar_alicuotas()
        return res

    def write(self, vals):
        res = super(ResPartner, self).write(vals)
        # self.actualizar_alicuotas()
        return res

    def actualizar_alicuotas(self):
        for rec in self:
            if rec.vat:
                print('entro a actualizar:', rec.vat)
                # rec.arba_alicuot_ids.filtered(lambda alicuot: alicuot.company_id == self.env.user.company_id).unlink()
                rec.arba_alicuot_ids.unlink()
                impuesto_caba = self.env.ref('perc_ret.imp_percepcion_iibb_caba_aplicada')
                impuesto_arba = self.env.ref('perc_ret.imp_percepcion_iibb_arba_aplicada')
                date = fields.Date.context_today(self)
                # ARBA
                alicuota = impuesto_arba.get_partner_alicuota_percepcion_pr(rec, date, 'arba')
                # CABA
                alicuota = impuesto_caba.get_partner_alicuota_percepcion_pr(rec, date, 'caba')


                # alicuotas_padron = self.env['padron'].search([('cuit', '=', rec.vat)])
                # for alicuota in alicuotas_padron:
                #     vals = {}
                #     # vals['company_id'] = rec.company_id.id
                #     vals['from_date'] = alicuota.fecha_vig_desde
                #     vals['to_date'] = alicuota.fecha_vig_hasta
                #     if alicuota.tipo == 'ARBA':
                #         vals['tag_id'] = self.env.ref('l10n_ar_ux.tag_tax_jurisdiccion_902').id
                #     elif alicuota.tipo == 'CABA':
                #         vals['tag_id'] = self.env.ref('l10n_ar_ux.tag_tax_jurisdiccion_901').id
                #     vals['alicuota_percepcion'] = alicuota.percepcion
                #     vals['alicuota_retencion'] = alicuota.retencion
                #     vals['withholding_amount_type'] = 'untaxed_amount'
                #     vals['partner_id'] = rec.id
                #     vals['codigo_padron'] = alicuota.id
                #     alic = self.env['res.partner.arba_alicuot'].create(vals)
                #     alicuota.sudo().write({'id_alicuota': alic.id})


    def get_arba_data(self, company, date):
        self.ensure_one()
        from_date = (date + relativedelta(day=1)).strftime('%Y%m%d')
        to_date = (date + relativedelta(
            day=1, days=-1, months=+1)).strftime('%Y%m%d')
        commercial_partner = self.commercial_partner_id
        arba_tag_id = self.env.ref('l10n_ar_account.tag_tax_jurisdiccion_902').id
        arba = self.arba_alicuot_ids.search([
            ('from_date', '=', from_date),
            ('to_date', '=', to_date),
            ('company_id', '=', company.id),
            ('tag_id', '=', arba_tag_id),
            ('partner_id', '=', commercial_partner.id)], limit=1)
        if arba.alicuota_retencion == 0:
            arba_data = company.get_arba_data(
                commercial_partner,
                from_date, to_date,
            )
            arba.write({'alicuota_retencion': arba_data['alicuota_retencion']})
        if not arba:
            arba_data = company.get_arba_data(
                commercial_partner,
                from_date, to_date,
            )
            arba_data['tag_id'] = arba_tag_id
            arba_data['partner_id'] = commercial_partner.id
            arba_data['company_id'] = company.id
            arba = self.arba_alicuot_ids.sudo().create(arba_data)
        return arba

    def get_caba_alicuota_retencion(self, date):
        partner_padron = self.env['padron'].search([
            ('cuit', '=', self.main_id_number),
            ('tipo', '=', 'CABA'),
            ('fecha_vig_desde', '<=', date),
            ('fecha_vig_hasta', '>=', date),
        ], limit=1)
        if partner_padron:
            if partner_padron.retencion != 0:
                return partner_padron.retencion / 100
        return 0
