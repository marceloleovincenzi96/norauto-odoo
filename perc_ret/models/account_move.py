# -*- coding: utf-8 -*-

from odoo import api, models, fields

class AccountMove(models.Model):
    _name = 'account.move'
    _inherit = 'account.move'

    @api.model_create_multi
    def create(self, vals_list):
        # NOTE: Si el partner tiene una alicuota, le agregamos el impuesto de caba o arba a la linea
        for vals in vals_list:
            if vals.get('partner_id') and vals.get('invoice_line_ids', False):
                partner = self.env['res.partner'].browse(vals.get('partner_id'))
                # TODO: hacer un if de si el partner es responsable inscripto, asi  no corre lo de abajo si no es responsable
                impuesto_caba = self.env.ref('perc_ret.imp_percepcion_iibb_caba_aplicada')
                impuesto_arba = self.env.ref('perc_ret.imp_percepcion_iibb_arba_aplicada')
                date_invoice = vals.get('invoice_date') or fields.Date.context_today(self)
                alicuota = impuesto_arba.get_partner_alicuota_percepcion_pr(partner, date_invoice, 'arba')
                # ARBA
                if alicuota > 0:
                    # actualizar la lista de impuestos en la factura
                    for line in vals.get('invoice_line_ids'):
                        line[2]['tax_ids'][0][2].append(impuesto_arba.id)
                # CABA
                alicuota = impuesto_caba.get_partner_alicuota_percepcion_pr(partner, date_invoice, 'caba')
                if alicuota > 0:
                    # actualizar la lista de impuestos en la factura
                    for line in vals.get('invoice_line_ids'):
                        line[2]['tax_ids'][0][2].append(impuesto_caba.id)

        res = super(AccountMove, self).create(vals_list)
        return res

    def write(self, vals):
        # TODO: En la modificaciones de la factura (invoice_line_ids, partner)
        # NOTE: Si el partner tiene una alicuota, le agregamos el impuesto de caba o arba a la linea
        # for move in self:
        #     if 'partner_id' in vals and 'invoice_line_ids' in vals:
        #         # hace un write primero con los vals como lo hace el orm
        #         impuesto_caba = self.env.ref('perc_ret.imp_percepcion_iibb_caba_aplicada')
        #         partner_alicuota_caba = self.get_partner_alicuota_caba(vals.get('partner_id'))
        #         if partner_alicuota_caba > 0:
        #             vals['invoice_line_ids'][0][2]['tax_ids'][0][2].append(impuesto_caba.id)
        #             print(vals['invoice_line_ids'])
        #         # lineas = move.actualizar_lineas_existentes(vals.get('partner_id')) or False
        #         # if lineas:
        #         #     vals['invoice_line_ids'] = lineas
        #     # if 'invoice_line_ids' in vals and self._context.get('no_actualizar_tax'):
        #         pass
        #         # vals['invoice_line_ids'] = move.agregar_tax_a_lineas_nuevas(vals['invoice_line_ids'])
        # print('segundo vals', vals)
        res = super(AccountMove, self).write(vals)
        return res

    def get_partner_alicuota_caba(self, partner_id):
        partner = self.env['res.partner'].browse(partner_id)
        impuesto_caba = self.env.ref('perc_ret.imp_percepcion_iibb_caba_aplicada')
        date_invoice = self.invoice_date or fields.Date.context_today(self)
        alicuota = impuesto_caba.get_partner_alicuota_percepcion_pr(partner, date_invoice, 'caba')
        return alicuota

    def actualizar_lineas_existentes(self, partner_id):
        self.ensure_one()
        partner = self.env['res.partner'].browse(partner_id)
        impuesto_caba = self.env.ref('perc_ret.imp_percepcion_iibb_caba_aplicada')
        date_invoice = self.invoice_date or fields.Date.context_today(self)
        alicuota = impuesto_caba.get_partner_alicuota_percepcion_pr(partner, date_invoice, 'caba')
        lineas = []
        if alicuota > 0:
            for line in self.invoice_line_ids:
                lineas.append((1, line.id, {'recompute_tax_line': True, 'tax_ids': [(4, impuesto_caba.id)]}))
            # self.with_context({'no_actualizar_tax': True}).write({'invoice_line_ids': lineas})
        # impuesto_arba = self.env.ref('perc_ret.imp_percepcion_iibb_arba_aplicada')
        # alicuota = impuesto_arba.get_partner_alicuota_percepcion_pr(partner_factura, date_invoice, 'arba')
        return lineas

    def get_taxes_values(self):
        res = super(AccountMove, self).get_taxes_values()
        print('-----------------------------------------')
        print('res antes:', res)
        # import pdb; pdb.set_trace()
        # if (self.type in ['out_invoice', 'out_refund'] and self.journal_id.type == 'sale' and self.partner_id):
        #     print('ENTRO A if')
        #     partner_factura = self.partner_id.commercial_partner_id
        #     # partner_doc = self.partner_id.commercial_partner_id.vat
        #     # ###Busco las alícuotas
        #     impuesto_caba = self.env.ref('perc_ret.imp_percepcion_iibb_caba_aplicada')
        #     impuesto_arba = self.env.ref('perc_ret.imp_percepcion_iibb_arba_aplicada')
        #     date_invoice = self.date_invoice or fields.Date.context_today(self)
        #     # --------------------------------------------------
        #     alicuota = impuesto_caba.get_partner_alicuota_percepcion_pr(partner_factura, date_invoice)
        #     if alicuota != 0.0:
        #         clave_dic = str(impuesto_caba.id) + '-' + str(self.type in ('out_invoice', 'in_invoice') and (impuesto_caba.account_id.id or False) or (impuesto_caba.refund_account_id.id or False)) + "-False"
        #         res[clave_dic] = {
        #             'move_id': self.id,
        #             'name': impuesto_caba.name + " " + str((alicuota * 100)) + '%',
        #             'tax_id': [(0, impuesto_caba.id)],
        #             'base': self.amount_untaxed,
        #             'amount': self.amount_untaxed * (alicuota),
        #             'manual': False,
        #             'sequence': 99,
        #             'account_id': self.type in ('out_invoice', 'in_invoice') and (
        #                 impuesto_caba.account_id.id or False) or (
        #                 impuesto_caba.refund_account_id.id or False),
        #         }
        #     alicuota = impuesto_arba.get_partner_alicuota_percepcion_pr(partner_factura, date_invoice)
        #     if alicuota != 0.0:
        #         clave_dic = str(impuesto_arba.id) + '-' + str(self.type in ('out_invoice', 'in_invoice') and (impuesto_arba.account_id.id or False) or (impuesto_arba.refund_account_id.id or False)) + "-False"
        #         res[clave_dic] = {
        #             'move_id': self.id,
        #             'name': impuesto_arba.name + " " + str((alicuota * 100)) + '%',
        #             'tax_ids': [(4, impuesto_arba.id)],
        #             'base': self.amount_untaxed,
        #             'amount': self.amount_untaxed * (alicuota),
        #             'manual': False,
        #             'sequence': 99,
        #             'account_id': self.type in ('out_invoice', 'in_invoice') and (
        #                 impuesto_arba.account_id.id or False) or (
        #                 impuesto_arba.refund_account_id.id or False),
        #         }
        return res
