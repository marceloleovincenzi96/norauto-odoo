from odoo import models, fields
from dateutil.relativedelta import relativedelta


class AccountTax(models.Model):
    _inherit = "account.tax"

    def get_partner_alicuota_percepcion_pr(self, partner, date, tipo):
        print('partner alic:', partner, date)
        # TODO: hacer un if de si el partner es responsable inscripto, asi  no corre lo de abajo si no es responsable
        if partner and date:
            alicuota = self.get_partner_alicuota(partner, date, tipo)
            if alicuota:
                return alicuota.alicuota_percepcion / 100.0
        return 0.0

    # Hago que no llame a este metodo para conectarse
    def get_partner_alicuota(self, partner, date, tipo):
        print('get partner alic:', partner, date, tipo)

        self.ensure_one()
        commercial_partner = partner.commercial_partner_id
        company = self.company_id
        tags_in_tax = self.invoice_repartition_line_ids.mapped('tag_ids')

        alicuot = partner.arba_alicuot_ids.search([
            ('tag_id', 'in', tags_in_tax.ids),
            ('company_id', '=', company.id),
            ('partner_id', '=', commercial_partner.id),
            '|',
            ('from_date', '=', False),
            ('from_date', '<=', date),
            '|',
            ('to_date', '=', False),
            ('to_date', '>=', date),
        ], limit=1)
        print('alicuot:', alicuot)
        # return alicuot
        # solo buscamos en padron para estas responsabilidades
        # Esto tambien es para CABA asi que si no esta en las alicuotas de partner
        # le devuelve quizas la cuota de CABA, merda
        # TODO: hay que desdoblar la funcion para los casos en que sea caba o arba, y que termine aca o no
        # import pdb; pdb.set_trace()
        if not alicuot and \
                commercial_partner.l10n_ar_afip_responsibility_type_id.code in \
                ['1', '1FM', '2', '3', '4', '6', '11', '13']:
            date_date = fields.Date.from_string(date)
            from_date = (date_date + relativedelta(day=1))
            to_date = (date_date + relativedelta(
                day=1, days=-1, months=+1))

            # NOTE: Llama al webservice de arba
            arba_tag = self.env.ref('l10n_ar_ux.tag_tax_jurisdiccion_902')
            if arba_tag and arba_tag.id in tags_in_tax.ids:
                arba_data = company.get_arba_data(
                    commercial_partner,
                    from_date, to_date,
                )

                # si no hay numero de comprobante entonces es porque no
                # figura en el padron, aplicamos alicuota no inscripto
                if not arba_data['numero_comprobante']:
                    arba_data['numero_comprobante'] = \
                        'Alícuota no inscripto'
                    arba_data['alicuota_retencion'] = \
                        company.arba_alicuota_no_sincripto_retencion
                    arba_data['alicuota_percepcion'] = \
                        company.arba_alicuota_no_sincripto_percepcion
                    # arba_data['alicuota_retencion'] = 0.00
                    # arba_data['alicuota_percepcion'] = 0.00

                arba_data['partner_id'] = commercial_partner.id
                arba_data['company_id'] = company.id
                arba_data['tag_id'] = arba_tag.id
                print('Creacion de la nueva alicuota')
                alicuot = partner.arba_alicuot_ids.sudo().create(arba_data)

            # NOTE: Llama al webservice de arba
            agip_tag = self.env.ref('l10n_ar_ux.tag_tax_jurisdiccion_901')
            if agip_tag and agip_tag.id in tags_in_tax.ids:
                agip_data = company.get_agip_data(commercial_partner, date)
                if not agip_data:
                    arba_data['alicuota_retencion'] = 0.00
                    arba_data['alicuota_percepcion'] = 0.00
                agip_data['partner_id'] = commercial_partner.id
                agip_data['company_id'] = company.id
                agip_data['tag_id'] = agip_tag.id
                alicuot = partner.arba_alicuot_ids.sudo().create(agip_data)

        print('salgo con alicuot:_', alicuot)
        return alicuot
