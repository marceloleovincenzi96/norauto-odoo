
from odoo import models
import xmlrpc.client


class ResCompany(models.Model):
    _inherit = "res.company"
    # TODO: hacer una configuracion en la empresa para hacer la comunicacion con el web service
    # por separado

    def get_agip_data(self, partner, date):
        """Heredo de la funcion de adhoc ya que ellos no la usan salvo para advertir que hay que
        pagar o algo asi"""
        url = 'http://68.183.111.123:8069'
        db = 'cabaws'
        username = 'admin'
        password = 'Devoo9060NY'
        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
        uid = common.authenticate(db, username, password, {})
        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
        res = models.execute_kw(
            db, uid, password,
            'padron', 'get_caba_data', [[], partner.vat, date]
        )
        return res

    def get_arba_data(self, partner, from_date, to_date=False):
        url = 'http://68.183.111.123:8069'
        db = 'cabaws'
        username = 'admin'
        password = 'Devoo9060NY'
        """Heredo de la funcion de adhoc para obtener los datos de nuestro ws"""
        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
        uid = common.authenticate(db, username, password, {})

        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))

        res = models.execute_kw(
            db, uid, password,
            'padron.arba', 'get_arba_data', [[], partner.vat, from_date]
        )
        return res
