# -*- coding: utf-8 -*-

from odoo import models, fields
from datetime import datetime
from odoo.exceptions import UserError
import os


class ResPartnerArbaAlicuot(models.Model):
    _inherit = 'res.partner.arba_alicuot'

    codigo_padron = fields.Integer('ID Padron')


class Padron(models.Model):
    _name = 'padron'

    tipo = fields.Selection([('ARBA', 'ARBA'), ('CABA', 'CABA')], 'Tipo')
    fecha_publicacion = fields.Date(u'Fecha Publicación')
    fecha_vig_desde = fields.Date(u'Fecha Vig. Desde')
    fecha_vig_hasta = fields.Date(u'Fecha Vig. Hasta')
    cuit = fields.Char(u'CUIT')
    tipo_contr_insc = fields.Char(u'Tipo Contr. Insc.')
    marca_alta_sujeto = fields.Selection([('S', 'S'), ('N', 'N'), ('B', 'B')], 'Marca Alta Sujeto')
    marca_alicuota = fields.Selection([('S', 'S'), ('N', 'N'), ('B', 'B')], u'Marca Alícuota')
    percepcion = fields.Float('Percepción')
    retencion = fields.Float('Retención')
    num_grupo_per = fields.Char(u'Nro. Grupo Percepción')
    num_grupo_ret = fields.Char(u'Nro. Grupo Retención')
    razon_social = fields.Char(u'Razón Social')
    id_alicuota = fields.Many2one('res.partner.arba_alicuot', 'Alicuota asociada')


class WizardAjustes(models.TransientModel):
    _name = 'wzd_padron_delete'
    _description = 'Eliminar Padrones antiguos'

    def get_padrones(self):
        self.env.cr.execute("select tipo||' del '||to_char(fecha_vig_desde,'DD/MM/YYYY')||' al '||to_char(fecha_vig_hasta,'DD/MM/YYYY') from padron group by tipo,fecha_vig_desde,fecha_vig_hasta")
        res = self.env.cr.fetchall()
        items = []
        for item in res:
            items.append((item[0], item[0]))
        print('items_:', items)
        return items

    padrones = fields.Selection(get_padrones, string='Tipo')

    def eliminar(self):
        pos_padron_ini = 0
        pos_padron_fin = self.padrones.find(' del ')
        pos_fec1_ini = pos_padron_fin + 5
        pos_fec1_fin = pos_padron_fin + 15
        pos_fec2_ini = self.padrones.find(' al ') + 4
        pos_fec2_fin = pos_fec2_ini + 14
        tipo = self.padrones[pos_padron_ini:pos_padron_fin]
        fecha_desde = self.padrones[pos_fec1_ini:pos_fec1_fin]
        fecha_hasta = self.padrones[pos_fec2_ini:pos_fec2_fin]
        self.env.cr.execute("delete from padron where tipo='" + tipo + "' and fecha_vig_desde='" + fecha_desde + "' and fecha_vig_hasta='" + fecha_hasta + "'")
        return {'type': 'ir.actions.client', 'tag': 'reload', }


class WizardAjustesPadron(models.TransientModel):
    _name = 'wzd_padron'
    _description = 'Ajustes Padron'

    path_padron_arba = fields.Char(
        string=u'Nombre Padrón ARBA',
        default=lambda self: self.env['ir.config_parameter'].search([('key', '=', 'path_padron_arba')])[0].value if self.env['ir.config_parameter'].search([('key', '=', 'path_padron_arba')]) else False)
    path_padron_caba = fields.Char(
        string=u'Nombre Padrón CABA',
        default=lambda self: self.env['ir.config_parameter'].search([('key', '=', 'path_padron_caba')])[0].value if self.env['ir.config_parameter'].search([('key', '=', 'path_padron_caba')]) else False)


    def guardar(self):
        if self.path_padron_arba:
            parametro = self.env['ir.config_parameter'].search([('key', '=', 'path_padron_arba')])
            if not parametro:
                self.env['ir.config_parameter'].create({'key': 'path_padron_arba', 'value': self.path_padron_arba})
            else:
                parametro.write({'value': self.path_padron_arba})
        if self.path_padron_caba:
            parametro = self.env['ir.config_parameter'].search([('key', '=', 'path_padron_caba')])
            if not parametro:
                self.env['ir.config_parameter'].create({'key': 'path_padron_caba', 'value': self.path_padron_caba})
            else:
                parametro.write({'value': self.path_padron_caba})

    def importar_arba(self):
        self.guardar()
        with open('/home/admin/padrones/' + self.path_padron_arba, 'r') as archivo:
            primera_linea = archivo.readline()
            archivo.close()
        consulta = "select coalesce(max(id),0) from padron where tipo='ARBA' and fecha_vig_desde=TO_DATE('" + primera_linea[11:19] + "', 'DDMMYYYY') and fecha_vig_hasta=TO_DATE('" + primera_linea[20:28] + "', 'DDMMYYYY')"
        self.env.cr.execute(consulta)
        maximo = self.env.cr.fetchone()[0]
        if int(maximo) > 0:
            raise UserError('Ya se encuentra un padrón ARBA cargado para el periodo ' + datetime.strptime(primera_linea[11:19], '%d%m%Y').strftime('%d/%m/%Y') + ' - ' + datetime.strptime(primera_linea[20:28], '%d%m%Y').strftime('%d/%m/%Y'))
        self.env['ir.cron'].create({
            'name': 'Importar Padron ARBA back',
            'user_id': self._uid,
            'model_id': self.env['ir.model'].search([('model', '=', 'wzd_padron')])[0].id,
            'state': 'code',
            'code': 'model.importar_arba_back()'
        })

    def importar_caba(self):
        self.guardar()
        with open('/home/admin/padrones/' + self.path_padron_caba, 'r') as archivo:
            primera_linea = archivo.readline()
            archivo.close()
        consulta = "select coalesce(max(id),0) from padron where tipo='CABA' and fecha_vig_desde=TO_DATE('" + primera_linea[9:17] + "', 'DDMMYYYY') and fecha_vig_hasta=TO_DATE('" + primera_linea[18:26] + "', 'DDMMYYYY')"
        self.env.cr.execute(consulta)
        maximo = self.env.cr.fetchone()[0]
        if int(maximo) > 0:
            raise UserError('Ya se encuentra un padrón CABA cargado para el periodo ' + datetime.strptime(primera_linea[9:17], '%d%m%Y').strftime('%d/%m/%Y') + ' - ' + datetime.strptime(primera_linea[18:26], '%d%m%Y').strftime('%d/%m/%Y'))
        self.env['ir.cron'].create({
            'name': 'Importar Padron CABA back',
            'user_id': self._uid,
            'model_id': self.env['ir.model'].search([('model', '=', 'wzd_padron')])[0].id,
            'state': 'code',
            'code': 'model.importar_caba_back()'
        })

    def procesar_archivo(self, tipo, ubicacion, fecha_desde, fecha_hasta):
        self.env.cr.execute('DROP TABLE IF EXISTS padron_aux')
        if tipo == 'ARBA':
            tipo_padron = self.env.ref('l10n_ar_account.tag_tax_jurisdiccion_902')
            self.env.cr.execute('CREATE TABLE padron_aux(tipo varchar,fecha_publicacion varchar,fecha_vig_desde varchar,fecha_vig_hasta varchar,cuit varchar,tipo_contr_insc varchar,marca_alta_sujeto varchar,marca_alicuota varchar,percepcion varchar,num_grupo_per varchar,razon_social varchar)')
            # Vuelco datos de archivo en tabla auxiliar
            self.env.cr.execute("COPY padron_aux(tipo,fecha_publicacion,fecha_vig_desde,fecha_vig_hasta,cuit,tipo_contr_insc,marca_alta_sujeto,marca_alicuota, percepcion, num_grupo_per, razon_social) FROM '" + ubicacion + "' DELIMITER '|'")
            self.env.cr.execute("select coalesce(max(id),0) from padron")
            maximo = self.env.cr.fetchone()[0]
            # Convierto datos de tabla auxiliar y los paso al objeto padrón
            self.env.cr.execute("insert into padron(tipo,fecha_publicacion,fecha_vig_desde, fecha_vig_hasta, cuit, tipo_contr_insc, marca_alta_sujeto, marca_alicuota, percepcion, num_grupo_per,razon_social) select 'ARBA',to_date(right(fecha_publicacion,8),'DDMMYYYY'),to_date(right(fecha_vig_desde,8),'DDMMYYYY'),to_date(right(fecha_vig_hasta,8),'DDMMYYYY'),cuit,tipo_contr_insc,marca_alta_sujeto,marca_alicuota, cast(REPLACE(percepcion , ',', '.') as double precision),num_grupo_per,razon_social from padron_aux")
        elif tipo == 'CABA':
            tipo_padron = self.env.ref('l10n_ar_account.tag_tax_jurisdiccion_901')
            self.env.cr.execute('CREATE TABLE padron_aux(fecha_publicacion varchar,fecha_vig_desde varchar,fecha_vig_hasta varchar,cuit varchar,tipo_contr_insc varchar,marca_alta_sujeto varchar,marca_alicuota varchar,percepcion varchar,retencion varchar,num_grupo_per varchar,num_grupo_ret varchar,razon_social varchar)')
            # Vuelco datos de archivo en tabla auxiliar
            self.env.cr.execute("COPY padron_aux(fecha_publicacion,fecha_vig_desde,fecha_vig_hasta,cuit,tipo_contr_insc,marca_alta_sujeto,marca_alicuota, percepcion, retencion, num_grupo_per, num_grupo_ret, razon_social) FROM '" + ubicacion + "' DELIMITER '|'")
            # Convierto datos de tabla auxiliar y los paso al objeto padrón
            self.env.cr.execute("select coalesce(max(id),0) from padron")
            maximo = self.env.cr.fetchone()[0]
            self.env.cr.execute("insert into padron(tipo,fecha_publicacion,fecha_vig_desde, fecha_vig_hasta, cuit, tipo_contr_insc, marca_alta_sujeto, marca_alicuota, percepcion, retencion, num_grupo_per, num_grupo_ret,razon_social) select 'CABA',to_date(right(fecha_publicacion,8),'DDMMYYYY'),to_date(right(fecha_vig_desde,8),'DDMMYYYY'),to_date(right(fecha_vig_hasta,8),'DDMMYYYY'),cuit,tipo_contr_insc,marca_alta_sujeto,marca_alicuota, cast(REPLACE(percepcion , ',', '.') as double precision),cast(REPLACE(retencion, ',', '.') as double precision),num_grupo_per,num_grupo_ret,razon_social from padron_aux")
        #######################################
        # elimino tablay archivo formateado
        self.env.cr.execute("drop table padron_aux")
        os.remove(ubicacion)
        consulta = "INSERT INTO res_partner_arba_alicuot(company_id,from_date,to_date,tag_id,alicuota_percepcion,alicuota_retencion,withholding_amount_type,partner_id,codigo_padron) select rp.company_id,padron.fecha_vig_desde,padron.fecha_vig_hasta," + str(tipo_padron.id) + ",padron.percepcion,padron.retencion,'untaxed_amount',rp.id,padron.id from res_partner rp inner join padron padron on padron.cuit=rp.main_id_number where padron.tipo='" + tipo + "' and padron.id_alicuota is null and padron.id>" + str(maximo) + " and rp.id not in (select partner_id from res_partner_arba_alicuot where from_date=TO_DATE('" + fecha_desde + "', 'DDMMYYYY') and to_date=TO_DATE('" + fecha_hasta + "', 'DDMMYYYY') and tag_id=" + str(tipo_padron.id) + ")"
        print('EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE')
        print('consulta:', consulta)
        self.env.cr.execute(consulta)
        self.env.cr.execute("UPDATE padron set id_alicuota=subquery.id FROM (SELECT aa.id as id, codigo_padron FROM res_partner_arba_alicuot aa inner join padron pp on aa.codigo_padron=pp.id) as subquery WHERE padron.id=subquery.codigo_padron and padron.id>" + str(maximo) + ";")
        print('FIN')

    def importar_arba_back(self):
        parametro = self.env['ir.config_parameter'].search([('key', '=', 'path_padron_arba')])
        if parametro.value is not None or parametro.value is not False:
            ubicacion = '/home/admin/padrones/' + parametro.value
            ubicacion_back = ubicacion.replace(".txt", "CONV.txt")
            # Proceso archivo y vuelco 500.000 lineas procesadas en un archivo,lo proceso y lo borro
            contenido = list()
            print('ARRANCO')
            cont_linea = 0
            with open(ubicacion, 'r') as archivo:
                fecha_desde = (archivo.readline())[11:19]
                fecha_hasta = (archivo.readline())[20:28]
                for linea in archivo:
                    if ';0,00;' not in linea:
                        linea_nueva = linea.replace('|', ',')
                        linea_nueva = linea_nueva.replace(';', '|', 11)
                        contenido.append(linea_nueva)
                        cont_linea += 1
                    if cont_linea == 500000:
                        # Creo archivo y lo guardo
                        with open(ubicacion_back, 'w+') as archivo2:
                            archivo2.writelines(contenido)
                        archivo2.close()
                        self.procesar_archivo('ARBA', ubicacion_back, fecha_desde, fecha_hasta)
                        cont_linea = 0
                        contenido = list()
            archivo.close()
            if contenido != []:
                # Creo archivo y lo guardo
                with open(ubicacion_back, 'w+') as archivo2:
                    archivo2.writelines(contenido)
                archivo2.close()
                self.procesar_archivo('ARBA', ubicacion_back, fecha_desde, fecha_hasta)
            os.remove(ubicacion)

    def importar_caba_back(self):
        parametro = self.env['ir.config_parameter'].search([('key', '=', 'path_padron_caba')])
        if parametro.value is not None or parametro.value is not False:
            ubicacion = '/home/admin/padrones/' + parametro.value
            ubicacion_back = ubicacion.replace(".txt", "CONV.txt")
            # Proceso archivo y vuelco 500.000 lineas procesadas en un archivo,lo proceso y lo borro
            contenido = list()
            print('ARRANCO')
            cont_linea = 0
            with open(ubicacion, 'r') as archivo:
                fecha_desde = (archivo.readline())[9:17]
                fecha_hasta = (archivo.readline())[18:26]
                for linea in archivo:
                    if ';0,00;0,00;' not in linea:
                        linea_nueva = linea.replace('|', ',')
                        linea_nueva = linea_nueva.replace(';', '|', 11)
                        contenido.append(linea_nueva)
                        cont_linea += 1
                    if cont_linea == 500000:
                        # Creo archivo y lo guardo
                        with open(ubicacion_back, 'w+') as archivo2:
                            archivo2.writelines(contenido)
                        archivo2.close()
                        self.procesar_archivo('CABA', ubicacion_back, fecha_desde, fecha_hasta)
                        cont_linea = 0
                        contenido = list()
            archivo.close()
            print('########################################')
            print('contenido_final:', contenido)
            if contenido != []:
                # Creo archivo y lo guardo
                with open(ubicacion_back, 'w+') as archivo2:
                    archivo2.writelines(contenido)
                archivo2.close()
                self.procesar_archivo('CABA', ubicacion_back, fecha_desde, fecha_hasta)
            os.remove(ubicacion)
