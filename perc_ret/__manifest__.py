# -*- coding: utf-8 -*-
{
    'name': 'Perc_Ret',
    'version': '1.0',
    'summary': 'Perc_Ret',
    'sequence': 1,
    'description': """
Perc_Ret
======================
    """,
    'category': '',
    'website': '',
    'author': '',
    'depends': [
        'base',
        'l10n_ar',
        'l10n_ar_account_withholding',
        'l10n_ar_edi',
    ],
    'data': [
        'data/diarios_data.xml',
        # 'views/padron_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [
        # 'demo/base_demo.xml',
    ],
    'qweb': [
        # "static/src/xml/account_reconciliation.xml",
    ],
    'images': ['static/description/icon.png'],
    'installable': True,
    'application': True,
    'auto_install': False,
    # 'post_init_hook': '_auto_install_l10n',
}
