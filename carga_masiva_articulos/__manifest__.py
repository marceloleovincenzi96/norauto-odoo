
# -*- encoding: utf-8 -*-

{
    'name': 'Carga Masiva de Articulos no Temporales',
    'version': '1.0',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'Carga Masiva de Articulos no Temporales',
    'depends': ['stock', ],
    'author': 'NybbleGroup',
    'description': """
NorAuto Carga Masiva de Articulos no Temporales
===============
Agrega un wizard en productos para importar productos a partir de archivo excel
""",
    'data': [
        'wizards/carga_masiva_articulos.xml',
    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
