# -*- coding: utf-8 -*-

import xlrd
import base64
import datetime
from datetime import date
from odoo import api, fields, models


class CargaMasivaArticulos(models.TransientModel):
    _name = 'norauto.carga_masiva_articulos_wizard'

    archivo = fields.Binary('Archivo')

    def importar(self):
        wb = xlrd.open_workbook(file_contents=base64.decodestring(self.archivo))
        required_fields = ['name']
        productos_no_encontrados = ""
        listas_no_encontradas = ""
        excel_products = []
        for sheet in wb.sheets():
            for row in range(2, sheet.nrows):
                product_vals = {}
                # TODO: cambiar el 11 fijo por sheet.ncols
                for cel_num in range(0, sheet.ncols):
                    product_vals[sheet.cell(1, cel_num).value] = sheet.cell(row, cel_num).value
                excel_products.append(product_vals)

        for excel_product in excel_products:
            if excel_product.get('tipoart_dos'):
                excel_product['tipoart_dos'] = str(int(excel_product['tipoart_dos']))
            if excel_product.get('fecha_creacion'):
                excel_product['fecha_creacion'] = self.xldate_to_string_date(excel_product['fecha_creacion'])
            if excel_product.get('grupo_comprador'):
                excel_product['grupo_comprador'] = self.get_grupo_comprador(excel_product.get('grupo_comprador'))
            if 'taxes_id' in excel_product:
                excel_product['taxes_id'] = self.get_taxes(excel_product['taxes_id'])
            if excel_product.get('categ_id'):
                excel_product['categ_id'] = self.get_category_id(excel_product.get('categ_id'))
            if excel_product.get('list_price'):
                excel_product['list_price'] = self.get_clean_float(excel_product['list_price'])
            if excel_product.get('standard_price'):
                excel_product['standard_price'] = self.get_clean_float(excel_product['standard_price'])
            if excel_product.get('barcode'):
                excel_product['barcode'] = self.get_clean_integer(excel_product['barcode'])
            # NOTE: con clase 0, aca evalua False con .get
            if 'clase' in excel_product:
                excel_product['clase'] = self.get_clase(excel_product['clase'])
            if 'family_code' in excel_product:
                excel_product['family_code'] = self.get_clean_integer(excel_product['family_code'])
            if 'vendor_no' in excel_product:
                excel_product['vendor_no'] = self.get_clean_integer(excel_product['vendor_no'])

            # NOTE: buscar el producto, editar o crear
            producto = self.env['product.template'].search([('idart', '=', excel_product['idart'])])
            if producto:
                excel_product.pop('idart')
                # NOTE: como el tipo nativo de Odoo esta oculto hay que agregarlo por aca, pero si ya tiene dejarlo asi no jode
                if producto.type == 'consu' and 'type' in excel_product:
                    excel_product.pop('type')
                producto.write(excel_product)
            else:
                # NOTE: Revisa que tenga los campos requeridos para guardar en base de datos, no los de odoo sino los sql_constrains
                if not any([required_field in excel_product for required_field in required_fields]):
                    print("El articulo con idart: {idart} no pudo ser creado, por favor reviselo y vuelva a intentarlo".format(idart=excel_product['idart']))
                    pass
                else:
                    producto.create(excel_product)

    def get_clase(self, clase):
        if type(clase).__name__ == 'float':
            return str(int(clase))
        elif type(clase).__name__ == 'int':
            return str(clase)
        else:
            return self.clean_whitespace_at_sides(clase)

    def clean_whitespace_at_sides(self, string):
        string = string.rstrip(' ')
        string = string.lstrip(' ')
        return string

    def get_clean_integer(self, number):
        if number:
            if type(number).__name__ in ['int', 'float']:
                return str(number).split('.')[0]
            elif type(number).__name__ == 'str':
                val = self.clean_whitespace_at_sides(number)
                if val:
                    return val
        return False

    def get_clean_float(self, value):
        # import pdb; pdb.set_trace()
        if type(value).__name__ in ['float', 'int']:
            return value
        if type(value).__name__ == 'str':
            value = value.strip(' ')
            value = value.replace('.', '')
            value = value.replace(',', '.')
            return float(value)

    def get_category_id(self, category_name):
        if category_name:
            code = category_name.split(' ')[0]
            category = self.env['product.category'].search([('name', 'ilike', category_name)])
            return category.id
        return False

    def get_taxes(self, iva):
        if iva:
            taxesmap = {
                '0.0': 'l10n_ar.1_ri_tax_vat_no_corresponde_ventas',
                '10.5': 'l10n_ar.1_ri_tax_vat_10_ventas',
                '21.0': 'l10n_ar.1_ri_tax_vat_21_ventas',
            }
            tax = self.env.ref(taxesmap[str(iva)])
            taxes = [(4, tax.id)]
            return taxes
        return False

    def get_grupo_comprador(self, grupo_number):
        if grupo_number:
            grupo = self.env['stock.grupocomp'].search([('number', '=', grupo_number)])
            return grupo.id
        return False

    def xldate_to_string_date(self, xldate):
        temp = datetime.datetime(1899, 12, 30)
        delta = datetime.timedelta(days=xldate)
        return (temp + delta).strftime("%Y-%m-%d")
