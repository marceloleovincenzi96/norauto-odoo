# -*- encoding: utf-8 -*-

{
    "name": "Grupo Finanzas",
    "version": "1.0",
    'author': 'Nybble Group',
    "summary": """Añade las funcionalidades del grupo de Finanazas para Norauto""",
    "license": "AGPL-3",
    "depends": ['fields_norauto', 'norauto_rights'],
    "data": [
        'security/grupo_finanzas_data.xml',
        'views/res_partner_view.xml',
    ],
    'qweb': [],
    "installable": True,
    "active": False,
}
