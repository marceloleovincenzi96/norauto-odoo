# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    aprobado_finanzas = fields.Boolean("Aprobado por Finanzas")
    rechazado_finanzas = fields.Boolean("Rechazado por Finanzas")

    def aprobar_proveedor(self):
        for rec in self:
            rec.aprobado_finanzas = True

    def rechazar_proveedor(self):
        for rec in self:
            rec.rechazado_finanzas = True

    def aprobar_proveedores(self):
        proveedores = self.env['res.partner'].search([('supplier_rank', '>', 0)])
        proveedores.aprobar_proveedor()
