# -*- coding: utf-8 -*-
{
	'name': 'Configuración Tarjetas',
	'version': '13.0.0.0.0',
    'summary': """Permite crear tarjetas y agregarles recargos por cuotas.""",
	'category': 'Tools',
	'author': "Nybble Group",
    'website': "https://www.nybblegroup.com/",
	'depends': ['web','base'],
	'data': [
		'security/ir.model.access.csv',
		'views.xml',
	],
	'demo': [
	#	'data/demo.xml',
		],
	'css': [],
	'installable': True,
	'auto_install': False,
	'application': True,
}
