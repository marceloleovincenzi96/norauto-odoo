from odoo import fields, models, api
from odoo.exceptions import ValidationError
from datetime import datetime
import base64

class TarjetasTarjeta(models.Model):
    _name = 'tarjetas.tarjeta'

    name = fields.Char("Nombre")
    interes_ids = fields.One2many(comodel_name='tarjetas.cuotas',inverse_name='tarjeta_id',string='Cuotas')
    

class TarjetaInteres(models.Model):
    _name = 'tarjetas.cuotas'
    
    tarjeta_id = fields.Many2one('tarjetas.tarjeta',string='Cuotas')
    qty = fields.Char('Cuotas')
    interes = fields.Float('Interes')
    