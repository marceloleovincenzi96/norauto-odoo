odoo.define('perc_ret_pos', function (require) {
    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var rpc = require('web.rpc');
    var core = require('web.core');
    var qweb = core.qweb;

    //NO: añadir a todos los productos que se cargen la retencion de caba y arba
    //NO: cargar para los partners el campo alicuot ids (o hacerlo por rpc.query)
    //TODO: tambien habira que cambiar la funcion de pos_partner_load para que los carge dinamicamente
    //TODO: cambiar la funcion get_all_prices en models/exports.Orderline para que tome los taxes alicuota partner
    //      y despues medificar ese res para que incluya las percepciones

    models.load_fields('res.partner', ['arba_alicuot_ids'])

    var _super_order = models.Order.prototype;
    models.Order = models.Order.extend({
        initialize: function (attr, options) {
            this.alicuotas = false;
            _super_order.initialize.call(this, attr, options);
        },
        set_client: function(client){
            var self = this;
            if (client){
                rpc.query({
                    model: 'res.partner',
                    method: 'get_pos_partner_alicuotas',
                    args:[[],client.id]
                })
                .then(function(alicuotas){
                    self.alicuotas = alicuotas
                    _super_order.set_client.call(self, client);
                })
            } else {
                _super_order.set_client.call(self, client);
            }
        },
    })

    var _super_Orderline = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        get_all_prices: function(){
            var self = this;
            order = self.pos.get_order();
            var res = _super_Orderline.get_all_prices.apply(this);
            if (order && order.alicuotas && order.alicuotas instanceof(Array)){
                order.alicuotas.forEach(alicuota => {
                    let monto_alicuota = res.priceWithoutTax * alicuota.alicuota
                    res.priceWithTax += monto_alicuota
                    res.tax += monto_alicuota
                    res.taxDetails[alicuota.id] = monto_alicuota
                });
            }
            return res
        }

    })
});
