from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    def get_pos_partner_alicuotas(self, partner_id):
        # import pdb; pdb.set_trace()
        alicuotas = []
        partner = self.browse(partner_id)
        # TODO: hacer un if de si el partner es responsable inscripto, asi  no corre lo de abajo si no es responsable
        date_invoice = fields.Date.context_today(self)
        # CABA
        impuesto_caba = self.env.ref('perc_ret.imp_percepcion_iibb_caba_aplicada')
        alicuota = impuesto_caba.get_partner_alicuota_percepcion_pr(partner, date_invoice, 'caba')
        if alicuota > 0:
            alicuotas.append({'id': impuesto_caba.id, 'alicuota': alicuota, 'name': 'caba'})
        # ARBA
        impuesto_arba = self.env.ref('perc_ret.imp_percepcion_iibb_arba_aplicada')
        alicuota = impuesto_arba.get_partner_alicuota_percepcion_pr(partner, date_invoice, 'caba')
        if alicuota > 0:
            alicuotas.append({'id': impuesto_arba.id, 'alicuota': alicuota, 'name': 'arba'})
        # alicuotas = [{'id': 1, 'alicuota': 0.03, 'name': 'arba/caba'}]
        if len(alicuotas) > 0:
            return alicuotas
        return False
