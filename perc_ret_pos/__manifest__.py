# -*- coding: utf-8 -*-
{
    'name': 'Perc Ret POS',
    'version': '13.0',
    'summary': 'Perc_Ret',
    'sequence': 1,
    'description': """
Perc_Ret para POS
======================
    """,
    'category': '',
    'website': '',
    'author': '',
    'depends': [
        'perc_ret',
    ],
    'data': [
        'views/perc_ret_resources.xml'
    ],
    'demo': [
        # 'demo/base_demo.xml',
    ],
    'qweb': [
        # "static/src/xml/account_reconciliation.xml",
    ],
    'images': ['static/description/icon.png'],
    'installable': True,
    'application': False,
    'auto_install': False,
    # 'post_init_hook': '_auto_install_l10n',
}
