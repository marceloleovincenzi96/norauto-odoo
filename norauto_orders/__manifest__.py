
# -*- encoding: utf-8 -*-

{
    'name': 'Norauto Ordenes',
    'version': '1.0',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'recepción de ordenes de trabajo de Norauto',
    'depends': ['sale', 'operating_unit', 'stock_operating_unit', 'fields_norauto', 'stock_reserve'],
    'author': 'NybbleGroup',
    'description': """
NorAuto Orders
===============

""",
    'data': [
        "views/norauto_orders_view.xml",
        "views/product_view.xml",
        "views/res_partner_view.xml",
        "security/ir.model.access.csv"
    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
