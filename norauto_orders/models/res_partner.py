
from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    proveedor_temporales = fields.Boolean('Proveedor Articulos temporales')
