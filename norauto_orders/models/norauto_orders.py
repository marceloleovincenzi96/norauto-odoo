# -*- coding: utf-8 -*-

from odoo import api, fields, models
import datetime
import logging

_logger = logging.getLogger(__name__)


class NorautoOrder(models.Model):
    _name = 'norauto.order'

    name = fields.Char('Código')
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Cliente'
    )
    state = fields.Selection(
        selection=[
            ('to_do', 'En preparación'),
            ('paid', 'Pagado'),
            ('delivered', 'Entregado'),
            ('done', 'Hecha'),
            ('archived', 'Archivada'),
        ],
        default='to_do',
        string='Estado'
    )
    norauto_order_line_ids = fields.One2many(
        comodel_name='norauto.order.line',
        inverse_name='norauto_order_id',
        string='Insumos'
    )
    operating_unit_id = fields.Many2one(
        comodel_name='operating.unit',
        string='Sucursal'
    )

    valor_anticipo = fields.Float(
        string='Valor de anticipo',
        default=0
    )
    comentario_archivo = fields.Char('Comentario Archivo')

    ##################################################
    # ORM OVERRIDES
    ##################################################
    def write(self, vals):

        if vals.get('valor_anticipo'):
            vals['valor_anticipo'] = float(vals['valor_anticipo'])
            vals['valor_anticipo'] += self.valor_anticipo

        res = super(NorautoOrder, self).write(vals)

        return res

    ##################################################
    # GESTIOO METHODS
    ##################################################
    def set_to_done(self):
        self.state = 'done'
        return True

    def set_to_paid(self):
        self.state = 'paid'
        # self.clear_reservations()
        return True

    def set_to_delivered(self):
        self.state = 'delivered'
        return True

    def reject_order(self):
        self.ensure_one()
        self.state = 'to_do'
        return True

    def archive_order(self, comentario=False):
        if not comentario:
            return "Debe incluir un comentario para archivar la orden"
        self.write({'comentario_archivo': comentario, 'state': 'archived'})
        self.clear_reservations()
        return True

    def create_temporary_product(self, vals):
        # NOTE: al hacer esto de esta forma, tiene/puede verificar stock porque ya va a tener stock
        # Crear el producto con los vals y llenar los otros campos con default
        temporal = self.env['product.template'].sudo().with_context({'sucursal_id': vals.get('sucursal_id', False)}).create({
            'name': vals.get('name'),
            'tipoart_dos': '2',
            'tipoart_uno': 'Almacenable',
            'categ_id': vals.get('category_id'),
            'type': 'product',
            'sale_ok': True,
            'purchase_ok': True,
            'taxes_id': [(6, 0, [vals.get('iva_id')])],
            'clase': 'C',
            'list_price': vals.get('price_without_tax'),
            'available_in_pos': True
        })
        # Crear la solicitud de presupuesto con los vals
        prod_prod = self.env['product.product'].sudo().search([('product_tmpl_id', '=', temporal.id)], limit=1)
        proveedor = self.env['res.partner'].sudo().search([('proveedor_temporales', '=', True)], limit=1)
        if not proveedor:
            return "No hay un proveedor de pedidos urgentes configurado"
        warehouse = self.env['stock.warehouse'].search([('operating_unit_id', '=', vals.get('sucursal_id'))])
        picking_type = self.env['stock.picking.type'].search([('code', '=', 'incoming'), ('warehouse_id', '=', warehouse.id)], limit=1)
        order_compra = self.env['purchase.order'].sudo().create({
            'name': 'New',
            'partner_id': proveedor.id,       # es un provedor fijo, poner variable de control en proveedor para que sea configurable
            'date_order': datetime.datetime.now(),
            'creado_por_tienda': True,
            'requesting_operating_unit_id': vals.get('sucursal_id'),
            'operating_unit_id': vals.get('sucursal_id'),
            'picking_type_id': picking_type.id,
            'order_line': [(0, 0, {
                'name': prod_prod.name_get(),
                'product_id': prod_prod.id,
                'price_unit': vals.get('price_without_tax'),
                'product_uom': 1,
                'date_planned': datetime.datetime.now(),
                'product_qty': vals.get('cantidad'),
                'taxes_id': [(6, 0, [vals.get('iva_id')])]
            })]
        })

        # Pasar la solicitud a orden de compra
        order_compra.sudo().button_approve()

        # Acceder a la recepcion del producto y recibirla completamente
        carrier = self.env['delivery.carrier'].search([], limit=1)
        recepcion = self.env['stock.picking'].sudo().search([('origin', '=', order_compra.name)])
        linea = recepcion.move_line_ids_without_package.filtered(lambda line: line.product_id == prod_prod)
        linea.write({'qty_done': vals.get('cantidad')})
        recepcion.write({
            'voucher_ids': [(0, 0, {'name': vals.get('remito')})],
            'carrier_id': carrier.id
        })
        recepcion.button_validate()

        # NOTE: stock y gestio utilizan el product.product (prod_prod) no el product.template (temporal)
        return temporal.id

    def get_current_stock(self, operating_unit_id, producto_ids):
        """
        Devuelve una lista de tuplas [(id producto, cantidad)] con las cantidades para una unidad operativa
        operating_unit_id: integer, id de la unidad operativa, buscamos el warehouse de esa sucursal
        producto_ids: list, ids de los product.template a consultar
        """
        warehouse = self.env['stock.warehouse'].sudo().search([('operating_unit_id', '=', operating_unit_id)], limit=1)
        res = []
        if warehouse:
            for producto_id in producto_ids:
                producto = self.env['product.product'].sudo().search([('product_tmpl_id', '=', producto_id)], limit=1)
                if producto:
                    available = self.env['stock.quant'].sudo()._get_available_quantity(producto, warehouse.lot_stock_id)
                    # import pdb; pdb.set_trace()
                    res.append((producto_id, available))
                else:
                    res.append((producto_id, 'Not found'))
        return res

    def clear_reservations(self):
        """
        Borra las reservas relacionadas a la orden (de cada una de las lineas)
        """
        for orden in self:
            for line in orden.norauto_order_line_ids:
                # TODO: hacer un manejo de excepciones para que no salten carteles rojos
                # si por x razon no se puede terminar de sacar las reservas porque no hay stock, que la deje en 0 por lo menos
                line.delete_reservation(line)

    ##################################################
    # OTROS METODOS
    ##################################################
    def borrar_reservas_de_ordenes_finalizadas(self):
        ordenes = self.search([('state', 'in', ['paid', 'delivered'])])
        for orden in ordenes:

            _logger.info('--------------------------------------------------------------')
            _logger.info('Order: %s, ID %s' % (orden.name, orden.id))
            _logger.info('Lineas:')
            for line in orden.norauto_order_line_ids:
                _logger.info('Producto: [%s] %s, cantidad: %s' % (line.product_id.default_code, line.product_id.name, line.cantidad))
                try:
                    line.delete_reservation(line)
                except:
                    _logger.info('No se pudo eliminar la reserva')
            _logger.info('--------------------------------------------------------------')


class NorautoOrderLine(models.Model):
    _name = 'norauto.order.line'

    norauto_order_id = fields.Many2one(
        comodel_name='norauto.order',
        string='Orden',
        ondelete='cascade'
    )
    product_id = fields.Many2one(
        comodel_name='product.template',
        string='Producto'
    )
    cantidad = fields.Integer(
        'Cantidad'
    )
    stock_reservation_id = fields.Many2one(
        comodel_name='stock.reservation',
        string='Reserva'
    )
    descuento = fields.Float('Descuento')
    rubrica_descuento = fields.Char('Código Rúbrica Descuento')
    usuario_descuento = fields.Char('Usuario descuento')
    comentario_descuento = fields.Char('Comentario descuento')

    ##################################################
    # ORM OVERRIDES
    ##################################################
    @api.model
    def create(self, vals):
        res = super(NorautoOrderLine, self).create(vals)
        # NOTE: crear stock reservation y reservar
        # self._verify_stock(res.norauto_order_id.operating_unit_id.id, res.product_id.id, res.cantidad)
        res.create_reservation(res)
        return res

    def write(self, vals):
        if not self._context.get('deleted_reservation', False):
            self.delete_reservation(self)
        res = super(NorautoOrderLine, self).write(vals)
        self.create_reservation(self)
        return res

    def unlink(self):
        for rec in self:
            self.delete_reservation(rec)
            return super(NorautoOrderLine, self).unlink()

    ##################################################
    # API GESTIOO METHODS
    ##################################################
    def create_order_line(self, vals):
        # Verificar stock
        cantidad_demandada = vals.get('cantidad')
        orden = self.env['norauto.order'].browse(vals.get('norauto_order_id'))
        if self._product_is_service(vals.get('product_id')):
            linea = self.create(vals)
            return {
                'line_id': linea.id,
                'product_id': vals.get('product_id')
            }
        cantidad_disponible = self._verify_stock(orden.operating_unit_id.id, vals.get('product_id'), cantidad_demandada)
        if cantidad_demandada <= cantidad_disponible and cantidad_disponible > 0:
            linea = self.create(vals)
            return {
                'line_id': linea.id,
                'product_id': vals.get('product_id'),
                'cantidad_reservada': cantidad_demandada
            }
        elif cantidad_demandada >= cantidad_disponible > 0:
            vals.update(cantidad=cantidad_disponible)
            linea = self.create(vals)
            return {
                'line_id': linea.id,
                'product_id': vals.get('product_id'),
                'cantidad_reservada': cantidad_disponible
            }
        else:
            vals.update(cantidad=cantidad_disponible)
            linea = self.create(vals)
            return {
                'line_id': linea.id,
                'product_id': vals.get('product_id'),
                'cantidad_reservada': cantidad_disponible,
                'nota': 'sin stock'
            }

    def edit_order_line(self, vals):
        # Verificar stock
        line = self.env['norauto.order.line'].browse(vals.get('line_id'))
        vals.pop('line_id')
        # product = vals.get('product_id') or line.product_id
        cantidad_demandada = vals.get('cantidad')
        orden = line.norauto_order_id
        if self._product_is_service(vals.get('product_id')):
            line.write(vals)
            return {
                'line_id': line.id,
                'product_id': vals.get('product_id'),
                'descuento': line.descuento,
                'rubrica_descuento': line.rubrica_descuento,
                'usuario_descuento': line.usuario_descuento
            }
        line.delete_reservation(line)
        cantidad_disponible = self._verify_stock(orden.operating_unit_id.id, vals.get('product_id'), cantidad_demandada)

        # cantidad demandada disponible
        if cantidad_demandada <= cantidad_disponible and cantidad_disponible > 0:
            line.with_context({'deleted_reservation': True}).write(vals)
            return {
                'line_id': line.id,
                'product_id': vals.get('product_id'),
                'cantidad_reservada': cantidad_demandada,
                'descuento': line.descuento,
                'rubrica_descuento': line.rubrica_descuento,
                'usuario_descuento': line.usuario_descuento
            }
        # cantidad demandandada parcialmente disponible
        elif cantidad_demandada >= cantidad_disponible > 0:
            vals.update(cantidad=cantidad_disponible)
            line.with_context({'deleted_reservation': True}).write(vals)
            return {
                'line_id': line.id,
                'product_id': vals.get('product_id'),
                'cantidad_reservada': cantidad_disponible,
                'descuento': line.descuento,
                'rubrica_descuento': line.rubrica_descuento,
                'usuario_descuento': line.usuario_descuento
            }
        # no hay nada disponible
        else:
            vals.update(cantidad=cantidad_disponible)
            line.with_context({'deleted_reservation': True}).write(vals)
            return {
                'line_id': line.id,
                'product_id': vals.get('product_id'),
                'cantidad_reservada': cantidad_disponible,
                'nota': 'sin stock',
                'descuento': line.descuento,
                'rubrica_descuento': line.rubrica_descuento,
                'usuario_descuento': line.usuario_descuento
            }

    ##################################################
    # OTHER METHODS
    ##################################################
    def _product_is_service(self, product_id):
        producto = self.env['product.template'].browse(product_id)
        is_service = producto.tipoart_uno == 'Servicio' or producto.type == 'Service'
        return is_service or False

    def _verify_stock(self, operating_unit_id, producto_id, cantidad):
        warehouse = self.env['stock.warehouse'].search([('operating_unit_id', '=', operating_unit_id)], limit=1)
        producto = self.env['product.product'].search([('product_tmpl_id', '=', producto_id)], limit=1)
        if warehouse:
            available = self.env['stock.quant']._get_available_quantity(producto, warehouse.lot_stock_id)
            if cantidad > available:
                return available
            else:
                return cantidad
                # raise ValidationError(("Quedán disponibles {available} unidades del producto: {product_name}".format(available=available, product_name=producto.name)))

    def create_reservation(self, rec):
        prod = self.env['product.product'].search([('product_tmpl_id', '=', rec.product_id.id)], limit=1)
        if prod.tipoart_uno == 'Servicio':
            return
        warehouse = self.env['stock.warehouse'].search([('operating_unit_id', '=', rec.norauto_order_id.operating_unit_id.id)], limit=1)
        location = warehouse.lot_stock_id
        quant = self.env['stock.quant'].search([('location_id', '=', location.id), ('product_id', '=', prod.id)])
        quant._update_reserved_quantity(prod, location, rec.cantidad)

    def delete_reservation(self, rec):
        prod = self.env['product.product'].search([('product_tmpl_id', '=', rec.product_id.id)], limit=1)
        if prod.tipoart_uno == 'Servicio':
            return
        warehouse = self.env['stock.warehouse'].search([('operating_unit_id', '=', rec.norauto_order_id.operating_unit_id.id)], limit=1)
        location = warehouse.lot_stock_id
        quant = self.env['stock.quant'].search([('location_id', '=', location.id), ('product_id', '=', prod.id)])
        quant._update_reserved_quantity(prod, location, -rec.cantidad)
