# -*- coding: utf-8 -*-

from odoo import api, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    def search_products_with_stock(self, operating_unit_id=None):
        """Devuelve una lista de productos disponibles en las ubicaciones hijas del warehouse,
        solo selecciona el primer warehouse que se encouentra, configurar bien"""
        # operating_unit = self.env['operating.unit'].browse(operating_unit_id)
        warehouse = self.env['stock.warehouse'].search([('operating_unit_id', '=', operating_unit_id)], limit=1)
        res = []
        if warehouse:
            stock_quants = self.env['stock.quant'].search([('location_id', 'child_of', warehouse.lot_stock_id.id)])
            for quant in stock_quants:
                disponible = quant.quantity - quant.reserved_quantity
                res.append({
                    'product_id': quant.product_id.product_tmpl_id.id,
                    'product_name': quant.product_id.product_tmpl_id.name,
                    'cantidad_disponible': disponible
                })
        return res

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        """Si se tiene el contexto de buscar_stock = True; devuelve la cantidad en x sucursal"""
        if self._context.get('buscar_stock'):
            operating_unit_id = self._context.get('operating_unit_id')
            productos = self.search_products_with_stock(operating_unit_id)
            res = self._get_products_as_tuple(productos)
            return res
        res = super(ProductTemplate, self).name_search(name, args, operator, limit)
        return res

    def _get_products_as_tuple(self, productos):
        res = [(prod['product_id'], prod['product_name']) for prod in productos]
        return res
