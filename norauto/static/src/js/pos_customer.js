odoo.define('pos.consumidor_final', function (require) {
    "use strict";

    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var gui = require('point_of_sale.gui');

    models.load_fields("res.partner", ["l10n_ar_afip_responsibility_type_id"]);

    screens.PaymentScreenWidget.include({        

        validate_order: function (force_validation) {

            var self = this;

            var cliente = this.pos.get_order().attributes.client;
            console.log('El cliente: ', cliente)
            if (cliente){
                if (cliente.l10n_ar_afip_responsibility_type_id[1] == 'Consumidor Final'){
                    
                    this.pos.get_order().esConsumidorFinal = true;
                    console.log("Se cumple esto")
                }
            }
            // este dato de si es consumidor final esta cargado en el pos.db asi que no seria necesario
            /* this._rpc({

                model: 'res.partner',
                method: 'search_read',
                domain: [['id', '=', cliente[0]]],
                fields: ['id', 'name', 'l10n_ar_afip_responsibility_type_id']

            }).then(function (partners) {

                partners.forEach(function (partner) {

                    if (partner[0].l10n_ar_afip_responsibility_type_id[1] == 'Consumidor Final'){
                        
                        this.pos.get_order().esConsumidorFinal = true;
                    }

                });
            }); */

            this._super(force_validation);

        }

    });

    var _super_order = models.Order.prototype;
    models.Order = models.Order.extend({
        initialize: function(attr, options){
            this.esConsumidorFinal = false;
            _super_order.initialize.call(this, attr, options);
        }
    })


});