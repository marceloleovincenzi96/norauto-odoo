from odoo import models, fields, exceptions
import logging
_logger = logging.getLogger(__name__)

class MargenCesionWizard(models.TransientModel):
    _name = "update.margen_cesion"
    _description = "Actualizar valor de margen de cesión"
    new_margen_cesion = fields.Float(string="Nuevo margen de cesión", default=lambda self: self._obtener_margen_cesion())

    def actualizar_margen_cesion(self):    
        self.env['ir.config_parameter'].sudo().set_param("margen_cesion", self.new_margen_cesion)

    def _obtener_margen_cesion(self):
        return float(self.env['ir.config_parameter'].sudo().get_param("margen_cesion"))

