# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    All Rights Reserved.
#
############################################################################
#
############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name": "Norauto",
    "version": "1.0",
    'author': 'Nybble Group',
    "summary": """Customización para Norauto. Incluye margen de cesion, fondo fijo y unidad operativa en POS.""",
    "license": "AGPL-3",
    "depends": ['base', 'autoincremeno_articulo_temporal', 'carga_masiva_articulos', 'fields_norauto', 'grupo_finanzas', 'grupos_compradores', 'norauto_creations', 'norauto_gondola', 'norauto_idocs', 'norauto_orders', 'norauto_rights', 'operating_unit', 'purchase', 'salida_stock_sin_pago', 'gestion_litigios', 'smile_audit', 'stock_operating_unit', 'stock_reserve', 'transferencias_internas', 'backend_theme_v13'],
    "data": [
        'security/ir.model.access.csv',
        'data/fondo_fijo_default.xml',
        'wizards/margen_cesion.xml',
        'views/fondo_fijo.xml',
        'views/fondo_fijo_tienda.xml',
        'views/add_operating_unit_field_pos.xml',
        'views/pos_customer.xml',
        'data/cron_fondo_fijo.xml',
        'data/cron_ochenta_porciento.xml'
    ],
    'qweb': [
        'static/src/xml/pos_customer.xml'
    ],
    "installable": True,
    "active": False,
}
