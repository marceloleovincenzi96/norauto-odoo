from odoo import models, fields, api
from odoo.exceptions import UserError
import logging
from datetime import date
_logger = logging.getLogger(__name__)

class FondoFijo(models.Model):
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _name = "update.fondo_fijo"
    _description = "Fondo fijo"

    fondo_fijo = fields.Float(string="Fondo fijo", default=lambda self: self._obtener_fondo_fijo(), readonly=True)


    nuevo_fondo_fijo = fields.Float(
        string='Nuevo fondo fijo',
        
    )
    
    fecha_nuevo_fondo_fijo = fields.Date(
        string='Fecha de inicio del nuevo fondo fijo',
    )

    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id.id, required=True)
    
    @api.model
    def create(self, vals):

        registros = self.env['update.fondo_fijo'].search([])

        if registros:

            raise UserError ('El registro para el control del fondo fijo debe ser unico. No puede crear otro')

        return super(FondoFijo, self).create(vals)

    def actualizar_fondo_fijo(self):

        for record in self:
            if record.fecha_nuevo_fondo_fijo == date.today():
                self.env['ir.config_parameter'].sudo().set_param("fondo_fijo", self.fondo_fijo)
                record.fondo_fijo_ahora = record.fondo_fijo
                record.enviar_notificacion_cambio_fondo_fijo()

    def enviar_notificacion_cambio_fondo_fijo(self):
        main_users = self.env['res.users'].search([])
        partners_ids = [u.partner_id.id for u in main_users]
        self.message_subscribe(partner_ids=partners_ids, channel_ids=self.message_channel_ids)
        #user = main_tienda_users.filtered(lambda r: r.has_group('norauto_rights.group_tienda'))
        message1 = "Se ha cambiado el fondo fijo en el POS. Ahora es de %s" % (self.fondo_fijo)
        for x in main_users:
            self.activity_schedule(act_type_xmlid='mail.mail_activity_data_todo', summary=message1, user_id=x.id)
        self.message_post(body=message1)

    def _obtener_fondo_fijo(self):
        return float(self.env['ir.config_parameter'].sudo().get_param("fondo_fijo"))
