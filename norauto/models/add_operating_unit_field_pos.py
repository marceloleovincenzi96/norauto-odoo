from odoo import fields, models

class AddOperatingUnitFieldInPOS(models.Model):

    _inherit='pos.config'
    _name=_inherit

    operating_unit_id = fields.Many2one('operating.unit', 'Unidad operativa')