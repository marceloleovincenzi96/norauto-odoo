from odoo import models, fields, api
from odoo.exceptions import UserError
import logging
from datetime import date
_logger = logging.getLogger(__name__)

class FondoFijoPorTienda(models.Model):

    _inherit = ['mail.thread', 'mail.activity.mixin']
    _name = "fondo_fijo.tienda"
    _description = "Fondo fijo por tienda"
    _rec_name = 'name_operating_unit'

    fondo_fijo = fields.Float(string="Fondo fijo", default=lambda self: self._obtener_fondo_fijo(), readonly=True)

    fondo_fijo_ahora = fields.Float(string="Fondo fijo ahora", default=lambda self: self._obtener_fondo_fijo(), readonly=True)

    operating_unit_id = fields.Many2one('operating.unit', 'Unidad operativa', default=lambda self: self.env.user.default_operating_unit_id)

    name_operating_unit = fields.Char(related='operating_unit_id.name')

    def enviar_notificacion_reposicion_fondo_fijo(self):
        main_users = self.env['res.users'].search([]).filtered(lambda r: self.env.ref('norauto_rights.group_supervisor_tienda') and r.has_group('norauto_rights.group_supervisor_tienda'))
        partners_ids = [u.partner_id.id for u in main_users]

        for record in self:
            if record.fondo_fijo_ahora <= (record.fondo_fijo - record.fondo_fijo * 0.80):
                record.message_subscribe(partner_ids=partners_ids, channel_ids=record.message_channel_ids)
                #user = main_tienda_users.filtered(lambda r: r.has_group('norauto_rights.group_tienda'))
                message1 = "Se ha gastado el 80 porciento del fondo fijo actual en la unidad operativa %s. (Gastado: %f). Se sugiere que lo reponga." % (record.operating_unit_id.name, record.fondo_fijo)
                for x in main_users:
                    record.activity_schedule(act_type_xmlid='mail.mail_activity_data_todo', summary=message1, user_id=x.id)
                record.message_post(body=message1)

    def _obtener_fondo_fijo(self):
        return float(self.env['ir.config_parameter'].sudo().get_param("fondo_fijo"))