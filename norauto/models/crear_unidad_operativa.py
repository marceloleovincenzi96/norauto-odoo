from odoo import models, api
from datetime import date

class UnidadOperativa (models.Model):

    _name = 'operating.unit'
    _inherit = _name

    @api.model
    def create (self, vals):

        res = super(UnidadOperativa, self).create(vals)

        unidad = self.env['operating.unit'].search([('name', '=', vals['name'])])

        self.env['update.fondo_fijo'].create({

            'fondo_fijo': 30000,
            'fondo_fijo_ahora': 30000,
            'nuevo_fondo_fijo': 30000,
            'fecha_nuevo_fondo_fijo': date.today(),
            'operating_unit_id': unidad.id

        })

        return res