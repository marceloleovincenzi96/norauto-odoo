# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, exceptions


class PosSession(models.Model):
    _inherit = "pos.session"

    total_diferencia = fields.Float(string='Diferencia Total', store=True, compute='_get_total_diferencias')
    comentario = fields.Text(string='Comentario por sobrepasar umbral')
    comentario_id = fields.One2many(
        'pos.session.add.comentario', 'current_sesion_id', index=True)

    @api.depends('pos_payment_method_quantities_ids')
    def _get_total_diferencias(self):
        for sesion in self:
            diferencia_oficial = 0
            for cobro in sesion.pos_payment_method_quantities_ids:
                diferencia_oficial += cobro.difference
            sesion.total_diferencia = diferencia_oficial 

    def get_comentario_sesion(self):
        for sesion in self:
            for comentario in sesion.comentario_id:
                self.comentario = comentario.comentario
            return

    def action_pos_session_validate(self):
        if not self.env.context.get('comentario'):
            tolerancia = self.env['umbral.tolerancia.cc'].search([])
            for tol in tolerancia:
                if tol.name == 'Segundo Límite Tolerancia':
                    if self.total_diferencia >= tol.limt_tol_dif_cierre_caja:
                        self.enviar_notificacion_segundo_limite()
                        return self.create_comentario()
                elif tol.name == 'Primer Límite Tolerancia':
                    if self.total_diferencia >= tol.limt_tol_dif_cierre_caja:
                        return self.create_comentario()
                else:
                    return super(PosSession, self).action_pos_session_validate()
        else:
            return super(PosSession, self).action_pos_session_validate()

    def create_comentario(self):
        self.ensure_one()
        context = dict(self._context)
        context['session_id'] = self.id
        return {
            'type': 'ir.actions.act_window',
            'name': _('Comentario'),
            'view_mode': 'form',
            'res_model': 'pos.session.add.comentario',
            'views': [(False, 'form')],
            'context': context,
            'domain': '[]',
            'target': 'new'
        }

    def enviar_notificacion_segundo_limite(self):
        self.ensure_one()
        email_template = self.env.ref('norauto_umbral_tolerancia.umbral_tolerancia_email')
        all_users = self.env['res.users'].search([('active', '=', True)])

        # self.env.user.has_group('norauto_rights.group_supervisor_tienda')
        # self.env.user.has_group('norauto_rights.group_director_tienda')

        director_tienda = all_users.filtered(lambda user: user.has_group('norauto_rights.group_director_tienda'))
        for dir in director_tienda:
            ctx = {'email_to': dir.login}
            email_template.with_context(ctx).send_mail(self.id, force_send=True)


class comentarioSesion(models.TransientModel):
    _name = "pos.session.add.comentario"

    @api.model
    def _default_sesion_id(self):
        current_sesion_id = self.env['pos.session'].browse(
            self.env.context.get('active_ids', False))
        return current_sesion_id

    comentario = fields.Text(string='Comentario por sobrepasar umbral', required=True)
    current_sesion_id = fields.Many2one(
        'pos.session', string="Ref. de la sesion", default=_default_sesion_id)

    def confirm_comentary(self):
        self.current_sesion_id.get_comentario_sesion()
        return self.current_sesion_id.with_context({'comentario': True}).action_pos_session_validate()
