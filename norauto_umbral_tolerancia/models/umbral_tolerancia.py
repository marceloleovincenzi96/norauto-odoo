# -*- coding: utf-8 -*-

from odoo import models, fields


class UmbralToleranciaCc(models.Model):

    _name = 'umbral.tolerancia.cc'
    _description = "Umbral Tolerancia Cierre de Caja en POS"
    _order = 'limt_tol_dif_cierre_caja desc'

    name = fields.Char(string='Nombre del Umbral de Tolerancia',)

    limt_tol_dif_cierre_caja = fields.Integer(
        string='Límite de Tolerancia',
        help="Límite de tolerancia de las diferencias al cierre de la caja")
