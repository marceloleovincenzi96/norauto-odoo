# -*- coding: utf-8 -*-
{
    'name': "Norauto Umbral de Tolerancia",
    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,
    'author': "Nybble Group",
    'website': "",
    'category': 'Nybble',
    'version': '13.0.0.1',
    'depends': [
        'base',
        'point_of_sale',
        'mail',
        'contacts',
        'grupo_finanzas',
        'pos_payment_method_quantities'
    ],
    'data': [
        'security/user_groups.xml',
        'security/ir.model.access.csv',
        'data/umbral_tolerancia.xml',
        'data/email_template.xml',
        'views/umbral_tolerancia.xml',
        'views/pos_session.xml',
        'views/view_comentario_sesion.xml',
    ],
    'demo': [
    ],
    'installable': True,
}
