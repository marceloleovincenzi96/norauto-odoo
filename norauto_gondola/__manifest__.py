# -*- encoding: utf-8 -*-

{
    'name': 'Norauto Gondolas',
    'version': '13.0.0.1',
    'category': 'Nybble',
    'sequence': 10,
    'summary': 'Sistema de Gondolas Norauto',
    'depends': ['stock', 'operating_unit', 'norauto_rights', 'fields_norauto'],
    'author': 'NybbleGroup',
    'description': """
NorAuto Gondolas
===============
Sistema de Gondolas Norauto

""",
    'data': [
        "views/gondola_view.xml",
        "views/product_view.xml",
        "views/operating_unit_view.xml",
        "data/ir_cron.xml",
        "data/gondola_planos_descripcion.xml",
        "wizards/carga_gondolas.xml",
        "wizards/segundo_conteo.xml",
        "security/ir.model.access.csv",
        "security/security.xml",
        "views/dossier_inventario_general.xml",
        "views/dossier_inventario_parcial.xml",
        "views/tabla_artdos_invgen.xml",
        "views/tabla_artdos_invpar.xml",
        "views/inventario_parcial.xml",
        'views/inventario_general.xml',
        'views/zonas.xml',
        'wizards/confirmar_cierre_inventario_general.xml',
        'wizards/confirmar_cierre_inventario_parcial.xml'

    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
