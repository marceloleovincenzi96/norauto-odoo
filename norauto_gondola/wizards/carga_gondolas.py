# -*- coding: utf-8 -*-

import xlrd
import base64
import datetime
from datetime import date
from odoo import api, fields, models


class CargaMasivaIdoc(models.TransientModel):
    _name = 'norauto.carga.gondola'
    _description = 'Wizard carga gondola'

    archivo = fields.Binary('Archivo')

    def importar_pocisiones(self):
        wb = xlrd.open_workbook(file_contents=base64.decodestring(self.archivo))
        required_fields = ['name']
        productos_no_encontrados = ""
        listas_no_encontradas = ""
        data = []
        for sheet in wb.sheets():
            for row in range(2, sheet.nrows):
                vals = {}
                for cel_num in range(0, sheet.ncols):
                    vals[sheet.cell(1, cel_num).value] = sheet.cell(row, cel_num).value
                data.append(vals)

        ModuloPocision = self.env['norauto.modulo.pocision']
        for item in data:
            plano = self.env['norauto.gondola.plano'].search([('code', '=', self.clean_number(item.get('plano_code')))])
            producto = self.env['product.template'].search([('default_code', '=', self.clean_number(item.get('default_code')))])
            if not producto:
                productos_no_encontrados += ("{} {}\n".format(item['default_code'], item['nombre_producto']))
                continue
            print('producto', item.get('default_code'), item.get('nombre_producto'))
            item.pop('nombre_plano')
            item.pop('nombre_producto')
            item.pop('default_code')
            item.pop('plano_code')
            item.update(product_id=producto.id)
            item.update(plano_id=plano.id)
            ModuloPocision.create(item)

        print(productos_no_encontrados)

    def importar_tiendas(self):
        wb = xlrd.open_workbook(file_contents=base64.decodestring(self.archivo))
        required_fields = ['name']
        data = []
        planos_no_encontrados = ""
        for sheet in wb.sheets():
            for row in range(2, sheet.nrows):
                vals = {}
                for cel_num in range(0, sheet.ncols):
                    vals[sheet.cell(1, cel_num).value] = sheet.cell(row, cel_num).value
                data.append(vals)
        Plano = self.env['norauto.gondola.plano']
        for item in data:
            sucursal = self.env['operating.unit'].search([('code', '=', self.clean_number(item.get('centro_code')))])
            plano = self.env['norauto.gondola.plano'].search([('code', '=', self.clean_number(item.get('plano_code')))])
            if not plano:
                planos_no_encontrados += ("{} {}\n".format(item['plano_code'], item['nombre_plano']))
                continue
            item.pop('nombre_plano')
            item.pop('nombre_gondola')
            item.pop('centro_code')
            item.pop('plano_code')
            plano.write({'sucursal_ids': [(4, sucursal.id)]})
            print('creado', item)
        print(planos_no_encontrados)

    def clean_number(self, val):
        return str(int(float(val)))
