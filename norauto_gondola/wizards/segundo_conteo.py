from odoo import api, fields, models


class DossierSegundoConteoWizard(models.TransientModel):
    _name = 'dossier.segundo.conteo.wizard'
    _description = 'Segundo Conteo Wizard'

    dossier_parcial_id = fields.Many2one(
        comodel_name='norauto.gondola.dossier_inventario_parcial',
        string='Dossier',
        default=lambda self: self._default_dossier_parcial_id())
    producto_ids = fields.Many2many(
        comodel_name='dossier.tabart_par',
        string='Productos',
        default=lambda self: self._default_producto_ids())

    def _default_dossier_parcial_id(self):
        return self.env.context.get('dossier_id', False)

    def _default_producto_ids(self):
        # Tiene que traer los productos en la lista de articulos dossiers que tengan
        # segundo conteo ya habilitado
        if self.dossier_parcial_id:
            return self.dossier_parcial_id.articulos_dossier_ids.filtered(lambda art: art.segundo_conteo_habilitado)

    @api.onchange('dossier_parcial_id')
    def _onchange_dossier_parcial_id(self):
        self.producto_ids = self.dossier_parcial_id.articulos_dossier_ids.filtered(lambda art: art.segundo_conteo_habilitado)

    def habilitar_segundo_conteo(self):
        # tiene que poner las lineas de articulos dossiers en segundo conteo
        self.dossier_parcial_id.articulos_dossier_ids.write({'segundo_conteo_habilitado': False})
        self.producto_ids.write({'segundo_conteo_habilitado': True})
