from odoo import models
from odoo.exceptions import UserError
from datetime import datetime

class ConfirmacionCierreInventarioParcial(models.TransientModel):

    _name = 'norauto.gondola.invpar_confirm'

    def cerrar(self):

        inventario = self.env['norauto.gondolas.invpar'].browse(self.env.context.get('inventario'))

        for record in inventario.dossier_parcial_ids:

            if record.estado != 'Cerrado':

                raise UserError('Tiene dossieres de inventario parcial sin cerrar en la lista. Cierrelos y vuelva a intentar.')
        # TODO: aca pasar dossier por dossier y actualizar las cantidades
        QuantSudo = self.env['stock.quant'].sudo()
        for dossier in inventario.dossier_parcial_ids:
            for linea in dossier.articulos_dossier_ids:
                producto = self.env['product.product'].search([('product_tmpl_id', '=', linea.producto_id.id)])
                # quant = QuantSudo.search([('location_id', '=', dossier.location_id.id), ('product_id', '=', producto.id)])
                if linea.segundo_conteo_habilitado:
                    cantidad = linea.segundo_conteo - linea.cantidad_a_mano
                else:
                    cantidad = linea.primer_conteo - linea.cantidad_a_mano
                # con esta forma me actualiza la cantidad, pero solo para los que ya existen, los que no los salta
                # quant.with_context({'inventory_mode': True}).write({
                #     'inventory_quantity': cantidad
                # })
                # Solo updateamos las cantidades en el caso de que la cantidad sea diferente de 0
                # import pdb; pdb.set_trace()
                if cantidad != 0.00:
                    # print('producto: %s, location: %s, cantidad: %s' % (producto.name, dossier.location_id.name, cantidad))
                    QuantSudo._update_available_quantity(producto, dossier.location_id, cantidad)
        inventario.write({'estado': 'Cerrado', 'cerrado_por': self.env.uid, 'fecha_cierre': datetime.now()})
