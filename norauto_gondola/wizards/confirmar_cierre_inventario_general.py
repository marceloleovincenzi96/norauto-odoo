from odoo import fields, models
from odoo.exceptions import UserError
from datetime import datetime

class ConfirmacionCierreInventarioGeneral(models.TransientModel):

    _name = 'norauto.gondola.invgen_confirm'

    def cerrar(self):
        
        inventario = self.env['norauto.gondolas.invgen'].browse(self.env.context.get('inventario'))

        for record in inventario.dossier_general_ids:

            if record.estado != 'Cerrado':
                
                raise UserError('Tiene dossieres de inventario general sin cerrar en la lista. Cierrelos y vuelva a intentar.')

        inventario.write({'estado': 'Cerrado', 'cerrado_por': self.env.uid, 'fecha_cierre': datetime.now()})
        