# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    nombre_gondola = fields.Char(size=25, string="Nombre de góndola")
    gondola_ids = fields.Many2many(comodel_name='norauto.gondola', string="Góndola")
    plano_ids = fields.Many2many(comodel_name="norauto.gondola.plano", string="Plano")
    nombre_modulo = fields.Char(size=25, string="Nombre de módulo")
    nombre_posicion = fields.Char(size=25, string="Nombre de la posición")
    len_gondola = fields.Integer('Cantidad Gondolas', compute='_compute_len_gondola', store=True)
    
    @api.depends('gondola_ids')
    def _compute_len_gondola(self):
        for rec in self:
            rec['len_gondola'] = len(rec.gondola_ids)

    ##################################################
    # ORM OVERRIDES
    ##################################################
    @api.model
    def create(self, vals):
        res = super(ProductTemplate, self).create(vals)
        # NOTE: si viene de gondola, no se puede crear clase c, 7, 9
        if self.env.context.get('exclude_calss_c7'):
            if res.clase in ('C', '7', '9'):
                raise exceptions.ValidationError("Permiso Denegado: No puede crear articulos clase C, 7 ni 9 desde un plano")
        return res

    ##################################################
    # CONSTRAINS
    ##################################################
    @api.constrains('gondola_ids')
    def _validate_gondola_ids(self):
        if len(self.gondola_ids) > 1:
            raise exceptions.ValidationError("Solo puede asignar una gondola a un producto")

    # @api.model
    # def create(self, vals):
    #     res = super(ProductTemplate, self).create(vals)
    #     # NOTE: si viene de gondola, no se puede crear clase c, 7, 9
    #     if self.env.context.get('exclude_calss_c7'):
    #         if res.clase in ('C', '7', '9'):
    #             raise exceptions.ValidationError("Permiso Denegado: No puede crear articulos clase C, 7 ni 9 desde un plano")
    #     # NOTE: asignar al m2m de la gondola este producto
    #     res.gondola_id.with_context({'no_recurse': True}).write({'product_ids': [(4, res.id)]})
    #     return res

    # def write(self, vals):
    #     # NOTE: desvincular de la gondola preexistente
    #     if self.gondola_id and not self.env.context.get('no_recurse'):
    #         self.gondola_id.with_context({'no_recurse': True}).write({'product_ids': [(3, self.id)]})
    #     res = super(ProductTemplate, self).write(vals)
    #     # NOTE: vincular a la gondola nueva
    #     if not self.env.context.get('no_recurse'):
    #         self.gondola_id.with_context({'no_recurse': True}).write({'product_ids': [(4, self.id)]})
    #     return res
