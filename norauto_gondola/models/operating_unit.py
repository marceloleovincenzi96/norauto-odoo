# -*- coding: utf-8 -*-

from odoo import fields, models


class OperatingUnit(models.Model):
    _inherit = 'operating.unit'

    plano_ids = fields.Many2many(comodel_name='norauto.gondola.plano', string='Planos')
    inventario_parcial = fields.Boolean('Inventario Parcial', default=True, help='Si lo tilda, se generaran inventarios parciales para esta sucursal')
