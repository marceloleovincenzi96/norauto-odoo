from . import gondola
from . import operating_unit
from . import product
from . import dossier
from . import dosier_inventario_general
from . import dossier_inventario_parcial
from . import tabla_artdos
from . import tabla_artdos_invgen
from . import tabla_artdos_invpar
from . import inventario
from . import inventario_general
from . import inventario_parcial
from . import zonas

