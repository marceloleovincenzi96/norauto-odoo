# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models


class NorautoGondola(models.Model):
    _name = 'norauto.gondola'

    name = fields.Char('Nombre', required=True)
    code = fields.Integer('Id Gondola(code)', required=True)
    etiqueta_id = fields.Many2one(comodel_name='norauto.gondola.etiqueta', string='Etiqueta', required=True)
    product_ids = fields.Many2many(comodel_name='product.template', string='Productos')
    plano_ids = fields.One2many(
        comodel_name='norauto.gondola.plano',
        inverse_name='gondola_id',
        string='Planos')
    cantidad_modulos = fields.Integer('Cantidad de módulos', compute='_compute_cantidad_modulos')
    cantidad_pocisiones = fields.Integer('Cantidad de pocisiones', compute='_compute_cantidad_pocisiones')
    operating_unit_id = fields.Many2one('operating.unit', 'Sucursal')

    ##################################################
    # COMPUTE METHODS
    ##################################################
    @api.depends('plano_ids')
    def _compute_cantidad_modulos(self):
        for rec in self:
            rec['cantidad_modulos'] = sum(rec.plano_ids.mapped('cantidad_modulos'))

    @api.depends('plano_ids')
    def _compute_cantidad_pocisiones(self):
        for rec in self:
            rec['cantidad_pocisiones'] = sum(rec.plano_ids.mapped('cantidad_pocisiones'))

    ##################################################
    # ORM OVERRIDES
    ##################################################
    @api.model
    def create(self, vals):
        res = super(NorautoGondola, self).create(vals)
        return res

    def write(self, vals):
        delete_ids = []
        if vals.get('product_ids'):
            old_ids = self.product_ids.ids
            new_ids = vals.get('product_ids')[0][2]
            delete_ids = [rec_id for rec_id in old_ids if rec_id not in new_ids]
            print('--------------------------')
            print(delete_ids)

        res = super(NorautoGondola, self).write(vals)
        for plano in self.plano_ids:
            for pocision in plano.modulo_pocision_ids:
                if pocision.product_id.id in delete_ids:
                    plano.write({'modulo_pocision_ids': [(2, pocision.id)]})
                    # pocision.unlink()
            pass

        return res

    ##################################################
    # OTHER METHODS
    ##################################################
    def archivar_productos_clase_9(self):
        """Metodo para llamarse por cron"""
        productos_clase_9 = self.env['product.template'].search([('clase', '=', '9'), ('active', '=', True)])
        # import pdb; pdb.set_trace()
        products_to_remove = []
        for prod in productos_clase_9:
            product = self.env['product.product'].search([('product_tmpl_id', '=', prod.id)])
            quants = self.env['stock.quant'].search([('product_id', '=', product.id)])
            qty_available = sum(quants.mapped('inventory_quantity'))
            if qty_available <= 0:
                products_to_remove.append(prod)
        ids_to_remove = [p.id for p in products_to_remove]
        modulos = self.env['norauto.modulo.pocision'].search([('product_id', 'in', ids_to_remove)])
        modulos.unlink()
        gondolas = self.env['norauto.gondola'].search([])
        remove_codes = [(3, p.id) for p in products_to_remove]
        for gondola in gondolas:
            gondola.write({'product_ids': remove_codes})
        # Archivar productos
        print('--------------------')
        for p in products_to_remove:
            print(p.name)
            p.write({'active': False})


class NorautoGondolaTienda(models.Model):
    _name = 'norauto.gondola.tienda'
    _rec_name = 'gondola_id'

    gondola_id = fields.Many2one(comodel_name='norauto.gondola', string='Góndola', related='plano_id.gondola_id',
                                 store=True)
    operating_unit_id = fields.Many2one(comodel_name='operating.unit', string='Sucursal')
    sucursal_ids = fields.Many2many(comodel_name='operating.unit', string='Sucursales Plano',
                                    compute='_compute_sucursal_ids', store=True)
    location_id = fields.Many2one(comodel_name='stock.location', string='Location', compute='_compute_location_id')
    plano_id = fields.Many2one(comodel_name='norauto.gondola.plano', string='Plano')
    cantidad_pocisiones = fields.Integer('Pocisiones', related='plano_id.cantidad_pocisiones')
    cantidad_modulos = fields.Integer('Móduclos', related='plano_id.cantidad_modulos')
    code = fields.Integer('Id Góndola(code)', related='gondola_id.code')
    etiqueta_id = fields.Many2one(comodel_name='norauto.gondola.etiqueta', string='Etiqueta',
                                  related='gondola_id.etiqueta_id')
    modulo_pocision_ids = fields.One2many(
        comodel_name='norauto.modulo.pocision',
        inverse_name='plano_id',
        string='Modulo Pocision',
        related='plano_id.modulo_pocision_ids')

    ##################################################
    # COMPUTES
    ##################################################
    # def _compute_operating_unit_id(self):
    #     for rec in self:
    #         rec['operating_unit_id'] = self.env.user.default_operating_unit_id.id

    @api.depends('operating_unit_id')
    def _compute_location_id(self):
        for rec in self:
            location = self.env['stock.location'].search([('operating_unit_id', '=', rec.operating_unit_id.id)],
                                                         limit=1)
            rec['location_id'] = location.id

    @api.depends('plano_id')
    def _compute_sucursal_ids(self):
        for rec in self:
            rec['sucursal_ids'] = rec.plano_id.sucursal_ids


class NorautoGondolaTiendaLinea(models.Model):
    _name = 'norauto.gondola.tienda.linea'
    _rec_name = 'gondola_id'

    plano_id = fields.Many2one(comodel_name='norauto.gondola.plano', string='Plano', required=True)
    operating_unit_id = fields.Many2one('operating.unit', 'Sucursal')
    location_id = fields.Many2one(comodel_name='stock.location', string='Location', compute='_compute_location_id',
                                  store=True)
    gondola_id = fields.Many2one('norauto.gondola', 'Gondola', related='plano_id.gondola_id', store=True)

    modulo_pocision_id = fields.Many2one('norauto.modulo.pocision', 'Modulo Pocision')

    numero_modulo = fields.Integer("Número Módulo", related='modulo_pocision_id.numero_modulo', store=True)
    numero_pocision = fields.Integer("Número Pocisión", related='modulo_pocision_id.numero_pocision')
    cantidad_frentes = fields.Integer('Frentes', related='modulo_pocision_id.cantidad_frentes')
    facing = fields.Integer('Facing', related='modulo_pocision_id.facing')
    product_id = fields.Many2one(comodel_name='product.template', string='Producto',
                                 related='modulo_pocision_id.product_id')
    product_id_clase = fields.Selection(
        selection=[
            ('0', '0'),
            ('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('7', '7'),
            ('C', 'C'),
            ('9', '9')
        ],
        string='Clase',
        related='modulo_pocision_id.product_id.clase',
        store=True)
    familia_id = fields.Char('Familia', related='product_id.familia', store=True)

    qty_available = fields.Integer('Cantidad Disponible', compute='_compute_qty_available')
    valuacion = fields.Float('Valuación', compute='_compute_valuacion')

    ##################################################
    # COMPUTE
    ##################################################
    @api.depends('operating_unit_id')
    def _compute_location_id(self):
        for rec in self:
            location = self.env['stock.location'].search([('operating_unit_id', '=', rec.operating_unit_id.id)],
                                                         limit=1)
            rec['location_id'] = location.id

    @api.depends('product_id')
    def _compute_qty_available(self):
        for rec in self:
            # location = self.env.context.get('location_id')
            if rec.location_id:
                rec['qty_available'] = rec.product_id.with_context({'location': self.location_id.id}).qty_available
            else:
                rec['qty_available'] = 0

    @api.depends('product_id', 'qty_available')
    def _compute_valuacion(self):
        mg_ces = float(self.env['ir.config_parameter'].sudo().get_param('margen_cesion'))
        for rec in self:
            # location = self.env.context.get('location_id')
            if rec.location_id:
                prod = rec.product_id
                if rec.location_id.id == 8:
                    rec['valuacion'] = prod.standard_price * rec.qty_available
                else:
                    rec['valuacion'] = prod.standard_price * (1 + mg_ces) * rec.qty_available
            else:
                rec['valuacion'] = 0


class NorautoGondolaPlanoDescripcion(models.Model):
    _name = 'norauto.gondola.plano.descripcion'

    name = fields.Char('Letra', required=True)
    complete_name = fields.Char('Nombre Completo', required=True)
    plano_ids = fields.One2many(
        comodel_name='norauto.gondola.plano',
        inverse_name='gondola_id',
        string='Planos')


class NorautoGondolaPlano(models.Model):
    _name = 'norauto.gondola.plano'

    name = fields.Char(string='Nombre', required=True)
    code = fields.Integer('ID Plano(código)', required=True)
    descripcion_id = fields.Many2one(
        comodel_name='norauto.gondola.plano.descripcion',
        string='Descripción',
        required=True)
    cantidad_modulos = fields.Integer('Cantidad de módulos', required=True)
    cantidad_pocisiones = fields.Integer('Cantidad de pocisiones', compute='_compute_cantidad_pocisiones', store=True)
    gondola_id = fields.Many2one(
        comodel_name='norauto.gondola',
        string='Gondola',
        required=True)
    modulo_pocision_ids = fields.One2many(
        comodel_name='norauto.modulo.pocision',
        inverse_name='plano_id',
        string='Modulo Pocision')
    busqueda_por_id = fields.Integer('Busqueda por ID de articulo')
    modulo_pocision_filtrados_ids = fields.One2many(
        comodel_name='norauto.modulo.pocision',
        inverse_name='plano_filtrados_id',
        string='Modulo Posicion Filtrados')
    sucursal_ids = fields.Many2many(comodel_name='operating.unit', string='Sucursales')
    gondola_product_ids = fields.Many2many(comodel_name='product.template', string='Productos gondola',
                                           compute='_compute_gondola_product_ids')
    gondola_tienda_id = fields.Many2one(comodel_name='norauto.gondola.tienda', string='Gondola Tienda')

    ##################################################
    # ONCHANGES
    ##################################################

    @api.onchange('gondola_id', 'descripcion_id', 'cantidad_modulos')
    def _onchange_name(self):
        if self.gondola_id and self.descripcion_id and self.cantidad_modulos:
            self.name = "{} {}{}".format(self.gondola_id.name, self.cantidad_modulos, self.descripcion_id.name)

    @api.onchange('busqueda_por_id')
    def _onchange_busqueda(self):
        for rec in self:
            rec.modulo_pocision_filtrados_ids = rec.modulo_pocision_ids.filtered(lambda linea: linea.product_id.idart == rec.busqueda_por_id)

    ##################################################
    # COMPUTES
    ##################################################
    @api.depends('modulo_pocision_ids')
    def _compute_cantidad_pocisiones(self):
        for rec in self:
            rec['cantidad_pocisiones'] = rec.modulo_pocision_ids.search_count([])

    @api.depends("gondola_id")
    def _compute_gondola_product_ids(self):
        for rec in self:
            rec['gondola_product_ids'] = [(6, 0, [p.id for p in rec.gondola_id.product_ids])]

    ##################################################
    # ORM OVERRIDES
    ##################################################
    @api.model
    def create(self, vals):
        res = super(NorautoGondolaPlano, self).create(vals)
        # Crea una gondola.tienda por cada sucurssal
        GondolaTiendaLinea = self.env['norauto.gondola.tienda.linea']
        vals = {'plano_id': res.id}
        for op in res.sucursal_ids:
            vals.update(operating_unit_id=op.id)
            for mp in self.modulo_pocision_ids:
                vals.update(modulo_pocision_id=mp.id)
                gondola_tienda_linea = GondolaTiendaLinea.sudo().create(vals)
        return res

    def write(self, vals):
        # Validación de que la sucursal esté en un solo plano
        if 'sucursal_ids' in vals:
            print('sucursales: %s' % vals['sucursal_ids'])
            sucursales_ids = vals['sucursal_ids'][0][1]
            sucursales = self.env['operating.unit'].browse(sucursales_ids)
            planos_de_gondola = self.search([
                ('gondola_id', '=', self.gondola_id.id),
                ('id', '!=', self.id)
            ])
            for sucursal in sucursales:
                if sucursal in planos_de_gondola.sucursal_ids:
                    raise exceptions.ValidationError('No puede asignar un plano de una misma Gondola a dos tiendas')
        res = super(NorautoGondolaPlano, self).write(vals)

        # Modifica una gondola.tienda por cada sucursal
        GondolaTiendaLinea = self.env['norauto.gondola.tienda.linea']
        lineas_de_plano = GondolaTiendaLinea.sudo().search([('plano_id', '=', self.id)])

        # Si es nueva
        for op in self.sucursal_ids:
            lineas_sucursal = lineas_de_plano.filtered(lambda r: r.operating_unit_id.id == op.id)
            posiciones_en_lineas = lineas_sucursal.mapped('modulo_pocision_id')
            for linea in self.modulo_pocision_ids:
                if linea not in posiciones_en_lineas:
                    gondola_tienda_linea = GondolaTiendaLinea.sudo().create({
                        'plano_id': self.id,
                        'operating_unit_id': op.id,
                        'modulo_pocision_id': linea.id
                    })
        # si se borro la tienda de la lista del plano
        # import pdb; pdb.set_trace()
        for line in lineas_de_plano:
            # print('sucursal: %s, plano: %s, producto: %s' % (
            #     line.operating_unit_id.name, line.plano_id.name, line.product_id.name))
            if line.operating_unit_id not in self.sucursal_ids:
                line.sudo().unlink()
            elif not line.modulo_pocision_id:
                line.sudo().unlink()
        return res

    def unlink(self):
        # borra todas las gondola.tienda
        GondolaTiendaLinea = self.env['norauto.gondola.tienda.linea']
        for rec in self:
            gondolas_tienda_linea = GondolaTiendaLinea.sudo().search([('plano_id', '=', rec.id)])
            for pos in rec.modulo_pocision_ids:
                pos.unlink()
            rec.sucursal_ids = [(3, op.id) for op in rec.sucursal_ids]
            for linea in gondolas_tienda_linea:
                linea.sudo().unlink()
        res = super(NorautoGondolaPlano, self).unlink()
        return res

##################################################
# AUXILIARES
##################################################
def unlink_all_operating_units(self):
    planos = self.env['norauto.gondola.plano'].search([])
    for plano in planos:
        plano.sucursal_ids = [(6, 0, [])]


# class NorautoGondolaModulo(models.Model):
#     _name = 'norauto.gondola.modulo'
#     _rec_name = 'numero'

#     numero = fields.Integer('Número')
#     cantidad_pocisiones = fields.Integer('Cantidad Pocisiones')
#     plano_id = fields.Many2one(comodel_name='norauto.gondola.plano', string='Plano')
#     pocision_ids = fields.One2many(
#         comodel_name='norauto.gondola.pocision',
#         inverse_name='modulo_id',
#         string='Pocisiones')


class NorautoModuloPocision(models.Model):
    _name = 'norauto.modulo.pocision'
    _order = 'numero_modulo asc, numero_pocision asc'

    numero_modulo = fields.Integer("Número Módulo", required=True)
    numero_pocision = fields.Integer("Número Pocisión", required=True)
    cantidad_frentes = fields.Integer('Frentes', required=True)
    facing = fields.Integer('Facing', required=True)
    id_articulo = fields.Integer('ID Articulo', related='product_id.idart')
    product_id = fields.Many2one(comodel_name='product.template', string='Producto', required=True)
    plano_id = fields.Many2one(comodel_name='norauto.gondola.plano', string='Plano', required=True)
    plano_filtrados_id = fields.Many2one(comodel_name='norauto.gondola.plano', string='Plano filtrados', required=True)
    qty_available = fields.Integer('Cantidad Disponible', compute='_compute_qty_available')
    default_code = fields.Char('Código Producto', related='product_id.default_code')
    valuacion = fields.Float('Valuación', compute='_compute_valuacion')

    ##################################################
    # COMPUTE
    ##################################################


    @api.depends('product_id')
    def _compute_qty_available(self):
        for rec in self:
            location = self.env.context.get('location_id')
            if location:
                rec['qty_available'] = rec.product_id.with_context({'location': location}).qty_available
            else:
                rec['qty_available'] = 0

    @api.depends('product_id', 'qty_available')
    def _compute_valuacion(self):
        for rec in self:
            location = self.env.context.get('location_id')
            if location:
                prod = rec.product_id
                mg_ces = float(self.env['ir.config_parameter'].sudo().get_param('margen_cesion'))
                if location == 8:
                    rec['valuacion'] = prod.standard_price * rec.qty_available
                else:
                    rec['valuacion'] = prod.standard_price * (1 + mg_ces) * rec.qty_available
            else:
                rec['valuacion'] = 0

    ##################################################
    # ORM OVERRIDES
    ##################################################
    @api.model
    def create(self, vals):
        res = super(NorautoModuloPocision, self).create(vals)
        if not res.product_id.gondola_ids:
            res.product_id.write({'gondola_ids': [(6, 0, [res.plano_id.gondola_id.id])]})
        return res

    def write(self, vals):
        res = super(NorautoModuloPocision, self).write(vals)
        if len(self.product_id.gondola_ids) == 0:
            self.product_id.write({'gondola_ids': [(6, 0, [self.plano_id.gondola_id.id])]})
        return res

    ##################################################
    # CONSTRAINS
    ##################################################
    @api.constrains('numero_modulo')
    def _validate_numero_modulo(self):
        if self.numero_modulo <= 0:
            raise exceptions.ValidationError("El número de módulo debe ser mayor a 0")
        if self.numero_modulo > self.plano_id.cantidad_modulos:
            raise exceptions.ValidationError(
                "No puede asignar un módulo mayor a la cantidad de modulos definida en el plano")

    @api.constrains('numero_pocision')
    def _validate_numero_pocision(self):
        if self.numero_pocision <= 0:
            raise exceptions.ValidationError("El número de pocisión debe ser mayor a 0")
        modulos = self.plano_id.modulo_pocision_ids.filtered(
            lambda r: r.id != self.id and r.numero_modulo == self.numero_modulo)
        pocisiones = modulos.mapped('numero_pocision')
        if self.numero_pocision in pocisiones:
            raise exceptions.ValidationError(
                "La pocision %s del modulo %s ya está ocupada" % (self.numero_pocision, self.numero_modulo))

    @api.constrains('cantidad_frentes')
    def _validate_cantidad_frentes(self):
        if self.cantidad_frentes <= 0:
            raise exceptions.ValidationError("La cantidad de frentes debe ser mayor a 0")

    # @api.constrains('facing')
    # def _validate_facing(self):
    #     if self.facing <= 0:
    #         raise exceptions.ValidationError("El facing debe ser mayor a 0")

    @api.constrains('product_id')
    def _validate_product_id(self):
        if self.product_id and self.product_id.gondola_ids:
            gondola_id = self.product_id.gondola_ids[0]
            if gondola_id != self.plano_id.gondola_id:
                raise exceptions.ValidationError("No puede asignar el producto de otra gondola (%s)" % gondola_id.name)

    @api.constrains('product_id')
    def _validate_unique_product_id(self):
        planos = self.plano_id
        posiciones = planos.mapped('modulo_pocision_ids')
        articulos = []
        for pos in posiciones:
            for prod in pos.product_id:
                articulos.append(prod.id)
        for art in articulos:
            if self.product_id.id == art:
                index = articulos.index(art)
                articulos.pop(index)
                break
        if self.product_id.id in articulos:
                data = (str(self.plano_id.name), str(self.product_id.name))
                raise exceptions.ValidationError("No puede crear una posición en el plano %s ya que el producto %s existe actualmente en este plano" % data)


class NorautoGondolaEtiqueta(models.Model):
    _name = 'norauto.gondola.etiqueta'

    name = fields.Char('Nombre')
