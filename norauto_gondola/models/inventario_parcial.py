from odoo import api, fields, models
from odoo.exceptions import UserError

class InventarioParcial(models.Model):

    _inherit = 'norauto.gondolas.inventario'
    _name = 'norauto.gondolas.invpar'

    dossier_parcial_ids = fields.Many2many(
        string='Dossieres',
        comodel_name='norauto.gondola.dossier_inventario_parcial',
        relation='dossier_inventario_parcial_rel'
    )
    operating_unit_id = fields.Many2one(
        comodel_name='operating.unit',
        string='Sucursal',
        required=True,
        default=lambda self: self._default_operating_unit_id())
    location_id = fields.Many2one('stock.location', 'Ubicacion de Stock')
    transmitido_a_tiendas = fields.Boolean('Transmitido a tiendas')

    def _default_operating_unit_id(self):
        return self.env.ref('operating_unit.main_operating_unit').id

    @api.onchange('operating_unit_id')
    def _onchange_operating_unit_id(self):
        self.location_id = False
        if self.operating_unit_id:
            location = self.env['stock.location'].search([('operating_unit_id', '=', self.operating_unit_id.id)], limit=1)
            self.location_id = location

    def pasar_a_en_curso(self):
        if not (self.env.user.has_group('norauto_rights.group_supervisor_tienda') or self.env.user.has_group('norauto_rights.group_supervisor_tienda')):
            raise UserError('Permiso denegado. Usted no es supervisor o director de tienda.')
        self.write({'estado': 'En curso'})

    def crear_para_sucursales(self):
        sucursales = self.env['operating.unit'].search([('inventario_parcial', '=', True)])
        sucursales -= self.operating_unit_id
        for sucursal in sucursales:
            location = self.env['stock.location'].search([('operating_unit_id', '=', sucursal.id)], limit=1)
            for dossier in self.dossier_parcial_ids:
                if not dossier.transmitido_a_tiendas:
                    dossier.crear_para_sucursales()
            dossieres = self.dossier_parcial_ids.ids
            dossier_parcial_ids = self.env['norauto.gondola.dossier_inventario_parcial'].search([
                ('template_id', 'in', dossieres),
                ('operating_unit_id', '=', sucursal.id)
            ])
            default = {
                'operating_unit_id': sucursal.id,
                'dossier_parcial_ids': [(6, 0, dossier_parcial_ids.ids)]
            }
            if location:
                default['location_id'] = location.id
            self.write({'transmitido_a_tiendas': True})
            self.copy(default)

    def pasar_a_cerrado(self):
        if not (self.env.user.has_group('norauto_rights.group_supervisor_tienda') or self.env.user.has_group('norauto_rights.group_director_tienda')):
            raise UserError('Permiso denegado. Usted no es supervisor o director de tienda.')
        return {
            'name': 'Cerrar inventario parcial',
            'type': 'ir.actions.act_window',
            'res_model': 'norauto.gondola.invpar_confirm',
            'view_mode': 'form',
            'target': 'new',
            "context": {'inventario': self.id}
        }
