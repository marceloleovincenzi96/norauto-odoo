from odoo import api, fields, models

class TablaArticulosDossierParcial(models.Model):

    _inherit = 'dossier.tabart'
    _name = 'dossier.tabart_par'

    dossier_parcial_id = fields.Many2one(
        string='Dossier parcial',
        comodel_name='norauto.gondola.dossier_inventario_parcial',
        ondelete='restrict',
    )

    
    dossier_parcial_filtro_id = fields.Many2one(
        string='Dossier parcial filtro',
        comodel_name='norauto.gondola.dossier_inventario_parcial',
        ondelete='restrict', 
    )
    

    location_id = fields.Many2one(
        comodel_name='stock.location',
        string='Deposito',
        related='dossier_parcial_id.location_id')

    cantidad_a_mano = fields.Float(
        string='Cantidad a mano',
        # compute='_compute_cantidad_a_mano',
        related=None,
        default=lambda self: self._default_cantidad_a_mano()
    )

    @api.model
    def create(self, vals):
        res = super(TablaArticulosDossierParcial, self).create(vals)
        if res.location_id:
            res.cantidad_a_mano = res.producto_id.with_context({'location': res.location_id.id}).qty_available
        return res

    def _default_cantidad_a_mano(self):
        if self.location_id:
            return self.producto_id.with_context({'location': self.location_id.id}).qty_available

    @api.onchange('location_id')
    def _onchange_cantidad_a_mano(self):
        self.cantidad_a_mano = self.producto_id.with_context({'location': self.location_id.id}).qty_available


    @api.depends('location_id')
    def _compute_cantidad_a_mano(self):
        for rec in self:
            rec['cantidad_a_mano'] = rec.producto_id.with_context({'location': rec.location_id.id}).qty_available
