from odoo import fields, models, api
from odoo.exceptions import ValidationError, UserError


class DossierInventarioParcial(models.Model):

    _inherit = 'norauto.gondola.dossier'
    _name = 'norauto.gondola.dossier_inventario_parcial'

    familias_id = fields.Many2many('product.category', string="Familias", required=True)

    barcode_count = fields.Char('Barcode')

    codigo_articulo = fields.Integer('Codigo articulo')

    articulos_dossier_ids = fields.One2many(
        string='Articulos',
        comodel_name='dossier.tabart_par',
        inverse_name='dossier_parcial_id'
    )

    articulos_filtrados_ids = fields.One2many(
        string='Articulos filtrados',
        comodel_name='dossier.tabart_par',
        inverse_name='dossier_parcial_filtro_id',
        domain=['|', '|', ('producto_id.clase','!=','9'), ('producto_id.tipoart_uno','=','Almacenable'), ('producto_id.tipoart_dos','=','Temporal')]
    )
    

    inventario_parcial_id = fields.Many2one(
        string='Inventario parcial',
        comodel_name='norauto.gondolas.invpar'
    )
    operating_unit_id = fields.Many2one(
        comodel_name='operating.unit',
        string='Sucursal',
        required=True,
        default=lambda self: self._default_operating_unit_id())
    location_id = fields.Many2one('stock.location', 'Ubicacion de Stock')
    transmitido_a_tiendas = fields.Boolean('Transmitido a tiendas')
    para_sucursal = fields.Boolean('Creado para sucursal')
    template = fields.Boolean('Es template', default=True)
    template_id = fields.Many2one('norauto.gondola.dossier_inventario_parcial', 'Template')
    busqueda = fields.Char('Busqueda')

    
    @api.onchange('busqueda')
    def _onchange_busqueda(self):
        
        for rec in self:

            rec.articulos_filtrados_ids = rec.articulos_dossier_ids.filtered(lambda art: art.product_barcode == rec.busqueda)
    
    

    def _default_operating_unit_id(self):
        return self.env.ref('operating_unit.main_operating_unit').id

    @api.onchange('operating_unit_id')
    def _onchange_operating_unit_id(self):
        self.location_id = False
        if self.operating_unit_id:
            location = self.env['stock.location'].search([('operating_unit_id', '=', self.operating_unit_id.id)], limit=1)
            self.location_id = location

    @api.onchange('barcode_count')
    def _onchange_barcode_count(self):
        if self.barcode_count:
            for art in self.articulos_dossier_ids:
                if self.barcode_count == art.product_barcode:
                    art.primer_conteo += 1
                    break
            self.barcode_count = ''

    @api.onchange('codigo_articulo')
    def _onchange_codigo_articulo(self):
        if self.codigo_articulo:
            for art in self.articulos_dossier_ids:
                if self.codigo_articulo == art.codart:
                    art.primer_conteo += 1
                    break
            self.codigo_articulo = 0

    def dar_de_alta(self):
        if self.env.user.has_group('norauto_rights.group_supervisor_tienda'):
            self.chequear_familias()
            self.write({'estado': 'En curso'})
        else:
            raise ValidationError(
                'Permiso denegado. Solo supervisor de tienda puede dar de alta un dossier para inventario parcial.')


    def pasar_a_finalizado(self):
        if self.env.user.has_group('norauto_rights.group_director_tienda') or self.env.user.has_group('norauto_rights.group_supervisor_tienda'):
            self.write({'estado': 'Cerrado'})
        else:
            raise ValidationError(
                'Permiso denegado. Solo supervisor de tienda puede pasar a finalizado un dossier para inventario parcial.')

    @api.model
    def create(self, vals):

        res = super(DossierInventarioParcial, self).create(vals)

        for familia in res.familias_id:

            # articulos_familia = self.env['product.product'].search(
            articulos_familia = self.get_articulos_familia(familia.id)

            for producto in articulos_familia:
                self.env['dossier.tabart_par'].create({
                    'producto_id': producto.id,
                    'primer_conteo': 0,
                    'segundo_conteo': 0,
                    'dossier_parcial_id': res.id
                })

        return res

    def write(self, vals):
        # if not self.env.user.has_group('norauto_rights.group_supervisor_tienda'):
        #     raise ValidationError(
        #         'Permiso denegado. Solo supervisor de tienda puede modificar dossiers para inventario parcial.')

        # if self.estado != 'A inventariar':
        #     raise ValidationError(
        #         'Permiso denegado. Los dossieres solo son editables en estado A Inventariar.')

        res = super(DossierInventarioParcial, self).write(vals)

        if vals.get('familias_id'):
            self.cargar_articulos()

        return res

    def chequear_familias(self):
        dossiers_parciales = self.env['norauto.gondola.dossier_inventario_parcial'].search([
            ('id', '!=', self.id),
            ('operating_unit_id', '=', self.operating_unit_id.id)
        ])

        for record in dossiers_parciales:
            if record.estado != 'Cerrado':
                for familia in self.familias_id:
                    if familia in record.familias_id:
                        raise UserError(
                            "Se encontraron dossieres sin cerrar que contienen alguna de las familias que agregó. Cierrelos o modifique las familias incluidas en el dossier actual y vuelva a intentar.")

    def crear_para_sucursales(self):
        sucursales = self.env['operating.unit'].search([('inventario_parcial', '=', True)])
        sucursales -= self.operating_unit_id
        for sucursal in sucursales:
            print('sucursal: %s' % sucursal.name)
            location = self.env['stock.location'].sudo().search([('operating_unit_id', '=', sucursal.id)], limit=1)
            default = {
                'operating_unit_id': sucursal.id,
                'para_sucursal': True,
                'template': False,
                'template_id': self.id
            }
            if location:
                default['location_id'] = location.id
            self.write({'transmitido_a_tiendas': True})
            self.sudo().copy(default)

    def cargar_articulos(self):

        self.articulos_dossier_ids = [(2, art.id) for art in self.articulos_dossier_ids]

        for familia in self.familias_id:
            articulos_familia = self.get_articulos_familia(familia.id)
            tabart_par = []
            for producto in articulos_familia:
                tabart_par.append((0, 0, {
                    'producto_id': producto.id,
                    'primer_conteo': 0,
                    'segundo_conteo': 0
                }))
            self.articulos_dossier_ids = tabart_par

    def get_articulos_familia(self, familia_id):
        return self.env['product.template'].search([
            ('categ_id', '=', familia_id),
            ('tipoart_uno', '=', 'Almacenable'),
            ('tipoart_dos', '=', '1')
        ])
