from odoo import fields, models


class TablaArticulosDossierGeneral(models.Model):

    _inherit = 'dossier.tabart'
    _name = 'dossier.tabart_gen'

    dossier_general_id = fields.Many2one(
        string='Dossier',
        comodel_name='norauto.gondola.dossier_inventario_general',
        ondelete='restrict',
    )
