from odoo import fields, models, api
from odoo.exceptions import UserError


class InventarioGeneral(models.Model):

    _inherit = 'norauto.gondolas.inventario'
    _name = 'norauto.gondolas.invgen'

    dossier_general_ids = fields.Many2many(
        string='Dossieres',
        comodel_name='norauto.gondola.dossier_inventario_general',
        relation='norauto_inv_dossier',
        
        #inverse_name='inventario_general_id'
    )

    zona_ids = fields.One2many(
        string='Zonas',
        comodel_name='norauto.gondolas.zonas',
        inverse_name='inventario_general_id',
    )


    @api.onchange('zona_ids')
    def _onchange_zona_ids(self):

        item_ids = self.zona_ids.dossier_general_ids.ids

        self.dossier_general_ids = [(6,0,tuple(item_ids))]

    
    def pasar_a_en_curso(self):

        if not (self.env.user.has_group('norauto_rights.group_supervisor_tienda') or self.env.user.has_group('norauto_rights.group_supervisor_tienda')):

            raise UserError ('Permiso denegado. Usted no es supervisor o director de tienda.')
        
        if not self.zona_ids:

            raise UserError ('No se puede continuar si no hay zonas asignadas.')

        if not self.dossier_general_ids:

            raise UserError ('No se puede continuar si no hay dossieres asignados.')

        self.validar_restriccion_inventario()
                    
        self.write({'estado': 'En curso'})


    def pasar_a_cerrado(self):

        if not (self.env.user.has_group('norauto_rights.group_supervisor_tienda') or self.env.user.has_group('norauto_rights.group_supervisor_tienda')):

            raise UserError ('Permiso denegado. Usted no es supervisor o director de tienda.')

        return {
            'name': 'Cerrar inventario general',
            'type': 'ir.actions.act_window',
            'res_model': 'norauto.gondola.invgen_confirm',
            'view_mode': 'form',
            'target': 'new',
            "context": {'inventario': self.id}
        }


    def validar_restriccion_inventario(self):

        gondolas = self.env['norauto.gondola'].search([])
        dossieres = self.env['norauto.gondola.dossier_inventario_general'].search([('estado', '!=', 'Cerrado')])
        lista_gondolas = []

        for gondola in gondolas:

            for j, dossier in enumerate(dossieres, start=1):

                if gondola in dossier.gondolas_id:
                    break
                
                if j == len(dossieres):
                    lista_gondolas.append(gondola.name)
            
        if len(lista_gondolas) > 0:

            raise UserError('Hay gondolas que no estan asignadas a ningun dossier activo: '+ str(lista_gondolas))


