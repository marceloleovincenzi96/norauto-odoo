from odoo import fields, models
from abc import abstractmethod

class Inventario(models.AbstractModel):

    _name = 'norauto.gondolas.inventario'
    _description = 'Ajuste de inventario en gondolas'

    name = fields.Char('Nombre del inventario')

    estado = fields.Selection(
        string='Estado',
        selection=[('Borrador', 'Borrador'), ('En curso', 'En curso'), ('Cerrado', 'Cerrado')],
        default='Borrador'
    )

    fecha_conteo = fields.Date(
        string='Fecha de conteo'
    )

    cerrado_por = fields.Many2one(
        string='Cerrado por',
        comodel_name='res.users',
        readonly=True
    )

    fecha_cierre = fields.Datetime(
        string='Fecha y hora de cierre',
        readonly=True,
    )

    @abstractmethod
    def pasar_a_en_curso(self):
        pass

    @abstractmethod
    def pasar_a_cerrado(self):
        pass
