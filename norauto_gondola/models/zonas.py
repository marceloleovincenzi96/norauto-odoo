from odoo import fields, models, api
from odoo.exceptions import ValidationError



class Zonas(models.Model):

    _name = 'norauto.gondolas.zonas'
    _description = 'Zona'
    _rec_name = 'nombre_zona'

    
    tienda_id = fields.Many2one(
        string='Tienda',
        comodel_name='operating.unit',
        required=True
        
    )

    
    codigo_zona = fields.Integer(
        string='Codigo de zona',
        required=True
        
    )

    
    nombre_zona = fields.Char(
        string='Nombre de zona',
        required=True
        
    )

    
    dossier_general_ids = fields.Many2many(
        string='Dossieres de inventario general asignados',
        comodel_name='norauto.gondola.dossier_inventario_general',
        relation='norauto_gondola_zondos',
        required=True
        
    )

    
    inventario_general_id = fields.Many2one(
        string='Inventario general',
        comodel_name='norauto.gondolas.invgen',
        ondelete='restrict', 
        
    )

    @api.onchange('inventario_general_id')
    def _onchange_inventario_general(self):

        if self.inventario_general_id:

            dossieres_ids = self.inventario_general_id.dossier_general_ids.ids

            self.dossier_general_ids = [(6,0,tuple(dossieres_ids))]


    @api.model
    def create(self, vals):
        
        res = super(Zonas, self).create(vals)

        dossieres_inv_ids = res.inventario_general_id.dossier_general_ids.ids

        dossieres_ids = res.dossier_general_ids.ids

        count = 0

        for dos_id in dossieres_ids:

            if not dos_id in dossieres_inv_ids:

                count += 1

        if count > 0:

            raise ValidationError('Los dossieres de inventario general deben estar en la tabla de dossieres generales del inventario general asignado.')
        
        return res


    def write(self, vals):

        dossieres_inv_ids = self.inventario_general_id.dossier_general_ids.ids

        dossieres_ids = self.dossier_general_ids.ids

        count = 0

        for dos_id in dossieres_ids:

            if not dos_id in dossieres_inv_ids:

                count += 1

        if count > 0:

            raise ValidationError('Los dossieres de inventario general deben estar en la tabla de dossieres generales del inventario general asignado.')

        return super(Zonas, self).write(vals)

    
    
    