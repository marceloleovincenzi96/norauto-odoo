from odoo import fields, models, api
from odoo.exceptions import UserError, ValidationError


class DossierInventarioGeneral(models.Model):

    _inherit = 'norauto.gondola.dossier'
    _name = 'norauto.gondola.dossier_inventario_general'

    
    codigo_barra_a_buscar = fields.Char(
        string='Buscar por codigo de barra',
    )
    

    articulos_dossier_ids = fields.One2many(
        string='Articulos',
        comodel_name='dossier.tabart_gen',
        inverse_name='dossier_general_id'
    )

    inventario_general_id = fields.Many2one(
        string='Inventario general',
        comodel_name='norauto.gondolas.invgen'
    )
    

    def dar_de_alta(self):

        if self.env.user.has_group('norauto_rights.group_CCA'):
            #self.chequear_gondolas()
            self.write({'estado': 'En curso'})

        else:
            raise ValidationError(
                'Permiso denegado. Solo usuarios de CCA pueden dar de alta un dossier para inventario general.')


    def pasar_a_finalizado(self):
        if self.env.user.has_group('norauto_rights.group_director_tienda') or self.env.user.has_group('norauto_rights.group_supervisor_tienda'):
            self.write({'estado': 'Cerrado'})

        else:
            raise ValidationError(
                'Permiso denegado. Solo usuarios de CCA pueden cerrar un dossier para inventario general.')

    @api.model
    def create(self, vals):

        res = super(DossierInventarioGeneral, self).create(vals)

        for gondola in res.gondolas_id:

            for producto in gondola.product_ids:

                self.env['dossier.tabart_gen'].create({
                    'producto_id': producto.id,
                    'primer_conteo': 0,
                    'segundo_conteo': 0,
                    'dossier_general_id': res.id
                })

        return res

    def write(self, vals):

        res = super(DossierInventarioGeneral, self).write(vals)

        if not self.env.user.has_group('norauto_rights.group_CCA'):
            raise ValidationError(
                'Permiso denegado. Solo usuarios de CCA pueden modificar dossiers para inventario general.')

        if self.estado != 'A inventariar':
            raise ValidationError('Permiso denegado. Los dossieres solo son editables en estado A Inventariar.') 

        return res


    @api.onchange('codigo_barra_a_buscar')
    def cargar_articulos(self):

        result = []

        for record in self:
            if record.codigo_barra_a_buscar:
                product = self.env['product.template'].search([('barcode', '=',  record.codigo_barra_a_buscar)])
                import pdb; pdb.set_trace()
                if product:
                    ya_agregado = []

                    if not record.articulos_dossier_ids:
                        result.append((0, 0, {'producto_id': product.id, 'primer_conteo': 1, 'segundo_conteo': 0}))
                        ya_agregado.append(product.id)

                    else: 
                        for item in record.articulos_dossier_ids:
                            if item.producto_id.id == product.id:
                                if not item.segundo_conteo_habilitado:
                                    result.append((1, item.id, {'primer_conteo': item.primer_conteo + 1}))
                                else:
                                    result.append((1, item.id, {'segundo_conteo': item.segundo_conteo + 1}))

                            elif product.id not in record.articulos_dossier_ids.mapped('producto_id').ids and product.id not in ya_agregado:
                                if not item.segundo_conteo_habilitado:
                                    result.append((0, 0, {'producto_id': product.id, 'primer_conteo': 1, 'segundo_conteo': 0}))
                                else:
                                    result.append((0, 0, {'producto_id': product.id, 'primer_conteo': 0, 'segundo_conteo': 1}))
                                ya_agregado.append(product.id)

            record.articulos_dossier_ids = result
