from odoo import fields, models, api
from odoo.exceptions import ValidationError

class TablaArticulosDossier(models.AbstractModel):

    _name = 'dossier.tabart'

    producto_id = fields.Many2one(
        string='Producto',
        comodel_name='product.template',
        ondelete='restrict'
    )
    product_barcode = fields.Char('Barcode', related='producto_id.barcode', store=True)

    cantidad_a_mano = fields.Float(
        string='Cantidad a mano',
        related='producto_id.qty_available',
        default=lambda self: self._obtener_cantidad_tienda()
    )

    primer_conteo = fields.Float('Primer conteo', readonly=True)

    segundo_conteo = fields.Float('Segundo conteo', default=-1, readonly=True)

    segundo_conteo_habilitado = fields.Boolean('Segundo Conteo Habilitado')

    diferencia_uno = fields.Float('Diferencia 1', compute='_obtener_diferencia_uno')

    diferencia_dos = fields.Float('Diferencia 2', compute='_obtener_diferencia_dos')

    codigo_barra_primer_conteo = fields.Char('Codigo de barra (Primer Conteo)')

    codigo_barra_segundo_conteo = fields.Char('Codigo de barra (Segundo Conteo)')

    restar_cantidades = fields.Boolean(
        string='Restar Cantidades?',
        help='Si esta activo, cuando se introduzca un codigo de barras para algun conteo, decrementara la cantidad de ese articulo en 1.'
    )

    agregar_por_codigo = fields.Boolean(
        string='Agregar por codigo de articulo?',
        help='Si esta activo, se agregaran cantidades de articulos solo a traves de su codigo de articulo.'
    )

    
    codart_primer_conteo = fields.Integer(
        string='Codigo de articulo (Primer conteo)',
    )

    codart_segundo_conteo = fields.Integer(
        string='Codigo de articulo (Segundo conteo)',
    )
    

    cantidad_manual_primer_conteo = fields.Integer(
        string='Introducir cantidad manual (Primer Conteo)',
    )

    cantidad_manual_segundo_conteo = fields.Integer(
        string='Introducir cantidad manual (Segundo Conteo)',
    )
    

    def _obtener_cantidad_tienda(self):

        almacen = self.env['stock.warehouse'].search([('operating_unit_id', '=', self.env.user.default_operating_unit_id.id)])

        return self.producto_id.with_context({'warehouse_id': almacen.id}).qty_available


    @api.depends('primer_conteo')
    def _obtener_diferencia_uno(self):

        for record in self:

            record.diferencia_uno = record.primer_conteo - record.cantidad_a_mano


    @api.depends('segundo_conteo')
    def _obtener_diferencia_dos(self):

        for record in self:
            record.diferencia_dos = record.segundo_conteo - record.primer_conteo

    @api.onchange('codigo_barra_primer_conteo')
    def cargar_cantidad_primer_conteo(self):
        if self.codigo_barra_primer_conteo:
            if not (self.producto_id.barcode == self.codigo_barra_primer_conteo):

                raise ValidationError('El codigo de barras ingresado no se corresponde con este articulo.')
            
            if not self.restar_cantidades:
                self.primer_conteo = self.primer_conteo + 1
            else:
                if (self.primer_conteo - 1 >= 0):
                    self.primer_conteo = self.primer_conteo - 1
                else:
                    raise ValidationError ('No se puede quitar mas articulos del primer conteo.')

            self.codigo_barra_primer_conteo = ''

    @api.onchange('codart_primer_conteo')
    def cargar_cantidad_codart_primer_conteo(self):
        if self.codart_primer_conteo:
            if not (self.producto_id.idart == self.codart_primer_conteo):

                raise ValidationError('El codigo de barras ingresado no se corresponde con este articulo.')
            
            if not self.restar_cantidades:
                self.primer_conteo = self.primer_conteo + 1
            else:
                if (self.primer_conteo - 1 >= 0):
                    self.primer_conteo = self.primer_conteo - 1
                else:
                    raise ValidationError ('No se puede quitar mas articulos del primer conteo.')

            self.codart_primer_conteo = 0

    @api.onchange('codigo_barra_segundo_conteo')
    def cargar_cantidad_segundo_conteo(self):
        if self.codigo_barra_segundo_conteo:
            if not (self.producto_id.barcode == self.codigo_barra_segundo_conteo):

                raise ValidationError('El codigo de barras ingresado no se corresponde con este articulo.')
            
            if not self.restar_cantidades:
                self.segundo_conteo = self.segundo_conteo + 1
            else:
                if (self.segundo_conteo - 1 >= 0):
                    self.segundo_conteo = self.segundo_conteo - 1
                else:
                    raise ValidationError ('No se puede quitar mas articulos del segundo conteo.')

            self.codigo_barra_segundo_conteo = ''

    @api.onchange('codart_segundo_conteo')
    def cargar_cantidad_codart_segundo_conteo(self):
        if self.codart_segundo_conteo:
            if not (self.producto_id.idart == self.codart_segundo_conteo):

                raise ValidationError('El codigo de barras ingresado no se corresponde con este articulo.')
            
            if not self.restar_cantidades:
                self.segundo_conteo = self.segundo_conteo + 1
            else:
                if (self.segundo_conteo - 1 >= 0):
                    self.segundo_conteo = self.segundo_conteo - 1
                else:
                    raise ValidationError ('No se puede quitar mas articulos del segundo conteo.')

            self.codart_segundo_conteo = 0


    @api.onchange('cantidad_manual_primer_conteo')
    def cargar_cantidad_a_mano_primer_conteo(self):
        if self.cantidad_manual_primer_conteo:
            
            if (self.primer_conteo + self.cantidad_manual_primer_conteo > 0): #se pueden introducir cantidades negativas en el campo
                self.primer_conteo += self.cantidad_manual_primer_conteo
            else:
                raise ValidationError ('No se puede quitar mas articulos del primer conteo.')
 
            self.cantidad_manual_primer_conteo = 0

    @api.onchange('cantidad_manual_segundo_conteo')
    def cargar_cantidad_a_mano_segundo_conteo(self):
        if self.cantidad_manual_segundo_conteo:
            
            if (self.segundo_conteo + self.cantidad_manual_segundo_conteo > 0):
                self.segundo_conteo += self.cantidad_manual_segundo_conteo
            else:
                raise ValidationError ('No se puede quitar mas articulos del segundo conteo.')

            self.cantidad_manual_primer_conteo = 0