from odoo import fields, models
from abc import abstractmethod

class Dossier(models.AbstractModel):

    _name = 'norauto.gondola.dossier'

    _rec_name = 'name'

    name = fields.Char('Nombre', required=True)

    fecha_conteo = fields.Date('Fecha de conteo', required=True)

    estado = fields.Selection(
        selection=[
            ('A inventariar', 'A inventariar'),
            ('En curso', 'En curso'),
            ('Cerrado', 'Cerrado')
        ],
        string="Estado",
        required=True,
        default='A inventariar')

    @abstractmethod
    def dar_de_alta(self):
        pass

    @abstractmethod
    def pasar_a_finalizado(self):
        pass

    @abstractmethod
    def cargar_articulos(self):
        pass
