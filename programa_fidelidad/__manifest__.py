
# -*- encoding: utf-8 -*-

{
    'name': 'Programa de Fidelidad - Norauto',
    'version': '1.0',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'Programa de fildelidad',
    'depends': [
        'base',
        'mail',
        'norauto_rights',
        'vouchers_pos'
    ],
    'author': 'NybbleGroup',
    'description': """
NorAuto Programa de Fildelidad
===============

""",
    'data': [
        "views/res_partner_view.xml",
        "views/gift_voucher_view.xml",
        "data/cron.xml",
        "data/mail_template.xml",
        "data/voucher.xml"
    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
