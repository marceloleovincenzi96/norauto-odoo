# -*- coding:utf-8 -*-
from odoo import exceptions, fields, models
import random


class GiftVoucherPos(models.Model):
    _inherit = 'gift.voucher.pos'

    voucher_cumple = fields.Boolean('Voucher de cumpleaños')

    def unlink(self):
        for rec in self:
            if rec.id == self.env.ref('programa_fidelidad.voucher_cumpleanos').id:
                raise exceptions.UserError('No se puede borrar el voucher para cumpleaños')
        res = super(GiftVoucherPos, self).unlink()
        return res

    def _get_random_value(self):
        for voucher in self:
            value = random.randint(voucher.min_value, voucher.max_value)
            return int(round(value, -1))
