# -*- coding:utf-8 -*-
from odoo import api, fields, models
# from odoo.tools.misc import DEFAULT_SERVER_DATE_FORMAT
import datetime


class ResPartner(models.Model):
    _inherit = 'res.partner'

    fecha_nacimiento = fields.Date('Fecha Nacimiento')
    cupon_cumpleanios = fields.Many2one('gift.coupon.pos', 'Cupón de cumpleaños')

    @api.model
    def _cron_cumpleanos(self):
        clientes_individuos = self.search([
            ('active', '=', True),
            ('customer_rank', '>', 1),
            ('company_type', '=', 'person'),
            ('fecha_nacimiento', '!=', False),
            ('email', '!=', False)
        ])
        clientes_individuos.enviar_promocion_cumpleanos()

    def enviar_promocion_cumpleanos(self):
        # voucher_cumple = self.env['gift.voucher.pos'].search([('voucher_cumple', '=', True)], limit=1) 	programa_fidelidad.voucher_cumpleanos
        voucher_cumple = self.env.ref('programa_fidelidad.voucher_cumpleanos')

        # NOTE: extender la validez del voucher por un dia, todos los dias asi no vence
        expiry_date_plus_one = voucher_cumple.expiry_date + datetime.timedelta(days=1)
        voucher_cumple.write({'expiry_date': expiry_date_plus_one})
        # import pdb; pdb.set_trace()
        today = datetime.date.today()
        if voucher_cumple:
            for cliente in self:
                if cliente.cumpleanos_manana():
                    fecha_cupon = datetime.date(year=today.year, month=cliente.fecha_nacimiento.month, day=cliente.fecha_nacimiento.day)
                    cupon = self.env['gift.coupon.pos'].sudo().create({
                        'name': '%s - Cumpleaños %s' % (cliente.name, today.year),
                        'start_date': fecha_cupon,
                        'end_date': fecha_cupon,
                        'total_avail': 1,
                        'limit': 1,
                        'partner_id': cliente.id,
                        'type': 'fixed',
                        'voucher': voucher_cumple.id,
                        'voucher_val': voucher_cumple._get_random_value()
                    })
                    cliente.sudo().write({'cupon_cumpleanios': cupon.id})
                    # TODO: send email
                    cliente.send_cupon_cumpleanos()
                elif cliente.cupon_cumpleanios:
                    if not cliente.cumpleanos():
                        cliente.sudo().write({'cupon_cumpleanios': False})

    def cumpleanos_manana(self):
        today = datetime.date.today()
        cumpleano = self.fecha_nacimiento
        dia_anterior = cumpleano - datetime.timedelta(days=1)
        res = False
        if today.month == dia_anterior.month and today.day == dia_anterior.day:
            print('Mañana es el cumpleaños')
            res = True
        return res

    def cumpleanos(self):
        today = datetime.date.today()
        cumpleano = self.fecha_nacimiento
        res = False
        if today.month == cumpleano.month and today.day == cumpleano.day:
            res = True
        return res

    def send_cupon_cumpleanos(self):
        print('Aca tenes tu cupon!!!')
        template = self.env.ref('programa_fidelidad.promocion_cumpleanos_email')
        for partner in self:
            # template.with_context(context).send_mail(partner.id)
            template.send_mail(partner.id, force_send=True, raise_exception=True)
        pass
