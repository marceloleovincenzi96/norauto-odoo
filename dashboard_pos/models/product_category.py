from odoo import models, fields

class ProductCategory(models.Model):

    _inherit='product.category'
    _name = _inherit
    
    in_pos_dashboard = fields.Boolean(
        string='En POS Dashboard',
        store=True
    )
    