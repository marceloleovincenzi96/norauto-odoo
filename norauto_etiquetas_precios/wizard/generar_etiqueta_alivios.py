from odoo import models, fields


class GenerarEtiquetaAlivios(models.TransientModel):
    _name = 'norauto_etiquetas.generar_etiqueta_alivios'
    _description = 'Genera etiqueta de alivios'

    
    fecha_desde = fields.Datetime(
        string='Fecha desde',
        default=fields.Datetime.now,
    )

    fecha_hasta = fields.Datetime(
        string='Fecha hasta',
        default=fields.Datetime.now,
    )


    def generar_etiqueta(self):

        self.ensure_one()

        self = self.with_context(
            active_model=self._name, active_id=self.id, active_ids=self.ids)

        return self.env.ref('alivios.action_etiqueta_alivios').report_action(self)
    

    
