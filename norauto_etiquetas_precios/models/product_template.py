from odoo import models, fields, api, _


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    unimes = fields.Char(string="Unidad de medida etiqueta")
    nbrunimes = fields.Float(string="nbrunimes")
    precio_uni_med = fields.Float(string="Precio por Unidad de Medida", store=True, compute='_compute_precio_uni_med')

    @api.depends('nbrunimes', 'precio_con_impuestos')
    def _compute_precio_uni_med(self):
        for producto in self:
            if producto.nbrunimes:
                producto.precio_uni_med = round(producto.precio_con_impuestos/producto.nbrunimes, 2)
