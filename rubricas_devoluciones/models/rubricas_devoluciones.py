from odoo import models, fields

class RubricaDevolucion(models.Model):

    _name = 'rubrica.devolucion'
    _description = 'Rubrica de devolucion'
    _rec_name = 'name'

    name = fields.Char('Nombre de la rubrica')

    descripcion = fields.Char('Descripcion')

