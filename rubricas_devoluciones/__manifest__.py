# -*- coding: utf-8 -*-
{
	'name': 'Rubricas de devoluciones',
	'version': '13.0.0.0.0',
    'summary': """Rubricas de devoluciones""",
	'category': 'Tools',
	'author': "Nybble Group",
    'website': "https://www.nybblegroup.com/",
	'depends': ['base', 'sale_management'],
	'data': [

		'data/rubricas_devoluciones.xml',
        'views/rubricas_devoluciones.xml',
        'security/ir.model.access.csv'
		
	],
	'demo': [],
	'css': [],
	'installable': True,
	'auto_install': False,
	'application': True,
}
