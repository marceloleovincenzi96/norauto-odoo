
# -*- encoding: utf-8 -*-

{
    'name': 'Norauto MRP',
    'version': '13.0.1.0',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'Norauto MRP',
    'depends': [
        'fields_norauto',
        'norauto_rights',
        'purchase',
        'stock',
        'norauto_mrp_calc',
    ],
    'author': 'NybbleGroup',
    'description': """
NorAuto MRP
===============
Gestión de propuestas automaticas de compra
""",
    'data': [
        'data/ir_cron.xml',
        'data/ir_sequence.xml',
        'views/mrp_coeficiente.xml',
        'views/mrp_necesidades_tienda.xml',
        'views/mrp_propuesta.xml',
        'views/mrp_historico_venta.xml',
        'views/mrp_parametro.xml',
        'views/product_template.xml',
        'security/ir.model.access.csv'
    ],
    'qweb': [],
    'demo': [],
    'external_dependencies': {"python": ['odoorpc']},
    'installable': True,
    'application': False,
    'auto_install': False,
}
