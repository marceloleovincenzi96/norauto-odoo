# -*- coding: utf-8 -*-

from odoo import models, fields


class Partner(models.Model):
    _inherit = 'res.partner'

    mrp_propuesta_ids = fields.One2many('mrp.propuesta', 'proveedor_id', string='Propuestas')
