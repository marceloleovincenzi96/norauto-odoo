# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.exceptions import ValidationError, UserError


class MrpNecesidadTienda(models.Model):
    _name = 'mrp.necesidad.tienda'

    def _get_default_stock_picking_type_id(self):
        almacen = self.env['stock.warehouse'].search([('operating_unit_id', '=', self.operating_unit_id.id)], limit=1)
        picking_type = self.env['stock.picking.type'].search([('warehouse_id', '=', almacen.id), ('code', '=', 'internal')])
        if picking_type:
            return picking_type.id

    def _get_default_stock_location_id(self):
        return self.env.ref('stock.stock_location_stock')

    name = fields.Char('Nombre')
    stock_picking_type_id = fields.Many2one(
        comodel_name='stock.picking.type',
        string='Tipo de albarán',
        default=lambda self: self._get_default_stock_picking_type_id())
    stock_location_id = fields.Many2one(
        comodel_name='stock.location',
        string='Ubicacion de origen',
        default=lambda self: self._get_default_stock_location_id())
    stock_location_dest_id = fields.Many2one(
        comodel_name='stock.location',
        string='Ubicacion de destino',
        related='stock_picking_type_id.default_location_dest_id')
    semana = fields.Char('Semana')
    operating_unit_id = fields.Many2one('operating.unit', 'Sucursal')
    fecha = fields.Datetime('Fecha', default=fields.Datetime.now)
    comentario = fields.Text('Comentario')
    necesidad_linea_ids = fields.One2many(
        comodel_name='mrp.necesidad.linea',
        inverse_name='necesidad_id',
        string='Lineas'
    )
    state = fields.Selection([
        ('borrador', 'Borrador'),
        ('aprobado', 'Aprobado'),
    ], string='Estado', default='borrador')

    def action_aprobar(self):
        self.ensure_one()
        if self.user_has_groups('norauto_rights.group_director_tienda') or self.user_has_groups(
                'norauto_rights.group_supervisor_tienda'):
            carrier = self.env['delivery.carrier'].search([], limit=1)
            self.env['stock.picking'].create({
                'name': self.name,
                'state': 'draft',
                'location_id': self.stock_location_id.id,
                'location_dest_id': self.stock_location_dest_id.id,
                'carrier_id': carrier.id,
                'picking_type_id': self.stock_picking_type_id.id,
                'scheduled_date': self.fecha,
            })
            for linea in self.necesidad_linea_ids:
                transferencia = self.env['stock.picking'].search([('name', '=', self.name)], limit=1)
                self.env['stock.move'].create({
                    'name': transferencia.id,
                    'date': self.fecha,
                    'location_id': self.stock_location_id.id,
                    'location_dest_id': self.stock_location_dest_id.id,
                    'procure_method': 'make_to_stock',
                    'picking_id': transferencia.id,
                    'product_id': linea.product_id.id,
                    'product_uom': linea.product_id.uom_id.id,
                    'product_uom_qty': float(linea.cantidad_pedida),
                })
            self.write({'state': 'aprobado'})
        else:
            raise ValidationError(
                'Permiso denegado. Solo el supervisor de tienda o el director de tienda pueden aprobar una necesidad de tienda.')
    #
    # def action_borrador(self):
    #     self.ensure_one()
    #     if self.user_has_groups('norauto_rights.group_director_tienda') or self.user_has_groups(
    #             'norauto_rights.group_supervisor_tienda'):
    #         self.write({'state': 'borrador'})
    #     else:
    #         raise ValidationError(
    #             'Permiso denegado. Solo el supervisor de tienda o el director de tienda pueden pasar a borrador una necesidad de tienda.')

    def compute_necesidades(self):
        # Obtenemos la lista de las necesidades en estado borrador
        necesidades_list = self.env['mrp.necesidad.tienda'].search([('state', '=', 'borrador')])
        # Iteramos por cada necesidad y por cada una de sus lineas
        for necesidad in necesidades_list:

            oper_unit = necesidad.operating_unit_id.mapped('plano_ids')
            print("Oper unit1", oper_unit)
            # oper_units = self.env['norauto.gondola.plano'].search([('','=',)])
            # planos = self.env['norauto.gondola.plano'].search([('sucursal_ids','in',oper_unit.id)])
            # print("planos",planos)
            for nec_linea in necesidad.necesidad_linea_ids:
                """
                    Obtenemos los valores del producto:
                    Cantidad propuesta, cantidad en transito, cantidad en stock
                    y el facing del producto.
                    
                    Hacemos múltiples validaciones para verificar que la 
                    necesidad sea realmente requerida o no.
                """
                print("============================================")
                print("============================================")
                print("Necesidad", necesidad)
                print("Producto ID", nec_linea.product_id)
                print("Producto Nombre", nec_linea.product_id.name)
                cant_prod_in_transit = 0
                print("Cantidad a pedir VIEJA", nec_linea.cantidad_propuesta)
                transferencias = self.env['stock.picking'].search([('state', '=', 'in_transit')])
                stock_moves = transferencias.mapped('move_ids_without_package')
                for linea in stock_moves:
                    if linea.product_id == nec_linea.product_id:
                        cant_prod_in_transit += linea.product_id.product_uom_qty
                print("Cantidad en TRANSITO", cant_prod_in_transit)
                cant_prod_in_stock = nec_linea.product_id.qty_available
                print("Cantidad en STOCK", cant_prod_in_stock)
                gondola_prod = nec_linea.product_id.gondola_ids.id
                facing_prod = necesidad.operating_unit_id.mapped(
                    'plano_ids').filtered(
                    lambda fol: fol.gondola_id.id == gondola_prod).mapped(
                    'modulo_pocision_ids').filtered(lambda
                                                        fol: fol.product_id.id == nec_linea.product_id.id).facing
                print("Facing producto", facing_prod)
                cant_product_total = cant_prod_in_transit + cant_prod_in_stock
                print("Cantidad TOTAL en stock", cant_product_total)
                if cant_product_total >= facing_prod and nec_linea.cantidad_propuesta <= 0:
                    # Se elimina linea por ser una necesidad vacia y hay
                    # suficiente en stock
                    print("Entro a CASE 1")
                    necesidad.write({'necesidad_linea_ids': [
                        (2, nec_linea.id)
                    ]})
                    # nec_linea.unlink()
                elif cant_product_total < facing_prod and nec_linea.cantidad_propuesta <= 0:
                    # Se modifica la cantidad a pedir ya que aunque la
                    # necesidad sea 0, el stock es menor que el facing
                    print("Entro a CASE 2")
                    cant_a_pedir = facing_prod - cant_product_total
                    necesidad.write({'necesidad_linea_ids': [
                        (1, nec_linea.id, {
                            'cantidad_propuesta': cant_a_pedir,
                            'cantidad_pedida': cant_a_pedir,
                        })]
                    })
                    print("Cantidad a pedir NUEVA", nec_linea.cantidad_propuesta)
                elif cant_product_total < facing_prod and nec_linea.cantidad_propuesta > 0:
                    # Se modifica la cantidad a pedir cuando el stock actual
                    # es menor al facing y aparte hay una necesidad
                    print("Entro a CASE 3")
                    cant_a_pedir = facing_prod - cant_product_total + nec_linea.cantidad_propuesta
                    necesidad.write({'necesidad_linea_ids': [
                        (1, nec_linea.id, {
                            'cantidad_propuesta': cant_a_pedir,
                            'cantidad_pedida': cant_a_pedir,
                        })]
                    })
                    print("Cantidad a pedir NUEVA", nec_linea.cantidad_propuesta)
                elif cant_product_total > facing_prod and nec_linea.cantidad_propuesta > 0:
                    # Se modifica la cantidad a pedir cuando el stock actual
                    # es mayor al facing y aparte hay una necesidad
                    print("Entro a CASE 4")
                    cant_disponible = cant_product_total - facing_prod
                    if cant_disponible >= nec_linea.cantidad_propuesta:
                        # Hay sobrestock y se borra la linea
                        print("Entro a CASE 5")
                        necesidad.write({'necesidad_linea_ids': [
                            (2, nec_linea.id)
                        ]})
                    else:
                        # Se modifica la cantidad necesitada para cumplir el
                        # facing luego de las transferencias
                        print("Entro a CASE 6")
                        cant_a_pedir = nec_linea.cantidad_propuesta - cant_disponible
                        necesidad.write({'necesidad_linea_ids': [
                            (1, nec_linea.id, {
                                'cantidad_propuesta': cant_a_pedir,
                                'cantidad_pedida': cant_a_pedir,
                            })]
                        })
                        print("Cantidad a pedir NUEVA",
                              nec_linea.cantidad_propuesta)
                else:
                    print("Entro al ELSE")
                    pass
            if not necesidad.necesidad_linea_ids:
                necesidad.unlink()

    @api.model
    def create(self, vals):
        if vals.get('operating_unit_id'):
            almacen = self.env['stock.warehouse'].search([('operating_unit_id', '=', vals.get('operating_unit_id'))], limit=1)
            picking_type = self.env['stock.picking.type'].search([('warehouse_id', '=', almacen.id), ('code', '=', 'internal')])
            if picking_type:
                vals['stock_picking_type_id'] = picking_type.id
        vals['name'] = self.env['ir.sequence'].next_by_code('necesidad.tienda')
        res = super(MrpNecesidadTienda, self).create(vals)
        return res


class MrpNecesidLinea(models.Model):
    _name = 'mrp.necesidad.linea'

    product_id = fields.Many2one('product.template', 'Producto')
    fecha = fields.Datetime('Fecha', related='necesidad_id.fecha')
    cantidad_propuesta = fields.Integer('Cantidad Propuesta', readonly=True)
    cantidad_pedida = fields.Integer('Cantidad Pedida')
    operating_unit_id = fields.Many2one(
        comodel_name='operating.unit',
        string='Sucursal',
        related='necesidad_id.operating_unit_id',
        store=True)
    semana = fields.Char(
        string='Semana',
        related='necesidad_id.semana',
        store=True)
    necesidad_id = fields.Many2one('mrp.necesidad.tienda', 'Necesidad')
