# -*- coding: utf-8 -*-

from odoo import fields, models


class MrpCoeficiente(models.Model):
    _name = 'mrp.coeficiente'
    _rec_name = 'operating_unit_id'

    operating_unit_id = fields.Many2one('operating.unit', 'Sucursal')
    fecha_1 = fields.Datetime("Fecha 1er Semestre")
    fecha_2 = fields.Datetime("Fecha 2do Semestre")
