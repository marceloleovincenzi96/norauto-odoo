# -*- coding: utf-8 -*-

from odoo import fields, models


class MrpHitoricoVenta(models.Model):
    _name = 'mrp.historico.venta'

    operating_unit_id = fields.Many2one('operating.unit', 'Sucursal')
    product_id = fields.Many2one('product.template', 'Producto')
    product_code = fields.Integer('Código Artículo',
                                  related='product_id.idart', store='True')
    product_name = fields.Char('Nombre Artículo', related='product_id.name',
                               store='True')
    fecha = fields.Date('Fecha')
    mes = fields.Integer('Mes')
    anio = fields.Integer('Año')
    cantidad = fields.Integer('Cantidad')
    tipo = fields.Char('Tipo')

    def _agregar_registro(self, product_id, sucursal, fecha):
        """Llamar a esta funcion desde sale.order, pos.order, o account.move para crear nuevos registros de ventas
        """
        pass

    def get_historicos(self, unidad_operativa_id=False, producto_id=False):
        domain = []
        if unidad_operativa_id:
            unidad_operativa = self.env['operating.unit'].search(
                [('code', '=', unidad_operativa_id)], limit=1)
            domain.append(('operating_unit_id', '=', unidad_operativa.id))
        if producto_id:
            producto_id = self.env['product.template'].search(
                [('default_code', '=', producto_id)])
            domain.append(('product_id', '=', producto_id.id))
            # domain.append(('product_code', '=', producto_id))
        historicos = self.search(domain, order="fecha asc")
        dicto = {'FECHA': [], 'PRODUCTO': [], 'VENTAS': []}
        for h in historicos:
            dicto['FECHA'].append(h.fecha)
            dicto['PRODUCTO'].append(h.product_id.default_code)
            dicto['VENTAS'].append(h.cantidad)
        # coso = pd.DataFrame(data=dicto)
        # print(coso)
        return dicto
