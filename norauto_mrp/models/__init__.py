from . import mrp_coeficientes
from . import mrp_necesidades_tienda
from . import mrp_historico_ventas
from . import mrp_parametros
from . import mrp_propuesta
from . import res_partner
from . import product_template
