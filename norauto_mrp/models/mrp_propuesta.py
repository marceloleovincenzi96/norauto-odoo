# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.exceptions import ValidationError, UserError
import odoorpc
import datetime


class MrpPropuesta(models.Model):
    _name = 'mrp.propuesta'

    name = fields.Char('Nombre')
    state = fields.Selection(
        selection=[
            ('borrador', 'Borrador'),
            ('aprobado', 'Aprobado'),
        ], string='Estado', default='borrador')
    proveedor_id = fields.Many2one('res.partner', 'Proveedor')
    operating_unit_id = fields.Many2one('operating.unit', 'Sucursal')
    purchase_order_id = fields.Many2one('purchase.order',
                                        'Solicitud de compra')
    fecha = fields.Datetime('Fecha', default=fields.Datetime.now)
    grupo_comprador_id = fields.Many2one('stock.grupocomp', 'Grupo comprador')
    fecha_aprobacion = fields.Datetime('Fecha Aprobación', readonly=True)
    stock_picking_type_id = fields.Many2one('stock.picking.type',
                                            'Tipo de albarán')
    payment_term_id = fields.Many2one('account.payment.term', 'Plazos de pago')
    propuesta_linea_ids = fields.One2many(
        comodel_name='mrp.propuesta.linea',
        inverse_name='propuesta_id',
        string='Lineas de propuesta'
    )

    def action_aprobar(self):
        self.ensure_one()
        if self.user_has_groups(
                'norauto_rights.group_director_tienda') or self.user_has_groups(
                'norauto_rights.group_supervisor_tienda'):
            self.env['purchase.order'].create({
                'name': self.name,
                'date_order': self.fecha,
                'state': 'draft',
                'partner_id': self.proveedor_id.id,
                'creado_por_tienda': True,
                'payment_term_id': self.payment_term_id.id,
                # 'picking_type_id': self.stock_picking_type_id.id,
            })
            orden_compra = self.env['purchase.order'].search(
                [('name', '=', self.name)], limit=1)
            self.purchase_order_id = orden_compra.id
            self.fecha_aprobacion = datetime.datetime.now()
            for linea in self.propuesta_linea_ids:
                self.env['purchase.order.line'].create({
                    'name': orden_compra.name,
                    'product_id': linea.product_id.id,
                    'product_qty': float(linea.product_qty_pedida),
                    'product_uom': linea.product_id.uom_id.id,
                    'date_planned': self.fecha,
                    'price_unit': linea.product_price,
                    'order_id': orden_compra.id,
                    'taxes_id': linea.tax_ids.ids,
                    'price_subtotal': linea.subtotal_modificado
                })
            self.write({'state': 'aprobado'})
        else:
            raise ValidationError(
                'Permiso denegado. Solo el supervisor de tienda o el director de tienda pueden aprobar una necesidad de tienda.')

    def generar_modelos_mrp(self, unit_oper):
        data = self._get_data()
        # Sobreescribimos el diccionario que se trae get data y cambiar el valor
        data.update({"operating_units": [unit_oper]})
        odoo = self._get_conexion()
        if 'norauto.mrp.calc' in odoo.env:
            Calc = odoo.env['norauto.mrp.calc']
            Calc.generar_modelos_mrp([], data)

    def get_propuestas(self, unit_oper):
        print('Inicio de get_propuestas')
        data = self._get_data()
        # Sobreescribimos el diccionario que se trae get data y cambiar el valor
        data.update({"operating_units": [unit_oper]})
        odoo = self._get_conexion()
        if 'norauto.mrp.calc' in odoo.env:
            Calc = odoo.env['norauto.mrp.calc']
            response = Calc.get_prediccion([], data)
            MrpNecesidadTienda = self.env['mrp.necesidad.tienda']
            MrpPropuestaTienda = self.env['mrp.propuesta']
            OperatingUnit = self.env['operating.unit']
            ProductTemplate = self.env['product.template']
            for op_code, propuesta in response.items():
                print("OP", op_code)
                operating_unit = OperatingUnit.search([('code', '=', op_code)], limit=1)
                productos_nec_sem_1 = []
                productos_nec_sem_2 = []
                sem1 = datetime.datetime.now().date()
                sem2 = sem1 + datetime.timedelta(weeks=1)
                sem3 = sem1 + datetime.timedelta(weeks=2)
                sem4 = sem1 + datetime.timedelta(weeks=3)
                for p in propuesta:
                    print("~~~~~~~~~~~PRODUCTO NUEVO~~~~~~~~~~~")
                    print("Preddicción", p)
                    productos = ProductTemplate.search([('default_code', '=', p['product_code'])])
                    if productos:
                        for product in productos:
                            productos_nec_sem_1.append((0, 0, {
                                'product_id': product.id,
                                'cantidad_propuesta': p['prediccion'][0],
                                'cantidad_pedida': p['prediccion'][0]
                            }))
                            productos_nec_sem_2.append((0, 0, {
                                'product_id': product.id,
                                'cantidad_propuesta': p['prediccion'][1],
                                'cantidad_pedida': p['prediccion'][1]
                            }))
                            print("=========================================")
                            print("producto origen:", product.name)
                            # import pdb; pdb.set_trace()
                            if product.default_seller:
                                print("No se modifica")
                                pass
                            else:
                                product.get_default_seller()
                                print("Se modifica producto def selller")
                            print("=========================================")
                            prod_product = self.env['product.product'].search(
                                [('default_code', '=', product.default_code)],limit=1)
                            grupo_comp = prod_product.grupo_comprador.id
                            print("producto:", prod_product.name)
                            print("grupo:", prod_product.grupo_comprador.number)
                            seq = self.env['ir.sequence'].next_by_code(
                                'mrp.propuesta')
                            proveedor_pref = prod_product.default_seller.name
                            prop_proveedor_sem3 = self.env['mrp.propuesta'].search([('proveedor_id', '=', proveedor_pref.id),('grupo_comprador_id', '=', grupo_comp), ('fecha', '=',  sem3)], limit=1)
                            prop_proveedor_sem4 = self.env['mrp.propuesta'].search([('proveedor_id', '=', proveedor_pref.id),('grupo_comprador_id', '=', grupo_comp), ('fecha', '=',  sem4)], limit=1)
                            productos_prop_sem3 = self.env['mrp.propuesta.linea'].search([('product_id.default_code', '=', prod_product.default_code),('grupo_comprador_id', '=', grupo_comp), ('fecha', '=',  sem3)], limit=1)
                            productos_prop_sem4 = self.env['mrp.propuesta.linea'].search([('product_id.default_code', '=', prod_product.default_code),('grupo_comprador_id', '=', grupo_comp), ('fecha', '=',  sem4)], limit=1)
                            print("PROPUESTA PROVEEDOR SEM3", prop_proveedor_sem3.name)
                            print("PROPUESTA PROVEEDOR SEM4", prop_proveedor_sem4.name)
                            if prop_proveedor_sem3:
                                if productos_prop_sem3:
                                    cant_old = productos_prop_sem3.product_qty_proposed
                                    prop_proveedor_sem3.write({'propuesta_linea_ids': [
                                        (1, productos_prop_sem3.id, {
                                            'product_qty_proposed': cant_old+p['prediccion'][2],
                                            'product_qty_pedida': cant_old+p['prediccion'][2],
                                        })]
                                    })
                                else:
                                    linea = self.env['mrp.propuesta.linea'].create({
                                        'product_id': prod_product.id,
                                        'grupo_comprador_id': grupo_comp,
                                        'product_qty_proposed': p['prediccion'][2],
                                        'product_qty_pedida': p['prediccion'][2],
                                        'product_price': prod_product.default_seller.price,
                                        'proveedor_id': proveedor_pref.id,
                                        'tax_ids': prod_product.taxes_id,
                                        'propuesta_id': prop_proveedor_sem3.id
                                    })
                                    print("IF Linea Prop Name", linea.propuesta_id.name)
                            else:
                                propuesta_creada_sem3 = MrpPropuestaTienda.create({
                                    'name': "PROP-" + str(prod_product.grupo_comprador.number) + "-" + str(seq),
                                    'operating_unit_id': operating_unit.id,
                                    'proveedor_id': proveedor_pref.id,
                                    'grupo_comprador_id': grupo_comp,
                                    'fecha': sem3
                                })
                                linea_sem3 = self.env['mrp.propuesta.linea'].create({
                                    'product_id': prod_product.id,
                                    'grupo_comprador_id': grupo_comp,
                                    'product_qty_proposed': p['prediccion'][2],
                                    'product_qty_pedida': p['prediccion'][2],
                                    'fecha': sem3,
                                    'product_price': prod_product.default_seller.price,
                                    'proveedor_id': proveedor_pref.id,
                                    'tax_ids': prod_product.taxes_id,
                                    'propuesta_id': propuesta_creada_sem3.id
                                })
                                print("Linea Prop Name", linea_sem3.propuesta_id.name)

                            if prop_proveedor_sem4:
                                if productos_prop_sem4:
                                    cant_old = productos_prop_sem4.product_qty_proposed
                                    prop_proveedor_sem3.write({'propuesta_linea_ids': [
                                        (1, productos_prop_sem4.id, {
                                            'product_qty_proposed': cant_old+p['prediccion'][3],
                                            'product_qty_pedida': cant_old+p['prediccion'][3],
                                        })]
                                    })
                                else:
                                    linea = self.env['mrp.propuesta.linea'].create({
                                        'product_id': prod_product.id,
                                        'grupo_comprador_id': grupo_comp,
                                        'product_qty_proposed': p['prediccion'][3],
                                        'product_qty_pedida': p['prediccion'][3],
                                        'product_price': prod_product.default_seller.price,
                                        'proveedor_id': proveedor_pref.id,
                                        'tax_ids': prod_product.taxes_id,
                                        'propuesta_id': prop_proveedor_sem3.id
                                    })
                                    print("IF Linea Prop Name", linea.propuesta_id.name)
                            else:
                                seq = self.env['ir.sequence'].next_by_code(
                                    'mrp.propuesta')
                                propuesta_creada_sem4 = MrpPropuestaTienda.create({
                                    'name': "PROP-" + str(prod_product.grupo_comprador.number) + "-" + str(seq),
                                    'operating_unit_id': operating_unit.id,
                                    'proveedor_id': proveedor_pref.id,
                                    'grupo_comprador_id': grupo_comp,
                                    'fecha': sem4
                                })
                                linea_sem4 = self.env['mrp.propuesta.linea'].create({
                                    'product_id': prod_product.id,
                                    'grupo_comprador_id': grupo_comp,
                                    'product_qty_proposed': p['prediccion'][3],
                                    'product_qty_pedida': p['prediccion'][3],
                                    'fecha': sem4,
                                    'product_price': prod_product.default_seller.price,
                                    'proveedor_id': proveedor_pref.id,
                                    'tax_ids': prod_product.taxes_id,
                                    'propuesta_id': propuesta_creada_sem4.id
                                })
                                print("Linea Prop Name", linea_sem4.propuesta_id.name)

                MrpNecesidadTienda.create({
                    'operating_unit_id': operating_unit.id,
                    'necesidad_linea_ids': productos_nec_sem_1,
                    'fecha': sem1
                })
                MrpNecesidadTienda.create({
                    'operating_unit_id': operating_unit.id,
                    'necesidad_linea_ids': productos_nec_sem_2,
                    'fecha': sem2
                })

    def _get_data(self):
        operating_units = self.env['operating.unit'].search([])
        operating_unit_codes = operating_units.filtered(lambda x: x.code not in ['DC01', '01']).mapped('code')
        products = self.env['product.template'].search([
            ('tipoart_uno', 'in', ['Almacenable', 'Consumible']),
            ('active', '=', True),
            ('default_code', '!=', '0'),
            ('default_code', '!=', False),
            ('clase', '!=', '9'),
            ('clase', '!=', 'C'),
            ('categ_id', '!=', 1)
        ])
        products_codes = products.mapped('default_code')
        data = {
            'operating_units': operating_unit_codes,
            'products_codes': products_codes
        }
        return data

    def _get_conexion(self):
        db_name = 'norauto'
        # db_name = 'norauto-3108'
        user = 'admin'
        psw = 'Devoo9060!!'
        # Prepare the connection to the server
        # odoo = odoorpc.ODOO('localhost', port=8069)
        odoo = odoorpc.ODOO('200.10.108.182', port=80)
        # setea el timeout a una hora
        odoo.config['timeout'] = 36000
        odoo.login(db_name, user, psw)
        return odoo


class MrpPropuestaLinea(models.Model):
    _name = 'mrp.propuesta.linea'

    name = fields.Char('Nombre')
    fecha = fields.Datetime('Fecha', related='propuesta_id.fecha')
    product_id = fields.Many2one('product.product', 'Producto')
    product_price = fields.Float('Precio unitario')
    product_qty_proposed = fields.Integer('Cantidad propuesta', readonly=True)
    product_qty_pedida = fields.Integer('Cantidad pedida')
    diferencia_qty = fields.Integer('Diferencia',
                                    compute='_compute_diferencia')
    tax_ids = fields.Many2many(comodel_name='account.tax', string='Impuestos',
                               related='product_id.taxes_id')
    grupo_comprador_id = fields.Many2one('stock.grupocomp', 'Grupo comprador',
                                         related='product_id.grupo_comprador')
    subtotal_presupuesto = fields.Float('Subtotal presupuesto', store=True,
                                        compute='_compute_subtotal_presupuesto')
    subtotal_modificado = fields.Float('Subtotal modificado', store=True,
                                       compute='_compute_subtotal_modificado')
    proveedor_id = fields.Many2one(
        comodel_name='res.partner',
        string='Proveedor',
        related='propuesta_id.proveedor_id',
        store=True
    )
    operating_unit_id = fields.Many2one(
        comodel_name='operating.unit',
        string='Proveedor',
        related='propuesta_id.operating_unit_id',
        store=True
    )
    propuesta_id = fields.Many2one('mrp.propuesta', 'Propuesta')

    @api.depends('product_qty_proposed', 'product_qty_pedida')
    def _compute_diferencia(self):
        for rec in self:
            rec['diferencia_qty'] = rec.product_qty_proposed - rec.product_qty_pedida

    @api.depends('product_qty_pedida', 'product_id')
    def _compute_subtotal_presupuesto(self):
        for rec in self:
            rec['subtotal_presupuesto'] = rec.product_qty_proposed * rec.product_id.standard_price

    @api.depends('product_qty_pedida', 'product_price')
    def _compute_subtotal_modificado(self):
        for rec in self:
            rec['subtotal_modificado'] = rec.product_qty_pedida * rec.product_price
