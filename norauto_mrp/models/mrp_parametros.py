# -*- coding: utf-8 -*-

from odoo import fields, models


class MrpParametro(models.Model):
    _name = 'mrp.parametro'

    operating_unit_id = fields.Many2one('operating.unit', 'Sucursal')
    producto = fields.Integer('Código Producto')
    fecha = fields.Datetime("Fecha")
    param_p = fields.Float('p')
    param_d = fields.Float('d')
    param_q = fields.Float('q')
    bias = fields.Float('Bias')
    rmse = fields.Float('RMSE')
    demora = fields.Float('Demora')
