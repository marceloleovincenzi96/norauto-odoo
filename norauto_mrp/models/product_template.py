# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    def get_default_seller(self):
        for rec in self:
            if rec.seller_ids:
                rec.default_seller = rec.seller_ids[0]
            else:
                rec.default_seller = self.env['product.supplierinfo'].create({
                    'name': self.env['res.partner'].search([('codigo_proveedor','=', '1100005')], limit=1).id,
                    'product_tmpl_id': rec.id,
                })

    default_seller = fields.Many2one(string="Proveedor preferente",
                                     comodel_name="product.supplierinfo",)



