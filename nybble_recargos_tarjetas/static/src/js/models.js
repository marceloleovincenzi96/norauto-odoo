odoo.define('pos_payment_ref.models', function (require) {
    "use strict";

    var models = require('point_of_sale.models');

    models.load_fields("pos.payment.method", "pos_payment_ref");
    /* esto arreglo la cosa del cache :v */
    models.load_fields("pos.payment", ["payment_tar", "payment_ref", "payment_cuot"]);

    models.load_models({
        model: 'tarjetas.tarjeta',
        fields: ['id', 'name'],
        domain: [],
        loaded: function (self, tarjetas) {
            self.tarjetas = tarjetas;
        }
    });

    models.load_models({
        model: 'tarjetas.cuotas',
        fields: ['id', 'tarjeta_id', 'qty', 'interes'],
        domain: [],
        loaded: function (self, cuotas) {
            self.cuotas = cuotas;
        }
    });


    var paymentline_super = models.Paymentline.prototype;
    models.Paymentline = models.Paymentline.extend({
        /* This fucking shit right here is th fucking shit that's breaking everything >:| */
        init_from_JSON: function (json) {
            paymentline_super.init_from_JSON.apply(this, arguments);
            this.payment_tar = json.payment_tar;
            this.payment_ref = json.payment_ref;
            this.payment_cuot = json.payment_cuot;
            // NOTE: variable json doesn't have fucking pyament_cuot nor tar nor ref
            // console.log(this.payment_ref)
            // console.log(this.payment_tar)
            // document.write(this.payment_ref);
            // document.write(this.payment_tar);
            // document.write(this.payment_cuot);
        },
        export_as_JSON: function () {
            return _.extend(paymentline_super.export_as_JSON.apply(this, arguments), {
                payment_tar: this.payment_tar,
                payment_ref: this.payment_ref,
                payment_cuot: this.payment_cuot,
            });
        },
    });

    var order_super = models.Order.prototype;
    models.Order = models.Order.extend({
        add_paymentline_with_details: function (cashregister, infos) {
            this.assert_editable();

            var newPaymentline = new models.Paymentline({}, { order: this, payment_method: cashregister, pos: this.pos });
            $.extend(newPaymentline, infos);

            if (cashregister.type !== 'cash' || this.pos.config.iface_precompute_cash) {
                newPaymentline.set_amount(Math.max(this.get_due(), 0));
            }
            this.paymentlines.add(newPaymentline);
            this.select_paymentline(newPaymentline);
        },

        update_paymentline_with_details: function (paymentline, infos) {
            this.assert_editable();
            $.extend(paymentline, infos);
            this.select_paymentline(paymentline);
        },
    });

    return models;
});