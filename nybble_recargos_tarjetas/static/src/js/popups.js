odoo.define('pos_payment_ref.popups', function (require) {
    "use strict";

    var PopupWidget = require('point_of_sale.popups');
    var gui = require('point_of_sale.gui');
    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');

    screens.ReceiptScreenWidget.include({
        get_receipt_render_env: function () {
            var receipt_env = this._super();
            var order = this.pos.get_order();
            receipt_env['interes_total'] = order.interes_total;
            return receipt_env;
        }
    });


    var PaymentInfoWidget = PopupWidget.extend({
        template: 'PaymentInfoWidget',
        show: function (options) {
            var self = this;
            options = options || {};
            this._super(options);
            this.renderElement();
            // $('body').off('keypress', this.keyboard_handler);
            // $('body').off('keydown', this.keyboard_keydown_handler);
            // window.document.body.addEventListener('keypress', this.keyboard_handler);
            // window.document.body.addEventListener('keydown', this.keyboard_keydown_handler);
            if (options.data) {
                var data = options.data;
                this.$('input[name=payment_ref]').val(data.payment_ref);
                this.$('select[name=payment_cuot]').val(data.payment_cuot);
                this.$('select[name=payment_tar]').val(data.payment_tar);
            }

            this.$('select[name=payment_tar]').on("change", function (event) {
                console.log('TuViejaRemixConChocolate')
                var cuotas = self.pos.cuotas.filter(function (cuota) {
                    return cuota.tarjeta_id[0] == event.target.value;
                });

                $('select[name=payment_cuot] option').each(function () {

                    $(this).remove();

                });

                cuotas.forEach(function (item) {

                    $('select[name=payment_cuot]').append($('<option>', {
                        value: item.id,
                        text: item.qty + ' - %' + item.interes
                    }));

                })
            });


        },
        click_confirm: function () {
            var self = this
            var infos = {
                'payment_ref': this.$('input[name=payment_ref]').val(),
                'payment_cuot': this.$('select[name=payment_cuot]').val(),
                'payment_tar': this.$('select[name=payment_tar]').val(),
            };

            var valid = true;
            if (this.options.validate_info) {
                valid = this.options.validate_info.call(this, infos);
            }

            var order = this.pos.get_order()
            order.payment_ref = infos.payment_ref
            order.payment_tar = infos.payment_tar
            order.payment_cuot = infos.payment_cuot

            var cuota = this.pos.cuotas.filter(function (item) {

                return item.id == $('select[name=payment_cuot]').val()
            })

            //esto es para pagar con una sola tarjeta, o para combinar una tarjeta con pago en efectivo.
            
            // aca añadir una orderline
            // el producto es un producto de database
            order.interes_total = order.get_total_without_tax() * (cuota[0].interes / 100);
            var interes_line = new models.Orderline({},{
                pos: self.pos,
                order: order,
                product: self.pos.db.get_product_by_id(26919),
                price: order.interes_total,
                quantity: 1
            });
            order.add_orderline(interes_line)

            this.gui.close_popup();
            if (this.options.confirm) {
                this.options.confirm.call(this, infos);
            }


        },
    });
    gui.define_popup({ name: 'payment-info-input', widget: PaymentInfoWidget });

    var _super_order = models.Order.prototype;
    models.Order = models.Order.extend({
        initialize: function (attr, options) {
            this.interes_total = 0;
            _super_order.initialize.call(this, attr, options);
        },
        get_total_with_tax: function () {
            var total = _super_order.get_total_with_tax.apply(this);
            return total;
            // return total + this.interes_total;
        },
        remove_paymentline: function (line) {
            this.interes_total = 0;
            _super_order.remove_paymentline.apply(this, [line]);
        },
    })

    return PopupWidget;
});