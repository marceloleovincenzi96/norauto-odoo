odoo.define('pos_payment_ref.screens', function (require) {
"use strict";

var screens = require('point_of_sale.screens');
var core = require('web.core');
var QWeb = core.qweb;

console.log('Saquenme de este calvario por favor.')

screens.PaymentScreenWidget.include({
    init: function (parent, options) {
        console.log('La caracola ha hablado');
        this._super(parent, options);
        var self = this;
        this.keyboard_handler = function (event) {
            if ($(".modal-dialog.recargo_tarjeta").not('.oe_hidden').length)  {
                    return;
            }
            
            else {
                // On mobile Chrome BarcodeEvents relies on an invisible
                // input being filled by a barcode device. Let events go
                // through when this input is focused.
                if (BarcodeEvents.$barcodeInput && BarcodeEvents.$barcodeInput.is(":focus")) {
                    return;
                }

                var key = '';

                if (event.type === "keypress") {
                    if (event.keyCode === 13) { // Enter
                        self.validate_order();
                    } else if ( event.keyCode === 190 || // Dot
                                event.keyCode === 110 ||  // Decimal point (numpad)
                                event.keyCode === 188 ||  // Comma
                                event.keyCode === 46 ) {  // Numpad dot
                        key = self.decimal_point;
                    } else if (event.keyCode >= 48 && event.keyCode <= 57) { // Numbers
                        key = '' + (event.keyCode - 48);
                    } else if (event.keyCode === 45) { // Minus
                        key = '-';
                    } else if (event.keyCode === 43) { // Plus
                        key = '+';
                    }
                } else { // keyup/keydown
                    if (event.keyCode === 46) { // Delete
                        key = 'CLEAR';
                    } else if (event.keyCode === 8) { // Backspace
                        key = 'BACKSPACE';
                    }
                }

                self.payment_input(key);
                event.preventDefault();
            };
            
        }
        
        this.keyboard_keydown_handler = function (event) {
            if ($(".modal-dialog.pos_password_manager").not('.oe_hidden').length) {
                return;
            }

            else {
                if (event.keyCode === 8 || event.keyCode === 46) { // Backspace and Delete
                    event.preventDefault();
    
                    // These do not generate keypress events in
                    // Chrom{e,ium}. Even if they did, we just called
                    // preventDefault which will cancel any keypress that
                    // would normally follow. So we call keyboard_handler
                    // explicitly with this keydown event.
                    self.keyboard_handler(event);
                }
            }
        }
    },
    show_popup_payment_info: function(options) {
        var self = this;
        // window.document.body.removeEventListener('keypress', self.keyboard_handler);
        // window.document.body.removeEventListener('keydown', self.keyboard_keydown_handler);
        this.gui.show_popup('payment-info-input',{
            data: options.data,
            validate_info: function(infos){
                this.$('input').removeClass('error');
                if(!infos.bank_name) {
                    this.$('input[name=payment_ref]').addClass('error');
                    this.$('input[name=payment_ref]').focus();
                    return false;
                }
//                }
                return true;
            },
            confirm: function(infos){
                options.confirm.call(self, infos);
                self.reset_input();
                self.render_paymentlines();
                // window.document.body.addEventListener('keypress', self.keyboard_handler);
                // window.document.body.addEventListener('keydown', self.keyboard_keydown_handler);
            },
            cancel: function(){
                // window.document.body.addEventListener('keypress', self.keyboard_handler);
                // window.document.body.addEventListener('keydown', self.keyboard_keydown_handler);
            },
        });
    },
    click_paymentmethods: function(id) {
        var self = this;
        var cashregister = null;

        for ( var i = 0; i < this.pos.payment_methods.length; i++ ) {
            if ( this.pos.payment_methods[i].id === id ){
                cashregister = this.pos.payment_methods[i];
                break;
            }
        }
        if (cashregister.pos_payment_ref) {
            this.show_popup_payment_info({
                confirm: function(infos) {
                    //merge infos to new paymentline
                    self.pos.get_order().add_paymentline_with_details(cashregister, infos);
                },
            });
        }
        else {
            this._super(id);
        }
    },

    click_numpad: function(button) {
        var paymentlines = this.pos.get_order().get_paymentlines();
        var open_paymentline = false;

        for (var i = 0; i < paymentlines.length; i++) {
            if (! paymentlines[i].paid) {
                open_paymentline = true;
            }
        }

        if (! open_paymentline) {
            var cashregister = null;
            for ( var i = 0; i < this.pos.payment_methods.length; i++ ) {
                if (!this.pos.payment_methods[i].pos_payment_ref){
                    cashregister = this.pos.payment_methods[i];
                    break;
                }
            }
            this.pos.get_order().add_paymentline(cashregister);
            this.render_paymentlines();
        }

        this.payment_input(button.data('action'));
    },

    click_payment_info_paymentline: function(cid){
        var self = this;
        var lines = this.pos.get_order().get_paymentlines();
        for ( var i = 0; i < lines.length; i++ ) {
            if (lines[i].cid === cid) {
                this.show_popup_payment_info({
                    data: lines[i],
                    confirm: function(infos) {
                        //merge infos to updated paymentline
                        self.pos.get_order().update_paymentline_with_details(lines[i], infos);
                    },
                });
                return;
            }
        }
    },

    render_paymentlines: function() {
        var self = this;
        this._super();
        var lines = this.$('.paymentlines-container table.paymentlines');
        lines.on('click','.payment-info-button', function(){
            self.click_payment_info_paymentline($(this).data('cid'));
        });
    },
    render_paymentmethods: function() {
        var self = this;

        var payment_methods = this.pos.payment_methods.filter(function(method){
            return method.pago_anticipos == false;
        })

        var dummy_pos = {
            payment_methods:  payment_methods
        }
        var dummy_this = {pos: dummy_pos}
        var methods = $(QWeb.render('PaymentScreen-Paymentmethods', { widget:dummy_this }));
            methods.on('click','.paymentmethod',function(){
                self.click_paymentmethods($(this).data('id'));
            });

        return methods;
    }
});

});
