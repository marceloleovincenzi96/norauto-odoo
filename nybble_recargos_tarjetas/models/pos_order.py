# -*- coding: utf-8 -*-
from odoo import models, api


class PosOrder(models.Model):
    _inherit = "pos.order"

    @api.model
    def _payment_fields(self, order, ui_paymentline):
        res = super(PosOrder, self)._payment_fields(order, ui_paymentline)
        res.update({
            'payment_ref': ui_paymentline.get('payment_ref'),
            'payment_cuot': ui_paymentline.get('payment_cuot'),
            'payment_tar': ui_paymentline.get('payment_tar'),
        })
        return res

    def add_payment(self, data):
        statement_id = super(PosOrder, self).add_payment(data)
        payments = self.env['pos.payment']
        payment_lines = payments.search([
            ('pos_order_id', '=', self.id),
            ('payment_method_id', '=', data['payment_method_id']),
            ('amount', '=', data['amount'])
        ])

        for line in payment_lines:
            if line.payment_method_id.pos_payment_ref:  # si es tarjeta
                payment_tar = self.env['tarjetas.tarjeta'].browse(
                    int(data.get('payment_tar')))
                payment_cuot = self.env['tarjetas.cuotas'].browse(
                    int(data.get('payment_cuot')))
                cuota = str(payment_cuot.qty) + ' - ' + \
                    str(payment_cuot.interes) + '%'
                card_vals = {
                    'payment_ref': data.get('payment_ref'),
                    'payment_cuot': cuota,
                    'payment_tar': payment_tar.name,
                }
                line.write(card_vals)

                print(data)
                print(payment_tar)
                print(self.session_id.id)
                payment_method_qty = self.env['pos.payment.method.quantities'].search([
                    ('payment_method_id', '=', data['payment_method_id']),
                    ('pos_session_id', '=', self.session_id.id),
                    ('card_name', '=', payment_tar.name)
                ])

                payment_method_con_cuotas_qty = self.env['pos.payment.method.quantities.cuota'].search([
                    ('payment_method_id', '=', data['payment_method_id']),
                    ('cuotas', '=', payment_cuot.qty),
                    ('pos_session_id', '=', self.session_id.id),
                    ('card_name', '=', payment_tar.name)
                ])

                if not payment_method_qty:

                    self.env['pos.payment.method.quantities'].create({

                        'payment_method_id': data['payment_method_id'],
                        'card_name': payment_tar.name,
                        'theoretical_qty': data['amount'],
                        'actual_qty': 0,
                        'difference': data['amount'],
                        'pos_session_id': self.session_id.id

                    })

                else:

                    for record in payment_method_qty:
                        theoretical_quant = record.theoretical_qty

                        record.write({
                            'theoretical_qty': theoretical_quant + data['amount']
                        })

                if not payment_method_con_cuotas_qty:

                    self.env['pos.payment.method.quantities.cuota'].create({

                        'payment_method_id': data['payment_method_id'],
                        'card_name': payment_tar.name,
                        'cuotas': payment_cuot.qty,
                        'theoretical_qty': data['amount'],
                        'actual_qty': 0,
                        'difference': data['amount'],
                        'pos_session_id': self.session_id.id

                    })

                else:

                    for record in payment_method_con_cuotas_qty:
                        theoretical_quant = record.theoretical_qty

                        record.write({
                            'theoretical_qty': theoretical_quant + data['amount']
                        })

            else:  # si es efectivo

                payment_method_qty = self.env['pos.payment.method.quantities'].search(
                    [('payment_method_id', '=', data['payment_method_id']), ('pos_session_id', '=', self.session_id.id)])

                payment_method_con_cuotas_qty = self.env['pos.payment.method.quantities.cuota'].search(
                    [('payment_method_id', '=', data['payment_method_id']), ('pos_session_id', '=', self.session_id.id)])

                if not payment_method_qty:
                    self.env['pos.payment.method.quantities'].create({

                        'payment_method_id': data['payment_method_id'],
                        'theoretical_qty': data['amount'],
                        'actual_qty': 0,
                        'difference': data['amount'],
                        'pos_session_id': self.session_id.id

                    })

                else:
                    for record in payment_method_qty:
                        theoretical_quant = record.theoretical_qty

                        record.write({
                            'theoretical_qty': theoretical_quant + data['amount']
                        })

                if not payment_method_con_cuotas_qty:

                    self.env['pos.payment.method.quantities.cuota'].create({

                        'payment_method_id': data['payment_method_id'],
                        'cuotas': 0,
                        'theoretical_qty': data['amount'],
                        'actual_qty': 0,
                        'difference': data['amount'],
                        'pos_session_id': self.session_id.id

                    })

                else:

                    for record in payment_method_con_cuotas_qty:
                        theoretical_quant = record.theoretical_qty

                        record.write({
                            'theoretical_qty': theoretical_quant + data['amount']
                        })

        return statement_id
