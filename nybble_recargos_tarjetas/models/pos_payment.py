# -*- coding: utf-8 -*-
from odoo import fields, models


class PosPayment(models.Model):
    _inherit = 'pos.payment'

    pos_payment_ref = fields.Boolean(related='payment_method_id.pos_payment_ref', readonly=True)
    payment_tar = fields.Char('Tarjeta')
    payment_ref = fields.Char('Cupon')
    payment_cuot = fields.Char('Cuotas')