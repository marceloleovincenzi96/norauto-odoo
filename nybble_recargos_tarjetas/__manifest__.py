# -*- coding: utf-8 -*-
{
    'name': 'Card Payment POS',
    'version': '13.0.0.0.0',
    'summary': """Allows users to pay with cards in POS, including surcharges and time payments""",
    'category': 'Point of Sale',
    'license': 'LGPL-3',
    'author': "Nybble Group",
    'website': "https://www.nybblegroup.com/",
    'depends': [
        'point_of_sale',
        'tarjetas_master',
        'fields_norauto'
        ],
    'data': [
        'data/product.xml',
        'views/pos_payment_ref.xml',
        'views/pos_payment_view.xml',
        'views/pos_order_view.xml',
    ],
    'images': [],
    'installable': True,
    'application': True,
    'qweb': ['static/src/xml/pos_payment_ref.xml'],
}
