from odoo import fields, models, api
from odoo.exceptions import ValidationError

class Litigio (models.Model):
    _name="litigios.litigio"
    _description="Litigio"
    _inherit = ['mail.thread', 'mail.activity.mixin']


    def _obtener_unidad_operativa(self):
        return self.env.user.default_operating_unit_id
    
    def enviar_notificacion_litigio_aprobado_rechazado(self, message):
        main_tienda_users = self.env['res.users'].search([]).filtered(lambda r: self.env.ref('norauto_rights.group_tienda') and r.has_group('norauto_rights.group_tienda'))
        tienda_partners_ids = [u.partner_id.id for u in main_tienda_users]
        self.message_subscribe(partner_ids=tienda_partners_ids, channel_ids=self.message_channel_ids)
        #user = main_tienda_users.filtered(lambda r: r.has_group('norauto_rights.group_tienda'))
        for x in main_tienda_users:
            self.activity_schedule(act_type_xmlid='mail.mail_activity_data_todo', summary=message, user_id=x.id)
        self.message_post(body=message)
    
    def enviar_notificacion_creacion_litigio(self):

        main_CCA_users = self.env['res.users'].search([]).filtered(lambda r: self.env.ref('norauto_rights.group_CCA') and r.has_group('norauto_rights.group_CCA'))
        CCA_partners_ids = [u.partner_id.id for u in main_CCA_users]
        self.message_subscribe(partner_ids=CCA_partners_ids, channel_ids=self.message_channel_ids)
        #user = main_tienda_users.filtered(lambda r: r.has_group('norauto_rights.group_tienda'))
        message1 = "Se ha generado un litigio por el producto %s de la transferencia %s. Por favor haga un control de su stock." % (self.descripcion, self.pedido_id.name)
        for x in main_CCA_users:
            self.activity_schedule(act_type_xmlid='mail.mail_activity_data_todo', summary=message1, user_id=x.id)
        self.message_post(body=message1)

    def enviar_notificacion_litigio_pendiente(self):

        main_CCA_users = self.env['res.users'].search([]).filtered(lambda r: self.env.ref('norauto_rights.group_CCA') and r.has_group('norauto_rights.group_CCA'))
        CCA_partners_ids = [u.partner_id.id for u in main_CCA_users]
        #user = main_tienda_users.filtered(lambda r: r.has_group('norauto_rights.group_tienda'))
        message = "Este litigio aún no ha sido aprobado o rechazado. Recuerde hacer un control de su stock si aún no lo ha hecho."
        for record in self:
            if record.estado != "Aceptada" or record.estado != "Rechazada":
                for x in main_CCA_users:
                    record.activity_schedule(act_type_xmlid='mail.mail_activity_data_todo', summary=message, user_id=x.id)
                    record.message_post(body=message)

    def transmitir_litigio(self):
        if not self.env.user.default_operating_unit_id == self.env["operating.unit"].search([('code', '=', 'DC01')]):
            self.enviar_notificacion_creacion_litigio()
            quant = self.env['stock.quant'].search([('location_id', '=', self.pedido_id.location_dest_id.id), ('product_id', '=', self.articulo.id)])
            quant.with_context({'inventory_mode': True}).write({
                    'inventory_quantity': quant.quantity - self.diferencia
                })
            quant_origen = self.env['stock.quant'].search([('location_id', '=', self.pedido_id.location_id.id), ('product_id', '=', self.articulo.id)])
            quant_origen.with_context({'inventory_mode': True}).write({
                    'inventory_quantity': quant.quantity - self.cantidad_facturada
                })
            self.write({'estado': 'Transmitida'})
        else:
            raise ValidationError("Permiso denegado. Usuarios de depósito central no pueden iniciar litigios.")


    def finalizar_litigio(self):
        if self.env.user.default_operating_unit_id == self.env["operating.unit"].search([('code', '=', 'DC01')]): 
            self.write({'estado': 'Finalizada'})
        else:
            raise ValidationError("Permiso denegado. Sólo usuarios de depósito central pueden finalizar litigios.")


    def aprobar_litigio(self):
        if self.env.user.default_operating_unit_id == self.env["operating.unit"].search([('code', '=', 'DC01')]): 
            quant_almacen = self.env["stock.quant"].search([('location_id', '=', self.pedido_id.location_id.id), ('product_id', '=', self.articulo.id)])
            
            self.resultado_litigio = self.diferencia*self.articulo.standard_price

            quant_almacen.with_context({'inventory_mode': True}).write({
                'inventory_quantity': quant_almacen.quantity + self.diferencia,
            })
            mensaje = "Se ha aceptado el litigio de la transferencia %s por el articulo %s" % (self.pedido_id.name, self.descripcion)
            self.enviar_notificacion_litigio_aprobado_rechazado(mensaje)

            self.write({'estado': 'Aceptada'})
            
        else:
            raise ValidationError("Permiso denegado. Sólo usuarios de depósito central pueden aprobar litigios.")
    
    def rechazar_litigio(self):
        if self.env.user.default_operating_unit_id == self.env["operating.unit"].search([('code', '=', 'DC01')]):
            
            self.resultado_litigio = self.diferencia*self.articulo.precio_cesion

            mensaje = "Se ha rechazado el litigio de la transferencia %s por el articulo %s" % (self.pedido_id.name, self.descripcion)
            self.enviar_notificacion_litigio_aprobado_rechazado(mensaje)
            
            self.write({'estado': 'Rechazada'})
        else:
            raise ValidationError("Permiso denegado. Sólo usuarios de depósito central pueden rechazar litigios.")
    
    @api.depends('cantidad_facturada', 'cantidad_recibida_por_tienda')
    def _calcular_diferencia(self):
        for record in self:
            record.diferencia = record.cantidad_facturada - record.cantidad_recibida_por_tienda
            if record.diferencia > 0:
                record.tipo = "Faltante"
            elif record.diferencia < 0:
                record.tipo = "Sobrante"


    movimiento = fields.Selection([('Entrada', 'Entrada'),('Salida', 'Salida')],'Movimiento')

    fecha_introduccion = fields.Date("Fecha de introducción del artículo", required=True)

    pedido_id = fields.Many2one("stock.picking", "Número de pedido", required=True)

    articulo = fields.Many2one("product.product", "Artículo", required=True)

    descripcion = fields.Char("Descripción", related="articulo.name")

    cantidad_facturada = fields.Integer("Cantidad facturada", required=True)

    cantidad_recibida_por_tienda = fields.Integer("Cantidad recibida por tienda", required=True)

    diferencia = fields.Integer("Diferencia", required=True, compute="_calcular_diferencia")

    tipo = fields.Selection([('Faltante', 'Faltante'),('Sobrante', 'Sobrante')],'Tipo', required=True)

    precio_diferencia_tienda = fields.Float("Precio de diferencia para Tienda")

    precio_diferencia_CCA = fields.Float("Precio de diferencia para Depósito Central")

    estado = fields.Selection([('Introducida', 'Introducida'),('Transmitida', 'Transmitida'),('Finalizada', 'Finalizada'),('Aceptada', 'Aceptada'),('Rechazada', 'Rechazada')],'Estado', default="Introducida", required=True)

    tienda_litigio = fields.Many2one("operating.unit", "Tienda iniciadora", default=lambda self: self._obtener_unidad_operativa(), required=True)

    resultado_litigio = fields.Integer("Resultado del litigio", readonly=True)

    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id.id, required=True)

    _rec_name = "pedido_id"