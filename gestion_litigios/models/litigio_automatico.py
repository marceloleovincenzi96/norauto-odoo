from odoo import fields, models
import datetime


class GeneradorDeLitigioAutomatico(models.Model):
    _inherit = "stock.internal.transfer"

    def guardar_confirmar(self):

        if self.env.context.get('confirmar'):
            res = super(GeneradorDeLitigioAutomatico, self).guardar_confirmar()
            return res

        items_a_litigiar = []
        datos_transferencia = []

        for record in self:

            datos_transferencia.append((record.picking_id.id, record.location_dest_id.operating_unit_id.id, datetime.date.today().strftime("%Y-%m-%d")))

            for item in record.stock_transfer_line_ids:

                if item.cantidad_diferencia != 0:
                    items_a_litigiar.append(item.id)

            if len(items_a_litigiar) > 0:

                return {
                        'name': 'Confirmar litigio',
                        'type': 'ir.actions.act_window',
                        'res_model': 'litigios.confirmar_litigio',
                        'view_mode': 'form',
                        'target': 'new',
                        "context": {'articulos': items_a_litigiar, 'datos_transferencia': datos_transferencia, 'internal_transfer_id': self.id, 'picking_id': self.env.context.get('picking_id')}
                    }
            elif len(items_a_litigiar) == 0:
                return{ 
                    record.picking_id.assign_and_validate()
                }