from odoo import models


class ConfirmarLitigio(models.TransientModel):
    _name = 'litigios.confirmar_litigio'

    def confirmar(self):
        articulos_a_litigiar = self.env.context.get('articulos')
        lista_datos_transferencia = self.env.context.get('datos_transferencia')

        for item in articulos_a_litigiar:
            elemento = self.env["stock.internal.transfer.line"].browse(item)
            vals = {
                'articulo': elemento.product_id.id,
                'descripcion': elemento.product_id.name,
                'cantidad_facturada': elemento.cantidad_enviada,
                'cantidad_recibida_por_tienda': elemento.cantidad_recibida,
                'diferencia': elemento.cantidad_diferencia,
                'estado': 'Transmitida'
            }

            tupla = lista_datos_transferencia[0]
            vals['pedido_id'] = tupla[0]
            vals['tienda_litigio'] = tupla[1]
            vals['fecha_introduccion'] = tupla[2]

            if elemento.cantidad_diferencia > 0:
                vals['tipo'] = 'Faltante'
            elif elemento.cantidad_diferencia < 0:
                vals['tipo'] = 'Sobrante'

            litigio_creado = self.env["litigios.litigio"].create(vals)

            litigio_creado.enviar_notificacion_creacion_litigio()

            # NOTE: llamar al guardar_confirmar con contexto aqui
            # import pdb; pdb.set_trace()
        transfer = self.env[self.env.context.get('active_model')].browse(self.env.context.get('active_id'))
        transfer.with_context({'confirmar': True}).guardar_confirmar()
        # picking_id = self.env["stock.picking"].browse(self.env.context.get("picking_id"))
        # picking_id.assign_and_validate()



    def cancelar(self):

        return {
            'name': "Recepción de Productos",
            'view_mode': 'form',
            'res_model': 'stock.internal.transfer',
            'res_id': self.env.context.get('internal_transfer_id'),
            'type': 'ir.actions.act_window',
            'target': 'new',
            'domain': '[]',
            'flags': {'action_buttons': False},
            'context': {'recepcion': True}
        }

    
