# -*- coding: utf-8 -*-

from odoo import api, fields, models


class AccountMove(models.Model):
    _inherit = 'account.move'

    send_by_idoc_compras = fields.Boolean('Enviar por IDOC compras')
    send_by_idoc_ventas = fields.Boolean('Enviar por IDOC ventas')
    generated_idoc = fields.Boolean('IDOC generado')

    # Condiciones para informar por idoc
    # TODAS las facturas y notas de credito/debito
    @api.model
    def create(self, vals):
        if vals.get('type') in ['in_invoice', 'in_refund']:
            vals['send_by_idoc_compras'] = True
        if vals.get('type') in ['out_invoice', 'out_refund']:
            vals['send_by_idoc_ventas'] = True
        return super(AccountMove, self).create(vals)

    def _get_tasa(self):
        for line in self.line_ids:
            if line.tax_line_id:
                return line.tax_line_id.name

    def _get_alicuota(self):
        # devuelve valor en cientos de la tasa de impuesto aplicado
        for line in self.line_ids:
            if line.tax_line_id:
                return line.tax_line_id.amount

    def _get_valor_iva(self):
        # devuelve valor en cientos de la tasa de impuesto aplicado
        for line in self.line_ids:
            if line.tax_line_id:
                return line.debit

    def _get_cuenta(self):
        for line in self.line_ids:
            if line.debit > 0 and line.product_id:
                return line.account_id.code

    def _get_cuenta_costo(self):
        for line in self.line_ids:
            if line.credit > 0:
                return line.account_id.code

    def _get_importe(self):
        importe = 0
        for line in self.line_ids:
            if line.debit > 0 and line.product_id:
                importe += line.debit
        return importe

    def _get_tienda_code(self):
        if self.operating_unit_id:
            return self.operating_unit_id.code

    def _get_caja(self):
        if self.journal_id:
            return self.journal_id.l10n_ar_afip_pos_number

    def _get_base_iva_ventas21(self):
        base_iva = 0
        for line in self.line_ids:
            if self.env.ref('l10n_ar.1_ri_tax_vat_21_ventas') in line.tax_ids:
                if self.type == 'out_invoice':
                    base_iva += line.credit
                elif self.type == 'out_refund':
                    base_iva += line.debit
        base_iva = "{:.2f}".format(base_iva)
        return str(base_iva)

    def _get_base_iva_ventas105(self):
        base_iva = 0
        for line in self.line_ids:
            if self.env.ref('l10n_ar.1_ri_tax_vat_10_ventas') in line.tax_ids:
                if self.type == 'out_invoice':
                    base_iva += line.credit
                elif self.type == 'out_refund':
                    base_iva += line.debit
        return str(base_iva)

    def _get_iva_ventas_21(self):
        for line in self.line_ids:
            if line.tax_line_id == self.env.ref('l10n_ar.1_ri_tax_vat_21_ventas'):
                if self.type == 'out_invoice':
                    return str(line.credit) or "0.00"
                elif self.type == 'out_refund':
                    return str(line.debit) or "0.00"
        return "0.00"

    def _get_iva_ventas_105(self):
        for line in self.line_ids:
            if line.tax_line_id == self.env.ref('l10n_ar.1_ri_tax_vat_10_ventas'):
                if self.type == 'out_invoice':
                    return str(line.credit) or "0.00"
                elif self.type == 'out_refund':
                    return str(line.debit) or "0.00"
        return "0.00"

    def _get_iibb_caba(self):
        for line in self.line_ids:
            if self.env.ref('l10n_ar_ux.tag_tax_jurisdiccion_901') in line.tag_ids:
                if self.type == 'out_invoice':
                    return str(line.credit) or "0.00"
                elif self.type == 'out_refund':
                    return str(line.debit) or "0.00"
        return "0.00"

    def _get_iibb_arba(self):
        for line in self.line_ids:
            if self.env.ref('l10n_ar_ux.tag_tax_jurisdiccion_902') in line.tag_ids:
                if self.type == 'out_invoice':
                    return str(line.credit) or "0.00"
                elif self.type == 'out_refund':
                    return str(line.debit) or "0.00"
        return "0.00"

    def _get_tipo_responsabilidad_waldbot(self):
        responsabilidades = {
            '1': 'RI',
            '5': 'CF',
            '6': 'RM',
            '4': 'EX',
            '9': 'EE'
        }
        return responsabilidades.get(self.partner_id.l10n_ar_afip_responsibility_type_id.code, '0')

    def _get_tipo(self):
        if self.type == 'out_invoice':
            return 'FC'
        elif self.type == 'out_refund':
            return 'NC'
