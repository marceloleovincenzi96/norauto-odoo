# -*- coding: utf-8 -*-

import datetime
from odoo import api, fields, models
from odoo.tools import safe_eval
from xml.dom import minidom
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import os
from ftplib import FTP
import fileinput
import shutil

class NorautoIdocHistory(models.Model):
    _name = 'norauto.idoc.history'
    _description = 'Registro de Idocs'

    name = fields.Char('Nombre')
    idoc_id = fields.Many2one(comodel_name='norauto.idoc', string='Tipo Idoc')
    docnum = fields.Char('Número Documento')
    modelo_id = fields.Char('Modelo')
    res_ids = fields.Char('Id del registro')
    res_names = fields.Char('Nombre del registro')
    estado = fields.Selection(
        selection=[('creado', 'Creado'), ('enviado', 'Enviado'), ('error', 'Error envio')],
        string='Estado',
        default='creado')
    filename = fields.Char('Nombre de archivo')
    idoc_text = fields.Text('Cuerpo del IDOC')
    observations = fields.Text('Observaciones')


class NorautoIdoc(models.Model):
    _name = 'norauto.idoc'
    _description = 'Modelo de Idocs'

    name = fields.Char('Nombre')
    xml_tag = fields.Char('Tag XML')
    parent_tag = fields.Char('Tag XML padre')
    tag_for_each_record = fields.Char('Tag por cada documento')
    base_model = fields.Char('Modelo base')
    sequence_id = fields.Many2one(comodel_name='ir.sequence', string='Secuencia')
    tipo = fields.Selection(
        selection=[('outbound', 'Outbound'), ('inbound', 'Inbound')],
        string='Tipo',
        help='Outbound es para crear IDOCS, Inbound para recibirlos'
    )
    segmentos_ids = fields.One2many(
        comodel_name='norauto.idoc.segmento',
        inverse_name='norauto_idoc_id'
    )
    condition_field = fields.Char('Campo condicional de envio')
    sent_field = fields.Char('Campo de confirmacion de envio')

    norauto_idoc_posnrs = {}
    date = datetime.datetime.now(),
    contexto = {
        'norauto_idoc_posnrs': {},
        'date': ''
    }

    def create_idoc(self, record_ids=None):
        # record = self.env['stock.picking'].browse(180)
        record_ids = self._get_object_to_send_ids(self.base_model, self.condition_field, self.sent_field)
        if self.name == 'ARTMAS':
            record_ids = self._filter_artmas_products(record_ids)
        view = self.env.ref('sh_message.sh_message_wizard')
        view_id = view and view.id or False
        context = dict(self._context or {})
        if record_ids is None:
            context['message'] = "No se han encontrado registros para generar este IDOC"
            return {
                'name': 'No hay registros',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'sh.message.wizard',
                'views': [(view.id, 'form')],
                'target': 'new',
                'context': context
            }
        model = self.base_model
        self.contexto['date'] = datetime.datetime.now()
        credat = self.contexto['date'].strftime('%Y%m%d')
        cretim = self.contexto['date'].strftime('%H%M%S')
        serial = self.contexto['date'].strftime('%Y%m%d%H%M%S')
        records = self.env[model].browse(record_ids)
        # records = self.env[model].search([('id', 'in', record_ids)])
        idoc_file = Element(self.parent_tag)
        for record in records:
            self.contexto['norauto_idoc_posnrs'] = {}
            if self.tag_for_each_record:
                idoc = SubElement(idoc_file, self.tag_for_each_record)
                idoc.attrib['BEGIN'] = '1'
            else:
                idoc = idoc_file
            segmentos_ordenados = self.segmentos_ids.sorted(key=lambda r: r.posicion)
            for segmento in segmentos_ordenados:
                segmentoxml = self.generar_segmento(idoc, segmento, record)
        records.write({self.sent_field: True})
        idoc_text = self.prettify(idoc_file)
        # secuencia = self.env['ir.sequence'].next_by_code('idoc.shpord')
        secuencia = self.sequence_id.next_by_id()
        filename = 'W12_%s_%s_%s_%s.xml' % (self.xml_tag, credat, self.xml_tag, secuencia[-9:])
        idoc_creado = self.env['norauto.idoc.history'].create({
            'name': "%s %s" % (self.xml_tag, secuencia),
            'idoc_id': self.id,
            'docnum': secuencia,
            'modelo_id': self.base_model,
            'res_ids': ', '.join([str(num) for num in record_ids]),
            'res_names': ', '.join([r.name for r in records]),
            'filename': filename,
            'idoc_text': idoc_text
        })
        # ICPSudo = self.env['ir.config_parameter'].sudo()
        # path_guardar = ICPSudo.get_param('norauto_idoc_path_local')
        # if path_guardar[-1] != '/':
        #     path_guardar += '/'
        # file_path = path_guardar + filename
        # f = open(file_path, "w")
        # f.write(idoc_text)
        # f.close()
        # NOTE: esto tiene que ser en un cron con funcion aparte
        # que levante los idocs del path de nuevos y despues pasarlos a archivo una vez enviados
        # ftp_host = ICPSudo.get_param('norauto_idoc_host')
        # ftp_port = ICPSudo.get_param('norauto_idoc_port')
        # ftp_user = ICPSudo.get_param('norauto_idoc_ftp_user')
        # ftp_pass = ICPSudo.get_param('norauto_idoc_ftp_pswd')
        # ftp_path = ICPSudo.get_param('norauto_idoc_path')
        # if ftp_host and ftp_port and ftp_user and ftp_pass and ftp_path:
        #     if ftp_path[-1] != '/':
        #         ftp_path += '/'
        #     # NOTE: send file to ftp
        #     ftp = FTP()
        #     ftp.set_debuglevel(2)
        #     ftp.connect(ftp_host, int(ftp_port))
        #     ftp.login(ftp_user, ftp_pass)
        #     # ftp.cwd(ftp_path)
        #     fp = open(file_path, 'rb')
        #     remote_file = ftp_path + filename
        #     ftp.storbinary('STOR %s' % os.path.basename(remote_file), fp, 1024)
        #     fp.close()
        #     ftp.quit()
        # NOTE:  después de enviar pasar a carpeta archivo

    def read_idoc(self):
        # doc = minidom.parse('file.xml')
        ICPSudo = self.env['ir.config_parameter'].sudo()
        incoming_path = ICPSudo.get_param('norauto_idoc_path_local_inc_nuevo')
        archive_path = ICPSudo.get_param('norauto_idoc_path_local_inc_archivo')

        files = os.listdir(incoming_path)
        files_full_path = [os.path.abspath(os.path.join(incoming_path, x)) for x in files]
        for f, f_full in zip(files, files_full_path):
            notas = ''
            if self.env['norauto.idoc.history'].search([('filename', '=', f)]):
                # NOTE: move file to archivo porque ya existe
                print('-----------------------------------------------')
                print('Este Archivo ya se leyo!. %s' % f_full)
                print('mover a %s' % os.path.join(archive_path, f))
                print('-----------------------------------------------')
                shutil.move(f_full, os.path.join(archive_path, f))
                pass
            else:
                print('-----------------------------------------------')
                print('Leyendo archivo. %s' % f_full)
                print('-----------------------------------------------')
                # Hay que leer el idoc y registrarlo en historial
                # Ver tipo de idoc y conseguir el documento
                is_xml = f.split('.')[-1].lower() == 'xml'
                if is_xml:
                    tipo_idoc = f.split('_')[1]
                    records = None
                    if tipo_idoc == 'SHPMNT':
                        records, products_for_rec = self._get_records_shpmnt(f_full)
                        # Llamar al paso de estado
                        for rec, products in zip(records, products_for_rec):
                            # Validar que no este en estado enviado primero
                            # NOTE: de ser necesario aca puedo procesar las cantidades con el
                            # wizard que cree para las cantidades
                            if rec.state in ['in_transit']:
                                notas += 'El movimiento %s ya está en Transito \n' % (rec.name)
                            elif rec.state not in ['assigned', 'confirmed']:
                                notas += 'El movimiento %s no está en un estádo válido\n' % (rec.name)
                            else:
                                notas += 'El movimiento %s se ha pasado a Transito \n' % (rec.name)
                                # TODO: hacer la comprobacion de disponibilidad nativa
                                # si no tiene stock
                                # NOTE: aca no procesa las cantidades reales, quizas deberia cambiar, no las cantidades
                                # hechas, sino las cantidades demandadas, pero eso es diferente....
                                # NOTE: a la verghulis, yo cambio la cantidad demandada y despues lo arreglamos
                                rec_products = rec.move_lines.mapped('product_id')
                                for prod in products:
                                    prod_template = self.env['product.template'].search([('default_code', '=', prod[0].lstrip('0'))], limit=1)
                                    prod_prod = self.env['product.product'].search([('product_tmpl_id', '=', prod_template.id)], limit=1)
                                    if prod_prod in rec_products:
                                        # move = rec.move_lines.search([('product_id', '=', prod_prod.id)])
                                        move = rec.move_lines.filtered(lambda line: line.product_id == prod_prod)
                                        move.write({'product_uom_qty': float(prod[1])})
                                    else:
                                        rec.write({'move_lines': [(0, 0, {
                                            'name': prod_prod.name_get()[0][1],
                                            'product_id': prod_prod.id,
                                            'product_uom': prod_prod.uom_id.id,
                                            'location_id': rec.location_id.id,
                                            'location_dest_id': rec.location_dest_id.id,
                                            'product_uom_qty': float(prod[1])
                                        })
                                        ]})
                                # TODO: hacer la reserva en base a las cantidades demandadas
                                # Apretar boton de reservar cantidades para lo nuevo
                                rec.action_assign()
                                rec.confirmar_envio()
                        # Crear el idoc historial
                        # secuencia = self.env['ir.sequence'].next_by_code('idoc.shpord')
                        secuencia = self.sequence_id.next_by_id()
                        idoc_creado = self.env['norauto.idoc.history'].create({
                            'name': "%s %s" % (tipo_idoc, secuencia),
                            'idoc_id': self.id,
                            'docnum': secuencia,
                            'modelo_id': self.base_model,
                            'res_ids': ', '.join([str(num) for num in records.ids]),
                            'res_names': ', '.join([r.name for r in records]),
                            'filename': f,
                            'observations': notas,
                        })
                        # Pasar el archivo a carpeta archivo una vez procesado
                        shutil.move(f_full, os.path.join(archive_path, f))

                    elif tipo_idoc == 'SHPCON':
                        # records = self._get_records_shpcon(f_full)
                        records, products_for_rec = self._get_records_shpcon(f_full)
                        # Acá debería validar la recepción del producto
                        # for rec in records:
                        for rec, products in zip(records, products_for_rec):
                            # Validar que no este en estado validado primero
                            if rec.state in ['done']:
                                notas += 'El movimiento %s ya está Validado \n' % (rec.name)
                            elif rec.state not in ['assigned']:
                                notas += 'El movimiento %s no está en un estádo válido\n' % (rec.name)
                            else:
                                notas += 'El movimiento %s se ha validado \n' % (rec.name)
                                # TODO: Comprobar las cantidades del idoc y del registro
                                rec_products = rec.move_lines.mapped('product_id')
                                for prod in products:
                                    prod_template = self.env['product.template'].search([('default_code', '=', prod[0].lstrip('0'))], limit=1)
                                    prod_prod = self.env['product.product'].search([('product_tmpl_id', '=', prod_template.id)], limit=1)
                                    if prod_prod in rec_products:
                                        # move = rec.move_lines.search([('product_id', '=', prod_prod.id)])
                                        move = rec.move_lines.filtered(lambda line: line.product_id == prod_prod)
                                        move.write({'quantity_done': float(prod[1])})
                                    else:
                                        rec.write({'move_lines': [(0, 0, {
                                            'name': prod_prod.name_get()[0][1],
                                            'product_id': prod_prod.id,
                                            'product_uom': prod_prod.uom_id.id,
                                            'location_id': rec.location_id.id,
                                            'location_dest_id': rec.location_dest_id.id,
                                            'quantity_done': float(prod[1])
                                        })
                                        ]})

                                # rec.button_validate()
                                rec.action_done()
                        # secuencia = self.env['ir.sequence'].next_by_code('idoc.shpord')
                        secuencia = self.sequence_id.next_by_id()
                        idoc_creado = self.env['norauto.idoc.history'].create({
                            'name': "%s %s" % (tipo_idoc, secuencia),
                            'idoc_id': self.id,
                            'docnum': secuencia,
                            'modelo_id': self.base_model,
                            'res_ids': ', '.join([str(num) for num in records.ids]),
                            'res_names': ', '.join([r.name for r in records]),
                            'filename': f,
                            'observations': notas,
                        })
                        # TODO: pasar el archivo a carpeta archivo una vez procesado
                        shutil.move(f_full, os.path.join(archive_path, f))

    def generar_segmento(self, padre, segmento, record):
        print('Segmento: %s' % segmento.name)
        # si hago un loop de field tengo que generar todas las wtiquetas xml
        if segmento.loop_field and segmento.loopable_field:
            # lo que pasa es que estaba haciendo un return y me salia del loop :v
            for line in getattr(record, segmento.loopable_field):
                segmentoxml = SubElement(padre, segmento.xml_tag)
                segmentoxml.attrib['SEGMENT'] = '1'
                for field in segmento.fields_ids:
                    child = self.generar_campo(segmentoxml, field, line, record)
                for segmento_hijo in segmento.segmentos_ids.sorted(key=lambda r: r.posicion):
                    segmentoxmlhijo = self.generar_segmento(segmentoxml, segmento_hijo, record)
                # return segmentoxml
        else:
            # if segmento.name == 'E1EDT13 2':
            segmentoxml = SubElement(padre, segmento.xml_tag)
            segmentoxml.attrib['SEGMENT'] = '1'
            for field in segmento.fields_ids:
                child = self.generar_campo(segmentoxml, field, record, record)
            for segmento_hijo in segmento.segmentos_ids.sorted(key=lambda r: r.posicion):
                segmentoxmlhijo = self.generar_segmento(segmentoxml, segmento_hijo, record)
            return segmentoxml

    def generar_campo(self, padre, field, field_record, parent_record=False):
        campoxml = SubElement(padre, field.xml_tag)
        # field_record = self.env['product.template'].browse(7237)
        # field_record = self.env['stock.picking'].browse(3)
        clean_field = ''
        if field.model_field_name:
            if field.name == "QUALF":
                print("valor_default: %s" % field.valor_default)
            clean_field = field.model_field_name.rstrip(' ').lstrip(' ')
        if clean_field and clean_field != '' and field.model:
            # value = str(getattr(field_record, clean_field))
            value = self.get_field_value(field_record, clean_field, parent_record)
            print("field_name: %s; value: %s" % (field.name, value))
            if field.zero_fill > 0:
                value = value.zfill(field.zero_fill)
            if field.tipo_dato == 'date':
                value = self.fecha_formato_sap(value)
            if field.tipo_dato == 'time':
                value = self.fecha_formato_sap(value)
            if field.tipo_dato == 'datetime':
                value = self.fecha_formato_sap(value)
            campoxml.text = value
        elif field.valor_default:
            print("field_name: %s" % field.name)
            print("valor_default: %s" % field.valor_default)
            campoxml.text = field.valor_default
        else:
            campoxml.text = 'X'
        return campoxml

    def get_field_value(self, record, field_name, parent_record):
        field_rel_list = field_name.split('.')
        val = record
        for rel in field_rel_list:
            if rel[:1] == '_':
                function = rel.split(':')
                if len(function) > 1:
                    params = function[1]
                    params = params.split(',')
                    val = getattr(val, function[0])(*params)
                else:
                    val = getattr(val, rel)()
            elif rel == 'Parent':
                val = parent_record
            elif rel == 'IDOC':
                val = self
            else:
                val = getattr(val, rel)
        return str(val or '')

    def _get_numeracion_posnr(self, name, incremento):
        name = safe_eval(name)
        incremento = safe_eval(incremento)
        posnr_key = '%s_number' % name
        if posnr_key in self.contexto['norauto_idoc_posnrs']:
            self.contexto['norauto_idoc_posnrs'][posnr_key] += incremento
        else:
            self.contexto['norauto_idoc_posnrs'][posnr_key] = incremento
        return self.contexto['norauto_idoc_posnrs'][posnr_key]

    def _get_object_to_send_ids(self, model, condition_field, sent_field):
        """Return ids of objects based on model and condition field that must be send by this idoc
        model: name of model
        condition_field: name of the BOOLEAN field if checks true it'll generate the idoc
        sent_field: name of the BOOLEAN field to check if the record has already been made an IDOC
        """
        records = self.env[model].search([(condition_field, '=', True), (sent_field, '=', False)])
        return records.ids if records else None

    def _get_create_date(self):
        return self.contexto['date'].strftime('%Y%m%d')

    def _get_create_time(self):
        return self.contexto['date'].strftime('%H%M%S')

    def _get_serial_datetime(self):
        return self.contexto['date'].strftime('%Y%m%d%H%M%S')

    def _get_records_shpmnt(self, file):
        tree = ElementTree.parse(file)
        elementos = tree.getroot()
        documentos = self.env['stock.picking']
        products = []
        for idoc in elementos:
            e1edt20 = idoc.find('E1EDT20')
            e1edl20 = e1edt20.find('E1EDL20')
            document_number = e1edl20.find('VBELN').text
            if '|' in document_number:
                document_number = document_number.split('|')[0]
            print(document_number)
            e1adrm4 = e1edt20.findall('E1ADRM4')
            for item in e1adrm4:
                if item.find('PARTNER_Q').text == 'SP':
                    operating_unit_code = item.find('PARTNER_ID').text[-3:]
            origen = self.env.ref('stock.stock_location_stock')
            destino_padre = self.env['stock.location'].search([('name', '=', operating_unit_code)], limit=1)
            destino = self.env['stock.location'].search([('location_id', '=', destino_padre.id)])
            documento = self.env['stock.picking'].search([
                ('name', 'ilike', document_number),
                ('location_id', '=', origen.id),
                ('location_dest_id', '=', destino.id)
            ], limit=1)
            # note: el operating_unit_code es el del deposito receptor
            # self.env['stock.picking'].search([('name', 'ilike', document_number),('operating_unit_id.code', '=', operating_unit_code)], limit=1)
            # self.env['stock.picking'].search([('name', 'ilike', document_number)], limit=1)
            if documento:
                documentos = documentos | documento
            idoc_prods = []
            lines = e1edl20.findall('E1EDL24')
            for line in lines:
                product_code = line.find('MATNR').text
                product_qty = line.find('LFIMG').text
                idoc_prods.append((product_code, product_qty))
            products.append(idoc_prods)
        return documentos, products

    def _get_records_shpcon(self, file):
        print('esto es un SHPCON!!!!')
        tree = ElementTree.parse(file)
        elementos = tree.getroot()
        documentos = self.env['stock.picking']
        products = []
        for idoc in elementos:
            # e1edt20 = idoc.find('E1EDT20')
            e1edl20 = idoc.find('E1EDL20')
            document_number = e1edl20.find('VBELN').text
            if '|' in document_number:
                document_number = document_number.split('|')[0]
            print(document_number)
            documento = self.env['stock.picking'].search([
                ('name', 'ilike', document_number),
                ('picking_type_code', '=', 'incoming'),
                ('location_dest_id', '=', self.env.ref('stock.stock_location_stock').id)
            ], limit=1)
            if documento:
                documentos = documentos | documento
            idoc_prods = []
            lines = e1edl20.findall('E1EDL24')
            for line in lines:
                product_code = line.find('MATNR').text
                product_qty = line.find('LFIMG').text
                idoc_prods.append((product_code, product_qty))
            products.append(idoc_prods)
        return documentos, products

    def _get_products_and_qty(self, file):
        """recibe el archivo y tiene que devolver una lista de tuplas con el producto_code
        y la cantidad efectiva para ese producto"""
        res = []
        tree = ElementTree.parse(file)
        elementos = tree.getroot()
        for idoc in elementos:
            e1edl20 = idoc.find('E1EDL20')
            lines = e1edl20.findall('E1DL24')
            for line in lines:
                product_code = line.find('MATNR').text
                product_qty = line.find('LFIMG').text
                res.append((product_code, product_qty))
        return res

    def _filter_artmas_products(self, records_ids):
        """Para idocs artmas filtra los records segun si son Almacenables y de CCA"""
        productos = self.env['product.template'].browse(records_ids)
        filtrados = productos.filtered(lambda prod: prod.tipoart_uno == 'Almacenable' and prod.tipoart_dos == '1')
        return filtrados.ids

    def prettify(self, elem):
        """Return a pretty-printed XML string for the Element."""
        print(elem)
        rough_string = ElementTree.tostring(elem, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="  ")

    def fecha_formato_sap(self, fecha):
        if fecha not in ['X', '']:
            try:
                fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d %H:%M:%S.%f')
            except ValueError:
                try:
                    fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d %H:%M:%S')
                except ValueError:
                    fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d')

            fecha = fecha.strftime('%Y%m%d')
        return fecha

    def hora_formato_sap(self, fecha):
        try:
            fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d %H:%M:%S.%f')
        except ValueError:
            fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d %H:%M:%S')
        fecha = datetime.datetime.strptime(fecha, '%Y-%m-%d %H:%M:%S.%f')
        hora = fecha.strftime('%H%M%S')
        return hora


class NorautoIdocSegmento(models.Model):
    _name = 'norauto.idoc.segmento'
    _description = 'Modelo de segmentos para IDOCs'

    name = fields.Char('Nombre')
    xml_tag = fields.Char('Tag XML')
    norauto_idoc_id = fields.Many2one(comodel_name='norauto.idoc', string='Idoc')
    segmentos_ids = fields.One2many(
        comodel_name='norauto.idoc.segmento',
        inverse_name='norauto_idoc_segmento_id',
        string='Segmentos'
    )
    fields_ids = fields.One2many(
        comodel_name='norauto.idoc.field',
        inverse_name='norauto_segmento_id',
        string='Campos'
    )
    norauto_idoc_segmento_id = fields.Many2one(comodel_name='norauto.idoc.segmento', string='Segmento Padre')
    posicion = fields.Integer("Posicion")
    loop_field = fields.Boolean('Loop campo', help="Genera tantos segmentos como registros haya en el campo iterable, ej: lista de productos en envio")
    loopable_field = fields.Char('Campo a loopear')


class NorautoIdocField(models.Model):
    _name = 'norauto.idoc.field'
    _description = 'Modelo de campos para IDOCs'

    name = fields.Char('Nombre')
    xml_tag = fields.Char('Tag XML')
    model = fields.Char('Modelo')
    model_field_name = fields.Char('Nombre en el modelo')
    norauto_segmento_id = fields.Many2one(comodel_name='norauto.idoc.segmento', string='Segmento')
    tipo_dato = fields.Selection(
        selection=[
            ('char', 'String'),
            ('integer', 'Numero'),
            ('float', 'Decimal'),
            ('date', 'Fecha'),
            ('time', 'Hora(HHMMSS)'),
            ('datetime', 'Fecha y Hora'),
        ],
        string='Tipo de dato'
    )
    zero_fill = fields.Integer('Zero fill', help="Llena de = a la izquierda hasta completar esta cantidad de caracteres")
    valor_default = fields.Char("Default")


# pa que no se queje en local
class NorautoHistoryIdoc(models.Model):
    _name = 'norauto.history.idoc'
