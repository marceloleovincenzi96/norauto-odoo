# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    send_by_idoc = fields.Boolean('Enviar por IDOC', default=False)
    generated_idoc = fields.Boolean('IDOC generado', default=False)
    idoc_function = fields.Char('Idoc Function')  # 005 en creacion/edicion, 003 para borrar/clase9

    ##################################################
    # ORM OVERRIDES
    ##################################################
    """Overrides para ver si se debe generar un idoc basado en los campos a informar,
    si es temporal o no, o si se paso a clase 9
    """

    @api.model
    def create(self, vals):
        res = super(ProductTemplate, self).create(vals)
        if res.tipoart_dos == '1' and res.tipoart_uno == 'Almacenable' and res.clase != '9':
            res.write({
                'send_by_idoc': True,
                'idoc_function': '005',
                'generated_idoc': False
            })

        return res

    def write(self, vals):
        res = super(ProductTemplate, self).write(vals)
        for rec in self:
            if vals.get('clase') == '9' and rec.tipoart_dos == '1' and rec.tipoart_uno == 'Almacenable':
                rec.write({
                    'send_by_idoc': True,
                    'idoc_function': '003',
                    'generated_idoc': False
                })
            elif rec.tipoart_dos == '1' and rec.tipoart_uno == 'Almacenable' and rec.clase != '9':
                if 'idart' in vals or 'category_id' in vals or 'name' in vals or 'acondicionamiento_tienda' in vals or 'barcode' in vals or 'clase' in vals:
                    rec.write({
                        'send_by_idoc': True,
                        'idoc_function': '005',
                        'generated_idoc': False
                    })
        return res

    ##################################################
    # OTHERMETHODS
    ##################################################

    ##################################################
    # IDOCS GENERATION METHODS
    ##################################################
    def _get_category_id(self):
        category_name = self.categ_id.name
        name_list = category_name.lstrip(' ').rstrip(' ').split(' ')
        return name_list[0]

    def _get_categ_id_name(self):
        category_name = self.categ_id.name
        name_list = category_name.lstrip(' ').rstrip(' ').split(' ')
        return name_list[1]

    def _get_parent_categ_id_num(self):
        category_name = self.categ_id.parent_id.name
        name_list = category_name.lstrip(' ').rstrip(' ').split(' ')
        return name_list[0]

    def _get_parent_categ_id_name(self):
        category_name = self.categ_id.parent_id.name
        name_list = category_name.lstrip(' ').rstrip(' ').split(' ')
        return name_list[1]
