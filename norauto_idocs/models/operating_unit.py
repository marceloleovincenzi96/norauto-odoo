# -*- coding: utf-8 -*-

from odoo import fields, models


class OperatingUnit(models.Model):
    _inherit = 'operating.unit'

    def _default_idoc_path(self):
        return self.code

    idoc_path = fields.Char('Path Idoc', default=lambda self: self._default_idoc_path())
