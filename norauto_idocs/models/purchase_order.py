# -*- coding: utf-8 -*-

from odoo import fields, models


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    send_by_idoc = fields.Boolean('Enviar por IDOC')
    generated_idoc = fields.Boolean('IDOC generado')
    informado_idoc = fields.Boolean('Informado por SHPCON')

    # NOTE: los purchase.order que se envian a traves de desadv son los
    # pedidos al deposito central

    def _get_numero_remito(self):
        picking = self._get_picking()
        if picking and picking.voucher_ids:
            return picking.voucher_ids[0].name
            # name.split('/')[2]
        return ''

    def _get_picking_date(self):
        # Falla aca en entregas parciales cuando una orden tiene dos o mas entregas
        picking = self._get_picking()
        if picking:
            return picking.date_done
        return ''

    def _get_picking(self):
        self.ensure_one()
        picking = self.env['stock.picking'].search([('origin', '=', self.name)])
        return picking

    def _get_name_sequence(self):
        return self.name[1:]
