# -*- coding: utf-8 -*-

from odoo import fields, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    send_by_idoc_shpord = fields.Boolean('Enviar por IDOC SHPORD')
    send_by_idoc_desadv = fields.Boolean('Enviar por IDOC DESADV')
    generated_idoc = fields.Boolean('IDOC generado')

    # NOTE: los stock.picking que se envian a traves de shpord son los pedidos de transferencia intercentro
    # 5.- el desadv se activa cuando se crea/escribe y tiene como origen un documento de tipo purchase.order y es picking_type 'incoming'

    ##################################################
    # ORM OVERRIDES
    ##################################################
    def write(self, vals):
        res = super(StockPicking, self).write(vals)
        # 6.- el shpmnt se escribe con la condicion actual, que sea un picking_type_code 'internal' desde el deposito central
        for rec in self:
            send_by_desadv = rec.picking_type_code == 'incoming' and \
                rec.location_dest_id == self.env.ref('stock.stock_location_stock') and \
                len(rec.voucher_ids) > 0 and not self._context.get('written_desadv', False)
            if send_by_desadv:
                rec.with_context({'written_desadv': True}).write({'send_by_idoc_desadv': True})
        return res

    def copy(self, default=None):
        default = dict(default or {})
        default.update(
            send_by_idoc_shpord=False,
            send_by_idoc_desadv=False,
            generated_idoc=False
        )
        return super(StockPicking, self).copy(default)

    def confirmar_envio(self):
        res = super(StockPicking, self).confirmar_envio()
        self.write({'send_by_idoc_shpord': True})
        return res

    def button_validate(self):
        # NOTE: cuando se valida, si es una recepcion pongo el documento de origen tipo
        # purchase.order para enviar por Idoc
        res = super(StockPicking, self).button_validate()
        if self.picking_type_id.code == 'incoming' and self.origin:
            po = self.env['purchase.order'].search([('name', '=', self.origin)])
            # TODO: validar que no sea de tipo creado en tienda sino de central de compras
            if po:
                po.write({
                    'send_by_idoc': True,
                    'generated_idoc': False
                })
        return res

    def _get_total_weight(self):
        return 10

    def _get_total_volume(self):
        return 0

    def _get_name_sequence(self):
        return self.name.split('/')[-1]

    def _get_numero_remito(self):
        return self.voucher_ids[0].name
