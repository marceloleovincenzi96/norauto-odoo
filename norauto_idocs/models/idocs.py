# -*- coding: utf-8 -*-

from odoo import fields, models


class NorautoIdocs(models.Model):
    """Idoc en particular"""
    _name = 'norauto.idocs'
    _description = 'Norauto Idocs'

    name = fields.Char('Nombre/número')
    idoc_type_id = fields.Many2one(
        comodel_name='norauto.idoc.type'
    )
    idoc_fields_ids = fields.One2many(
        comodel_name='norauto.idoc.fields',
        inverse_name='idoc_id',
        string='Valores'
    )

    ##################################################
    # ACTION METHODS
    ##################################################
    def create_idoc_file(self):
        """Genera un archivo idoc en un path del server"""
        for rec in self:
            # TODO: generate idoc individually
            pass
        # TODO: post all created idocs
        pass

    def post_idoc(self):
        """Manda los idocs seleccionados desde el path del server a la ubicacion determinada (en configuracion)"""
        for rec in self:
            # TODO: generate idoc individually
            pass
        # TODO: post all created idocs
        pass


class NorautoIdocFields(models.Model):
    """Modelo para crear los diferentes tipos de Idocs (ARTMAS/DESAV/SHPCON/ETC)"""
    _name = 'norauto.idoc.fields'
    _description = 'Norauto Idoc Fields'

    idoc_id = fields.Many2one(
        comodel_name='norauto.idocs',
        string='Idoc'
    )
    sap_name = fields.Char('Nombre Sap')
    sap_value = fields.Char('Valor Sap')


class NorautoIdocType(models.Model):
    """Modelo para crear los diferentes tipos de Idocs (ARTMAS/DESAV/SHPCON/ETC)"""
    _name = 'norauto.idoc.type'
    _description = 'Norauto Idoc Type'

    name = fields.Char('Nombre')
    fecha = fields.Datetime('Fecha')
    objecto = fields.Char('Objeto', help='Ej.: Ordenes de Compra/Proveedores')
    tipo = fields.Selection(
        selection=[
            ('read', 'Leer'),
            ('create', 'Crear'),
            ('read_and_create', 'Leer y Crear')
        ],
        string='Tipo'
    )
    estado = fields.Selection(
        selection=[
            ('published', 'Publicado'),
            ('not_published', 'No Publicado')
        ],
        string='Estado'
    )
    cron_id = fields.Many2one(
        comodel_name='ir.cron',
        string='Cron'
    )

    ##################################################
    # ORM OVERRIDES
    ##################################################
    def create(self, vals):
        # TODO: hay que crear un cron con los datos estos
        res = super(NorautoIdocs, self).create(vals)
        return res

    def write(self, vals):
        # TODO: hay que crear un cron con los datos estos
        res = super(NorautoIdocs, self).write(vals)
        return res

    def unlink(self):
        # TODO: hay que crear un cron con los datos estos
        res = super(NorautoIdocs, self).unlink()
        return res

    ##################################################
    # ACTION METHODS
    ##################################################
    @api.multi
    def get_idoc(self):
        """Lee Idoc dependiendo el nombre desde la locación especificada (local o remota) y crea registros en Odoo"""
        """Copia Idocs del remoto al local de este tipo"""
        # Agrupar los campos en los modelos para hacer un solo write en cada modelo
        for rec in self:
            # TODO: convert
            pass
        pass

    @api.multi
    def write_idoc(self):
        """Escribe en norauto.idocs los registros de los idocs """
        pass


class NorautoIdocTypeFields(models.Model):
    """Relacion de campos y modelos del Idoc con Odoo para los tipos de Idoc"""
    _name = 'norauto.idoc.type.fields'
    _description = 'Norauto Idoc Type Fields'

    name = fields.Char('Nombre')
    sap_codigo = fields.Char('Identificación SAP')
    model_id = fields.Many2one(
        comodel_name='ir.models',
        string='Modelo',
        help='Seleccionar para filtrar los campos de cada modelo'
    )
    fields_ids = fields.Many2one(
        comodel_name='ir.model.fields',
        string='Campo'
    )
