# -*- coding: utf-8 -*-

from odoo import api, fields, models


class NorautoIdocConfig(models.TransientModel):
    _name = 'norauto.idoc.config'

    host = fields.Char('Host')
    port = fields.Char('Puerto')
    ftp_user = fields.Char('Usuario FTP')
    ftp_pswd = fields.Char('Contraseña FTP')
    path = fields.Char('Path')


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    norauto_idoc_host = fields.Char('Norauto Idoc Host')
    norauto_idoc_port = fields.Char('Norauto Idoc Port')
    norauto_idoc_ftp_user = fields.Char('Norauto Idoc FTP user')
    norauto_idoc_ftp_pswd = fields.Char('Norauto Idoc FTP pswd')
    norauto_idoc_path = fields.Char('Norauto Idoc FTP path')
    norauto_idoc_path_local = fields.Char('Path Idoc local - Nuevos')
    norauto_idoc_path_local_archivo = fields.Char('Path Idocs local - Archivo')
    norauto_idoc_path_local_inc_nuevo = fields.Char('Path Idocs local incoiming - Nuevos')
    norauto_idoc_path_local_inc_archivo = fields.Char('Path Idocs local incoming - Archivo')

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param("norauto_idoc_host", self.norauto_idoc_host)
        self.env['ir.config_parameter'].sudo().set_param("norauto_idoc_port", self.norauto_idoc_port)
        self.env['ir.config_parameter'].sudo().set_param("norauto_idoc_ftp_user", self.norauto_idoc_ftp_user)
        self.env['ir.config_parameter'].sudo().set_param("norauto_idoc_ftp_pswd", self.norauto_idoc_ftp_pswd)
        self.env['ir.config_parameter'].sudo().set_param("norauto_idoc_path", self.norauto_idoc_path)
        self.env['ir.config_parameter'].sudo().set_param("norauto_idoc_path_local", self.norauto_idoc_path_local)
        self.env['ir.config_parameter'].sudo().set_param("norauto_idoc_path_local_archivo", self.norauto_idoc_path_local_archivo)
        self.env['ir.config_parameter'].sudo().set_param("norauto_idoc_path_local_inc_nuevo", self.norauto_idoc_path_local_inc_nuevo)
        self.env['ir.config_parameter'].sudo().set_param("norauto_idoc_path_local_inc_archivo", self.norauto_idoc_path_local_inc_archivo)

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        params = self.env['ir.config_parameter'].sudo()
        norauto_idoc_host = params.get_param('norauto_idoc_host', False)
        norauto_idoc_port = params.get_param('norauto_idoc_port', False)
        norauto_idoc_ftp_user = params.get_param('norauto_idoc_ftp_user', False)
        norauto_idoc_ftp_pswd = params.get_param('norauto_idoc_ftp_pswd', False)
        norauto_idoc_path = params.get_param('norauto_idoc_path', False)
        norauto_idoc_path_local = params.get_param('norauto_idoc_path_local', False)
        norauto_idoc_path_local_archivo = params.get_param('norauto_idoc_path_local_archivo', False)
        norauto_idoc_path_local_inc_nuevo = params.get_param('norauto_idoc_path_local_inc_nuevo', False)
        norauto_idoc_path_local_inc_archivo = params.get_param('norauto_idoc_path_local_inc_archivo', False)
        res.update(
            norauto_idoc_host=norauto_idoc_host,
            norauto_idoc_port=norauto_idoc_port,
            norauto_idoc_ftp_user=norauto_idoc_ftp_user,
            norauto_idoc_ftp_pswd=norauto_idoc_ftp_pswd,
            norauto_idoc_path=norauto_idoc_path,
            norauto_idoc_path_local=norauto_idoc_path_local,
            norauto_idoc_path_local_archivo=norauto_idoc_path_local_archivo,
            norauto_idoc_path_local_inc_nuevo=norauto_idoc_path_local_inc_nuevo,
            norauto_idoc_path_local_inc_archivo=norauto_idoc_path_local_inc_archivo
        )
        return res
