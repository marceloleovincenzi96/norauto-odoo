# -*- coding: utf8 -*-
from . import account_move
from . import idoc_config
from . import norauto_idoc
from . import operating_unit
from . import product_template
from . import purchase_order
from . import stock_picking
