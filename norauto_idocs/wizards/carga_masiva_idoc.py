# -*- coding: utf-8 -*-

import xlrd
import base64
import datetime
from datetime import date
from odoo import api, fields, models


class CargaMasivaIdoc(models.TransientModel):
    _name = 'norauto.carga_masiva_idoc_wizard'
    _description = 'Wizard para cargar idocs con tabla excel'

    archivo = fields.Binary('Archivo')

    def importar(self):
        wb = xlrd.open_workbook(file_contents=base64.decodestring(self.archivo))
        required_fields = ['name']
        productos_no_encontrados = ""
        listas_no_encontradas = ""
        data = []
        for sheet in wb.sheets():
            for row in range(1, sheet.nrows):
                vals = {}
                for cel_num in range(0, sheet.ncols):
                    vals[sheet.cell(0, cel_num).value] = sheet.cell(row, cel_num).value
                data.append(vals)

        for item in data:
            idoc = self.get_idoc(item.get('idoc'))
            # NOTE: Esto va a hacer que salte las filas vacias
            if not idoc:
                continue
            segmento = self.get_segmento(item['segmento'], item['tag_xml_segmento'], idoc.id)
            campo_vals = {
                'name': str(item.get('campo')),
                'xml_tag': str(item.get('xml_tag', item.get('campo'))),
                'tipo_dato': str(item.get('tipo_dato')),
                'zero_fill': str(item.get('zero_fill')),
                'valor_default': str(item.get('valor_default')),
                'model': str(item.get('model')),
                'model_field_name': str(item.get('model_field_name')),
            }
            if item.get('subsegmento'):
                subsegmento = self.get_segmento(item['subsegmento'], item['tag_xml_subsegmento'], segmento.id, subsegment=True)
                campo = self.get_campo(campo_vals, subsegmento.id)
            else:
                campo = self.get_campo(campo_vals, segmento.id)

    def get_idoc(self, name):
        idoc = self.env['norauto.idoc'].search([('name', '=', name)])
        return idoc

    def get_segmento(self, name, tag, parent_id, subsegment=False):
        query = [('name', '=', name), ('norauto_idoc_id', '=', parent_id)]
        if subsegment:
            query = [('name', '=', name), ('norauto_idoc_segmento_id', '=', parent_id)]

        segmento = self.env['norauto.idoc.segmento'].search(query)
        if not segmento:
            segmento_vals = {'name': name, 'xml_tag': tag}
            if subsegment:
                segmento_vals.update(norauto_idoc_segmento_id=parent_id)
            else:
                segmento_vals.update(norauto_idoc_id=parent_id)
            segmento = self.env['norauto.idoc.segmento'].create(segmento_vals)
        return segmento

    def get_campo(self, vals, parent_id):
        campo = self.env['norauto.idoc.field'].search([('name', '=', vals.get('name')), ('norauto_segmento_id', '=', parent_id)])
        if not campo:
            vals['norauto_segmento_id'] = parent_id
            campo = self.env['norauto.idoc.field'].create(vals)
        else:
            campo.write(vals)
        return campo
