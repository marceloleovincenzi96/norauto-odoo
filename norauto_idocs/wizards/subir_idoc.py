# -*- coding: utf-8 -*-

import os
import base64
from odoo import exceptions, fields, models


class CargaMasivaIdoc(models.TransientModel):
    _name = 'norauto.subir_idoc_wizard'
    _description = 'Wizard para cargar idocs con tabla excel'

    archivo = fields.Binary('Archivo')
    nombre_archivo = fields.Char('Archivo')

    def subir(self):
        archivo = base64.decodestring(self.archivo)
        archivo = archivo.decode('utf-8')
        nombre = self.nombre_archivo
        if nombre.split('.')[-1].lower() != 'xml':
            raise exceptions.ValidationError('El archivo debe ser xml')
        ICPSudo = self.env['ir.config_parameter'].sudo()
        incoming_path = ICPSudo.get_param('norauto_idoc_path_local_inc_nuevo')
        new_file = os.path.join(incoming_path, nombre)
        f = open(new_file, "w")
        f.write(str(archivo))
        f.close()
