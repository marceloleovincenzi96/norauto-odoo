
# -*- encoding: utf-8 -*-

{
    'name': 'Norauto Idocs',
    'version': '13.0.1.0',
    'category': 'Nybble',
    'sequence': 10,
    'summary': 'Idocs Generation for Norauto',
    'depends': ['stock',
                'purchase',
                'transferencias_internas',
                'fields_norauto'
                # 'operating_unit',
                # 'stock_operating_unit',
                # 'purchase_operating_unit',
                ],
    'author': 'NybbleGroup',
    'description': """
NorAuto Idocs
===============
* Define Norauto Idocs requirements and path/credentials to save files to specific location

""",
    'data': [
        "security/groups.xml",
        "views/norauto_idoc_view.xml",
        "views/norauto_idoc_history_view.xml",
        "views/stock_picking_view.xml",
        # "views/purchase_order_view.xml",
        "views/product_template_view.xml",
        "views/account_move_view.xml",
        "views/idoc_config_view.xml",
        "views/operating_unit_view.xml",
        "wizards/carga_masiva_idoc.xml",
        "wizards/subir_idoc.xml",
        "security/ir.model.access.csv",
    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
