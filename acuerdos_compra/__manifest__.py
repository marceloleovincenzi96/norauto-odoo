# -*- coding: utf-8 -*-
{
    'name': "acuerdos_compra",
    'description': """
        Módulo que busca crear acuerdos de compra basado en proveedores o marcas y asignando sus respectivos descuentos al precio unitario
        del producto
    """,
    'version': "1.0",
    'author': 'Nybble Group',
    'website': "https://www.nybblegroup.com/",
    'summary': """Módulo para la creación de acuerdos de compra basado en proveedores o marcas""",
    "license": "AGPL-3",
    'category': 'Operations/Purchase',
    'depends': ['base', 'mail', 'purchase', 'aspl_pos_promotion'],
    'data': [
        'data/acuerdos_compra_data.xml',
        'security/ir.model.access.csv',
        'views/views_acuerdos_compra.xml',
        'views/purchase_views_acuerdos.xml',
    ],
    'qweb': [],
    "installable": True,
    "active": False,
}