# -*- coding: utf-8 -*-

from odoo import api, fields, models


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    # Campos ocultos para el area de compras y visibles para finanzas
    # Realizar validaciones de fechas en el domain o search
    # Calcular valor en el reporte (stock.quant)
    # Falta desarrollar el descuento por marca centralizado

    @api.depends('order_line.price_total')
    def _acuerdo_amount_all(self):
        for record in self:
            for order in record:
                acuerdo_amount_untaxed = acuerdo_amount_tax = 0.0
                for line in order.order_line:
                    acuerdo_amount_untaxed += line.acuerdo_subtotal-(line.acuerdo_subtotal*(record.acuerdo_descuento_total/100))
                    acuerdo_amount_tax += line.acuerdo_total_price_tax-(line.acuerdo_total_price_tax*(record.acuerdo_descuento_total/100))
                order.update({
                    'acuerdo_amount_untaxed': order.currency_id.round(acuerdo_amount_untaxed),
                    'acuerdo_amount_tax': order.currency_id.round(acuerdo_amount_tax),
                    'acuerdo_descuento_volumen': order.currency_id.round(record.acuerdo_descuento_volumen),
                    'acuerdo_amount_total': acuerdo_amount_untaxed + acuerdo_amount_tax - record.acuerdo_descuento_volumen,
                })

    acuerdo_id = fields.Many2one('purchase.acuerdos_compra', string="Acuerdo de compra", store=True, default=None, compute='_get_acuerdo_proveedor')
    acuerdo_amount_untaxed = fields.Monetary(string='Base imponible con descuento', store=True,
                                             readonly=True, compute='_acuerdo_amount_all',
                                             tracking=True)
    acuerdo_amount_tax = fields.Monetary(string='Impuestos con descuento', store=True, readonly=True,
                                         compute='_acuerdo_amount_all')
    acuerdo_amount_total = fields.Monetary(string='Total con descuento', store=True, readonly=True,
                                           compute='_acuerdo_amount_all')
    acuerdo_descuento_total = fields.Float("Porcentaje de descuento total", store=True, default=None, compute='_get_descuento_total')
    acuerdo_descuento_volumen = fields.Float("Descuento por volumen", store=True, default=None, compute='_get_descuento_total')

    @api.depends('partner_id')
    def _get_acuerdo_proveedor(self):
        for rec in self:
            if rec.partner_id:
                # import pdb; pdb.set_trace()
                acuerdo = rec.env['purchase.acuerdos_compra'].search([('proveedor', '=', rec.partner_id.id)], limit=1) or None
                fecha_valida = False
                rec.acuerdo_id = acuerdo

    @api.depends('order_line.price_total')
    def _get_descuento_total(self):
        self.acuerdo_descuento_total = 0
        self.acuerdo_descuento_volumen = 0
        for record in self:
            # Centralizado
            if record.acuerdo_id.descuento_centralizado and record.picking_type_id.warehouse_id.code == 'WH':
                record.acuerdo_descuento_total += record.acuerdo_id.centralizado
            # Acciones
            if record.acuerdo_id.descuento_acciones:
                record.acuerdo_descuento_total += record.acuerdo_id.acciones
            # Volumen
            if record.acuerdo_id.descuento_volumen:
                cantidad_total = 0
                for product in record.order_line:
                    cantidad_total += product.product_qty
                for cantidades_rec in record.acuerdo_id.volumen:
                    if cantidad_total >= cantidades_rec.cantidad_articulos:
                        record.acuerdo_descuento_volumen = cantidad_total * cantidades_rec.bonificacion_unit_articulo
                        break
        return


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    acuerdo_precio_unit = fields.Float(string='Precio unitario con descuento', store=True, compute='_get_acuerdo_subtotal')
    acuerdo_subtotal = fields.Monetary(string='Subtotal con descuento', store=True, compute='_get_acuerdo_subtotal')
    acuerdo_total_price_tax = fields.Monetary(string='Impuestos al subtotal', store=True, compute='_get_impuesto_total')
    # acuerdo_centralizado = fields.Boolean(string='Aplica centralizado', default=False, compute='_get_acuerdo_subtotal')

    @api.depends('price_unit', 'price_subtotal')
    def _get_acuerdo_subtotal(self):
        # desc_maxi = False
        # desc_fate = False
        # desc_sent = False
        for record in self:
            prod_marca = record.product_id.product_brand_id
            # import pdb; pdb.set_trace()
            acuerdo_marcas = self.env['purchase.acuerdos_compra'].search(
                [('marca', '=', prod_marca.id)], limit=1) or None
            if prod_marca and acuerdo_marcas:
                porcentaje_desc = acuerdo_marcas.porcentaje_descuento
                record.acuerdo_precio_unit = record.price_unit - (
                        record.price_unit * (porcentaje_desc / 100))
                record.acuerdo_subtotal = record.price_subtotal-(record.price_subtotal*(porcentaje_desc/100))
                # if prod_marca.name == 'MAXISPORT':
                #     desc_maxi = True
                # elif prod_marca.name == 'LINEA FATE':
                #     desc_fate = True
                # elif prod_marca.name == 'SENTIVA':
                #     desc_sent = True
            else:
                record.acuerdo_precio_unit = record.price_unit
                record.acuerdo_subtotal = record.price_subtotal
        # Esta validación debería ir en purchase.order no en line, y el descuento centralizado por marca aplica al total
        # if desc_maxi and desc_fate and desc_sent:
        #     self.acuerdo_centralizado = True

    @api.depends('price_unit', 'taxes_id')
    def _get_impuesto_total(self):
        for line in self:
            total_tax = 0.0
            if line.taxes_id:
                for tax in line.taxes_id:
                    total_tax += tax.amount
                line.acuerdo_total_price_tax = line.acuerdo_subtotal*(total_tax/100)
            else:
                line.acuerdo_total_price_tax = line.acuerdo_subtotal*(total_tax/100)
                break
