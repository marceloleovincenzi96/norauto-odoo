# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _, exceptions


class acuerdosCompra(models.Model):
    _name = 'purchase.acuerdos_compra'
    _description = 'Acuerdos de compra'
    _inherit = ['mail.thread']

    @api.model
    def create(self, vals):
        if 'name' not in vals or vals['name'] == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('purchase.acuerdos_compra') or _('New')
        return super(acuerdosCompra, self).create(vals)

    name = fields.Char('Nombre del acuerdo de compra', required=True, index=True,
                       copy=False, default=lambda self: _('New'))
    rubrica = fields.Selection([('1', '01 - Proveedor'), ('2', '02 - Marca')], string='Rúbrica acuerdo de compra', default='1')
    codigo_prove = fields.Integer('Código del proveedor', default=None)
    proveedor = fields.Many2one('res.partner', string="Proveedor", track_visibility='onchange', store=True, default=None, compute='_get_proveedor')
    marca = fields.Many2one('product.brand', string='Marca', track_visibility='onchange', default=lambda s: s.env['product.brand'].search([], limit=1))
    porcentaje_descuento = fields.Float(string='Porcentaje de descuento sobre el precio')
    product_id = fields.One2many('product.product', 'acuerdo_id', string='Productos')
    descuento_centralizado = fields.Boolean(string='Aplica descuento centralizado')
    descuento_acciones = fields.Boolean(string='Aplica descuento por acciones')
    descuento_volumen = fields.Boolean(string='Aplica descuento por volumen')
    centralizado = fields.Float(string='Centralizado')
    acciones = fields.Float(string='Acciones')
    start_date = fields.Date(string="Fecha de inicio", default=fields.Datetime.now)
    end_date = fields.Date(string="Fecha de fin")
    volumen = fields.One2many('purchase.acuerdos_compra_volumen', 'acuerdo_compra', string='acuerdosdecompravolumen')
    purchase_id = fields.One2many('purchase.order', 'acuerdo_id', string='Orden de compra')

    @api.depends('codigo_prove')
    def _get_proveedor(self):
        self.proveedor = self.env['res.partner'].search([('codigo_proveedor', '=', self.codigo_prove)], limit=1)

    @api.onchange('marca')
    def get_products(self):
        product_rec = self.env['product.product'].search(
            [('product_brand_id', '=', self.marca.id)])
        self.product_id = product_rec


class Acuerdos_Compra_Volumen(models.Model):
    _name = 'purchase.acuerdos_compra_volumen'
    _order = 'cantidad_articulos desc'

    cantidad_articulos = fields.Integer(string='Cantidad de artículos')
    bonificacion_unit_articulo = fields.Integer(string='Bonificación unitaria del artículo')
    total_bonificado = fields.Float(string='Total bonificado', compute='_get_total_bonificado')
    acuerdo_compra = fields.Many2one('purchase.acuerdos_compra', string='Acuerdo de compra')

    def _get_total_bonificado(self):
        for rec in self:
            rec.total_bonificado = rec.cantidad_articulos * rec.bonificacion_unit_articulo
