# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ProductProduct(models.Model):
    _inherit = 'product.product'

    acuerdo_id = fields.Many2one('purchase.acuerdos_compra',
                                 string="Acuerdo de compra", store=True,
                                 default=None)

