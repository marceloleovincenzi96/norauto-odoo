from odoo import api, fields, models


class ProductBrand(models.Model):
    _inherit = 'product.brand'
    _description = "Product Brand"

    acuerdos_ids = fields.One2many('purchase.acuerdos_compra', 'marca', string='Acuerdos por marca')


