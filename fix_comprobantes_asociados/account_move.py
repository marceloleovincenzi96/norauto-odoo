# -*- coding:utf-8 -*-

from odoo import models
import logging
_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = "account.move"

    def _get_related_invoice_data(self):
        # Llamo primero a la funcion padre, si devuelve un documento usamos ese, en caso contrario
        # dependiento del tipo de responsbilidad del partner en la factura cambio el document type del objecto dummy
        # 1 (facturas A) si es responsable inscripto o M; 6(facturas B) los demás
        # NOTE: esto tiene que ser solamente cuando son notas de credito
        # nc nce y ncemypyme; notas de debito, nde nd mypyme
        res = super(AccountMove, self)._get_related_invoice_data()
        # NOTE: 10151: si el tipo de comprobante que esta autorizando es FCE MiPyme debito o credito, hay que informar cuit de cbte.asociado
        # TODO: si el comprobante esta en Mipyme codes, sacar fecha de vencimiento
        # En la version 13, res no es un record, sino un diccionario asi que puedo evitar la creacion de la clase y crear el dict nomas

        print('----------------------------')
        print('res: %s' % res)
        # res: {'Tipo': '6', 'PtoVta': 10, 'Nro': 28}

        # _logger.info('get_related_invoices res: %s' % res)
        if not res and self.l10n_latam_document_type_id.internal_type in ['debit_note', 'credit_note']:
            res = {'Tipo': '6', 'PtoVta': 1, 'Nro': 1}
            # NOTE: hay que informar cuit de self.partner_id si el tipo de documento es mipyme
            # NOTE: ver si el codigo de tipo de documento esta entre los de mipyme (debito o credito)
            if self.partner_id.l10n_ar_afip_responsibility_type_id.code not in ['1', '1FM']:
                res['Tipo'] = 6
            else:
                res['Tipo'] = 1
            print('----------------------------')
            print('res dummy: %s' % res)
        _logger.info('get_related_invoices after dummy res: %s' % res)
        return res

# class DummyInvoiceTypeA:
#     code = 1


# class DummyInv:
#     document_type_id = DummyInvoiceTypeA()
#     point_of_sale_number = 1
#     invoice_number = 1
