
# -*- encoding: utf-8 -*-

{
    'name': 'Fix Comprovantes Asociados para notas de credito',
    'version': '1.0',
    'category': 'Nybble',
    'sequence': 1,
    'summary': 'Fix Comprovantes Asociados para notas de credito',
    'depends': ['l10n_ar_edi', ],
    'author': 'NybbleGroup',
    'description': """
Fix Comprovantes Asociados para notas de credito
===============
Pasa al metodo de conexion a AFIP un objeto dummy con valores 1,1,1 de comprobante asociado
""",
    'data': [],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
