# -*- coding: utf: 8-*-

from odoo import api, models


class OperatingUnit(models.Model):
    _inherit = 'operating.unit'

    @api.model
    def create(self, vals):
        res = super(OperatingUnit, self).create(vals)
        Warehouse = self.env['stock.warehouse']
        warehouse = Warehouse.search([('operating_unit_id', '=', res.id)])
        if not warehouse:
            warehouse = Warehouse.create({
                'name': res.name,
                'code': res.code,
                'operating_unit_id': res.id,
            })
        return res

    def write(self, vals):
        res = super(OperatingUnit, self).write(vals)
        Warehouse = self.env['stock.warehouse']
        for rec in self:
            warehouse = Warehouse.search([('operating_unit_id', '=', rec.id)])
            if warehouse:
                w_vals = {
                    'name': rec.name,
                    'code': rec.code,
                }
                # import pdb; pdb.set_trace()
                warehouse.write(w_vals)
        return res
