# -*- coding: utf-8 -*-

from odoo import api, models


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    @api.model
    def _get_quants_action(self, domain=None, extend=False):
        # NOTE: add context to edit the default_filters
        res = super(StockQuant, self)._get_quants_action(domain, extend)
        res['context'].update({
            'search_default_productgroup': 0,
            'search_default_locationgroup': 1
        })
        return res

    @api.model
    def _is_inventory_mode(self):
        """ Used to control whether a quant was written on or created during an
        "inventory session", meaning a mode where we need to create the stock.move
        record necessary to be consistent with the `inventory_quantity` field.
        """
        # NOTE: I return false so the report doesn't open on edit mode EVER
        # NOTE: the second return is the original functionality
        # return False
        return self.env.context.get('inventory_mode') is True and self.user_has_groups('stock.group_stock_manager')
