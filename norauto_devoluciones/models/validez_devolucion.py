from odoo import models, fields
from odoo.exceptions import UserError
from datetime import date, timedelta


class ValidezDevolucion(models.Model):

    _inherit = 'account.move'
    _name = _inherit

    validez_devolucion = fields.Date('Fecha de validez de devolucion', default=lambda self: self._calcular_fecha_validez_default(), required=True)

    def _calcular_fecha_validez_default(self):

        return date.today() + timedelta(days=15)

    def action_reverse(self):

        if date.today() >= self.validez_devolucion:

            raise UserError ("No se puede generar una rectificativa para esta factura. La fecha de devolucion ha expirado.")

        return super(ValidezDevolucion, self).action_reverse()
        