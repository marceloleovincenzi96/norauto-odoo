# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = ['res.partner']

    cliente_proveedor = fields.Selection(
        selection=[
            ('cliente', 'Cliente'),
            ('proveedor', 'Proveedor')
        ],
        store=True,)
    codigo_cliente = fields.Integer(
        string='Codigo del cliente',
        readonly=True,
        help='Codigo del cliente',
        copy=False,
        default=0
    )
    descuento = fields.Float(string='Descuento', help='Descuento a aplicar',)
    descuento_esp = fields.Float(string='DescuentoESP', help='Porcentaje de bonificación especial',)
    bonifadto = fields.Float(string='Bonifadto', help='Porcentaje de bonificación otorgado',)
    codFact = fields.Char(string='CodFact', help='Código Facturación',)
    condVta = fields.Char(string='CondVta', help='Condiciones de venta',)
    condvta_fija = fields.Char(string='Condvta fija', help='Condiciones de venta fija',)
    fecultvta = fields.Date(string='Fecultvta', help='Fecha última venta')
    imputacml = fields.Text(string='Imputacml', help='Imputación contable ML',)
    agrupa = fields.Selection([('flota', 'Flota'), ('otros', 'Otros'), (' ', ' ')], store=True,)
    fields.Integer(string='Agrupa', help='codigo que permite agrupar o clasificar.Buscador Maestro de clientes')
    Dom_entrega = fields.Text(string='Domentrega', help='Domicilio de entrega',)
    contacto = fields.Char(string='Contacto', help='Contacto habitual',)
    cobranzas = fields.Char(string='Cobranzas', help='Cobranzas',)
    fecha_credito = fields.Date(string='FECCREDITO', help='Fecha de credito otorgado')
    imp_credito = fields.Float(string='IMPCREDITO', help='Impuesto credito',)
    vendedor = fields.Char(string='VENDEDOR', help='Vendedor',)
    fecinhab = fields.Date(string='FECINHAB', help='Fecha de inabilitación')
    lista_prec = fields.Integer(string='LISTAPREC', help='Lista de Precio',)
    lista_fija = fields.Char(string='LISTAFIJA', help='LISTA FIJA')
    monemision_id = fields.Many2one('res.currency', string='Monemision', help='Divisa')
    # genera_est
    facturable = fields.Selection([('si', 'Si'), ('no', 'No')], default='si', string="facturable", help='SI es facturable o no')
    codigo_cuit = fields.Char(string='CodigoCUIT', help='Codigo cuit',)
    num_cta_cte = fields.Integer(string='NUMCTACTE', help='Número cuenta corriente',)
    fecha_mod = fields.Datetime(string='FECMOD', help='Fecha de modificación del cliente')
    usr_mod_id = fields.Char(string='USRMOD', help='Usuario que modificó al cliente',)
    cuenta = fields.Integer(string='CUENTA', help='Número cuenta contable')
    agente_ret = fields.Selection([('si', 'Si'), ('no', 'No')], string='AGENTERET', help='Si es AGENTE DE RECEPCIÓN(SI O NO)',)
    percep_iva = fields.Selection([('si', 'Si'), ('no', 'No')], string='PercepIva', help='Percepcion de IVA')
    lugar_cob = fields.Text(string='LUGARCOB', help='Lugar de Cobranza',)
    # fecha_alta_sap = fields.Datetime(string='FECALTA')
    usuario_alta = fields.Char(string='USUARIO')

    @api.onchange('cliente_proveedor')
    def _onchange_cliente_proveedor(self):
        if self.cliente_proveedor:
            if self.cliente_proveedor == 'cliente':
                self.customer_rank = 1
                self.supplier_rank = 0
            if self.cliente_proveedor == 'proveedor':
                self.supplier_rank = 1
                self.customer_rank = 0

    @api.model
    def create(self, vals):
        if vals.get('cliente_proveedor') == 'cliente':
            vals['codigo_cliente'] = self.env['ir.sequence'].next_by_code('cliente')
        elif vals.get('cliente_proveedor') == 'proveedor':
            vals['codigo_proveedor'] = self.env['ir.sequence'].next_by_code('proveedor')
        result = super(ResPartner, self).create(vals)
        return result
