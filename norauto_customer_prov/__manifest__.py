# -*- coding: utf-8 -*-
{
    'name': "Contacts",

    'summary': """
        Contactos""",

    'description': """
        Nybble Contacts
    """,

    'author': "Nybble Gruop",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Nybble Tools',
    'version': '13.0.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','mail','contacts','fields_norauto'],

    # always loaded
    'data': [
        'data/ir_sequence_data.xml',
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/res_partner.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
