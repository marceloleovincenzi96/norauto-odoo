odoo.define('pos_alert.caja', function (require) {
    "use strict";

    var screens = require('point_of_sale.screens');
    var gui = require('point_of_sale.gui');
    var PopupWidget = require('point_of_sale.popups');
    var models = require('point_of_sale.models')


    models.load_fields('pos.config', ['limite_advertencia_alivio', 'limite_operativo'])

    screens.ProductScreenWidget.include({
        show: function () {
            var self = this;
            this._super();
            
            console.log(this.pos);
            
            var dom = [['id', '=', this.pos.pos_session.id]];
            
            this._rpc({
                model: 'pos.session',
                method: 'search_read',
                domain: dom,
                fields: ['cash_register_balance_end']

            }).then(function (session) {

                session.forEach(function (item) {
                    
                    console.log(item);

                    if ((item.cash_register_balance_end > self.pos.config.limite_advertencia_alivio) && (item.cash_register_balance_end < self.pos.config.limite_operativo)) {

                        self.gui.show_popup('alert', {
                            title: "Advertencia",
                            body: "Por favor solicite supervisor",
                        });

                    }
                    
                    else if ((item.cash_register_balance_end > self.pos.config.limite_advertencia_alivio) && (item.cash_register_balance_end >= self.pos.config.limite_operativo)){
                        
                        self.gui.show_popup('LimiteOperativoPopup');

                    }
                });
            });

        }
    });

    var LimiteOperativoPopupWidget = PopupWidget.extend({
        template: 'LimiteOperativoPopup',

        init: function (parent, options) {
            this._super(parent, options);
            this.options = {};
        },

        show: function (options) {
            var self = this;
            this._super(options);

        },

        events: {
            'click .button.refresh': 'refresh',
        },

        refresh: function () {

            location.reload();

        },


    })
    gui.define_popup({ name: 'LimiteOperativoPopup', widget: LimiteOperativoPopupWidget });

});