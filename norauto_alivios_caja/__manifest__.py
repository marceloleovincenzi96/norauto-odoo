# -*- coding: utf-8 -*-
{
    'name': "Alivios de Caja",

    'summary': """
        """,

    'description': """
        
    """,

    'author': "Nybble Group",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Pos',
    'version': '13.0.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','point_of_sale','norauto_rights','grupo_finanzas','operating_unit'],

    # always loaded
    'data': [
        'data/rubricas_alivios.xml',
        'data/billetes.xml',
        'data/monedas.xml',
        'data/ir_sequence_data.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/alivios_caja.xml',
        'views/billetera.xml',
        'views/rubrica_alivios.xml',
        'views/pos_alivio_assets.xml',
        'views/add_warning_limit_pos_config.xml',
        'views/field_moneda_billete_cierre_caja.xml',
        #'views/informe_cierre_caja_original.xml',
        'views/pos_session.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml'
    ],
    'qweb': [
        'static/src/xml/alivio_warning.xml'
    ]
}
