from odoo import api, fields, models
from odoo.exceptions import ValidationError
from datetime import datetime


class PosSession(models.Model):

    _inherit = 'pos.session'
    _name = _inherit

    state = fields.Selection(selection_add=[('A aprobar', 'A aprobar')])

    billetera_ids = fields.Many2many(
        string='Billetes',
        comodel_name='billetera',
        relation='billetes_pos'
    )

    acumulado_alivios = fields.Float(
        string='Acumulado alivios',
    )

    alivio_fin_dia_realizado = fields.Boolean(
        string='Alivio de fin de dia realizado?',
        default=False
    )

    @api.depends('payment_method_ids', 'order_ids', 'cash_register_balance_start', 'cash_register_id')
    def _compute_cash_balance(self):

        res = super(PosSession, self)._compute_cash_balance()

        for session in self:

            if session.state in ['opened', 'closing_control', 'A aprobar']:
                session.cash_register_total_entry_encoding = session.cash_register_total_entry_encoding - session.acumulado_alivios
                session.cash_register_balance_end = session.cash_register_balance_start + session.cash_register_total_entry_encoding
                session.cash_register_difference = session.cash_register_balance_end_real - session.cash_register_balance_end

    def es_supervisor_director(self):

        if not (self.env.user.has_group('norauto_rights.group_supervisor_tienda') or self.env.user.has_group('norauto_rights.group_director_tienda')):

            raise ValidationError(
                'Permiso denegado. Usted no es supervisor o director de tienda')

    def pasar_a_aprobar(self):

        if not self.env.user.has_group('norauto_rights.group_cajero_tienda'):

            raise ValidationError(
                'Permiso denegado. Usted no pertenece al grupo de cajeros.')

        self.validar_alivio_fin_dia()

        self.write({'state': 'A aprobar'})

    def rechazar(self):

        self.es_supervisor_director()

        self.write({'state': 'closing_control'})


    def validar_alivio_fin_dia(self):

        if not self.alivio_fin_dia_realizado:

            raise ValidationError('Para avanzar con el proceso de cierre de esta sesion primero debe realizar el alivio de fin del dia.')


    def action_pos_session_validate(self):

        self.es_supervisor_director()
        self.completar_billetera_alivio()
        rubrica_cierre_caja = self.env['rubricas.alivios'].search([('code', '=', '05')], limit=1)

        alivio_creado = self.env['alivios'].with_context({'created_from_pos': True}).create({

            'tipo_rubrica_id': rubrica_cierre_caja.id,
            'aprobado_por': self.env.user.id,
            'diferencia': self.cash_register_balance_end_real,
            'fecha_alivio': datetime.now(),
            'point_of_sale_id': self.config_id.id,
            'session_id': self.id,
            'state': 'aprobado',
            'billetera_ids': [(4, billetera.id) for billetera in self.billetera_ids]

        })

        res = super(PosSession, self).action_pos_session_validate()

        return res

    def open_cashbox_pos(self):
        if self.state in ['closing_control']:
            self.validar_alivio_fin_dia()

        view_id = self.env.ref(
            'point_of_sale.view_account_bnk_stmt_cashbox_footer').id

        pos_abierto = False

        if self.state in ['new_session', 'opening_control', 'opened']:
            pos_abierto = True

        context = dict(self.env.context)

        context['estado_pos'] = pos_abierto
        context['pos_session_id'] = self.id
        context['estado_pos_session'] = self.state

        if context.get('balance'):
            context['statement_id'] = self.cash_register_id.id

        return {
            'name': "Control de efectivo",
            'view_mode': 'form',
            'res_model': 'account.bank.statement.cashbox',
            'res_id': self.env.context.get('control_efectivo_id'),
            'view_id': view_id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'flags': {'action_buttons': False},
            'domain': '[]',
            'context': context
        }

    def completar_billetera_alivio(self):

        billetera_ids = []

        control_billetes = self.cash_register_id.cashbox_end_id

        cashbox_lines = control_billetes.cashbox_lines_ids

        for linea in cashbox_lines:

            billetera = self.env['billetera'].create({

                'cantidad_billete': linea.number,
                'tipo_moneda': linea.billete_moneda,
                'denominacion_billetes_id': linea.denominaciones_billetes_id.id,
                'total': linea.subtotal

            })

            billetera_ids.append((4, billetera.id))

        self.write({

            'billetera_ids': billetera_ids

        })
