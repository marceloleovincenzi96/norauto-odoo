 #-*- coding: utf-8 -*-
from odoo import api, exceptions, fields, models


#'Información a completar en el sistema para la Billetera'
class Billetera(models.Model):
    _name = 'billetera'

    
    cantidad_billete = fields.Integer(string='Cantidad',)
    tipo_moneda = fields.Selection([('Billete','Billete')],string="Tipo de Moneda", default='Billete')
    denominacion_billetes_id = fields.Many2one('denominacion.billetes', string='Denominación billete',)
    denominacion_monedas_id = fields.Many2one('denominacion.moneda', string='Denominación moneda',)
    total = fields.Float(string='Total',)

    #inverse
    alivios_id = fields.Many2one('alivios', string='Alivios',)

    @api.onchange('cantidad_billete','denominacion_billetes_id')
    def _onchange_cantidad_billete(self):
    	if self.cantidad_billete and self.denominacion_billetes_id:
    		self.total = self.cantidad_billete*self.denominacion_billetes_id.billete

    @api.onchange('cantidad_billete','denominacion_monedas_id')
    def _onchange_denominacion_monedas_id(self):
    	if self.cantidad_billete and self.denominacion_monedas_id:
    		self.total = self.cantidad_billete*self.denominacion_monedas_id.moneda


class DenominacionMoneda(models.Model):
    _name = 'denominacion.moneda'
    _rec_name ='moneda'


    moneda = fields.Float(string='Moneda',)

class DenominacionBilletes(models.Model):
    _name = 'denominacion.billetes'
    _rec_name ='billete'

    billete = fields.Float(string='Billete',)