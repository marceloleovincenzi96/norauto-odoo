from odoo import fields, models

class Cashbox (models.Model):

    _inherit = 'account.bank.statement.cashbox'
    _name = _inherit

    es_usuario_finanzas = fields.Boolean(
        string='Es usuario finanzas?',
        compute='_es_usuario_finanzas'
    )


    def _es_usuario_finanzas(self):

        es_usuario_finanzas = False

        if self.env.user.has_group('grupo_finanzas.group_finanzas'):

            es_usuario_finanzas = True

        self.es_usuario_finanzas = es_usuario_finanzas 
    