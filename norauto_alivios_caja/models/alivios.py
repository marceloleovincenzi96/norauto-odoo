# -*- coding: utf-8 -*-
from odoo import api, fields, models, exceptions
from datetime import datetime
from odoo.exceptions import UserError


# 'Información a completar en el sistema para los Alivios'
class Alivios(models.Model):
    _name = 'alivios'
    _inherit = "mail.thread"

    state = fields.Selection(
        selection=[
            ('borrador', 'Borrador'),
            ('aaprobar', 'a Aprobar'),
            ('aprobado', 'Aprobado')],
        string='Estado del anticipo',
        default='borrador')

    name = fields.Char(string="Name", readonly=True, required=True, copy=False, default='New')

    id_alivio = fields.Integer(string='ID del alivio')

    session_id = fields.Many2one('pos.session', string='Sesión')

    point_of_sale_id = fields.Many2one('pos.config', string='Punto de venta',)

    # cajero_id = fields.Many2one('res.users', string='Cajero',)

    cajero_id = fields.Many2one(related="session_id.user_id", string='Cajero',)

    aprobado_por = fields.Many2one('res.users', string='Aprobado por')

    fecha_alivio = fields.Datetime(string='Fecha y hora de creacion', default=datetime.now())

    tienda_id = fields.Many2one(related='point_of_sale_id.operating_unit_id', string='Tienda',)

    monto_alivio = fields.Float(string='Monto del Alivio', store=True)

    tipo_rubrica_id = fields.Many2one('rubricas.alivios', string='Tipo de rúbrica',)

    comentario = fields.Char(string='Comentario',)

    billetera_ids = fields.One2many('billetera', 'alivios_id', string='Billetera',)

    # amount_total = fields.Monetary(string='Total Billetera', store=True, readonly=True, compute='_total_billetera')

    total_billetera = fields.Float(string="Total Billetera", store=True, readonly=True, compute='_total_billetera')

    diferencia = fields.Float(string='Diferencia')

    rendicion_gasto_id = fields.Many2one('pos.rendicion_gastos', string='Rendicion de gasto')

    # Notas de los codes de las rubricas
    # '01' = Alivio de caja
    # '02' = Alivio atm
    # '03' = Movimiento al fondo fijo
    # '04' = Remesa de cambio
    # '05' = Cierre de caja
    # '06' = Alivio error tarjeta
    # '07' = Alivio error efectivo
    # '08' = Efectivo en caja fin del dia
    # '09' = Gasto Fondo Fijo-Transitoria

    @api.onchange('tipo_rubrica_id', 'point_of_sale_id')
    def onchange_tipo_rubrica_id(self):
        res = {}
        if self.tipo_rubrica_id.code == '08':
            res['domain'] = {'session_id': ["&", ["state", "=", 'closing_control'], ["config_id.operating_unit_id", "=", self.tienda_id.id]]}

        else:
            res['domain'] = {'session_id': ["&", ["state", "=", 'opened'], ["config_id.operating_unit_id", "=", self.tienda_id.id]]}

        return res

    @api.depends('billetera_ids')
    def _total_billetera(self):
        billetera = self.billetera_ids
        total_linea = 0.0
        for line in billetera:
            total_linea += line.total
        self.total_billetera = total_linea

    def es_rubrica_cierre_caja(self, vals):
        if vals.get('tipo_rubrica_id'):
            rubrica = self.env['rubricas.alivios'].browse(vals.get('tipo_rubrica_id'))
            if rubrica.code == '05':
                raise UserError('Los cierres de caja solo pueden realizarse desde el Punto de Venta al cerrar una sesion de los mismos (Punto de Venta / Boton "Cerrar" sobre el POS).')


    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('alivios') or 'New'

        if not self._context.get('created_from_pos'):
            self.es_rubrica_cierre_caja(vals)

        result = super(Alivios, self).create(vals)
        return result

    def write(self, vals):

        # self.es_rubrica_cierre_caja(vals)

        return super(Alivios, self).write(vals)

    def action_pasar_aaprobar(self):
        if self.env.user.has_group('norauto_rights.group_cajero_tienda'):
            if self.tipo_rubrica_id.code in ['01', '02', '03', '04', '09']:
                self.validar_monto_alivio()
            self.write({'state': 'aaprobar'})
        else:
            raise exceptions.ValidationError('Permiso denegado. Solo usuarios Cajero on los unicos que pueden pasarlos el estado a aprobar')


    def action_aprobado(self):
        # self.state = 'aprobado'
        if self.env.user.has_group('norauto_rights.group_supervisor_tienda'):

            if self.tipo_rubrica_id.code == '03':
                fondo_fijo_tienda = self.env["fondo_fijo.tienda"].search([('operating_unit_id', '=', self.tienda_id.id)])
                nuevo_fondo_fijo = fondo_fijo_tienda.fondo_fijo_ahora + self.monto_alivio

                if nuevo_fondo_fijo > fondo_fijo_tienda.fondo_fijo:
                    raise exceptions.UserError('Se esta excediendo de la cantidad original del fondo fijo. Fondo fijo original: %.2f. Fondo fijo con el monto del alivio: %.2f.' % (fondo_fijo_tienda.fondo_fijo, nuevo_fondo_fijo))

                fondo_fijo_tienda.write({'fondo_fijo_ahora': nuevo_fondo_fijo})

            if self.tipo_rubrica_id.code not in ['04', '07']:
                pos_session = self.session_id
                # cash = pos_session.cash_register_total_entry_encoding - self.total_billetera
                pos_session.write({'acumulado_alivios': pos_session.acumulado_alivios + self.total_billetera})

                if self.tipo_rubrica_id.code == '08':
                    pos_session.write({'alivio_fin_dia_realizado': True})

            if self.tipo_rubrica_id.code == '07':
                pos_session = self.session_id
                # cash = pos_session.cash_register_total_entry_encoding - self.total_billetera
                pos_session.write({'acumulado_alivios': pos_session.acumulado_alivios - self.total_billetera})

            self.write({'state': 'aprobado'})

        else:
            raise exceptions.ValidationError('Permiso denegado. Solo usuarios Supervisor de Tienda son los unicos que pueden pasarlos el estado a aprobar')


    def action_anulado(self):
        if self.env.user.has_group('norauto_rights.group_supervisor_tienda'):

            if self.tipo_rubrica_id.code == '03':
                fondo_fijo_tienda = self.env["fondo_fijo.tienda"].search([('operating_unit_id', '=', self.tienda_id.id)])
                nuevo_fondo_fijo = fondo_fijo_tienda.fondo_fijo_ahora - self.monto_alivio

                if nuevo_fondo_fijo < 0:
                    raise exceptions.UserError('No puede quitar mas dinero al fondo fijo. Fondo fijo original: %.2f. Fondo fijo con el monto del alivio: %.2f.' % (fondo_fijo_tienda.fondo_fijo, nuevo_fondo_fijo))

                fondo_fijo_tienda.write({'fondo_fijo_ahora': nuevo_fondo_fijo})

            if self.tipo_rubrica_id.code not in ['04', '07']:
                pos_session = self.session_id
                # cash = pos_session.cash_register_total_entry_encoding + self.total_billetera
                pos_session.write({'acumulado_alivios': pos_session.acumulado_alivios - self.total_billetera})

                if self.tipo_rubrica_id.code == '08':
                    pos_session.write({'alivio_fin_dia_realizado': False})

            if self.tipo_rubrica_id.code == '07':
                pos_session = self.session_id
                # cash = pos_session.cash_register_total_entry_encoding - self.total_billetera
                pos_session.write({'acumulado_alivios': pos_session.acumulado_alivios + self.total_billetera})

            self.write({'state': 'borrador'})

        else:
            raise exceptions.ValidationError('Permiso denegado. Solo usuarios Supervisor de Tienda son los unicos que pueden pasarlos el estado a aprobar')

    @api.onchange('total_billetera', 'rendicion_gasto_id')
    def _onchange_monto_alivio(self):

        if self.tipo_rubrica_id.code == '03':
            self.monto_alivio = self.rendicion_gasto_id.monto_rendido
        else:
            self.monto_alivio = self.total_billetera

    def validar_monto_alivio(self):
        monto_rubrica_alivio = self.tipo_rubrica_id.monto_rubrica_alivio
        if self.monto_alivio > monto_rubrica_alivio:
            raise exceptions.ValidationError("El monto del Alivio es mayor al establecido: %s" % monto_rubrica_alivio)


    # @api.onchange('monto_alivio','total_billetera')
    # def _onchange_billetera_ids(self):
    #     if self.monto_alivio != 0 and self.total_billetera and self.monto_alivio != self.total_billetera:
    #         raise exceptions.ValidationError("El monto del Alivio: %s" % monto_alivio % "es diferente al total de la billetera: %s" % total_billetera)
    #     else:
    #         pass
