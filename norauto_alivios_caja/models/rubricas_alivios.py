# -*- coding: utf-8 -*-
from odoo import fields, models


# 'Información a completar en el sistema para los Alivios'
class RubricasAlivios(models.Model):
    _name = 'rubricas.alivios'

    name = fields.Char(string='Nombre',)
    description_rubrica = fields.Char(string='Descripción de la rúbica')
    monto_rubrica_alivio = fields.Float(string='Monto rúbica del alivio')
    code = fields.Char('Código')
