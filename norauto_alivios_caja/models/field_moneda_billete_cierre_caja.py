from odoo import fields, models, api

class FieldMonedaBilleteWizardCierreCaja(models.Model):

    _inherit = 'account.cashbox.line'
    _name = _inherit

    
    
    billete_moneda = fields.Selection(
        string='Es moneda o billete?',
        selection=[('Billete', 'Billete')],
        default='Billete'
    )

    '''denominaciones_monedas_id = fields.Many2one(
        string='Denominacion moneda',
        comodel_name='denominacion.moneda',
        ondelete='restrict',
    )'''

    
    denominaciones_billetes_id = fields.Many2one(
        string='Denominacion billete',
        comodel_name='denominacion.billetes',
        ondelete='restrict',
    )
    
    
    
    @api.onchange('denominaciones_billetes_id')
    def calculo_coin_value_linea(self):
        self.coin_value = self.denominaciones_billetes_id.billete

