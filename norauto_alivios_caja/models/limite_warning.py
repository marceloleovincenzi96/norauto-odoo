from odoo import api, fields, models
from odoo.exceptions import ValidationError

class LimiteAdvertenciaAlivio(models.Model):

    _inherit = 'pos.config'
    _name = _inherit


    limite_advertencia_alivio = fields.Float('Limite monetario para advertencia en POS', default=12500)

    limite_operativo = fields.Float('Limite operativo', default=28000)

    
    @api.model
    def create(self, vals):

        res = super(LimiteAdvertenciaAlivio, self).create(vals)
        
        if res.limite_operativo <= res.limite_advertencia_alivio:

            raise ValidationError('El limite operativo no puede ser menor que el limite de la advertencia del alivio.')
        
        return res
    

    def write(self, vals):

        res = super(LimiteAdvertenciaAlivio, self).write(vals)

        if self.limite_operativo <= self.limite_advertencia_alivio:

            raise ValidationError('El limite operativo no puede ser menor que el limite de la advertencia del alivio.')
        
        return res