
# -*- encoding: utf-8 -*-

{
    'name': 'POS Payment Method Quantities',
    'version': '13.0.1.0',
    'summary': 'POS Payment Method Quantities',
    'depends': [
        'point_of_sale',
        'nybble_recargos_tarjetas',
        'tarjetas_master'
    ],
    'author': 'Nybble Group',
    'data': [

        'views/pos_session.xml',
        'views/cobros_medios_pago.xml',
        'security/ir.model.access.csv'
    ],
    'qweb': [],
    'demo': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
