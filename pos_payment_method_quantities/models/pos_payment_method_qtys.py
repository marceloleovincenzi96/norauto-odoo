from odoo import models, api, fields

class PosPaymentMethodQuantities(models.Model):

    _name = 'pos.payment.method.quantities'
    _description = 'POS Payment Method Quantities'

    
    payment_method_id = fields.Many2one(
        string='Metodo de pago',
        comodel_name='pos.payment.method',
        ondelete='restrict',
    )

    card_name = fields.Char(
        string='Nombre de la tarjeta'
    )
    
    theoretical_qty = fields.Float(
        string='Cantidad teorica total',
        readonly = True
    )

    
    actual_qty = fields.Float(
        string='Cantidad real',
    )

    difference = fields.Float(
        string='Diferencia',
        compute='calculate_difference'
    )
    
    pos_session_id = fields.Many2one(
        string='POS Session',
        comodel_name='pos.session',
        ondelete='restrict',
    )
    


    @api.depends('theoretical_qty', 'actual_qty')
    def calculate_difference(self):

        for record in self:
            record.difference = record.theoretical_qty - record.actual_qty
    


    
    
