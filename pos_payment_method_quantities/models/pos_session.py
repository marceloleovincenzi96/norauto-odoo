from odoo import models, api, fields

class PosSession(models.Model):

    _inherit = 'pos.session'
    _name = _inherit

    
    pos_payment_method_quantities_ids = fields.One2many(
        string='POS Payment Method Quantities',
        comodel_name='pos.payment.method.quantities',
        inverse_name='pos_session_id',
    )

    pos_payment_method_quantities_con_cuotas_ids = fields.One2many(
        string='POS Payment Method Quantities Con Cuotas',
        comodel_name='pos.payment.method.quantities.cuota',
        inverse_name='pos_session_id',
    )

    
    