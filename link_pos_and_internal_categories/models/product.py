# -*- coding:utf-8 -*-

from odoo import api, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    ##################################################
    # ONCHANGES
    ##################################################
    @api.onchange('categ_id')
    def onchange_categ_id(self):
        if self.categ_id:
            pos_categ = self.env['pos.category'].search([('name', '=', self.categ_id.name)])
            if pos_categ:
                self.pos_categ_id = pos_categ.id

    @api.onchange('pos_categ_id')
    def onchange_pos_categ_id(self):
        if self.pos_categ_id:
            categ = self.env['product.category'].search([('name', '=', self.pos_categ_id.name)])
            if categ:
                self.categ_id = categ.id

    ##################################################
    # ACTION METHODS
    ##################################################

    def create_categories(self):
        """Deletes all pos categories and create new ones based on product.categories"""
        # NOTE: next line deletes all pos category records
        self.env['pos.category'].search([]).unlink()
        categories = self.env['product.category'].search([])

        for category in categories:
            pos_category = self.env['pos.category'].create({
                'name': category.name,
                'product_categ_id': category.id
            })
            category.write({'pos_categ_id': pos_category.id})

        # TODO: crear las relaciones con los parent_ids
        for category in categories:
            if category.parent_id:
                pos_categ = self.env['pos.category'].search([('name', '=', category.name)])
                pos_categ.parent_id = self.env['pos.category'].search([('name', '=', category.parent_id.name)])

    
    def link_products_with_pos_categories(self):

        products = self.env['product.template'].browse(self.id)
        for product in products:
            self.available_in_pos = True
            product.pos_categ_id = self.env['pos.category'].search([('name', '=', product.categ_id.name)]).id


    def link_all_products_with_pos_categories(self):

        products = self.env['product.template'].search([])
        for product in products:
            self.available_in_pos = True
            product.pos_categ_id = self.env['pos.category'].search([('name', '=', product.categ_id.name)]).id
