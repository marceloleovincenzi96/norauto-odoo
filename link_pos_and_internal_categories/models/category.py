# -*- coding: utf-8 -*-

from odoo import api, exceptions, fields, models


class ProductCategory(models.Model):
    _inherit = 'product.category'

    pos_categ_id = fields.Many2one(
        comodel_name='pos.category',
        string='Pos Category equivalent'
    )

    ##################################################
    # CONSTRAINS
    ##################################################
    @api.constrains('name')
    def _validate_name(self):
        category_with_same_name = self.search([('name', '=', self.name), ('id', '!=', self.id)])
        if category_with_same_name:
            raise exceptions.ValidationError('Dos categorias no pueden tener el mismo nombre')

    ##################################################
    # ORM OVERRIDES
    ##################################################
    @api.model
    def create(self, vals):
        res = super(ProductCategory, self).create(vals)
        pos_parent = False
        if res.parent_id:
            pos_parent = res.parent_id.pos_categ_id

        pos_categ = self.env['pos.category'].create({
            'name': res.name,
            'parent_id': pos_parent.id if pos_parent else False
        })
        res.write({
            'pos_categ_id': pos_categ.id
        })
        return res

    def write(self, vals):
        res = super(ProductCategory, self).write(vals)

        self.pos_categ_id.write({
            'name': self.name,
            'parent_id': self.parent_id.pos_categ_id.id
        })

        return res

    
    def unlink(self):
        res = super(ProductCategory, self).unlink()
        return res


class PosCategory(models.Model):
    _inherit = 'pos.category'

    product_categ_id = fields.Many2one(
        comodel_name='product.category',
        string='Product Category equivalent'
    )

    ##################################################
    # CONSTRAINS
    ##################################################
    @api.constrains('name')
    def _validate_name(self):
        category_with_same_name = self.search([('name', '=', self.name), ('id', '!=', self.id)])
        if category_with_same_name:
            raise exceptions.ValidationError('Dos categorias no pueden tener el mismo nombre')
