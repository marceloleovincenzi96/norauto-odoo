# -*- coding: utf-8 -*-
{
    'name': 'Link Internal and POS categories',
    'version': '1.0',
    'summary': 'Link Internal and POS categories',
    'sequence': 1,
    'description': """
Link Internal and POS categories
======================
When you create, edit, or delete a category it will also affect POS category \n
Inside a product record as admin, click Create Categories and then Link Categories \n
Note that create categories will delete previous Pos Categories
    """,
    'category': 'Nybble',
    'website': '',
    'author': 'NybbleGroup',
    'depends': [
        'product',
        'point_of_sale',
    ],
    'data': [
        'views/product.xml',
    ],
    'demo': [
    ],
    'qweb': [
        # "static/src/xml/account_reconciliation.xml",
    ],
    # 'images': ['static/description/icon.png'],
    'installable': True,
    'application': False,
    'auto_install': False,
}
