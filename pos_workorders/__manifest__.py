# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    All Rights Reserved.
#
############################################################################
#
############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name": "Ordenes de trabajo en POS",
    "version": "1.0",
    'author': 'Nybble Group',
    "summary": """Ordenes de trabajo en POS para Norauto""",
    "license": "AGPL-3",
    "depends": ['stock', 'base', 'point_of_sale'],
    "data": [
        'views/pos_assets.xml',
        'views/pos_workorders.xml',
        'views/payment_method.xml'

    ],
    'qweb': ['static/src/xml/pos_button.xml'],
    "installable": True,
    "active": False,
}
