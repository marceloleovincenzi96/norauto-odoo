odoo.define('pos_button.custom', function (require) {
    "use strict";

    var gui = require('point_of_sale.gui');
    var core = require('web.core');
    var models = require('point_of_sale.models');
    var rpc = require('web.rpc');
    var PopupWidget = require('point_of_sale.popups');
    var pos_screens = require('point_of_sale.screens');
    var Paymentline = models.Paymentline;
    var QWeb = core.qweb;

    models.load_fields('pos.config', ['operating_unit_id']);
    models.load_fields('res.users', ['operating_unit_ids', 'default_operating_unit_id']);
    models.load_fields('pos.payment.method', ['pago_anticipos']);

    /* load_models tambien puede tomar el dominio asi que podemos cargar solo las
    ordenes necesarias en vez de cargar todo y LUEGO filtrarlas */
    models.load_models({
        model: 'norauto.order',
        fields: ['name', 'partner_id', 'state', 'norauto_order_line_ids',
                 'operating_unit_id', 'valor_anticipo'],
        domain: [['state', 'in', ['done']]],
        loaded: function (self, workorders) {

            var filtrados = workorders.filter(function (order) {
                return order.operating_unit_id[0] == self.config.operating_unit_id[0];
            })
            self.workorders = filtrados;

            self.workorders_ids = filtrados.map(function(orden){
                return orden.id
            })

        },
        
    })
    /* En este momento se cargan en sincronia asi que esta disponible los workorders_ids
    Hacemos filtro de las lineas de workorders a las efectivamente cargadas y no todas
    como ahora, esto va a sacar mucho tiempo de carga */    
    models.load_models({
        model: 'norauto.order.line',
        fields: ['norauto_order_id', 'product_id', 'cantidad',
        'descuento', 'rubrica_descuento', 'usuario_descuento'],
        loaded: function (self, workorder_lines) {
            var filtrados = workorder_lines.filter(function (line) {
                return self.workorders_ids.includes(line.norauto_order_id[0]);
            })
            self.workorder_lines = filtrados;

        }
    })



    var _super_pos_model = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({
        get_workorder_fields(){
            return ['name', 'partner_id', 'state', 'norauto_order_line_ids',
                    'operating_unit_id', 'valor_anticipo']
        },
        actualizar_ordenes_taller: function(){
            var self = this;
            rpc.query({
                'model': 'norauto.order',
                'method': 'search_read',
                'domain': [
                    ['operating_unit_id', '=', self.config.operating_unit_id[0]],
                    ['state', 'in', ['done']],
                    ['id', 'not in', self.workorders_ids]
                ],
                'fields': self.get_workorder_fields()
            }).then(function(ordenes_nuevas){
                /* Cargar las lineas de la nueva workorder, por el dominio solo van a 
                venir las que no esten cargadas asi que le hago push directamente */
                var workorders_nuevas_ids = []
                ordenes_nuevas.forEach(function(orden){
                    self.workorders_ids.push(orden.id);
                    self.workorders.push(orden);
                    workorders_nuevas_ids.push(orden.id)
                })

                rpc.query({
                    'model': 'norauto.order.line',
                    'method': 'search_read',
                    'domain':[['norauto_order_id', 'in', workorders_nuevas_ids]]
                }).then(function(lineas_nuevas){
                    lineas_nuevas.forEach(function(linea){
                        self.workorder_lines.push(linea);
                    })

                })
            })
        },
        get_workorder_by_id: function(id){
            var workorder = this.workorders.filter(function(workorder){
                return workorder.id == id;
            })
            if (workorder.length > 0){
                return workorder[0]
            } else {
                return false
            }
        }
        
    })

    var _super_order = models.Order.prototype;
    models.Order = models.Order.extend({
        initialize: function(attr, options){
            this.anticipo_workorder = 0;
            this.workorder_id = null;
            _super_order.initialize.call(this, attr, options);
        },
        get_total_with_tax: function() {
            return this.get_total_without_tax() + this.get_total_tax() - this.anticipo_workorder;
        },

        export_as_JSON: function(){
            var json = _super_order.export_as_JSON.apply(this, arguments);
            json.anticipo_workorder = this.anticipo_workorder;
            json.workorder_id = this.workorder_id;
            return json;

        },

        init_from_JSON: function(json){
            _super_order.init_from_JSON.apply(this, arguments);
            this.anticipo_workorder = json.anticipo_workorder;
            this.workorder_id = json.workorder_id;
        },
        remove_orderline: function( line ){
            _super_order.remove_orderline.call(this, line);
            this.sacar_anticipo();
        },
        sacar_anticipo: function(){
            var self = this;
            var orderlines = this.get_orderlines()
            if (orderlines.length < 1){
                self.anticipo_workorder = 0;
                self.workorder_id = null
            }
        },
        
    })
    
    pos_screens.OrderWidget.include({
        update_summary: function(){
            var self = this;
            this._super();
            var order = this.pos.get_order()
            if (!order.get_orderlines().length) {
                return;
            }

            var anticipo = order ? order.anticipo_workorder : 0;
            
            this.el.querySelector('.anticipototal .value').textContent = this.format_currency(anticipo);
            /* Redefino la asignacion al total de taxes porque sino hace el total con impuestos menos el total sin impuestos
            y esto da resultados raros cuando en la orden tiene anticipo
            Y DE TODOS MODOS DEBERIAN HABERLO HECHO ASI DESDE EL COMIENZO! */
            var taxes     = order ? order.get_total_tax() : 0;
            this.el.querySelector('.summary .total .subentry .value').textContent = this.format_currency(taxes);
        }
    })

    var OrdersScreenWidget = pos_screens.ScreenWidget.extend({
        template: 'OrdersScreenWidget',
        init: function (parent, options) {
            this._super(parent, options);
        },

        show: function () {
            var self = this;
            this._super();
            this.renderElement();

            var orders = this.pos.workorders;

            this.$('.back').click(function () {
                self.gui.back();
            });
            this.$('.refresh').click(function () {
                location.reload();
            });

            this.render_list(orders);
            var search_timeout = null;
            this.$('.searchbox input').on('keypress', function (event) {
                clearTimeout(search_timeout);

                var searchbox = this;

                search_timeout = setTimeout(function () {
                    self.perform_search(searchbox.value, event.which === 13);
                }, 70);
            });

            this.$('.searchbox .search-clear').click(function () {
                self.clear_search();
            });
            // this.$('.return_workorder').click(function (e) {
            //     var order = $(e.target).closest("tr").data('id');
            //     var anticipo = $(e.target).closest("tr").data('anticipo');
            //     self.return_workorder(order, anticipo);
            // });
            // this.$('.return_anticipo').click(function (e) {
            //     var anticipo = $(e.target).closest("tr").data('id');
            //     self.return_anticipo(anticipo);
            // });
            // this.$('.reject_workorder').click(function (e) {
            //     var workorder_id = $(e.target).closest("tr").data('id');
            //     self.reject_workorder(workorder_id);
            // });
        },

        return_anticipo: function(anticipo){
            this.gui.show_popup('AnticipoWidget', { workorder : anticipo});
        },

        hide: function () {
            this._super();
        },

        get_orders: function () {
            return this.gui.get_current_screen_param('workorders');
        },

        perform_search: function (query, associate_result) {
            var workorders;
            if (query) {
                workorders = this.search_order(query);
                this.render_list(workorders);
            } else {
                workorders = this.pos.workorders;
                this.render_list(workorders);
            }
        },

        search_order: function (query) {
            try {
                var re = RegExp(query, 'i');
            } catch (e) {
                return [];
            }
            var results = [];
            for (var workorder_id in this.pos.workorders) {
                var r = re.exec(this.pos.workorders[workorder_id]['name'] + '|' + this.pos.workorders[workorder_id]['partner_id'][1]);
                if (r) {
                    results.push(this.pos.workorders[workorder_id]);
                }
            }
            return results;
        },

        render_list: function (workorders) {
            var self = this;
            var contents = this.$el[0].querySelector('.workorder-list-contents');
            contents.innerHTML = "";
            for (var i = 0, len = Math.min(workorders.length, 1000); i < len; i++) {
                var workorder = workorders[i];
                var workorderline_html = QWeb.render('WorkOrderLine', { widget: this, workorder: workorder });
                var workorderline = document.createElement('tbody');
                workorderline.innerHTML = workorderline_html;
                workorderline = workorderline.childNodes[1];
                contents.appendChild(workorderline);
            }
            self.$('.return_workorder').click(function (e) {
                var order = $(e.target).closest("tr").data('id');
                var anticipo = $(e.target).closest("tr").data('anticipo');
                self.return_workorder(order, anticipo);
            });
            self.$('.return_anticipo').click(function (e) {
                var anticipo = $(e.target).closest("tr").data('id');
                self.return_anticipo(anticipo);
            });
            self.$('.reject_workorder').click(function (e) {
                var workorder_id = $(e.target).closest("tr").data('id');
                self.reject_workorder(workorder_id);
            });
        },

        return_workorder: function (workorder_id, anticipo) {
            var self = this;
            var order = self.pos.get_order()
            var workorder = self.pos.workorders.filter(function (item) {
                return workorder_id == item.id;
            })
            console.log('workordeeeeeeer: ', workorder)
            var product_tmpl_ids = workorder[0].norauto_order_line_ids.map(function(line_id){
                var line = self.pos.workorder_lines.filter(function (line_wo) {
                    return line_wo.id == line_id;
                })
                console.log('lineaaaaaaa: ', line)
                return line[0].product_id[0]
            })

            workorder[0].norauto_order_line_ids.forEach(function (line_id) {
                var line = self.pos.workorder_lines.filter(function (line_wo) {
                    return line_wo.id == line_id;
                })

                var product = self.get_product_by_product_tmpl_id(line[0].product_id[0]);
                if(!product){
                    console.log('----------------------------------------')
                    console.log('Cargar producto dinamicamente via rpc')
                    console.log('----------------------------------------')
                    rpc.query({
                        model:'product.product',
                        method:'search_read',
                        fields: ['display_name', 'lst_price', 'standard_price', 'categ_id', 'pos_categ_id', 'taxes_id',
                        'barcode', 'default_code', 'to_weight', 'uom_id', 'description_sale', 'description',
                        'product_tmpl_id','tracking'],
                        domain: [['product_tmpl_id', 'in', product_tmpl_ids]],
                        args: []
                    }).then(function(products){
                        console.log('produuuuuuuuuuuuuuuuucts: ', products)
                        // self.pos.db.add_products(products)
                        var using_company_currency = self.pos.config.currency_id[0] === self.pos.company.currency_id[0];
                        var conversion_rate = self.pos.currency.rate / self.pos.company_currency.rate;
                        self.pos.db.add_products(_.map(products, function (product) {
                            if (!using_company_currency) {
                                product.lst_price = round_pr(product.lst_price * conversion_rate, self.currency.rounding);
                            }
                            product.categ = _.findWhere(self.product_categories, {'id': product.categ_id[0]});
                            return new models.Product({}, product);
                        }));
                        
                        product = self.get_product_by_product_tmpl_id(line[0].product_id[0]);
                        order.anticipo_workorder = parseFloat(anticipo);
                        order.add_product(product, {
                            price: product.lst_price,
                            quantity: line[0].cantidad,
                            discount: line[0].descuento,
                            merge: false,
                            extras: {},
                        });
                    })
                } else {

                    order.anticipo_workorder = parseFloat(anticipo);
                    order.add_product(product, {
                        price: product.lst_price,
                        quantity: line[0].cantidad,
                        discount: line[0].descuento,
                        merge: false,
                        extras: {},
                    });
                }

            })

            /* Si no hay partner en pos.db los buscamos por rpc query y lo agregamos */
            if (!self.pos.db.get_partner_by_id(workorder[0].partner_id[0])){
                this._rpc({
                    model:'res.partner',
                    method:'search_read',
                    fields:['name','street','city','state_id','country_id','vat',
                            'phone','zip','mobile','email','barcode','write_date',
                            'property_account_position_id','property_product_pricelist',
                            'l10n_latam_identification_type_id', 'l10n_ar_afip_responsibility_type_id'],
                    domain: [['id', '=', workorder[0].partner_id[0]]],
                    args: []
    
                }).then(function(new_partners){
                    
                    if (new_partners){
                        self.pos.db.add_partners(new_partners)
                    }
 
                    order.set_client(self.pos.db.get_partner_by_id(workorder[0].partner_id[0]))
                })
            }
            else{
                order.set_client(self.pos.db.get_partner_by_id(workorder[0].partner_id[0]))

            }
            order.workorder_id = workorder[0].id;
            self.gui.show_screen('products');

            /*if (self.options.client) {
                self.pos.get_order().set_client(self.pos.db.get_partner_by_id(self.options.client));
            }*/
        },

        reject_workorder: function(workorder_id){
            // Pasa el estado de la workorder a to_do por rpc_query a la db y en el pos. Como sigue apareciendo
            // en el pos con ese estado no hay que esconderlo en el pos
            var self = this;
            var order = self.pos.get_order()
            var workorder = self.pos.workorders.filter(function (item) {
                return workorder_id == item.id;
            })
            this._rpc({
                model:'norauto.order',
                method:'reject_order',
                args: [[workorder_id]]
            }).then(function(res){
                workorder[0].state = 'to_do';
            })
            self.gui.show_screen('products');
        },

        get_product_by_product_tmpl_id: function (product_tmpl_id) {
            self = this;
            let key;
            for (key in self.pos.db.product_by_id) {
                if (self.pos.db.product_by_id[key].product_tmpl_id == product_tmpl_id) {
                    return self.pos.db.product_by_id[key];
                }
            }
            return false;
        }
        // OrderSuper.prototype.set_client.call(this, this.client);

    });

    gui.define_screen({ name: 'workorderlist', widget: OrdersScreenWidget });
    var workorder_button = pos_screens.ActionButtonWidget.extend({
        template: 'BtnWorkorder',
        button_click: function () {
            var self = this;
            this.pos.actualizar_ordenes_taller()
            var workorders = this.pos.workorders;
            setTimeout(function(){
                self.gui.show_screen('workorderlist', { workorders: workorders });
            }, 500)
        }
    });
    pos_screens.define_action_button({
        'name': 'button_wo',
        'widget': workorder_button
    });

    var AnticipoWidget = PopupWidget.extend({
        template: 'AnticipoWidget',

        init: function (parent, options) {
            this._super(parent, options);
            this.options = {};
            this.anticipo = "";
            this.workorder = "";

        },
        show: function (options) {
            this._super(options);

        },

        events: {
            'click .button.cancel': 'click_cancel',
            'click .button.confirm': 'click_confirm',
        },


        click_confirm: function () {

            var self = this;
            var anticipado = $('.anticipado').val()

            this._rpc({

                model:'norauto.order',
                method:'write',
                args: [[self.options.workorder], {'valor_anticipo': anticipado}]

            })
            /* Crear un cash.box.out y luego ejecutar la funcion run 
            Esto imita el boton de tomar/sacar efectivo dentro de la sesion de pos*/
            var cash_box_out_vals = {
                'name': 'Anticipo orden de trabajo',
                'amount': anticipado
            }
            this._rpc({

                model:'cash.box.out',
                method:'create',
                args: [cash_box_out_vals]

            }).then(function(cash_box_out){
                self._rpc({
                    model:'cash.box.out',
                    method:'run',
                    context: {'active_model': 'pos.session', 'active_ids': [self.pos.pos_session.id]},
                    args: [[cash_box_out]]
                })
            })

            self.pos.get_order().valor_anticipo += parseFloat(anticipado);
            $("tr[data-id='"+self.options.workorder+"'] .valor_anticipo_line")[0].textContent = anticipado;

            /* Tomar la workorder del pos y actualizar el valor del anticipo */
            var workorder = self.pos.get_workorder_by_id(self.options.workorder)
            if (workorder){
                workorder.valor_anticipo += parseFloat(anticipado);
            }

            self.gui.close_popup();
            self.gui.show_screen('products');

        },
        click_cancel: function () {
            this.gui.close_popup();

        }

    });
    gui.define_popup({ name: 'AnticipoWidget', widget: AnticipoWidget });

    /* En la validacion de la orden creamos una linea de pago de anticipos
    y tambien un producto de intereses de tarjetas
    Ver como lo hace en descuento*/
    pos_screens.PaymentScreenWidget.include({
        
        validate_order: function (force_validation) {
            var self = this;
            var order = this.pos.get_order()
            // agregar paymentmethod anticipo si tiene workorder_id y el aniticipo es mayor a 0

            if (order.workorder_id && order.anticipo_workorder > 0){
                var payment_method = self.pos.payment_methods.find(function(pm){
                        return pm.pago_anticipos == true;
                    })
                // NOTE: se añade pero no lo está renderizando

                var newPaymentline = new Paymentline({},{order: order, payment_method:payment_method, pos: self.pos});
                newPaymentline.set_amount( order.anticipo_workorder );
                order.paymentlines.add(newPaymentline);

                order.anticipo_workorder = 0

            }
                
            this._super(force_validation);
            // antes de finalizar la validacion, si hay u workorder_id, lo sacamos del pos.workorders
            self.pos.workorders = self.pos.workorders.filter(function(wo){
                return wo.id != order.workorder_id;
            })

        }
    })
});
