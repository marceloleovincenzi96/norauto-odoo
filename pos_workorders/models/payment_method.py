# -*- coding: utf:-8 -*-
from odoo import fields, models

class PosPaymentMethod(models.Model):

    _inherit = 'pos.payment.method'

    pago_anticipos = fields.Boolean('Pago Anticipos')
