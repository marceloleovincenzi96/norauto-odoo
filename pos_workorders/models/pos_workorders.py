from odoo import models, fields, api

class PosWorkorders(models.Model):

    _inherit = 'pos.order'
    _name = _inherit

    anticipo_workorder = fields.Float(
        string='Anticipo'
    )

    workorder_id = fields.Many2one(
        string='Orden de trabajo',
        comodel_name='norauto.order',
        ondelete='restrict',
    )

    @api.model
    def _order_fields(self, ui_order):
        order_fields = super(PosWorkorders, self)._order_fields(ui_order)
        order_fields['anticipo_workorder'] = ui_order.get('anticipo_workorder')
        order_fields['workorder_id'] = ui_order.get('workorder_id')
        return order_fields

    def action_pos_order_paid(self):
        # Si la orden tiene un workorder_id la paso a estado pagado
        if self.workorder_id:
            self.workorder_id.set_to_paid()
        res = super(PosWorkorders, self).action_pos_order_paid()
        return res

    def create_picking(self):
        # Si la orden tiene un workorder_id borro las reservas de la workorder antes de hacer el picking
        if self.workorder_id:
            self.workorder_id.clear_reservations()
        res = super(PosWorkorders, self).create_picking()
        return res
