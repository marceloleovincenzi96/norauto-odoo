# -*- coding: utf-8 -*-

from odoo import api, models,fields
from datetime import date,datetime 


#'Información a completar en el sistema para los anticipos'
class Anticipos(models.Model):
    _name = 'anticipos'
    _inherit = "mail.thread"
    
    state = fields.Selection([('borrador','Borrador'),('pagada','Pagada'),('anulada','Anulada')], string='Estado del anticipo',default='borrador')
    name = fields.Char(string="Name", readonly=True, required=True, copy=False, default='New')
    id_anticipo = fields.Integer(string='ID del anticipo',)
    cajero_id = fields.Many2one('res.users', string='Cajero',)
    fecha_anticipo = fields.Datetime(string='Fecha y hora de creacion')
    monto_reservado  = fields.Float(string='Monto reservado',)
    orden_id = fields.Many2one('norauto.order', string='Orden de trabajo',)
    proveedor_id = fields.Many2one('res.partner', string='Proveedor',)
    tienda_id = fields.Many2one('operating.unit', string='Tienda',)

    comprobante = fields.Binary(string='Comprobante Anticipo', attachment=True,)
    #articulos_reservados_ids = fields.One2many('source.model.name', 'o2m_field_id', string='Field Label',)

    @api.model
    def create(self, vals):
       if vals.get('name', 'New') == 'New':
           vals['name'] = self.env['ir.sequence'].next_by_code('anticipos') or 'New'
       result = super(Anticipos, self).create(vals)
       return result

    def action_pasar_pagada(self):
      self.state = 'pagada'
      order = self.env['norauto.order'].search([('id','=',self.order_id)])
      #ingresar dinero en el 

    def action_anular(self):
      self.state = 'anulada'
      # devolver el dinero de...
