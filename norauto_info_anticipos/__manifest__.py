# -*- coding: utf-8 -*-
{
    'name': "Información Anticipos",

    'summary': """ Información a completar en el sistema para los anticipos
        """,

    'description': """
        Los cajero de la tienda tendrán disponible un formulario para completar los datos de la seña en estado borrador para tener una correcta registración de los anticipos.
    """,

    'author': "Nybble Group",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Nybble',
    'version': '13.0.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale','norauto_rights','norauto_orders','operating_unit'],

    # always loaded
    'data': [
        'data/ir_sequence_data.xml',
        'security/ir.model.access.csv',
        'views/info_anticipos.xml',
        #'security/ir.model.access.csv',
        #'views/views.xml',
        #'views/templates.xml',

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
}
