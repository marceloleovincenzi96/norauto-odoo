# -*- coding: utf-8 -*-
{
    'name': "FIX Informes gastos",

    'summary': """
        Permite publicar asientos con proveedor por defecto""",
    'author': 'NybbleGroup',
    'description': """
NorAuto MRP
===============
Gestión de propuestas automaticas de compra
""",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base','hr_expense'],
    'data': [
        # 'security/ir.model.access.csv',
    ],
    'demo': [
    ],
}
